<?php

    session_start();
   // ob_start();
    error_reporting(FALSE);
    
    require_once("include/app_top.php");
    require_once("include/global.php");
    require_once("include/lib.util.common.inc.php");
    require_once("include/db.php");
    require_once("include/class/client/cs_client_category.php");
    require_once("include/class/client/cs_client_type_category.php");
    require_once("include/class/client/cs_client_news.php");
    
    $page = new Page();

    $server=STR_SERVER;
    $headerTitle = "Truong MN Hai Au";
    $description="";
    $keywords="";
    
    $body_main = "";
    $count_news="";//dem so luot tin.
    
    $pagetitle_active="";
    $page_index = isset($_GET["page_index"])?$_GET["page_index"]:"";
    
    $func="";
    $func= isset($_GET["func"])?$_GET["func"]:"";
    
    $cate="";
    $cate= isset($_GET["cate"])?$_GET["cate"]:"";   
    
    include("include/client/leftmenu.php");
    

    switch($page_index)
    {
           
        case "trang-chu":
            $status_active_trang_chu="active";
            $body_main="client/trang_chu.tpl";
            include("include/client/trang_chu.php");
            break;
        case "moi-ngay-lam-gi":
        //case "http://haiau.edu.vn/chimcanhcut.php?page_index=moi-ngay-lam-gi":
            $status_active_moi_ngay_lam_gi="active";
            $body_main="client/moi_ngay_lam_gi.tpl";
            include("include/client/moi_ngay_lam_gi.php");
            break;
        case "lop-hoc":
            $status_active_lop_hoc="active";
            $body_main="client/lop_hoc.tpl";
            include("include/client/lop_hoc.php");
            break;
        case "dinh-duong-cho-tre":
            $status_active_dinh_duong_cho_tre="active";
            $body_main="client/dinh_duong_cho_tre.tpl";
            include("include/client/dinh_duong_cho_tre.php");
            break;
        case "nhap-hoc":
            $status_active_nhap_hoc="active";
            $body_main="client/nhap_hoc.tpl";
            //include("include/client/nhap_hoc.php");
            break;
        case "thu-vien-anh":
            $status_active_thu_vien_anh="active";
            $body_main="client/thu_vien_anh.tpl";
            include("include/client/thu_vien_anh.php");
            break;
        case "camera-online":
            $status_active_video_online="active";
            $body_main="client/video_online.tpl";
            include("include/client/video_online.php");
            break;
        case "thong-tin-lien-he":
            $status_active_thong_tin_lien_he="active";
            $body_main="client/thong_tin_lien_he.tpl";
            $headerTitle ="Li&#234;n h&#7879; chim c&#225;nh c&#7909;t";
            //include("include/client/thong_tin_lien_he.php");
            break;
        default:
           $status_active_trang_chu='active';
            $body_main="client/trang_chu.tpl";
            include("include/client/trang_chu.php");
           break;
    
    }
    if($_SESSION["ThongBao"]!=null)
    {
            $thongbao = $_SESSION["ThongBao"];            
          $strThongBao = "<div class=\"thong_bao_gui\">
                        <label id=\"messages\">".$thongbao."</label></div>";  
          $page->assign("strThongBao",$strThongBao);
          session_unset($_SESSION["ThongBao"]) ;                 
    }
  
  
    $i=1;
    $page->assign("i",$i);
    
    $page->assign("func",$func);
    $page->assign("page_index",$page_index);
    $page->assign("cate",$cate);
    
    $page->assign("status_active_trang_chu", $status_active_trang_chu);
    $page->assign('status_active_moi_ngay_lam_gi',$status_active_moi_ngay_lam_gi);
    $page->assign('status_active_lop_hoc',$status_active_lop_hoc);
    $page->assign('status_active_dinh_duong_cho_tre',$status_active_dinh_duong_cho_tre);
    $page->assign('status_active_nhap_hoc',$status_active_nhap_hoc);
    $page->assign('status_active_thu_vien_anh',$status_active_thu_vien_anh);
    $page->assign('status_active_video_online',$status_active_video_online);
    $page->assign('status_active_thong_tin_lien_he',$status_active_thong_tin_lien_he);
  
    $page->assign("server",$server);
    $page->assign("headerTitle",$headerTitle);
    $page->assign("description",$description);
    $page->assign("keywords",$keywords);
    $page->assign("body_main", $body_main);
       
    $page->display("client/index.tpl");

?>