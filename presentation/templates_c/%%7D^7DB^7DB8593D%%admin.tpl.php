<?php /* Smarty version 2.6.19, created on 2013-02-25 11:03:31
         compiled from admin/admin.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>ADMIN TOOLS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          
        
        <link type="text/css" rel="stylesheet" href="css/library.css" />
        <link href="css/admin/style.css" rel="stylesheet" charset="utf-8" type="text/css" />
        <link href="css/admin/ajaxpagination.css" rel="stylesheet" charset="utf-8" type="text/css" />
        <link href="ckeditor/_samples/sample.css" rel="stylesheet" type="text/css"/>
        <link href="calenda/calendar.css" rel="stylesheet" type="text/css"/>
        <link href="validation/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
        <link href="validation/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />

        
        <!--<?php echo '-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/admin/menu.js"></script>
        <script type="text/javascript" src="js/library.js"></script>
        <script type="text/javascript" src="js/admin/scriptMenu.js"></script>
        
        <script type="text/javascript" src="js/admin/ajaxpagination.js"></script>
        
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="ckfinder/ckfinder.js.js"></script>
        

    	
        <script src="js/tooltip.js" type="text/javascript"  charset="utf-8"></script> <!--tab-->
        <script language="javascript" src="js/admin/jbcombox.js" type="text/javascript"></script>
        <script type="text/javascript" src="calenda/calendar_us.js"></script>
        <script src="validation/SpryValidationTextField.js" type="text/javascript"></script>
        <script src="validation/SpryValidationTextarea.js" type="text/javascript"></script>
        <script language="javascript" src="js/admin/jbcombox.js" type="text/javascript"></script>
        <script type="text/javascript" src="calenda/calendar_us.js"></script>
        
        </script>  
        <script language="javascript">
          function SetSite(u) {
       
            var isExist = "";
            $.ajax({
                async: false,
                type: "POST",
        	url: "include/admin/admin_set_site.php",
        	data: "idSite="+u,
        	success: function(msg){
        	     isExist = msg;
               
        	}
            });
          if(isExist!="")
          {
            location.reload(true);
          }
            return isExist;
}
        </script>  
        <!--'; ?>
-->
            
            
    </head>
    <body>
    
        <div class="wrapper">
            <div class="header">
                <div class="headerLeft">
                    <img src="images/admin/logo.gif"  width="308" height="99"/>
                  TR&#431;&#7900;NG M&#7846;M NON CHIM C&#193;NH C&#7908;T
                </div>
                <div class="headerRight">
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['admin_account']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> 
                </div>
            </div>
            <div class="headerContent">
                <div class="left">
                    <div class="menu_left">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['main_category_tree']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                     </div>
                </div>
              
            </div>
            
            <div class="section_menu">
               <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['admin_subMenu']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
            
            <div class="content">
                <?php if ($this->_tpl_vars['main_user'] != ""): ?>
                    <div class="contentLeft">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['main_user']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <div class="contentBorder">&nbsp;</div>
                    <div class="contentBody">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['main_content_template']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                <?php else: ?>
                    <div class="contentBody2">
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['main_content_template']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                <?php endif; ?>
            </div>
            
            
        </div>
    </body>
</html>