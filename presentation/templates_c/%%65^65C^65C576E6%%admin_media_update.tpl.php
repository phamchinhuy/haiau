<?php /* Smarty version 2.6.19, created on 2014-02-24 19:52:10
         compiled from admin/admin_media_update.tpl */ ?>
<?php echo '
<script language="javascript" src="js/admin/check_media_edit.js" type="text/javascript"></script>
<script language="javascript">
    function checkForm()
    {
        rs = true;
        if(document.frmAddImage.txtNews.value=="")
        {
            document.frmAddImage.txtErrorName.style.display = \'block\'; 
            document.frmAddImage.txtErrorName.value ="Ten hinh chua nhap";
            document.frmAddImage.txtName.focus();
            rs = false
        }
        return rs;
        
    }
</script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	$("#txtTitleSEO").blur(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass(\'messagebox\').text(\'Checking...\').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("include/admin/seo_availability.php",{ $txtSEOMediaEdit:$(this).val(), $demo:$("#txtSEOSub").val() } ,function(data)
        {
		  if(data==\'no\') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html(\'SEO đã tồn tại\').addClass(\'messageboxerror\').fadeTo(900,1);
              $(\'#submit\').attr(\'disabled\', \'disabled\');
			});		
          }
		  else
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html(\'SEO hợp lệ\').addClass(\'messageboxok\').fadeTo(900,1);
              $(\'#submit\').attr(\'disabled\', \'\');

             
              	
			});
		  }
				
        });
 
	});
});

                    
               
</script>
<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
'; ?>


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" onsubmit="return check_insert_media();">
            <fieldset class="fieldSet">
                <legend class="legen">CẬP NHẬT MEDIA</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Tiêu đề
                        </td>
                        <td class="tdRight">
                             <span id="errorTitle">
                            <input type="text" id="txtNews" name="txtNews" value="<?php echo $this->_tpl_vars['cs_video']->video[0]; ?>
" class="textField" />
                             <span class="textfieldRequiredMsg" id="error_name">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Tóm tắt
                        </td>
                        <td class="tdRight">
                            <span id="errorShortDescription">
                            <textarea name="txtShortDescription" id="txtShortDescription" class="textField" ><?php echo $this->_tpl_vars['cs_video']->short_description[0]; ?>
</textarea>
                             <span class="textareaRequiredMsg" id="error_short_des"></span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Chi tiết
                        </td>
                        <td class="tdRight">
                                <span id="errorLongDescription">
                               <?php echo $this->_tpl_vars['ckeditor']->editor('description_news',$this->_tpl_vars['cs_video']->long_description[0]); ?>

                               <!--<span class="textareaRequiredMsg">(Không được rổng)</span>-->
                               </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Hình nhỏ
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileSmall" id="fileSmall" />
                            <img src="<?php echo $this->_tpl_vars['folder_upload_media']; ?>
<?php echo $this->_tpl_vars['cs_video']->small_image[0]; ?>
" width="80" height="80" />
                            <br />
                           <span class="textareaRequiredMsg" id="error_file_small">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Hình lớn
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                            <img src="<?php echo $this->_tpl_vars['folder_upload_media']; ?>
<?php echo $this->_tpl_vars['cs_video']->large_image[0]; ?>
" width="80" height="80" />
                                   <span class="textareaRequiredMsg" id="error_file_large">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrl" name="txtUrl" value="<?php echo $this->_tpl_vars['cs_video']->url[0]; ?>
" class="textField" />
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url video
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrlVideo" name="txtNewsVideo" value="<?php echo $this->_tpl_vars['cs_video']->url[0]; ?>
" class="textField" />
                         <span class="textareaRequiredMsg" id="error_url_video">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    
                   <tr>
                        <td class="tdLeft">
                            Ngày bắt đầu
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginDate" value="<?php echo $this->_tpl_vars['cs_video']->begin_date[0]; ?>
" />
                        <?php echo '
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		\'formname\': \'frmAddImage\',
                            		// input name
                            		\'controlname\': \'beginDate\'
                            	});
    
    	                    </script>
                         '; ?>

                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ngày kết thúc
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginEnd" value="<?php echo $this->_tpl_vars['cs_video']->begin_end[0]; ?>
"/>
                        <?php echo '
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		\'formname\': \'frmAddImage\',
                            		// input name
                            		\'controlname\': \'beginEnd\'
                            	});
    
    	                    </script>
                         '; ?>

                                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Tin có thể bình luận
                        </td>
                        <td class="tdRight">
                          <?php if ($this->_tpl_vars['cs_video']->is_comment[0] == '1'): ?>
                                <input type="checkbox" name="isComment" id="isComment" checked="checked"   />
                            <?php else: ?>
                                <input type="checkbox" name="isComment" id="isComment"   />
                            <?php endif; ?>
                       </select>                          
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <select name="active" >
                            <option value="1" >Active</option>
                            <option value="2" >UnActive</option>
                            <option value="3" >Delete</option>
                        </select>                          
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ti&#234;u &#273;&#7873; cho SEO
                        </td>
                        <td class="tdRight">
                            <span id="errorSEO">
                            <input type="text" name="txtTitleSEO" id="txtTitleSEO" class="textField"  value="<?php echo $this->_tpl_vars['cs_video']->page_title[0]; ?>
" />
                            <span class="textfieldRequiredMsg" id="error_seo">(Không được rổng)</span></span>  
                            <span id="msgbox" style="display:none"></span>  
                            <input id="txtSEOSub"  value="<?php echo $this->_tpl_vars['cs_video']->page_title[0]; ?>
" name="txtErrorImageSmall" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />    
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                            M&#244; t&#7843; cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaDescription">
                            <textarea name="txtDescriptionMeta" id="txtDescriptionMeta" class="textField" ><?php echo $this->_tpl_vars['cs_video']->meta_description[0]; ?>
</textarea>
                            <span class="textareaRequiredMsg" id="error_meta"> (Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            T&#7917; kh&#243;a cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaKeyword">
                            <textarea name="txtKeywordMeta" id="txtKeywordMeta" class="textField" ><?php echo $this->_tpl_vars['cs_video']->meta_keyword[0]; ?>
</textarea>
                                                <span class="textareaRequiredMsg" id="error_key">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btEdit" value="L&#432;u" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<?php echo '
<script type="text/javascript">
<!--
var errorSEO = new Spry.Widget.ValidationTextField("errorSEO");
var errorMetaKeyword = new Spry.Widget.ValidationTextarea("errorMetaKeyword");
var errorMetaDescription = new Spry.Widget.ValidationTextarea("errorMetaDescription");
var errortTitle = new Spry.Widget.ValidationTextField("errorTitle");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var errorShortDescription = new Spry.Widget.ValidationTextarea("errorShortDescription");
var errorLongDescription = new Spry.Widget.ValidationTextarea("errorLongDescription");

//-->
</script>
'; ?>
