<?php /* Smarty version 2.6.19, created on 2013-05-28 11:22:18
         compiled from admin/admin_category.tpl */ ?>

<?php echo '

    <script language="javascript">
        function check()
        {
            sao=window.confirm("Do You want to delete ?");
            if(sao==true)
            return true;
            else return false;
        }
        function ClickToURL(linkVal)
        {
        	if (linkVal != 0)
            {
        	    // alert(frm.sumhd14.value);
            
                document.location=linkVal;
                }
        }
        
        function ClickCb()
        {             
            
            if(document.getElementById(\'site\').value=="")
            {
                   ClickToURL(\'?3nss=categories.manage\');
            }
            else{
             
                    ClickToURL(\'?3nss=categories.manage&id_site=\'+document.getElementById(\'site\').value);
            }
        }
        
    	function checksite()
        {
           
       	    document.getElementById(\'SiteError\').value= \'\';
            if(document.getElementById(\'site\').value=="")
            {
                 	document.getElementById(\'SiteError\').innerHTML="Xin chọn site";
                return false;
            }
            return true;    
        }
        //-->
        function checPageTitle(u) {
            var isExist = true;
            $.ajax({
                async: false,
                type: "POST",
        	url: "include/admin/admin_category_check_page_title.php",
        	data: "pagetile="+u,
        	success: function(msg){
        	     isExist = msg;
        	}
            });
        
            return isExist;
        }
     function onsubmitfrm()
      {		     
             ///
            site=document.getElementById(\'site\').value;
            
            category= document.getElementById(\'txtcategory\').value;
            dllparent=document.getElementById(\'ddlParent\').value;
            txtpageTitle=trim(document.getElementById(\'txtpageTitle\').value);
            txtmeta_description=document.getElementById(\'txtmeta_description\').value;
            txturl=document.getElementById(\'txturl\').value;
            repagetile=document.getElementById(\'repagetilte\').value;
            ////
            document.getElementById(\'SiteError\').value= \'\';     
            document.getElementById(\'txturlerror\').value= \'\';
 			    
                if( site=="")
                {
                    	document.getElementById(\'SiteError\').innerHTML="B&#7841;n ch&#432;a ch&#7885;n site";
                        document.getElementById(\'site\').focus();
                        return false;
                }
                if(category==0)
                {
                      document.getElementById(\'divErrorCategory\').innerHTML = "B&#7841;n ch&#432;a nh&#7853;p t&#234;n danh m&#7909;c";
                      document.getElementById(\'txtcategory\').focus();
                      return false;
                }
                if(txturl.length>0)
                {
                    if(CheckValidUrl(txturl)!=1)
                    {
                        document.getElementById(\'divErrorURL\').innerHTML= \'Kh&#244;ng &#273;&#250;ng &#273;&#7883;nh d&#7841;ng http:// ho&#7863;c https://\';
                        return false;
                    }
                }
                
                if(txtpageTitle=="")
                {
                    alert("Xin nhap tieu de trang");
                    document.getElementById(\'txtpageTitle\').focus();
                    return false;
                }
                if(checPageTitle(txtpageTitle)==1&&repagetile=="")
                {
                    alert("Tieu de trang  da co ton tai");
                    document.getElementById(\'txtpageTitle\').focus();
                    return false;
                }
                if(!CheckUnicodeChar(txtpageTitle))
                {
                     alert("Tieu de trang phai co format aa-bb-cc");
                    document.getElementById(\'txtpageTitle\').focus();
                    return false;
                }
                if(txtmeta_description=="")
                {
                    alert("Xin nhap meta description");
                    document.getElementById(\'txtmeta_description\').focus();
                    return false;
                }
                
    			return true;
    		}
            
        function CheckValidUrl(strUrl)
        {
                var RegexUrl = /(http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/
                if(strUrl.length==0)
                {           
                    return 12;
                }
                else
                {
                   if(RegexUrl.test(strUrl)==false)
                    {              
                        return 12;
                    }
                    else
                    {
                        return 1;
                    } 
                }
                
                return 1;
        
        }
        
        function trim(val)
        { 
        	return val.replace(/^\\s+|\\s+$/g,"");
        }
    
        //cho phep nhung ky tu tu A->Z, a->z, 0->9,va cac ky tu _-
        function CheckUnicodeChar(stringIn)
        {
            	retval = false;
             var i;
             for ( i=0; i <= stringIn.length-1; i++) 
        	 {
        	     
                
                 if ( ( ( stringIn.charCodeAt(i) >= 48)&&(stringIn.charCodeAt(i) <= 57)) || ((stringIn.charCodeAt(i) > 64)&&(stringIn.charCodeAt(i) <= 90)) || ((stringIn.charCodeAt(i) >= 97)&&(stringIn.charCodeAt(i) <= 122)) || (stringIn.charCodeAt(i) == 95) || (stringIn.charCodeAt(i) == 45) )
        		 {
        		     
                   	retval = true;
                 }
        		 else
        		 {
        			retval = false;
        			break;
                 }
             }
             for ( i=0; i<= stringIn.length-2; i++) 
             {
                if(stringIn.charCodeAt(i)==45&&stringIn.charCodeAt(i+1)==45)
                {
                    retval = false;
        			break;
                }
             }
             return retval;
        }
     function do_check(id)
   { 
      //$(this).siblings(\'.child-\'+id).toggle();
      //$(\'.child-\'+id).slideToggle("slow");
       if($(\'.child-\'+id).is(\':hidden\'))
       {
        $(\'.child-\'+id).show();

       }
       else{
      $(\'.child-\'+id).hide();
       }
      return false;
   }
      	$( function() {
			$( \'.checkAll\' ).live( \'change\', function() {
				$( \'.cb-element\' ).attr( \'checked\', $( this ).is( \':checked\' ) ? \'checked\' : \'\' );
				$( this ).next().text( $( this ).is( \':checked\' ) ? \'Uncheck All\' : \'Check All\' );
		          document.getElementById("btsave").value=1;
        	});
			$( \'.invertSelection\' ).live( \'click\', function() {
			  document.getElementById("btsave").value=0;
				$( \'.cb-element\' ).each( function() {
					$( this ).attr( \'checked\',$( this ).is( \':checked\' ) ? \'\' : \'checked\' );
				}).trigger( \'change\' );
			});
			$( \'.cb-element\' ).live( \'change\', function() {
				$( \'.cb-element\' ).length == $( \'.cb-element:checked\' ).length ? $( \'.checkAll\' ).attr( \'checked\', \'checked\' ).next().text( \'Uncheck All\' ) : $( \'.checkAll\' ).attr( \'checked\', \'\' ).next().text( \'Check All\' );
			});
            ////
    	  $(\'tr[@class^=child-]\').hide().children(\'td\');

		});
           function check()
        {
        sao=window.confirm("Do You want to delete ?");
        if(sao==true)
        return true;
        else return false;
        }
    </script>
'; ?>



<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="headerForm">
            <?php echo $this->_tpl_vars['headerTitle']; ?>

        </td>
    </tr>

    <tr>
        <td class="menuAdmin">
            <form name="frm_p" id="frm_p" method="post" action="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="catalogAddHeadTitleMessageFunc"><?php echo $this->_tpl_vars['mess']; ?>
</div>
                            <div class="catalogAddHeadTitleMenu">
                                <a  href="admin.php?3nss=categories.manage">
                                    Danh s&#225;ch
                                </a>
                                |  
                                <a   href="admin.php?3nss=categories.manage&type=add">
                                    Thêm m&#7899;i
                                </a>
                            </div>
                        </td>
                      </tr>
                </table>
            </form>
        </td>
    </tr>
    
    <?php if (! isset ( $this->_tpl_vars['type'] ) || ( $this->_tpl_vars['type'] != 'edit' && $this->_tpl_vars['type'] != 'add' )): ?>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0"> 
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    Ch&#7885;n site: 
                                </td>
                                <td>
                                     <form  action="" method="post" name="id_type">
                                        <?php echo $this->_tpl_vars['cbsite_list']; ?>

                                        <input  type="button" value="Go" name="cbtype" onclick="ClickCb();" />
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form  id="news" name="myform" method="post" >
                            <div class="admin_image_form_list">
                                <div class="admin_image_form_list_field">
                                
                                    <div id="paginate-top" class="paginatetop"></div>
                                    <div id="productcontent" class="productcontent"></div>
                                    <div id="paginate-bottom" class="paginatebottom"></div>
                                        <?php echo '
                                            <script type="text/javascript">
                                                var listNews={
                                                pages: ['; ?>
<?php echo $this->_tpl_vars['scriptPagingNews']; ?>
<?php echo '],
                                                selectedpage: 0 //set page shown by default (0=1st page)
                                                }
                                                var mybookinstance=new ajaxpageclass.createBook(listNews, "productcontent", ["paginate-top", "paginate-bottom"])
                                            </script>
                                        '; ?>

                                    
                                    
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php endif; ?>
    
    <?php if (isset ( $this->_tpl_vars['type'] ) && ( $this->_tpl_vars['type'] == 'edit' || $this->_tpl_vars['type'] == 'add' )): ?>
    <tr>
        <td>
            <form id="frmAddCategory" name="frm_catogory" method="POST" 
                 action="" onsubmit="return  onsubmitfrm();" enctype="multipart/form-data">
                 
                    <div class="catalogAdd">
                        <fieldset>
                            <legend class="titleForm">
                             
                                <?php if ($this->_tpl_vars['type'] == 'add'): ?>     
                                    Tạo chuyên mục mới
                                <?php endif; ?>
                                <?php if ($this->_tpl_vars['type'] == 'edit'): ?>
                                    Ch&#7881;nh s&#7917;a danh m&#7909;c  
                                <?php endif; ?>
                             
                            </legend>
                            
                        <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td colspan="2">
                                    <strong class="titleFormItem">Thông tin chuyên mục :</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Chọn site:
                                </td>
                                <td>
                                    <?php echo $this->_tpl_vars['cbsite']; ?>

                                    <input type="hidden" value="<?php echo $this->_tpl_vars['objCategory']->id_site[0]; ?>
" name="cbsited" />
                                    <div class="divError" id="SiteError"><?php echo $this->_tpl_vars['errorName']; ?>
</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Chọn loại chuyên mục:
                                </td>
                                <td>
                                    <?php echo $this->_tpl_vars['DllType']; ?>

                                    <div class="messageError"><?php echo $this->_tpl_vars['errorName']; ?>
</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Thứ tự:
                                </td>
                                <td>
                                     <select class="ddlCSS" id="ddlPosition" name="ddlPosition" style="width:200px" onchange="return checksite();">
                                        <option  value="0">-Trước-</option>
                                        <option  value="1"  selected="selected">-Con-</option>
                                        <option  value="2">-Sau-</option>
                                    </select>
                                    <?php echo $this->_tpl_vars['dllparent']; ?>

                                    <input  type="hidden" value="<?php echo $this->_tpl_vars['objCategory']->id_parent[0]; ?>
" name="parented"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tên chuyên mục:
                                </td>
                                <td>
                                    <input type="text" id="txtcategory"  name="txtcategory" style="width:500px"  value="<?php echo $this->_tpl_vars['objCategoryDetail']->category[0]; ?>
"/>
                                    <div class="divError" id="divErrorCategory" ></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ảnh nhỏ :
                                </td>
                                <td>
                                    <input type="file" name="image_thum" />
                                   <?php if (isset ( $this->_tpl_vars['objCategoryDetail']->small_images[0] ) && $this->_tpl_vars['objCategoryDetail']->small_images[0] != ""): ?>
                                        <img src="<?php echo $this->_tpl_vars['savefolder']; ?>
/<?php echo $this->_tpl_vars['objCategoryDetail']->small_images[0]; ?>
"  width="100" height="50"/>
                                   <?php endif; ?>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ảnh lớn :
                                </td>
                                <td>
                                     <input type="file" name="image_large" />
                                    <?php if (isset ( $this->_tpl_vars['objCategoryDetail']->large_image[0] ) && $this->_tpl_vars['objCategoryDetail']->large_image[0] != ""): ?>   
                                    <img src="<?php echo $this->_tpl_vars['savefolder']; ?>
/<?php echo $this->_tpl_vars['objCategoryDetail']->large_image[0]; ?>
" width="100" height="50" />
                                    <?php endif; ?>
                                 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Music :
                                </td>
                                <td>
                                     <input type="file" name="txtmusic" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     Hiển thị:
                                </td>
                                <td>
                                     Hi&#7875;n th&#7883; trang ch&#7911;&nbsp;<input type="checkbox" value="1"  name="showpage"
                                     <?php if ($this->_tpl_vars['objCategoryDetail']->pageAction[0] == 1): ?>
                                      checked="checked"
                                     <?php endif; ?>
                                      />
                                   &nbsp;  &nbsp; Tin nổi bật &nbsp;<input type="checkbox" value="1" name="specialnews"
                                      <?php if ($this->_tpl_vars['objCategoryDetail']->pageAction[1] == 1): ?>
                                      checked="checked"
                                     <?php endif; ?>
                                    />
                                   &nbsp;  &nbsp; Tin hot &nbsp;<input type="checkbox" value="1" name="hotnews"
                                      <?php if ($this->_tpl_vars['objCategoryDetail']->pageAction[2] == 1): ?>
                                      checked="checked"
                                     <?php endif; ?>
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hiển thị chuyên mục con:
                                </td>
                                <td>
                                    <input type="checkbox" name="show" value="1"  <?php if ($this->_tpl_vars['objCategoryDetail']->show_subCategory[0] == '1'): ?>   checked="checked"  <?php endif; ?> />
                                </td>
                            </tr>
                         
                            <tr>
                                <td>
                                     Link liên kết:
                                </td>
                                <td>
                                    <?php if ($this->_tpl_vars['objCategoryDetail']->url[0] != ""): ?>
                                        <input type="text" id="txturl"  name="txturl" style="width:500px" value="<?php echo $this->_tpl_vars['objCategoryDetail']->url[0]; ?>
" />
                                    <?php else: ?>
                                        <input type="text" id="txturl"  name="txturl" style="width:500px" value="http://3nss.net" />
                                    <?php endif; ?>
                                    <div class="divError" id="divErrorURL"></div>
                                    <input type="text" name="txturlerror" id="txturlerror" style="width:200px; background-color:#FFF; color:#F00; border:0px;" disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Target:
                                </td>
                                <td>
                                    <select name="target" >
                                     <option value="_Blank" <?php if ($this->_tpl_vars['objCategoryDetail']->target[0] == '0' || $this->_tpl_vars['objCategoryDetail']->target[0] == '_Blank'): ?> selected="selected"  <?php endif; ?> > _Blank</option>
                                     <option value="_Self" <?php if ($this->_tpl_vars['objCategoryDetail']->target[0] == '1' || $this->_tpl_vars['objCategoryDetail']->target[0] == '_Self'): ?> selected="selected"  <?php endif; ?>> _Self</option>
                                     
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     Kích hoạt:
                                </td>
                                <td>
                                    <input type="checkbox" name="status" value="1"<?php if ($this->_tpl_vars['objCategoryDetail']->status[0] == '1'): ?>   checked="checked"  <?php endif; ?>/>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
                                    <strong class="titleFormItem">SEO</strong>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    Từ khóa :
                                </td>
                                <td>
                                    <textarea class="textAreaDes" id="txtkeyWord" name="txtkeyWord"><?php echo $this->_tpl_vars['objCategoryDetail']->keyword[0]; ?>
</textarea>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    Tiêu đề trang:
                                </td>
                                <td>
                                    <textarea class="textAreaDes" id="txtpageTitle" name="txtPageTitle"><?php echo $this->_tpl_vars['objCategoryDetail']->page_title[0]; ?>
</textarea>  
                                    <input  type="hidden" value="<?php echo $this->_tpl_vars['objCategoryDetail']->page_title[0]; ?>
" name="repagetilte" id="repagetilte"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    M&#244; t&#7843; Meta:
                                </td>
                                <td>
                                     <textarea class="textAreaDes" id="txtmeta_description" name="txtmeta_description"><?php echo $this->_tpl_vars['objCategoryDetail']->meta_description[0]; ?>
</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     M&#244; t&#7843; keyword:
                                </td>
                                <td>
                                    <textarea class="textAreaDes" id="txtmeta_keyword" name="txtmeta_keyword"><?php echo $this->_tpl_vars['objCategoryDetail']->meta_keyword[0]; ?>
</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <input type="submit" <?php if ($this->_tpl_vars['type'] == 'add'): ?>  name="btn"  onclick=""      value="Tạo mới"<?php endif; ?>
                                    <?php if ($this->_tpl_vars['type'] == 'edit'): ?>  name="action"  onclick=""  value="Thay đổi"<?php endif; ?>  
                                     />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
            </form>
        </td>
    </tr>
    <?php endif; ?>
</table>
    
    
    
       
    
    
    





