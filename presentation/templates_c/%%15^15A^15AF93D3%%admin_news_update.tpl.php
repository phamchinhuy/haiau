<?php /* Smarty version 2.6.19, created on 2013-03-01 13:16:25
         compiled from admin/admin_news_update.tpl */ ?>
<?php echo '

<script type="text/javascript" src="js/calendarDateInput.js" ></script>

<script language="javascript">

    function checkfrminsert()
{
    var frm=document.getElementById("frmAddImage");
    var title=document.getElementById("txtNews").value;
    var shortdes=document.getElementById("txtShortDescription").value;
    var small_image=document.getElementById("fileSmall").value;
    var large_image=document.getElementById("fileLarge").value;
    var seo=document.getElementById("txtTitleSEO").value;
   // var meta=document.getElementById("txtDescriptionMeta").value;
    var keyword=document.getElementById("txtKeywordMeta").value;
    //handle error
  
      if(title=="")
      {
           
        document.getElementById("error_title").innerHTML="Nh&#7853;p ti&#234;u &#273;&#7873;";
        return false;
      }else{
         document.getElementById("error_title").innerHTML="";
      }
  
  
      if(shortdes=="")
      {       
        document.getElementById("error_short_des").innerHTML="Nh&#7853;p t&#243;m t&#7855;t";
        return false;
      }
      else{
         document.getElementById("error_short_des").innerHTML="";
      }
      /*
      if(small_image!="")
      {
           if(!checkfile(small_image))
           {
             document.getElementById("error_small_image").innerHTML="Nh&#7853;p v&#224;o h&#236;nh &#7843;nh";
            return false;
           }
           else
            document.getElementById("error_small_image").innerHTML=" ";
      }
      
      if(large_image!="")
      {
             if(!checkfile(large_image))
             {
                 document.getElementById("error_large_image").innerHTML="Nh&#7853;p v&#224;o h&#236;nh &#7843;nh";
                 return false;
             }
             else
             document.getElementById("error_large_image").innerHTML="";
      }*/
    
      if(seo=="")
      {
          document.getElementById("error_seo").innerHTML="Nh&#7853;p v&#224;o SEO";
        return false;
      }
      else
      {
            if(CheckUnicodeChar(seo)!=1)
            {
                document.getElementById("error_seo").innerHTML = "Page title co dạng format(aa-bb)";
                return false;
            }
            else
            {
                document.getElementById("error_seo").innerHTML="";
            }
      }
    
      if(keyword.length==0)
      {
            document.getElementById("err_key").innerHTML="Nh&#7853;p v&#224;o keyword";
         return false;
      }
      else{
           document.getElementById("err_key").innerHTML="";
      }
      
     return true;
}

 function checkfile(strfile)
    {  
        var ext = strfile.substring(strfile.lastIndexOf(\'.\') + 1); 
        if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png"||ext=="PNG")
        {          
             return true;
        }
        else
        {
            return false;
        }
    }
    function checkFileSize()
    {
        var size;
    	if (navigator.userAgent.indexOf("Firefox") != -1){
    		smallSize=checkFileSizeFireFox(\'fileSmall\');
            largeSize=checkFileSizeFireFox(\'fileLarge\');
    	}else{
    		smallSize=checkFileSizeIE(\'fileSmall\');
            largeSize=checkFileSizeIE(\'fileLarge\');
    	}
        if(smallSize>2148576||largeSize>2148576)
        {
            alert("size of file too large");
            return false;
        }
        else return true;
    }

    //Check file size before upload, it is working well with Firefox
    //It is not compatible with another browse
    function checkFileSizeFireFox(file)
     {
    	var node = document.getElementById(file);
    	var size = node.files[0].fileSize;
        return size;
     }
    
    // It is working well with IE
    function checkFileSizeIE(file)
    {
    	var myFSO = new ActiveXObject("Scripting.FileSystemObject");
    	//var filepath = document.upload.file.value;
    	var filepath= document.getElementById(file).value;
    	var thefile = myFSO.getFile(filepath);
    	var size = thefile.size;
   	    return size;
    }
    
    //cho phep nhung ky tu tu A->Z, a->z, 0->9,va cac ky tu _-
function CheckUnicodeChar(stringIn)
{
    	retval = false;
     var i;
     for ( i=0; i <= stringIn.length-1; i++) 
	 {
	     
        
         if ( ( ( stringIn.charCodeAt(i) >= 48)&&(stringIn.charCodeAt(i) <= 57)) || ((stringIn.charCodeAt(i) > 64)&&(stringIn.charCodeAt(i) <= 90)) || ((stringIn.charCodeAt(i) >= 97)&&(stringIn.charCodeAt(i) <= 122)) || (stringIn.charCodeAt(i) == 95) || (stringIn.charCodeAt(i) == 45) )
		 {
		     
           	retval = true;
         }
		 else
		 {
			retval = false;
			break;
         }
     }
     for ( i=0; i<= stringIn.length-2; i++) 
     {
        if(stringIn.charCodeAt(i)==45&&stringIn.charCodeAt(i+1)==45)
        {
            retval = false;
			break;
        }
     }
     return retval;
}
</script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	$("#txtTitleSEO").blur(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass(\'messagebox\').text(\'Checking...\').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("include/admin/seo_availability.php",{ $txtSEOEdit:$(this).val(), $demo:$("#txtSEOSub").val() } ,function(data)
        {
		  if(data==\'no\') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html(\'SEO đã tồn tại\').addClass(\'messageboxerror\').fadeTo(900,1);
              $(\'#submit\').attr(\'disabled\', \'disabled\');
			});		
          }
		  
				
        });
 
	});
});

                    
               
</script>
<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
'; ?>


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post"  onsubmit="return checkfrminsert();">
            <fieldset class="fieldSet">
                <legend class="legen">C&#7852;P NH&#7852;T TIN</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Tiêu đề
                        </td>
                        <td class="tdRight">
                            <span id="textTitle">
                            <input type="text" id="txtNews" name="txtNews" value="<?php echo $this->_tpl_vars['cs_news']->news[0]; ?>
" class="textField" />
                            <span class="textfieldRequiredMsg" id="error_title">(Không được rổng)</span></span>   
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Tóm tắt
                        </td>
                        <td class="tdRight">
                            <span id="txtShortDescription">
                            <textarea name="txtShortDescription" id="txtShortDescription" class="textField" ><?php echo $this->_tpl_vars['cs_news']->short_description[0]; ?>
</textarea>
                                        <span class="textareaRequiredMsg" id="error_short_des">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Chi tiết
                        </td>
                        <td class="tdRight">
                                <span id="errorLongDescription">
                               <?php echo $this->_tpl_vars['ckeditor']->editor('description_news',$this->_tpl_vars['cs_news']->long_description[0]); ?>

                               <!--<span class="textareaRequiredMsg">(Không được rổng)</span>--> 
                               </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            H&#236;nh nh&#7887;
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileSmall" id="fileSmall" />
                            <img src="<?php echo $this->_tpl_vars['folder_upload_news']; ?>
<?php echo $this->_tpl_vars['cs_news']->small_image[0]; ?>
" width="80" height="80" />
                                                   <span class="textfieldRequiredMsg" id="error_small_image">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            H&#236;nh l&#7899;n
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                            <img src="<?php echo $this->_tpl_vars['folder_upload_news']; ?>
<?php echo $this->_tpl_vars['cs_news']->large_image[0]; ?>
" width="80" height="80" />
                             <span class="textfieldRequiredMsg" id="error_large_image">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url file
                        </td>
                        <td class="tdRight">
                            <input type="file" name="file" id="file"/>
                            <a href="<?php echo $this->_tpl_vars['server']; ?>
/uploads/news/files/<?php echo $this->_tpl_vars['cs_news']->url[0]; ?>
" title="Download"><?php echo $this->_tpl_vars['cs_news']->url[0]; ?>
</label>
                               <span class="textfieldRequiredMsg" id="error_file"></span></span> 
                        </td>
                    </tr> 
                   <!-- <tr>
                        <td class="tdLeft">
                            Url
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrl" name="txtUrl" value="<?php echo $this->_tpl_vars['cs_news']->url[0]; ?>
" class="textField" />
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url video
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrlVideo" disabled="false" name="txtUrlVideo" value="<?php echo $this->_tpl_vars['cs_news']->url_video[0]; ?>
" class="textField" />
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                        </td>
                    </tr>
                   !--> 
                     <tr>
                        <td class="tdLeft">
                            Target
                        </td>
                        <td class="tdRight">
                        <select name="target" >
                                <option value="_self" > _self</option>
                                <option value="_blank" > _blank</option>
                     
                       </select>                          
                        </td>
                    </tr>
                  <!--  <tr>
                        <td class="tdLeft">
                            Topic
                        </td>
                        <td class="tdRight">
                        <select name="topic" >
                        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['cs_topic']->id) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                           <?php if ($this->_tpl_vars['cs_topic']->id[$this->_sections['i']['index']] == $this->_tpl_vars['cs_news']->id_topic[0]): ?>
                           <option value="<?php echo $this->_tpl_vars['cs_topic']->id[$this->_sections['i']['index']]; ?>
" selected="selected"><?php echo $this->_tpl_vars['cs_topic']->topic[$this->_sections['i']['index']]; ?>
</option>
                           <?php else: ?>
                           <option value="<?php echo $this->_tpl_vars['cs_topic']->id[$this->_sections['i']['index']]; ?>
"><?php echo $this->_tpl_vars['cs_topic']->topic[$this->_sections['i']['index']]; ?>
</option>
                           <?php endif; ?> 
                        <?php endfor; endif; ?> 
                         </select>    
                                                
                        </td>
                    </tr>
                    -->
                   <tr>
                        <td class="tdLeft">
                            Ngày :
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginDate"  value="<?php echo $this->_tpl_vars['cs_news']->begin_date[0]; ?>
"/>
                        <?php echo '
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		\'formname\': \'frmAddImage\',
                            		// input name
                            		\'controlname\': \'beginDate\'
                            	});
    
    	                    </script>
                         '; ?>

                                  <span class="textareaRequiredMsg" id="error_des">(Nhập ngày theo định dạng dd/mm/YYYY)</span></span>                
                                    
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td class="tdLeft">
                            Ngày kết thúc
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginEnd" value="<?php echo $this->_tpl_vars['cs_news']->begin_end[0]; ?>
" />
                        <?php echo '
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		\'formname\': \'frmAddImage\',
                            		// input name
                            		\'controlname\': \'beginEnd\'
                            	});
    
    	                    </script>
                         '; ?>

                                           
                        </td>
                    </tr>
                    !-->
                    <tr>
                        <td class="tdLeft">
                            Tin có thể bình luận
                        </td>
                        <td class="tdRight">
                          <?php if ($this->_tpl_vars['cs_news']->is_comment[0] == '1'): ?>
                                <input type="checkbox" name="isComment" id="isComment" checked="checked"   />
                            <?php else: ?>
                                <input type="checkbox" name="isComment" id="isComment"   />
                            <?php endif; ?>
                       </select>                          
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <select name="active" >
                            <option value="1" >Active</option>
                            <option value="2" >UnActive</option>
                            <option value="3" >Delete</option>
                        </select>                          
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                            Ti&#234;u &#273;&#7873; cho SEO
                        </td>
                        <td class="tdRight">
                            <span id="errorSEO">
                            <input type="text" name="txtTitleSEO" id="txtTitleSEO" class="textField"  value="<?php echo $this->_tpl_vars['cs_news']->page_title[0]; ?>
" />
                              <span class="textfieldRequiredMsg" id="error_seo">(Không được rổng)</span></span> 
                            <span id="msgbox" style="display:none"></span>  
                            
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                            M&#244; t&#7843; cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaDescription">
                            <textarea name="txtDescriptionMeta" id="txtDescriptionMeta" class="textField" ><?php echo $this->_tpl_vars['cs_news']->meta_description[0]; ?>
</textarea>
                             <span class="textareaRequiredMsg" id="error_des">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            T&#7917; kh&#243;a cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaKeyword">
                            <textarea name="txtKeywordMeta" id="txtKeywordMeta" class="textField" ><?php echo $this->_tpl_vars['cs_news']->meta_keyword[0]; ?>
</textarea>
                           <span class="textareaRequiredMsg" id="err_key">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btEdit" value="L&#432;u"  onclick="if(!checkFileSize()) return false;"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<?php echo '
<script type="text/javascript">
<!--
var errorSEO = new Spry.Widget.ValidationTextField("errorSEO");
var errorMetaKeyword = new Spry.Widget.ValidationTextarea("errorMetaKeyword");
var errorMetaDescription = new Spry.Widget.ValidationTextarea("errorMetaDescription");
var textTitle = new Spry.Widget.ValidationTextField("textTitle");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var txtShortDescription = new Spry.Widget.ValidationTextarea("txtShortDescription");
var errorLongDescription = new Spry.Widget.ValidationTextarea("errorLongDescription");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
//-->
</script>
'; ?>