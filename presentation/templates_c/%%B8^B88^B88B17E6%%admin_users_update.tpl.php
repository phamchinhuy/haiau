<?php /* Smarty version 2.6.19, created on 2013-02-25 11:10:05
         compiled from admin/admin_users_update.tpl */ ?>
<?php echo '
<script language="javascript" src="'; ?>
<?php echo $this->_tpl_vars['js_path']; ?>
<?php echo '/common.js" type="text/javascript"></script>

<script language="javascript">
function checkEmailed(u,o) {
    var isExist = false;
    if(u!=o)
    {
        $.ajax({
        async: false,
        type: "POST",
	url: "include/admin/bl_check_email_exist.php",
	data: "username=" + u,
	success: function(msg){
	     isExist = msg;
         //alert(isExist);
	}
    }); 
    }
   
    return isExist;
}           

</script>
<style type="text/css">
.main_box_content{
	width:749px;
	height:auto;
	overflow:hidden;
	padding:0 2px 0 2px;
	position:relative;
}

.main_box_content .ul_regis_acc{
	width:450px;
	height:auto;
	overflow:hidden;
	margin-top:5px;
	float:left;
}

 .main_box_content .ul_regis_acc li{
	width:450px;
	margin-top:4px;
	height:auto;
	overflow:hidden;
	clear:both;
	float:left;
}
 .main_box_content .ul_regis_acc li .box_text{
	width:143px;
	height:auto;
	overflow:hidden;
	padding-right:5px;
	font:11px/20px tahoma;
	color:#333;
	text-align:right;
	float:left;
}
 .main_box_content   .ul_regis_acc li .box_frm{
	width:214px;
	float:left;
	font:11px tahoma;
	color:#333;
	border:1px solid #fff;
	padding:2px;
}
   .main_box_content .ul_regis_acc li .box_frm .btn_check{
	width:54px;
	height:19px;
	border:1px solid #d9d9d9;
	text-align:center;
	font:11px tahoma;
	color:#000;
	cursor:pointer;
	float:left;
	padding-bottom:3px;
}
 .bottom_btn{
	overflow:hidden;
	margin-top:5px;
	margin-left:152px;
	clear:both;
	font:11px tahoma;
	color:#333;
}
 .bottom_btn .btn_cover{
	margin-right:18px;
	width:90px;
	padding-top:3px;
	margin-bottom:5px;
}
/**/
 .main_box_content .red_txt{
	color:#ff0000;
}
.main_box_content .ul_regis_acc li.error .box_frm{
	border:1px solid #ffe4a4;
	background:#fffdd4;
}
 .main_box_content .ul_regis_acc li .box_error
 {
    color:red;
 }
.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}
</style>
'; ?>

<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
    </div>
    <div class="admin_image_form_add_field">
      
            <fieldset class="fieldSet">
                <legend class="legen"><?php echo $this->_tpl_vars['headerCategorytable']; ?>
</legend>
                 
                 <form id="frmRegisterForm" name="frmRegisterForm" method="post" action="" onsubmit="return CheckForm()">
                <div class="main_box_content">
                    <ul class="ul_regis_acc">
                     <li >
                          <div class="box_text"><span class="red_txt"></span>Lo&#7841;i &#273;&#259;ng nh&#7853;p:</div>
                          <div class="box_frm" id="input_username_content">
				           <select name="user_type">
                           <option value="subadmin" <?php if ($this->_tpl_vars['do_useredit']->type[0] == 'subadmin'): ?> selected="selected" <?php endif; ?> >SubAdmin</option>
                
                           <option value="user" <?php if ($this->_tpl_vars['do_useredit']->type[0] == 'user'): ?> selected="selected"<?php endif; ?>>user</option>
                           
                           </select>
                           <?php echo $this->_tpl_vars['do_useredit']->user_type[0]; ?>

				        </div>

                        </li>
                        <li id="li_Username">
                          <div class="box_text"><span class="red_txt"> </span>  T&#234;n &#273;&#259;ng nh&#7853;p:</div>
                          <div class="box_frm" id="input_username_content">
				             <input name="username" id="username" alt="alert_box" type="text" class="txt_155"
                             
                              value="<?php echo $this->_tpl_vars['do_useredit']->username[0]; ?>
" disabled="disabled" />
				            <!--<input id="btCheck" name="btCheck" type="button" class="btn_check" value="Kiểm tra" onclick="Check()" />!-->				                      
                           <p class="gray_10_txt"></p>
                            <p class="box_error" id="err_Username"></p>
				        </div>
                        
                        </li>
                
                			  <li id="li_Fullname">
				<div class="box_text"><span class="red_txt"> </span>Họ và tên:</div>
				<div class="box_frm">
				  <input name="lastname" type="text" class="txt_133" id="lastName" value="<?php echo $this->_tpl_vars['do_useredit']->fullname[0]; ?>
" />
				  <div class="gray_10_txt">
				  </div>
				  <p class="box_error" id="err_Fullname"></p>
				</div>
			  </li>
              
			  </li>
                 <li id="li_Email">
				<div class="box_text"><span class="red_txt"> </span>Email của bạn:</div>
				<div class="box_frm">
				  <input name="email" type="text" class="txt_213" alt="alert_email" onfocus="TNPassShowAlert(this,3)" value="<?php echo $this->_tpl_vars['do_useredit']->email[0]; ?>
" />
				  <p class="gray_10_txt"></p>

				  <p class="box_error" id="err_Email"></p>
				</li>
                  <li id="li_Birthday">
				<div class="box_text"><span class="red_txt"></span>YahooID</div>
				<div class="box_frm">
				<input type="text" class="txt_133" id="yahooID" name="yahooid" value="<?php echo $this->_tpl_vars['do_useredit']->nick_yahoo[0]; ?>
" />
				  <p class="box_error" id="err_Birthday"></p>
				</div>
              
			  </li>
                 <li id="li_Birthday">
				<div class="box_text"><span class="red_txt"></span>SkypeID</div>
				<div class="box_frm">
				<input type="text" class="txt_133" id="skypeID" name="skypeid" value="<?php echo $this->_tpl_vars['do_useredit']->nick_skype[0]; ?>
" />
				  <p class="box_error" id="err_Birthday"></p>
				</div>
              
			  </li>
                  <li id="li_status">
				<div class="box_text"><span class="red_txt"> </span>T&#236;nh tr&#7841;ng:</div>
				<div class="box_frm">
				  <input  type="checkbox" name="status" value="1" <?php if ($this->_tpl_vars['do_useredit']->status[0] == 1): ?> checked <?php endif; ?>/>
                  </div>
				</li>
                    </ul>
                 </div>
                 <div class="bottom_btn">
			  <div class="btn_cover">
				<input name="register" type="submit" class="tnpp_button_login" value="L&#432;u" />
			  </div>
				
			</div>	

                 </form>   
                
                 </legend>
            </fieldset>
      
    </div>
</div>
<?php echo '
<script language="javascript">
var bolUsernameAccepted = false; //username da duoc chap nhan
var bolUsernameChecked 	= false; //username da duoc kiem tra

var bolErrorUsername 	= false;
var bolErrorPassword 	= false;
var bolErrorName 		= false;
var bolErrorPhone 		= false;
var bolErrorBirthday 	= false;
var bolErrorEmail	 	= false;
var bolErrorslcity	     = false;
var bolErrorSecurity	= false;

var strErrorUsername 	= \'\';
var strErrorPassword 	= \'\';
var strErrorName 		= \'\';
var strErrorphone 		= \'\';
var strErrorBirthday 	= \'\';
var strErrorEmail	 	= \'\';
var strErrorslcity	= \'\';
var strErrorSecurity	= \'\';


function Check()
{
	var frm 		= document.frmRegisterForm;
	var strUsername = trim(frm.username.value);
	bolUsernameChecked = true;	
	if ( strUsername.length == 0 )
	{
		document.getElementById("li_Username").className = "error";
		document.getElementById("err_Username").innerHTML = \'Username không được để trống, xin vui lòng nhập Username\';
		document.getElementById("username").focus();
		frm.username.focus();
		return false;
	}
	
	if (!CheckUsernameChar(strUsername))
	{
		document.getElementById("li_Username").className = "error";
		document.getElementById("err_Username").innerHTML = "Username không được sử dụng ký tự Unicode, xin vui lòng kiểm tra lại";
		document.getElementById("username").focus();
		frm.username.focus();
		return false;
	}
	else
	{
		ajax_checkUsername(strUsername);
	}
}

function ResetForm(strUsername)
{
	bolUsernameAccepted = false; // chua chap nhan username
	bolUsernameChecked = false; // chua check
	document.getElementById("input_username_content").innerHTML = \'<input name="username" id="username" alt="alert_box" type="text" class="txt_155"  onfocus="TNPassShowAlert(this,0)" value="\'+strUsername+\'" /> <input id="btCheck" name="btCheck" type="button" class="btn_check" value="Kiểm tra" onclick="Check()" /><p class="gray_10_txt">Username có thể chứa ký tự chữ cái, chữ số, dấu gạch dưới (_), dấu gạch ngang (-) và độ dài từ 6 đến 20 ký tự</p><p class="box_error" id="err_Username"></p>\';
}

function CheckForm()
{
	var frm 			= document.frmRegisterForm;
	var strUsername 	= trim(frm.username.value);
	//var strPassword 	= trim(frm.password.value);
	var strLastname 	= trim(frm.lastname.value);

	//var strDOB_Day		= trim(frm.slday.value);
	//var strDOB_Month	= trim(frm.slmonth.value);
	//var strDOB_Year		= trim(frm.slyear.value);
   
	var strEmail		= trim(frm.email.value);
	//var strLocation		= trim(frm.slcity.value);
	//var strCapcha 		= trim(frm.security_code.value);
	
	var chkUsername = chkPassword = chkFullname = chkDOB = chkEmail = chkLocation = chkSecurity = 0;
	chkUsername = CheckUsernameRegister(strUsername);
    
	//---- Username-------------
	

	
	//---- Fullname-------------
	chkFullname = CheckLastFirstnameRegister(strLastname);
	//---- Fullname-------------
   
	//---- Email-------------
   chkEmail = CheckEmailRegister(strEmail,\''; ?>
<?php echo $this->_tpl_vars['do_useredit']->email[0]; ?>
<?php echo '\');
    //alert(chkEmail);
	//---- Email-------------
         
	if (  chkEmail == 1&&chkFullname==1 ) 
	{
		return true;
	}
	else
	{
		setTimeout("TurnOnError();", 500);
		return false;
	}
	
}

function TurnOnError()
{ 
	if(bolErrorUsername)
	{  
		document.getElementById(\'li_Username\').className = \'error\';
		document.getElementById(\'err_Username\').innerHTML = strErrorUsername;      
	}
	else
	{
		document.getElementById(\'li_Username\').className = \'\';
        document.getElementById(\'err_Username\').innerHTML ="";
	}
	
	
	
	if(bolErrorName)
	{
		document.getElementById(\'li_Fullname\').className = \'error\';
		document.getElementById(\'err_Fullname\').innerHTML = strErrorName;
	}
	else
	{
		document.getElementById(\'li_Fullname\').className = \'\';
        	document.getElementById(\'err_Fullname\').innerHTML = "";
	}
/*	if(bolErrorBirthday)
	{
		document.getElementById(\'li_Birthday\').className = \'error\';
		document.getElementById(\'err_Birthday\').innerHTML = strErrorBirthday;
	}
	else
	{
		document.getElementById(\'li_Birthday\').className = \'\';
	}
	*/
	if(bolErrorEmail)
	{
		document.getElementById(\'li_Email\').className = \'error\';
		document.getElementById(\'err_Email\').innerHTML = strErrorEmail;
	}
	else
	{
		document.getElementById(\'li_Email\').className = \'\';
        document.getElementById(\'err_Email\').innerHTML = "";
	}
     /*
	if(bolErrorLocation)
	{
		document.getElementById(\'li_Location\').className = \'error\';
		document.getElementById(\'err_Location\').innerHTML = strErrorLocation;
	}
	else
	{
		document.getElementById(\'li_Location\').className = \'\';
	}
	
	if(bolErrorSecurity)
	{
		document.getElementById(\'li_Security\').className = \'error\';
		document.getElementById(\'err_Security\').innerHTML = strErrorSecurity;
	}
	else
	{
		document.getElementById(\'li_Security\').className = \'\';
	}
    */
}


//===== Username ===========
function CheckUsernameRegister(strUsername)
{
	if ( strUsername.length == 0 )
	{
		bolErrorUsername = true;
		strErrorUsername = \'Username không được để trống, xin vui lòng nhập Username\';
		return 2;
	}
	else
	{	
		if ( strUsername.indexOf("<")!= \'-1\' || strUsername.indexOf(">") != \'-1\' )
		{
			bolErrorUsername = true;
			strErrorUsername = \'Username không được sử dụng ký tự Unicode, xin vui lòng kiểm tra lại\';			
			return 2;
		}
		else
		{		
			if ( strUsername.length < 6 || strUsername.length > 20 )
			{
				bolErrorUsername = true;
				strErrorUsername = \'Username phải có độ dài lớn hơn 6 ký tự và nhỏ hơn 20 ký tự\';				
				return 2;
			}
			else
			{	
				if ( !CheckUsernameChar( strUsername ) )
				{
					bolErrorUsername = true;
					strErrorUsername = \'Username không được sử dụng ký tự Unicode, xin vui lòng kiểm tra lại\';		
					return 2;
				}
				else
				{
					if(bolUsernameChecked)//neu da nhan nut check
					{
						if ( !bolUsernameAccepted )
						{
							bolErrorUsername = true;
							strErrorUsername = \'Username này đã được đăng ký, xin vui lòng chọn Username khác\';	
							return 2;
						}
						else
						{
							bolErrorUsername = false;
							return 1;
						}
					}
					else
					{
						bolErrorUsername = false;
						return 1;
					}
				}
			}
		}
	}
	return 1;
}
//===== Ho ten ===========

function CheckLastFirstnameRegister(strLastname)
{
	if ( strLastname.length == 0  )
	{
		bolErrorName = true;
		strErrorName = \'Xin vui lòng nhập đầy đủ họ và tên\';
		return 51;
	}
	else
	{	
		if ( strLastname.indexOf("<") != \'-1\' || strLastname.indexOf(">") != \'-1\' ||!CheckCharFullName(strLastname))
		{
			bolErrorName = true;
			strErrorName = \'Họ, tên không được chứa ký tự đặc biệt\';		
			return 52;
		}
		else
		{		
			if ( strLastname.length > 30 )
			{
				bolErrorName = true;
				strErrorName = \'Họ và tên của bạn phải có độ dài nhỏ hơn 30 ký tự\';				
				return 53;
			}
			else
			{	
				bolErrorName = false;
				return 1;
			}
		}
	}
	return 1;
}

/*
//===== Birthday ===========
function CheckBirthdayRegister(intDay, intMonth, intYear)
{
	if ( intDay == 0 || intMonth == 0 || intYear == 0)
	{
		bolErrorBirthday = true;
		strErrorBirthday = \'Xin vui lòng chọn ngày sinh của bạn.\';
		return 101;
	}
	else
	{
		bolErrorBirthday = false;
		return 1;
	}
	return 1;
}
//===== Birthday ===========
*/
//===== Email ===========
function CheckEmailRegister(strEmail,stroldEmail)
{
	if ( strEmail.length == 0 )
	{
		bolErrorEmail = true;
		strErrorEmail = \'<span class="conerr">Xin vui lòng nhập địa chỉ email của bạn</span>\';
		return 71;
	}
	else
	{
		if ( strEmail.indexOf("<") != \'-1\' || strEmail.indexOf(">") != \'-1\' )
		{
			bolErrorEmail = true;
			strErrorEmail = \'<span class="conerr">Địa chỉ email không đúng định dạng, xin vui lòng kiểm tra lại</span>\';
			return 72;
		}
		else
		{		
			if ( !CheckEmail(strEmail ) )
			{
				bolErrorEmail = true;
				strErrorEmail = \'<span class="conerr">Địa chỉ email không đúng định dạng, xin vui lòng kiểm tra lại</span>\';
				return 73;
			}
			else
			{	
			    if(checkEmailed(strEmail,stroldEmail)==1)
                {
                    bolErrorEmail = true;
				strErrorEmail = \'<span class="conerr">Địa chỉ email đã tồn tại, xin vui lòng kiểm tra lại</span>\';
				return 73;
                }
                else
                {
                    bolErrorEmail = false;
				return 1;
                }
				
			}
		}
	}
	return 1;
}
//===== Email ===========
/*
//===== City ===========
function CheckCityRegister(strCity)
{
	if ( strCity == 0 )
	{
		bolErrorLocation = true;
		strErrorLocation = \'Xin vui lòng chọn tỉnh thành nơi bạn sinh sống\';
		return 81;
	}
	else
	{
		bolErrorLocation = false;
		return 1;
	}
	return 1;
}
//===== City ===========

//===== Capcha ===========
function CheckCapchaRegister(strCapcha)
{	
	if ( strCapcha.length == 0 )
	{	
		bolErrorSecurity	= true;
		strErrorSecurity	= \'Bạn cần nhập lại chính xác những ký tự trong hình bên\';
		return 91;
	}
	else
	{
		if ( strCapcha.indexOf("<") != \'-1\' || strCapcha.indexOf(">") != \'-1\' )
		{
			bolErrorSecurity	= true;
			strErrorSecurity	= \'Bạn cần nhập lại chính xác những ký tự trong hình bên\';
			return 92;
		}
		else
		{							
			if ( strCapcha.indexOf(\' \') != -1 )
			{
				bolErrorSecurity	= true;
				strErrorSecurity	= \'Bạn không cần nhập khoảng trắng\';
				return 93;
			}
			else
			{
				bolErrorSecurity	= false;
				return 1;
			}
		}
	}
	return 1;
}
//===== Capcha ===========
*/
setTimeout("TurnOnError();", 500);
</script>
'; ?>