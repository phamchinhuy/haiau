<link rel="stylesheet" href="css/admin/treeview/jquery.treeview.css" />
{literal}
<script type="text/javascript" src="js/treeview/jquery.min.js"></script>
<script src="js/treeview/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="js/treeview/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript" src="js/treeview/demo.js"></script>
</script>
{/literal}



<div class="contentLeftBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="tbl_menuLeft">
                    <tr>
                        <td class="td_header_left">
                            <img src="images/admin/bgheaderlist_left.png"  width="10" height="35"/>
                        </td>
                        <td class="td_header_center">DANH S&#193;CH CHUY&#202;N M&#7908;C</td>    
                        <td class="td_header_right">
                             <img src="images/admin/bgheaderlist_right.png"  width="10" height="35"/>
                        </td>        
                    </tr>
                    <tr>
                        <td colspan="3" class="tdContent">
                            {$str_tree_view}
                        </td>
                    </tr>
                </table>
            </td>
            
        </tr>
    </table>
   
</div>

