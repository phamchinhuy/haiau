<div class="order_detail">
    {if count($cs_order_detail->id)>0}
        <div class="order_detail_title">
            &#272;&#417;n h&#224;ng: #{$cs_order_detail->id_order[0]} - Ng&#224;y &#273;&#7863;t h&#224;ng: {$cs_orders->date_created[0]}
        </div>
        <div class="order_detail_status">
            <form id="frmStatus" method="post">
                T&#236;nh tr&#7841;ng 
                {$radio}
                <input type="submit" name="sbStatus" id="sbStatus" value="C&#7853;p nh&#7853;t" />
            </form>
        </div>
        <div class="order_detail_title">
            Th&#244;ng tin kh&#225;ch h&#224;ng
        </div>
        <div class="order_detail_customer">
            <table cellpadding="2" cellspacing="2" width="100%">
                <tr>
                    <td width="80">
                        H&#7885; t&#234;n: 
                    </td>
                    <td>
                        {$cs_users->name[0]}
                    </td>
                </tr>
                <tr>
                    <td>
                        &#272;&#7883;a ch&#7881;: 
                    </td>
                    <td>
                        {$cs_users->address[0]}
                    </td>
                </tr>
                <tr>
                    <td>
                        &#272;i&#7879;n tho&#7841;i: 
                    </td>
                    <td>
                        {$cs_users->phone[0]}
                    </td>
                </tr>
                <tr>
                    <td>
                        Email: 
                    </td>
                    <td>
                        {$cs_users->username[0]}
                    </td>
                </tr>
                <tr>
                    <td>
                        IP: 
                    </td>
                    <td>
                        {$cs_users->ip[0]}
                    </td>
                </tr>
            </table>
        </div>
        <div class="order_detail_title">
            Th&#244;ng tin &#273;&#417;n h&#224;ng
        </div>
        <div class="order_detail_list">
            <table cellpadding="0" cellspacing="0" width="100%" class="tblOrderDetail">
                <tr>
                    <th>M&#227; s&#7843;n ph&#7849;m</th>
                    <th>T&#234;n s&#7843;n ph&#7849;m</th>
                    <th>H&#236;nh &#7843;nh </th>
                    <th>S&#7889; l&#432;&#7907;ng</th>
                    <th>Gi&#225; VN&#272;</th>
                </tr>
                {section name=d loop=$cs_order_detail->id}
                <tr>
                    <td>
                        {$cs_order_detail->product_code[d]}
                    </td>
                    <td>
                        {$cs_product->product[d]}
                    </td>
                    <td>
                        <img src="uploads/product/{$cs_product->small_image[d]}" width="80" height="80" />
                    </td>
                    <td>
                        {$cs_order_detail->count[d]}
                    </td>
                    <td>
                        {$cs_order_detail->price[d]}
                    </td>
                </tr>
                {/section}
            </table>
        </div>
        <div class="order_detail_title">
            Th&#244;ng tin ghi ch&#250; 
        </div>
        <div class="order_detail_staff">
            <form id="frmStaff" name="Staff" method="post">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            Ghi ch&#250; c&#7911;a nh&#226;n vi&#234;n ph&#7909; tr&#225;ch    
                        </td> 
                    </tr>
                    <tr>
                        <td>
                             <textarea cols="40" rows="10" id="textAreaNoteStaff" name="textAreaNoteStaff">{$cs_orders->detail[0]}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="sbStaff" id="sbStaff" value="L&#432;u" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    {/if}
</div>