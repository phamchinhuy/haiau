{literal}
<script language="javascript">
    function checkfrm()
    {
       
        product= document.getElementById('txtNews').value;
        shortitle= document.getElementById('ShortDescription').value;
        image_large= document.getElementById('fileLarge').value;
        image_thum= document.getElementById('fileSmall').value;
        price= document.getElementById('txtPrice').value;
        page_tile= document.getElementById('txtpagetitle').value;
        meta_des= document.getElementById('txtmeta_des').value;
        meta_key= document.getElementById('txtmeta_keyword').value;
        price  = document.getElementById('txtPrice').value;
        
        document.getElementById('nameeror').innerHTML="";
        document.getElementById('titleeror').innerHTML="";
        document.getElementById('images_thum_error').innerHTML="";
        document.getElementById('images_large_error').innerHTML="";
        document.getElementById('metakey_eror').innerHTML="";
        document.getElementById('meta_des_error').innerHTML="";
        document.getElementById('priceerror').innerHTML="";
        document.getElementById('pagetitleerror').innerHTML="";
         document.getElementById('images_thum_error').innerHTML="";
        
        if(product==0)
        {
             document.getElementById('nameeror').innerHTML="Nhap ten san pham";
             product= document.getElementById('txtNews').focus();
             return false;
        }
        if(shortitle==0)
        {
             document.getElementById('titleeror').innerHTML="Nhap title";
             document.getElementById('txtShortDescription').focus();
             return false;
        }
        if(image_large.length==0)
        {
            
             document.getElementById('images_large_error').innerHTML="Xin nhap anh lon";
              return false;
        }
        else{
            if(!checkfile(image_large))
            {
                 document.getElementById('images_large_error').innerHTML="Xin nhap file anh";
              return false;
            }   
        }
        if(image_thum.length==0)
        {
              document.getElementById('images_thum_error').innerHTML="Xin nhap file ảnh";
              return false;
        }
        else
        {
            if(!checkfile(image_thum))
            {
                document.getElementById('images_thum_error').innerHTML="Xin nhap file ảnh";
                return false;
            }
        }
        if(price==0)
        {
              document.getElementById('priceerror').innerHTML="Nhap gia";
              document.getElementById('txtPrice').focus();
              return false;
        }
        if(page_tile==0)
        {
            document.getElementById('pagetitleerror').innerHTML="Nhap vao page title";
            document.getElementById('txtpagetitle').focus();
            return false;
        }
        if(CheckUnicodeChar(page_tile)!=1)
        {
            document.getElementById('pagetitleerror').innerHTML="Page title co dạng format(aa-bb)";
            document.getElementById('txtpagetitle').focus();
            return false;
        }
        if(checPageTitle(page_tile)==1)
        {
             document.getElementById('pagetitleerror').innerHTML="Page title da ton tai";
            document.getElementById('txtpagetitle').focus();
            return false;
        }
        if(meta_des==0)
        {
              document.getElementById('meta_des_error').innerHTML="Meta_descritption khong duoc de trong";
              return false;
        }
        if(meta_key=="")
        {
           document.getElementById('metakey_eror').innerHTML="Meta_Keyword khong duoc de trong";
              return false;
        }
        return true;
    }
    function checPageTitle(u) {
    var isExist = true;
    $.ajax({
        async: false,
        type: "POST",
	url: "include/admin/admin_product_check_page_title.php",
	data: "pagetile="+u,
	success: function(msg){
	     isExist = msg;
    
	}
    });

    return isExist;
}

//cho phep nhung ky tu tu A->Z, a->z, 0->9,va cac ky tu _-
function CheckUnicodeChar(stringIn)
{
    	retval = false;
     var i;
     for ( i=0; i <= stringIn.length-1; i++) 
	 {
	     
        
         if ( ( ( stringIn.charCodeAt(i) >= 48)&&(stringIn.charCodeAt(i) <= 57)) || ((stringIn.charCodeAt(i) > 64)&&(stringIn.charCodeAt(i) <= 90)) || ((stringIn.charCodeAt(i) >= 97)&&(stringIn.charCodeAt(i) <= 122)) || (stringIn.charCodeAt(i) == 95) || (stringIn.charCodeAt(i) == 45) )
		 {
		     
           	retval = true;
         }
		 else
		 {
			retval = false;
			break;
         }
     }
     for ( i=0; i<= stringIn.length-2; i++) 
     {
        if(stringIn.charCodeAt(i)==45&&stringIn.charCodeAt(i+1)==45)
        {
            retval = false;
			break;
        }
     }
     return retval;
}
function checkfile(strfile)
{  
    var ext = strfile.substring(strfile.lastIndexOf('.') + 1); 
    if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png"||ext=="PNG")
    {          
         return true;
    }
    else
    {
        return false;
    }
}
</script>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" onsubmit="return checkfrm();" >
            <fieldset class="fieldSet">
                <legend class="legen">THÊM SẢN PHẨM MỚI</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Tiêu đề
                        </td>
                        <td class="tdRight">
                            <span id="textTitle">
                            <input type="text" id="txtNews" name="txtNews" value="" class="textField" />
                            <span class="textfieldRequiredMsg" id="nameeror">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            M&#227; h&#224;ng
                        </td>
                        <td class="tdRight">
                            <span id="textTitle">
                            <input type="text" id="txtCodeProduct" name="txtCodeProduct" value="" class="textField" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Tóm tắt
                        </td>
                        <td class="tdRight">
                            <span id="txtShortDescription">
                            <textarea name="txtShortDescription" id="ShortDescription" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="titleeror">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Chi tiết
                        </td>
                        <td class="tdRight">
                            <span id="errorLongDescription">
                            {$ckeditor->editor("description_product")}
                            <!--<span class="textareaRequiredMsg">(Không được rổng)</span>--> 
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Hình nhỏ
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileSmall" id="fileSmall" />
                            <br />
                            <input id="txtErrorImageSmall" name="txtErrorImageSmall" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                                           <span class="textareaRequiredMsg" id="images_thum_error">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                     <tr>
                        <td class="tdLeft">
                            Hình lớn
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                             <br />
                            <input id="txtImageLarge" name="txtImageLarge" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />
                            <span class="textareaRequiredMsg" id="images_large_error"></span></span>           
                        </td>
                    </tr>  
                   
                      <tr>
                        <td class="tdLeft">
                           Color
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrl" name="txtcolor" value="" class="textField" />
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                      
                        </td>
                    </tr>
                       <tr>
                        <td class="tdLeft">
                            Size:
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrl" name="txtsize" value="" class="textField" />
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                        </td>
                    </tr>
                    
                   
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <input type="checkbox" name="active" value="1"  />                         
                        </td>
                    </tr>
                    
                     <tr>
                        <td class="tdLeft">
                            Price
                        </td>
                        <td class="tdRight">
                            <span id="errorPrice">
                           <input type="text" name="txtPrice" id="txtPrice" class="textField"  />
                            <span class="textfieldRequiredMsg" id="priceerror">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                           Page_Title:
                        </td>
                        <td class="tdRight">
                            
                            <input type="text" name="txtpagetitle" id="txtpagetitle" class="textField"  />
                            <span class="textfieldRequiredMsg" id="pagetitleerror">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                          Meta_Keyword:
                        </td>
                        <td class="tdRight">
                            
                       
                             <textarea name="txtmeta_keyword" id="txtmeta_keyword" class="textField" ></textarea>
                            <span class="textmetakeyworderror" id="metakey_eror">(Không được rổng)</span></span>
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                          Meta_Description:
                        </td>
                        <td class="tdRight">
                                                
                             <textarea name="txtDes" id="txtmeta_des" class="textField" ></textarea>
                             <span class="textmetadeserror" id="meta_des_error">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" name="btAdd" value="L&#432;u"  />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
