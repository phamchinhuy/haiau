

<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" >
            <fieldset class="fieldSet">
                <legend class="legen">CẬP NHẬT FAQ</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Họ tên 
                        </td>
                        <td class="tdRight">
                            <span id="errorFullname">
                            <input type="text" class="required"  id="txtFullname" name="txtFullname" value="{$cs_faq->fullname[0]}" />
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span>  
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Địa chỉ
                        </td>
                        <td class="tdRight">
                            <span id="errorAddress">
                            <input type="text" id="txtAddress" name="txtAddress" value="{$cs_faq->address[0]}" class="textField" />
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Email
                        </td>
                        <td class="tdRight">
                            <span id="errorEmail">
                            <span id="errorEmailSyntax">
                            <input type="text" id="txtEmail" name="txtEmail" value="{$cs_faq->email[0]}" class="textField" />
                            <span class="textfieldInvalidFormatMsg">(Không đúng định dạng email)</span></span> 
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                   
                    <tr>
                        <td class="tdLeft">
                            Điện thoại
                        </td>
                        <td class="tdRight">
                             <span id="errorPhoneNumber">
                            <span id="errorPhoneNumberSyntax">
                            <input type="text" id="txtPhone" name="txtPhone" value="{$cs_faq->phone[0]}" class="textField" />
                            <span class="textfieldInvalidFormatMsg">(Không đúng định dạng phone)</span></span> 
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ngày tạo
                        </td>
                        <td class="tdRight">
                        <input type="text" name="txtDateCreatedQuestion" value="{$cs_faq->dateCreatedQuestion[0]}"/>
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'txtDateCreatedQuestion'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Câu hỏi
                        </td>
                        <td class="tdRight">
                            
                            {$ckeditor->editor('txtQuestion',$cs_faq->question[0])}
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Phản hồi
                        </td>
                        <td class="tdRight">
                            
                            {$ckeditor->editor('txtReply',$cs_faq->reply[0])}
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Kích hoạt
                        </td>
                        <td class="tdRight">
                            {if $cs_faq->status[0]=="1"}
                                <input type="checkbox" name="status" id="status" checked="checked"   />
                            {else}
                                <input type="checkbox" name="status" id="status"   />
                            {/if}
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btEdit" value="L&#432;u" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
{literal}
<script type="text/javascript">
<!--
var errorFullname           = new Spry.Widget.ValidationTextField("errorFullname");
var errorAddress            = new Spry.Widget.ValidationTextField("errorAddress");
var errorEmailSyntax        = new Spry.Widget.ValidationTextField("errorEmailSyntax");
var errorEmail              = new Spry.Widget.ValidationTextField("errorEmail","email");
var errorPhoneNumber        = new Spry.Widget.ValidationTextField("errorPhoneNumber");
var errorPhoneNumberSyntax  = new Spry.Widget.ValidationTextField("errorPhoneNumberSyntax","integer");

//-->
</script>
{/literal}