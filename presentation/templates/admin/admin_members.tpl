<div class="members">
    <div class="members_title">
        QU&#7842;N L&#221; T&#7842;I KHO&#7842;N
    </div>
    <div class="members_list">
        <div id="page_top_member" class="paginatetop"></div>
        <div id="page_member_content" class="productcontent">
            Kh&#244;ng c&#243; t&#224;i kho&#7843;n
        </div>
        <div id="page_bottom_member" class="paginatebottom"></div>
        {literal}
            <script type="text/javascript">
                var listMember={
                pages: [{/literal}{$scriptMember}{literal}],
                selectedpage: 0 //set page shown by default (0=1st page)
                }
                var mybookinstance=new ajaxpageclass.createBook(listMember, "page_member_content", ["page_top_member", "page_bottom_member"])
            </script>
        {/literal}
    </div>
</div>