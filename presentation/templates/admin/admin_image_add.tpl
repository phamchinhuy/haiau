{literal}
   <script language="javascript" src="js/admin/admin_check_gallary.js" type="text/javascript"></script>
<script language="javascript">
    function checkFileSize()
    {
        var size;
    	if (navigator.userAgent.indexOf("Firefox") != -1){
    		smallSize=checkFileSizeFireFox('fileSmall');
            largeSize=checkFileSizeFireFox('fileLarge');
    	}else{
    		smallSize=checkFileSizeIE('fileSmall');
            largeSize=checkFileSizeIE('fileLarge');
    	}
        if(smallSize>2148576||largeSize>2148576)
        {
            alert("size of file too large");
            return false;
        }
        else return true;
    }

    //Check file size before upload, it is working well with Firefox
    //It is not compatible with another browse
    function checkFileSizeFireFox(file)
     {
    	var node = document.getElementById(file);
    	var size = node.files[0].fileSize;
        return size;
     }
    
    // It is working well with IE
    function checkFileSizeIE(file)
    {
    	var myFSO = new ActiveXObject("Scripting.FileSystemObject");
    	//var filepath = document.upload.file.value;
    	var filepath= document.getElementById(file).value;
    	var thefile = myFSO.getFile(filepath);
    	var size = thefile.size;
   	    return size;
    }
    //check file type 
    function getfileExtension(inputId) 
    { 
        alert(inputId)
     var fileinput = document.getElementById(inputId); 
     if(!fileinput ) return ""; 
     var filename = fileinput.value; 
     if( filename.length == 0 ) return ""; 
     var dot = filename.lastIndexof("."); 
     if( dot == -1 ) return ""; 
     var extension = filename.substr(dot,filename.length); 
     alert(fileinput.value)
     return extension; 
    } 
    function checkfileType(inputId,allowedExt) 
    { 
        
     var ext = getfileExtension(inputId); 
     if( ext == allowedExt ) 
      alert("Congrats"); 
     else 
      alert("Sorry"); 
    } 
</script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	$("#txtTitleSEO").blur(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("include/admin/seo_availability.php",{ $txtSEOImageAdd:$(this).val() } ,function(data)
        {
		  if(data=='no') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO đã tồn tại').addClass('messageboxerror').fadeTo(900,1);
              $('#submit').attr('disabled', 'disabled');
			});		
          }
		  else
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO hợp lệ').addClass('messageboxok').fadeTo(900,1);
              $('#submit').attr('disabled', '');

             
              	
			});
		  }
				
        });
 
	});
});

                    
               
</script>
<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" onsubmit="return check_insert();" >
            <fieldset class="fieldSet">
                <legend class="legen">TH&#202;M H&#204;NH &#7842;NH M&#7898;I</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            T&#234;n h&#236;nh
                        </td>
                        <td class="tdRight">
                            <span id="textTitle">
                            <input type="text" id="txtName" name="txtName" value="" class="textField" />
                            <span class="textfieldRequiredMsg" id="error_title">(Không được rổng)</span></span>   
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                        Logo
                        </td>
                        <td class="tdRight">
                            <span id="errorFileLarge">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                            <span class="textfieldRequiredMsg" id="error_file_large">(Không được rổng)</span></span>   
                             
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td class="tdLeft">
                            H&#236;nh nh&#7887;
                        </td>
                        <td class="tdRight">
                            <span id="errorFileSmall">
                            <input type="file" name="fileSmall" id="fileSmall" />
                            <span class="textfieldRequiredMsg" id="error_file_small">(Không được rổng)</span></span>  
                        </td>
                    </tr>
                    !-->
                    
                    <tr>
                        <td class="tdLeft">
                            &#272;&#432;&#7901;ng d&#7851;n h&#236;nh
                        </td>
                        <td class="tdRight">
                             <input type="text" name="txtURL" value="" class="textField"  />
                        </td>
                    </tr>
                <!--    <tr>
                        <td class="tdLeft">
                            M&#244; t&#7843; h&#236;nh &#7843;nh
                        </td>
                        <td class="tdRight">
                            {$ckeditor->editor('description_news')}
                        </td>
                    </tr>
                       !-->
                    <tr>
                        <td class="tdLeft">
                            K&#237;ch ho&#7841;t
                        </td>
                        <td class="tdRight">
                             <input type="checkbox" name="chkStatus" id="chkStatus" value="1"  />
                        </td>
                    </tr>
                <!--
                    <tr>
                        <td class="tdLeft">
                            Cho ph&#233;p b&#236;nh lu&#7853;n
                        </td>
                        <td class="tdRight">
                             <input type="checkbox" name="chkComment" id="chkComment" value="1"  />
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                             Ngày bắt đầu
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginDate" />
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'beginDate'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ngày kết thúc
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginEnd" />
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'beginEnd'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            T&#7915; kh&#243;a t&#236;m ki&#7871;m
                        </td>
                        <td class="tdRight">
                            <textarea name="txtKeyword" id="txtKeyword" class="textField" ></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Ti&#234;u &#273;&#7873; cho SEO
                        </td>
                        <td class="tdRight">
                            <span id="errorSEO">
                            <input type="text" name="txtTitleSEO" id="txtTitleSEO" class="textField"  />
                            <span class="textfieldRequiredMsg" id="error_seo">(Không được rổng)</span></span>  
                            <span id="msgbox" style="display:none"></span>   
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            M&#244; t&#7843; cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaDescription">
                            <textarea name="txtDescriptionMeta" id="txtDescriptionMeta" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="error_meta">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            T&#7917; kh&#243;a cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaKeyword">
                            <textarea name="txtKeywordMeta" id="txtKeywordMeta" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="error_key_meta">(Không được rổng)</span></span>
                        </td>
                    </tr>
                       !-->
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btAdd" value="L&#432;u" onclick="if(!checkFileSize()) return false; "/>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
{literal}
<script type="text/javascript">
<!--
var errorSEO = new Spry.Widget.ValidationTextField("errorSEO");
var errorMetaKeyword = new Spry.Widget.ValidationTextarea("errorMetaKeyword");
var errorMetaDescription = new Spry.Widget.ValidationTextarea("errorMetaDescription");
var textTitle = new Spry.Widget.ValidationTextField("textTitle");
var errorFileLarge = new Spry.Widget.ValidationTextField("errorFileLarge");
var errorFileSmall = new Spry.Widget.ValidationTextField("errorFileSmall");
var txtShortDescription = new Spry.Widget.ValidationTextarea("txtShortDescription");
var errorLongDescription = new Spry.Widget.ValidationTextarea("errorLongDescription");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
//-->
</script>
{/literal}