{literal}


<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
<script language="javascript">
 function check_frm()
 {
      var website=document.getElementById("txtname").value; 
      var href=document.getElementById("txthref").value;
      ///
      document.getElementById("error_title").innerHTML="";
  
          document.getElementById("msgbox").innerHTML="";
      if(website=="")
      {
          document.getElementById("error_title").innerHTML="Nh&#7853;p t&#234;n website";
          return false;
      }

      if(href.length==0)
      {
         document.getElementById("msgbox").innerHTML="Nh&#7853;p &#273;&#432;&#7901;ng d&#7849;n";
        return false;
      }
      return true;
 }
</script>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  id="frmAddImage" name="frmAddImage" method="post" onsubmit="return check_frm();">
            <fieldset class="fieldSet">
                <legend class="legen">TH&#202;M M&#7898;I TH&#212;NG B&#193;O</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Th&#244;ng b&#225;o
                        </td>
                        <td class="tdRight">
                          <span id="textTitle">
                            <input type="text" id="txtname" name="txtname" value="{$news}" class="textField" />
                          <span class="textfieldRequiredMsg" id="error_title">(Không được rổng)</span></span>   
                        </td>
                    </tr>
              <!--      <tr>
                        <td class="tdLeft">
                        &#272;&#7883;a ch&#7881;
                        </td>
                        <td class="tdRight">
                            <span id="txtShortDescription">
                            <textarea name="txtadress" id="txtadress" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="error_short_des"></span></span>
                        </td>
                    </tr>!-->
                   <!-- <tr>
                        <td class="tdLeft">
                       Email
                        </td>
                        <td class="tdRight">
                            <span id="errorLongDescription">
                         <textarea name="txtemail" id="txtemail" class="textField" ></textarea>
                          <span class="textareaRequiredMsg">(Không được rổng)</span> 
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                         Phone
                        </td>
                        <td class="tdRight">
                           <textarea name="txtphone" id="txtphone" class="textField" ></textarea>
                              <span class="textfieldRequiredMsg" id="error_small_image"></span></span> 
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           Ti&#234;u &#273;&#7873;
                        </td>
                        <td class="tdRight">
                           <textarea name="txt_des" id="txt_des" class="textField" ></textarea>
                               <span class="textfieldRequiredMsg" id="error_large_image">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                  
                    <tr>
                        <td class="tdLeft">
                            Target
                        </td>
                        <td class="tdRight">
                        <select name="target" >
                        
                            <option value="_self" selected="" >_self</option>
                        
                            <option value="_blank" selected="" >_blank</option>
                       
                         
                       </select>                          
                        </td>
                    </tr>
                 -->
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <input type="checkbox" name="active"  value="1" />                       
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &#272;&#432;&#7901;ng d&#7849;n
                        </td>
                        <td class="tdRight">
                            <span id="errorSEO">
                            <textarea name="txthref" id="txthref" class="textField" ></textarea>
                            <span id="msgbox" >(Không được rổng)</span>  
                        </td>
                    </tr>
                   
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btAdd" value="L&#432;u"  />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
