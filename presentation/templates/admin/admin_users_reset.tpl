{literal}
<script language="javascript" src="{/literal}{$js_path}{literal}/common.js" type="text/javascript"></script>

<script language="javascript">
function checkEmailed(u,o) {
    var isExist = false;
    if(u!=o)
    {
        $.ajax({
        async: false,
        type: "POST",
	url: "include/admin/bl_check_email_exist.php",
	data: "username=" + u,
	success: function(msg){
	     isExist = msg;
         //alert(isExist);
	}
    }); 
    }
   
    return isExist;
}           

</script>
<style type="text/css">
.main_box_content{
	width:749px;
	height:auto;
	overflow:hidden;
	padding:0 2px 0 2px;
	position:relative;
}

.main_box_content .ul_regis_acc{
	width:450px;
	height:auto;
	overflow:hidden;
	margin-top:5px;
	float:left;
}

 .main_box_content .ul_regis_acc li{
	width:450px;
	margin-top:4px;
	height:auto;
	overflow:hidden;
	clear:both;
	float:left;
}
 .main_box_content .ul_regis_acc li .box_text{
	width:143px;
	height:auto;
	overflow:hidden;
	padding-right:5px;
	font:11px/20px tahoma;
	color:#333;
	text-align:right;
	float:left;
}
 .main_box_content   .ul_regis_acc li .box_frm{
	width:214px;
	float:left;
	font:11px tahoma;
	color:#333;
	border:1px solid #fff;
	padding:2px;
}
   .main_box_content .ul_regis_acc li .box_frm .btn_check{
	width:54px;
	height:19px;
	border:1px solid #d9d9d9;
	text-align:center;
	font:11px tahoma;
	color:#000;
	cursor:pointer;
	float:left;
	padding-bottom:3px;
}
 .bottom_btn{
	overflow:hidden;
	margin-top:5px;
	margin-left:152px;
	clear:both;
	font:11px tahoma;
	color:#333;
}
 .bottom_btn .btn_cover{
	margin-right:18px;
	width:90px;
	padding-top:3px;
	margin-bottom:5px;
}
/**/
 .main_box_content .red_txt{
	color:#ff0000;
}
.main_box_content .ul_regis_acc li.error .box_frm{
	border:1px solid #ffe4a4;
	background:#fffdd4;
}
 .main_box_content .ul_regis_acc li .box_error
 {
    color:red;
 }
.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}
</style>
{/literal}
<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
    </div>
    <div class="admin_image_form_add_field">
      
            <fieldset class="fieldSet">
                <legend class="legen">{$headerCategorytable}</legend>
                 
                 <form id="frmRegisterForm" name="frmRegisterForm" method="post" action="" onsubmit="return CheckForm()">
                <div class="main_box_content">
                    <ul class="ul_regis_acc">
                    <!-- <li >
                          <div class="box_text"><span class="red_txt"></span>Lo&#7841;i &#273;&#259;ng nh&#7853;p:</div>
                          <div class="box_frm" id="input_username_content">
				           <select name="user_type">
                           <option value="subadmin" {if $do_useredit->type[0]=="subadmin"} selected="selected" {/if} >SubAdmin</option>
                           <option value="admin" {if $do_useredit->type[0]=="admin"} selected="selected"{/if} >Admin</option>
                           <option value="user" {if $do_useredit->type[0]=="user"} selected="selected"{/if}>user</option>
                           
                           </select>
                           { $do_useredit->user_type[0]}
				        </div>

                        </li>
                   !-->     
                        <li id="li_Username">
                          <div class="box_text"><span class="red_txt"> </span>  T&#234;n &#273;&#259;ng nh&#7853;p:</div>
                          <div class="box_frm" id="input_username_content">
				             <input name="username" id="username" alt="alert_box" type="text" class="txt_155"
                             
                              value="{$do_useredit->username[0]}" disabled="disabled" />
				            <!--<input id="btCheck" name="btCheck" type="button" class="btn_check" value="Kiểm tra" onclick="Check()" />!-->				                      
                           <p class="gray_10_txt"></p>
                            <p class="box_error" id="err_Username"></p>
				        </div>
                        
                        </li>
                
                			  <li id="li_Fullname">
				<div class="box_text"><span class="red_txt"> </span>T&#234;n ng&#432;&#7901;i d&#249;ng:</div>
				<div class="box_frm">
				  <input name="lastname" type="text" class="txt_133" id="lastName" disabled="disabled" value="{$do_useredit->fullname[0]}" />
				  <div class="gray_10_txt">
				  </div>
				  <p class="box_error" id="err_Fullname"></p>
				</div>
			  </li>
              
			  </li>
               
                  <li id="li_Password">
				<div class="box_text"><span class="red_txt"></span>Password m&#7899;i:</div>
				<div class="box_frm">
				<input type="password" class="txt_133" id="password" name="password" value="" />
				  <p class="box_error" id="err_Password"></p>
				</div>
              
			  </li>
                 
                  <li id="li_status">
				<div class="box_text"><span class="red_txt"> </span>G&#7903;i qua mail:</div>
				<div class="box_frm">
				  <input  type="checkbox" name="reset" value="1" /> {$do_useredit->email[0]}
                  </div>
				</li>
                    </ul>
                 </div>
                 <div class="bottom_btn">
			  <div class="btn_cover">
				<input name="register" type="submit" class="tnpp_button_login" value="L&#432;u" />
			   
              </div>
				
			</div>	

                 </form>   
                
                 </legend>
            </fieldset>
      
    </div>
</div>
{literal}
<script language="javascript">
var bolUsernameAccepted = false; //username da duoc chap nhan
var bolUsernameChecked 	= false; //username da duoc kiem tra
var bolErrorPassword 	= false;
var strErrorPassword 	= '';
function ResetForm(strUsername)
{
	bolUsernameAccepted = false; // chua chap nhan username
	bolUsernameChecked = false; // chua check
	document.getElementById("input_username_content").innerHTML = '<input name="username" id="username" alt="alert_box" type="text" class="txt_155"  onfocus="TNPassShowAlert(this,0)" value="'+strUsername+'" /> <input id="btCheck" name="btCheck" type="button" class="btn_check" value="Kiểm tra" onclick="Check()" /><p class="gray_10_txt">Username có thể chứa ký tự chữ cái, chữ số, dấu gạch dưới (_), dấu gạch ngang (-) và độ dài từ 6 đến 20 ký tự</p><p class="box_error" id="err_Username"></p>';
}

function CheckForm()
{
	var frm 			= document.frmRegisterForm;
	var strPassword 	= trim(frm.password.value);
    chkpass=0;  
    chkpass = CheckPassRegister(strPassword);    
  
	if ( chkpass==1 ) 
	{
		return true;
	}
	else
	{
		setTimeout("TurnOnError();", 500);
		return false;
	}	
}
function CheckPassRegister(strPassword)
{
	if ( strPassword.length == 0 )
	{
		bolErrorPassword = true;
		strErrorPassword = 'Password không được để trống, xin vui lòng nhập password';
		return 31;
	}
	else
	{
		if ( strPassword.indexOf("<") != '-1' || strPassword.indexOf(">") != '-1' )
		{
			bolErrorPassword = true;
			strErrorPassword = 'Password không được sử dụng ký tự Unicode, xin vui lòng kiểm tra lại';
			return 32;
		}
		else
		{		
			if ( strPassword.length < 6  )
			{
				bolErrorPassword = true;
				strErrorPassword = 'Password phải có độ dài lớn hơn 6 ký tự ';
				return 33;
			}
			else
			{	
				if ( !CheckCharPassword( strPassword ) )
				{
					bolErrorPassword = true;
					strErrorPassword = 'Password không được sử dụng ký tự Unicode, xin vui lòng kiểm tra lại';		
					return 34;
				}
				else
				{										
					bolErrorPassword = false;
					return 1;
				}
			}
		}	
	}
	return 1;
}
function TurnOnError()
{ 
	
	
	
	if(bolErrorPassword)
	{
		document.getElementById('li_Password').className = 'error';
		document.getElementById('err_Password').innerHTML = strErrorPassword;
	}
	else
	{
		document.getElementById('li_Password').className = '';
        document.getElementById('err_Password').innerHTML ="";
	}

}


setTimeout("TurnOnError();", 500);
</script>
{/literal}