
<div class="orders_list_header">
    Danh s&#225;ch &#273;&#417;n h&#224;ng
</div>
<div class="orders_list">
    <div id="page_top_orders" class="paginatetop"></div>
    <div id="page_orders_content" class="productcontent">
        Kh&#244;ng c&#243; &#273;&#417;n h&#224;ng
    </div>
    <div id="page_bottom_orders" class="paginatebottom"></div>
    {literal}
        <script type="text/javascript">
            var listOrders={
            pages: [{/literal}{$scriptOrders}{literal}],
            selectedpage: 0 //set page shown by default (0=1st page)
            }
            var mybookinstance=new ajaxpageclass.createBook(listOrders, "page_orders_content", ["page_top_orders", "page_bottom_orders"])
        </script>
    {/literal}
</div>