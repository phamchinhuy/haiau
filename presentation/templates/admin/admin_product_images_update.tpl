{literal}
<script language="javascript">
    function checkfrm()
    {
       
        image_large= document.getElementById('fileLarge').value;
        image_thum= document.getElementById('fileSmall').value;
    
        document.getElementById('images_thum_error').innerHTML="";
        document.getElementById('images_large_error').innerHTML="";
     if(image_large.length!=0&&!checkfile(image_large))
            {
                 document.getElementById('images_large_error').innerHTML="Xin nhap file anh";
              return false;
            }   
        
       
            if(image_thum.length!=0&&!checkfile(image_thum))
            {
                document.getElementById('images_thum_error').innerHTML="Xin nhap file ảnh";
                return false;
            }
        return true;
    }



 function checkfile(strfile)
    {  
        var ext = strfile.substring(strfile.lastIndexOf('.') + 1); 
        if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png"||ext=="PNG")
        {          
             return true;
        }
        else
        {
            return false;
        }
    }
</script>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAdd" name="frm" method="post" onsubmit="return checkfrm();" >
            <fieldset class="fieldSet">
                <legend class="legen">THAY  &#272;&#7892;I &#7842;NH C&#7910;A {$product_name}</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
           
                  
                        <td class="tdLeft">
                            Hình lớn
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                                    <img src="{$folder_upload_product}{$cs_images_list->path_image_large[0]}" width="80" height="80" />
                             <br />
                                          <input type="hidden" value="{$cs_images_list->path_image_large[0]}" name="reimage_large" />
                            <span class="textareaRequiredMsg" id="images_large_error">(Không được rổng)</span>           
                        </td>
                    </tr>  
                    <tr>
                        <td class="tdLeft">
                            Hình nhỏ
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileSmall" id="fileSmall" />
                                    <img src="{$folder_upload_product}{$cs_images_list->path_image_thumb[0]}" width="80" height="80" />
                            <img src=""  />
                            <br />
                            <input type="hidden" value="{$cs_images_list->path_image_thumb[0]}" name="reimage_thum" />
                                           <span class="textareaRequiredMsg" id="images_thum_error">(Không được rổng)</span></span>
                        </td>
                    </tr>                  
                      <tr>
                        <td class="tdLeft">
                         Ti&#234;u &#273;&#7873; :
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txttitle" name="txttitle" value="{$cs_images_list->alt[0]}" class="textField" />
                            <img src="" /> 
                            <br />
                            <input id="txtErrorName" name="txtErrorName" class="border_green"  
                                    type="text" disabled="disabled" 
                                    style="background-color:#FFF;  display:none; color:#F00; 
                                    border:1px dotted #c7c7c7;" />   
                      
                        </td>
                    </tr>                   
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <input type="checkbox" name="active" value="1"
                        {if $cs_images_list->status[0]==1} checked="checked" {/if}
                          />                         
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" name="btAdd" value="L&#432;u"  />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
