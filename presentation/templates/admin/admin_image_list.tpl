{literal}
   <script language="javascript" src="js/admin/change_position_gallary.js" type="text/javascript"></script>
{/literal}
<form  id="news" name="myform" method="post" >
<div class="admin_image_form_list">
    <div class="admin_image_form_list_field">
            
                <div id="paginate-top" class="paginatetop"></div>
                <div id="productcontent" class="productcontent"></div>
                <div id="paginate-bottom" class="paginatebottom"></div>
            
          
            {literal}
                <script type="text/javascript">
                    var listNews={
                    pages: [{/literal}{$scriptPaging}{literal}],
                    selectedpage: 0 //set page shown by default (0=1st page)
                    }
                    var mybookinstance=new ajaxpageclass.createBook(listNews, "productcontent", ["paginate-top", "paginate-bottom"])
                </script>
            {/literal}
            
    </div>
    
</div>
</form>