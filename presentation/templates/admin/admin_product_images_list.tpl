

<form  id="product"  enctype="multipart/form-data" name="myform" method="post" >
<div class="admin_image_form_list">
    <div class="admin_image_form_list_field">
            
            <div id="paginate-top" class="paginatetop"></div>
            <div id="productcontent" class="productcontent"></div>
            <div id="paginate-bottom" class="paginatebottom"></div>
            
            {literal}
                <script type="text/javascript">
                    var listProduct={
                    pages: [{/literal}{$scriptPagingProductimages}{literal}],
                    selectedpage: 0 //set page shown by default (0=1st page)
                    }
                    var mybookinstance=new ajaxpageclass.createBook(listProduct, "productcontent", ["paginate-top", "paginate-bottom"])
                </script>
            {/literal}
    </div>
    
</div>
</form>

