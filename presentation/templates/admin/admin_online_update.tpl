{literal}
<script language="javascript" src="js/admin/check_news.js" type="text/javascript"></script>
<script language="javascript">
    function checkFileSize()
    {
        var size;
    	if (navigator.userAgent.indexOf("Firefox") != -1){
    		smallSize=checkFileSizeFireFox('fileSmall');
            largeSize=checkFileSizeFireFox('fileLarge');
    	}else{
    		smallSize=checkFileSizeIE('fileSmall');
            largeSize=checkFileSizeIE('fileLarge');
    	}
        if(smallSize>2148576||largeSize>2148576)
        {
            alert("size of file too large");
            return false;
        }
        else return true;
    }

    //Check file size before upload, it is working well with Firefox
    //It is not compatible with another browse
    function checkFileSizeFireFox(file)
     {
    	var node = document.getElementById(file);
    	var size = node.files[0].fileSize;
        return size;
     }
    
    // It is working well with IE
    function checkFileSizeIE(file)
    {
    	var myFSO = new ActiveXObject("Scripting.FileSystemObject");
    	//var filepath = document.upload.file.value;
    	var filepath= document.getElementById(file).value;
    	var thefile = myFSO.getFile(filepath);
    	var size = thefile.size;
   	    return size;
    }
    //check file type 
    function getfileExtension(inputId) 
    { 
        alert(inputId)
     var fileinput = document.getElementById(inputId); 
     if(!fileinput ) return ""; 
     var filename = fileinput.value; 
     if( filename.length == 0 ) return ""; 
     var dot = filename.lastIndexof("."); 
     if( dot == -1 ) return ""; 
     var extension = filename.substr(dot,filename.length); 
     alert(fileinput.value)
     return extension; 
    } 
    function checkfileType(inputId,allowedExt) 
    { 
        
     var ext = getfileExtension(inputId); 
     if( ext == allowedExt ) 
      alert("Congrats"); 
     else 
      alert("Sorry"); 
    } 

</script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	$("#txtTitleSEO").blur(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("include/admin/seo_availability.php",{ $txtSEOAdd:$(this).val() } ,function(data)
        {
		  if(data=='no') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO đã tồn tại').addClass('messageboxerror').fadeTo(900,1);
              $('#submit').attr('disabled', 'disabled');
			});		
          }
		  else
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO hợp lệ').addClass('messageboxok').fadeTo(900,1);
              $('#submit').attr('disabled', '');
			});
		  }
				
        });
 
	});
});

                    
            
</script>
<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" onsubmit="return checkfrminsert();">
            <fieldset class="fieldSet">
                <legend class="legen">TH&#202;M M&#7898;I</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Nick chat
                        </td>
                        <td class="tdRight">
                          <span id="textTitle">
                            <input type="text" id="txtnick" name="txtnick" value="{$obj_nick_edit->nick_chat[0]}" class="textField" />
                          <span class="textfieldRequiredMsg" id="error_title">(Không được rổng)</span></span>   
                        </td>
                    </tr>  
                     <tr>
                        <td class="tdLeft">
                            T&#234;n 
                        </td>
                        <td class="tdRight">
                          <span id="textTitle">
                            <input type="text" id="txtname" name="txtname" value="{$obj_nick_edit->name[0]}" class="textField" />
                          <span class="textfieldRequiredMsg" id="error_title">(Không được rổng)</span></span>   
                        </td>
                    </tr>                                                        
                    <tr>
                        <td class="tdLeft">
                            Lo&#7841;i
                        </td>
                        <td class="tdRight">
                        <select name="type" >
                        
                            <option value="0" {if $obj_nick_edit->type[0]==0 } selected="selected" {/if}>Yahoo</option>
                        
                            <option value="1"  {if $obj_nick_edit->type[0]==1 } selected="selected" {/if}>Skype</option>                       
                         
                       </select>                          
                        </td>
                    </tr>
                                  
                                 <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                        <input type="checkbox" name="active"  value="1"  {if $obj_nick_edit->status[0]==1 }  checked="checked" {/if}/>                       
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btAdd" value="L&#432;u" onclick="if(!checkFileSize()) return false; " />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
{literal}
<script type="text/javascript">
<!--
var errorSEO = new Spry.Widget.ValidationTextField("errorSEO");
var errorMetaKeyword = new Spry.Widget.ValidationTextarea("errorMetaKeyword");
var errorMetaDescription = new Spry.Widget.ValidationTextarea("errorMetaDescription");
var textTitle = new Spry.Widget.ValidationTextField("textTitle");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var txtShortDescription = new Spry.Widget.ValidationTextarea("txtShortDescription");
var errorLongDescription = new Spry.Widget.ValidationTextarea("errorLongDescription");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
//-->
</script>
{/literal}