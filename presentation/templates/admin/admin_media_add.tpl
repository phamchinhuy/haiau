{literal}
<script language="javascript" src="js/admin/check_media.js" type="text/javascript"></script>
<script language="javascript">
  //check size of images
   function checkFileSize()
    {
        var size;
    	if (navigator.userAgent.indexOf("Firefox") != -1){
    		smallSize=checkFileSizeFireFox('fileSmall');
            largeSize=checkFileSizeFireFox('fileLarge');
    	}else{
    		smallSize=checkFileSizeIE('fileSmall');
            largeSize=checkFileSizeIE('fileLarge');
    	}
        if(smallSize>2148576||largeSize>2148576)
        {
            alert("size of file too large");
            return false;
        }
        else return true;
    }

    //Check file size before upload, it is working well with Firefox
    //It is not compatible with another browse
    function checkFileSizeFireFox(file)
     {
    	var node = document.getElementById(file);
    	var size = node.files[0].fileSize;
        return size;
     }
    
    // It is working well with IE
    function checkFileSizeIE(file)
    {
    	var myFSO = new ActiveXObject("Scripting.FileSystemObject");
    	//var filepath = document.upload.file.value;
    	var filepath= document.getElementById(file).value;
    	var thefile = myFSO.getFile(filepath);
    	var size = thefile.size;
   	    return size;
    }
</script>
<script language="javascript">
//<!---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	$("#txtTitleSEO").blur(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
		//check the username exists or not from ajax
		$.post("include/admin/seo_availability.php",{ $txtSEOMediaAdd:$(this).val() } ,function(data)
        {
		  if(data=='no') //if username not avaiable
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO đã tồn tại').addClass('messageboxerror').fadeTo(900,1);
              $('#submit').attr('disabled', 'disabled');
			});		
          }
		  else
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('SEO hợp lệ').addClass('messageboxok').fadeTo(900,1);
              $('#submit').attr('disabled', '');

             
              	
			});
		  }
				
        });
 
	});
});

                    
               
</script>
<style type="text/css">

.messagebox{
	position:absolute;
	width:100px;
	margin-left:30px;
	border:1px solid #c93;
	background:#ffc;
	padding:3px;
}
.messageboxok{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #349534;
	background:#C9FFCA;
	padding:3px;
	font-weight:bold;
	color:#008000;
	
}
.messageboxerror{
	position:absolute;
	width:auto;
	margin-left:30px;
	border:1px solid #CC0000;
	background:#F7CBCA;
	padding:3px;
	font-weight:bold;
	color:#CC0000;
}

</style>
{/literal}


<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" onsubmit="return check_insert_media();" >
            <fieldset class="fieldSet">
                <legend class="legen">THÊM VIDEO M&#7898;I</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Tiêu đề
                        </td>
                        <td class="tdRight">
                            <span id="textTitle">
                            <input type="text" id="txtNews" name="txtNews" value="" class="textField" />
                            <span class="textfieldRequiredMsg" id="error_name">(Không được rổng)</span></span>
                          
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Tóm tắt
                        </td>
                        <td class="tdRight">
                        <span id="txtShortDescription">
                            <textarea name="txtShortDescription" id="txtShortDescription" class="textField" ></textarea>
                        <span class="textareaRequiredMsg" id="error_short_des"></span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Chi tiết
                        </td>
                        <td class="tdRight">
                            <span id="errorLongDescription">
                            {$ckeditor->editor('description_news')}
                             <!--<span class="textareaRequiredMsg">(Không được rổng)</span>-->
                             </span> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Hình nhỏ
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileSmall" id="fileSmall" />
                             <span class="textareaRequiredMsg" id="error_file_small">(Không được rổng)</span></span>
                             
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Hình lớn
                        </td>
                        <td class="tdRight">
                            <input type="file" name="fileLarge" id="fileLarge"  />
                             <span class="textareaRequiredMsg" id="error_file_large">(Không được rổng)</span></span>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrl" name="txtUrl" value="" class="textField" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Url video
                        </td>
                        <td class="tdRight">
                            <input type="text" id="txtUrlVideo" name="txtNewsVideo" value="" class="textField" />
                              <span class="textareaRequiredMsg" id="error_url_video">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                  <tr>
                        <td class="tdLeft">
                            Ngày bắt đầu
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginDate" />
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'beginDate'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ngày kết thúc
                        </td>
                        <td class="tdRight">
                        <input type="text" name="beginEnd" />
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'beginEnd'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Tinh có thể bình luận
                        </td>
                        <td class="tdRight">
                        <input type="checkbox" name="isComment"  id="isComment" />
                       </select>                          
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Active
                        </td>
                        <td class="tdRight">
                       <input type="checkbox" name="active" value="1" />                             
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ti&#234;u &#273;&#7873; cho SEO
                        </td>
                        <td class="tdRight">
                            <span id="errorSEO">
                            <input type="text" name="txtTitleSEO" id="txtTitleSEO" class="textField"  />
                            <span class="textfieldRequiredMsg" id="error_seo"></span></span>  
                            <span id="msgbox" style="display:none"></span>  
                        </td>
                    </tr>
                     <tr>
                        <td class="tdLeft">
                            M&#244; t&#7843; cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaDescription">
                            <textarea name="txtDescriptionMeta" id="txtDescriptionMeta" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="error_meta"> (Không được rổng)</span></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            T&#7917; kh&#243;a cho Meta
                        </td>
                        <td class="tdRight">
                            <span id="errorMetaKeyword">
                            <textarea name="txtKeywordMeta" id="txtKeywordMeta" class="textField" ></textarea>
                            <span class="textareaRequiredMsg" id="error_key">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                           &nbsp;
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btAdd" value="L&#432;u"  onclick="if(!checkFileSize()) return false; " />
                        </td>
                    </tr>
                    
                </table>
            </fieldset>
        </form>
    </div>
</div>
{literal}
<script type="text/javascript">
<!--
var errorSEO = new Spry.Widget.ValidationTextField("errorSEO");
var errorMetaKeyword = new Spry.Widget.ValidationTextarea("errorMetaKeyword");
var errorMetaDescription = new Spry.Widget.ValidationTextarea("errorMetaDescription");
var textTitle = new Spry.Widget.ValidationTextField("textTitle");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var txtShortDescription = new Spry.Widget.ValidationTextarea("txtShortDescription");
var errorLongDescription = new Spry.Widget.ValidationTextarea("errorLongDescription");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprycheckbox1 = new Spry.Widget.ValidationCheckbox("sprycheckbox1");
//-->
</script>
{/literal}