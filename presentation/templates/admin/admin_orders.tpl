{literal}
    <script language="javascript">
        function ClickToURL(linkVal)
        {
        	if (linkVal != 0)
            {
                document.location=linkVal;
            }
        }
    </script>
{/literal}

<div class="orders">
    <div class="orders_header">
        QU&#7842;N L&#221; &#272;&#416;N H&#192;NG
    </div>
    <div class="orders_ddlStatus">
        <table>
            <tr>
                <td>
                    Danh s&#225;ch &#273;&#417;n h&#224;ng
                    <select id="ddlStatus" name="ddlStatus" onchange="ClickToURL(this.value)">
                        {$strDDLStatus}
                    </select>
                </td>
                <td>
                    <form id="frmOrderSearch" method="post">
                        M&#227; &#273;&#417;n h&#224;ng
                        <input type="text" name="txtOrderCode" id="txtOrderCode" value="{$id_order_detail}" />
                        <input type="submit" value="T&#236;m" name="sbSearchCode" id="sbSearchCode" />
                    </form>
                </td>
            </tr>
        </table>
        
        
    </div>
    <div class="order_main">
        {include file="$tblOrders"}
    </div>
</div>