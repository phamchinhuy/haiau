
<div class="admin_image_form_add">
    <div class="admin_image_form_add_message">
        
    </div>
    <div class="admin_image_form_add_field">
        <form  enctype="multipart/form-data" id="frmAddImage" name="frmAddImage" method="post" >
            <fieldset class="fieldSet">
                <legend class="legen">THÊM FAQ</legend>
                <table cellpadding="2" cellspacing="2" border="0" class="tblAdd">
                    <tr>
                        <td class="tdLeft">
                            Họ tên 
                        </td>
                        <td class="tdRight">
                            <span id="errorFullname">
                            <input type="text" class="required"  id="txtFullname" name="txtFullname"  />
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span>  
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Địa chỉ
                        </td>
                        <td class="tdRight">
                            <span id="errorAddress">
                            <input type="text" id="txtAddress" name="txtAddress"  />
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Email
                        </td>
                        <td class="tdRight">
                            <span id="errorEmail">
                            <span id="errorEmailSyntax">
                            <input type="text" id="txtEmail" name="txtEmail"  class="textField" />
                            <span class="textfieldInvalidFormatMsg">(Không đúng định dạng email)</span></span> 
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span> 
                            
                        </td>
                    </tr>
                   
                    <tr>
                        <td class="tdLeft">
                            Điện thoại
                        </td>
                        <td class="tdRight">
                            <span id="errorPhoneNumber">
                            <span id="errorPhoneNumberSyntax">
                            <input type="text" id="txtPhone" name="txtPhone" class="textField" />
                            <span class="textfieldInvalidFormatMsg">(Không đúng định dạng phone)</span></span> 
                            <span class="textfieldRequiredMsg">(Không được rổng)</span></span> 
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Ngày tạo
                        </td>
                        <td class="tdRight">
                        <input type="text" name="txtDateCreatedQuestion" />
                        {literal}
                            <script language="JavaScript">
                            	new tcal ({
                            		// form name
                            		'formname': 'frmAddImage',
                            		// input name
                            		'controlname': 'txtDateCreatedQuestion'
                            	});
    
    	                    </script>
                         {/literal}
                                           
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Câu hỏi
                        </td>
                        <td class="tdRight">
                            
                            {$ckeditor->editor('txtQuestion')}
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                            Phản hồi
                        </td>
                        <td class="tdRight">
                            
                            {$ckeditor->editor('txtReply')}
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            Kích hoạt
                        </td>
                        <td class="tdRight">
                           <input type="checkbox" name="status" id="status"   />
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tdLeft">
                           
                        </td>
                        <td class="tdRight">
                            <input type="submit" id="submit" name="btAdd" value="L&#432;u" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
{literal}
<script type="text/javascript">
<!--
var errorFullname           = new Spry.Widget.ValidationTextField("errorFullname");
var errorAddress            = new Spry.Widget.ValidationTextField("errorAddress");
var errorEmailSyntax        = new Spry.Widget.ValidationTextField("errorEmailSyntax");
var errorEmail              = new Spry.Widget.ValidationTextField("errorEmail","email");
var errorPhoneNumberSyntax  = new Spry.Widget.ValidationTextField("errorPhoneNumberSyntax","integer");
var errorPhoneNumber        = new Spry.Widget.ValidationTextField("errorPhoneNumber");

//-->
</script>
{/literal}