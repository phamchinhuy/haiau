<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>{$headerTitle}</title>

<link rel="SHORTCUT ICON" href="http://haiau.edu.vn/icon.ico" type="image/x-icon" />

<meta name="robots" content="INDEX,FOLLOW" />

<meta name="description" content="{$description}"/>
<meta name="keywords" content="{$keywords}"/>


{literal}
<!--[if IE 6]>
    <link href="{/literal}{$server}{literal}/css/client/ie6.css" rel="stylesheet" type="text/css" />
    <script src="{/literal}{$server}{literal}/js/client/DD_belatedPNG_0.0.8a.js"></script>
    <script>
      /* EXAMPLE */
      DD_belatedPNG.fix('textarea,.vd_o_show_video,#header #hd_menu .hd_space_top .sp_t_hover1 #sp_t_trang_chu,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_moi_ngay_lam_gi,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_lop_hoc,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_dinh_duong_cho_be,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_nhap_hoc,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_thu_vien_anh,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_lien_he,#body_left .nen_duoi_left_anh_cua_be,#body_left .nen_duoi_left_anh_cua_be .l_acb_tieu_de,#body_left .nen_duoi_left_anh_cua_be .l_acb_tieu_de,#body_left .sinh_nhat_be .sn_picture,#body_left .sinh_nhat_be .sn_header,#body_left .sinh_nhat_be  .sn_footer,#body_left .sinh_nhat_be .sn_header1 .sn_hd_tieu_de,#body_left .sinh_nhat_be .sn_header1,.ft_menu,.ft_menu .ft_len_dau_trang1,.ft_menu .ft_trang_chu,#header #hd_menu .hd_nav ul li1,#header #hd_menu .hd_space_top .sp_t_hover #sp_t_video_online,#body_left  .l_acb_tieu_de,#body_left .nen_duoi_left_anh_cua_be .khung_anh_left1 ,#body_content .mnlg_mot_ngay_cua_be .mglg_mgcb_tieu_de .mglg_td_text,#body_content .hoat_dong_ngoai_khoa .hdnk_tieu_de .hdnk_td_icon,#body_content .hoat_dong_ngoai_khoa .hdnk_tieu_de .hdnk_td_icon,#body_content .mnlg_mot_ngay_cua_be .mglg_table .tieu_de_thoi_gian,#body_content .mnlg_mot_ngay_cua_be .mglg_table .mnlg_tieu_de_noi_dung,#body_content .hoat_dong_ngoai_khoa .hdnk_next_preview .hdnk_preview .preview_picture .buttonChimCutTruoc,#body_content .hoat_dong_ngoai_khoa .hdnk_next_preview .hdnk_next .next_picture .buttonChimCutSau,#body_content .lop_hoc .lh_tieu_de .lh_td_icon,#body_content .lop_hoc  .lh_table .lh_tb_tr,#body_content .danh_sach_lop_hoc .ds_tieu_de .ds_td_icon,#body_content .danh_sach_lop_hoc .ds_tieu_de .ds_td_text1 .ds_td_text,#body_content .danh_sach_lop_hoc .ds_noi_dung .ds_link_danh_sach .ds_link_icon,#body_content .dinh_duong_cho_tre .cac_che_do_an_uong_khac .cdddk_tieu_de .cdddk_td_icon,#body_content .dinh_duong_cho_tre .cac_che_do_an_uong_khac .cdddk_noi_dung .cdddk_nd_icon,#body_content .nhap_hoc .tieu_de_thong_tin_tuyen_sinh .td_ttts_icon,#body_content .nhap_hoc .tieu_de_thong_tin_tuyen_sinh .td_ttss_text,#body_content .nhap_hoc .muc_tuyen_sinh .mts_tieu_de .mts_icon,#body_content .thu_vien_anh .cac_che_do_an_uong_khac .cdddk_noi_dung .cdddk_nd_icon,#body_content .thu_vien_anh .cac_che_do_an_uong_khac .cdddk_hinh_anh_khac .cdddk_hak_icon,#body_content .thu_vien_anh .cac_che_do_an_uong_khac .phan_trang  .pt_phan_tung_trang a:hover,#body_content .thu_vien_anh .cac_che_do_an_uong_khac .phan_trang  .pt_phan_tung_trang .pt_ptt_active,#body_content .thu_vien_anh .cac_che_do_an_uong_khac .tva_noi_dung,.wrapper1 #body_content .thong_tin_lien_he .ttlh_tieu_de,.wrapper1 #body_content .thong_tin_lien_he .ttlh_noi_dung,.wrapper1 #body_content .form_lien_he .flh_td_text,.wrapper1 #body_content .form_lien_he .flh_noi_dung .flh_nd_label_input .flh_nd_input,.wrapper1 #body_content .form_lien_he .flh_noi_dung .flh_nd_label_input .flh_nd_input_maxn,.wrapper1 #body_content .form_lien_he .flh_noi_dung .flh_nd_textarea .flh_nd_input,.wrapper1 #body_content .form_lien_he .flh_button .flh_bt_xoa input,.wrapper1 #body_content .form_lien_he .flh_button .flh_bt_gui input,#body_left .nen_duoi_left_anh_cua_be .l_acb_noi_dung1 .l_acb_nd_picture .l_acb_nd_p_pic,img');                                                                                                                   
   
      /* string argument can be any CSS selector */
      /* .png_bg example is unnecessary */
      /* change it to what suits you! */
    </script>
    <![endif]--> 
 {/literal}   
<link href="{$server}/css/client/dinh_duong_cho_tre.css" rel="stylesheet" type="text/css" />
<link href="{$server}/css/client/trang_chu.css" rel="stylesheet" type="text/css" />
<link href="{$server}/css/client/client.css" rel="stylesheet" type="text/css" />
<link href="{$server}/css/client/moi_ngay_lam_gi.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{$server}/css/client/gallery/jquery.lightbox-0.5.css" type="text/css" media="screen" charset="utf-8" />

<script src="{$server}/js/client/jquery.js" type="text/javascript"></script>
<script src="{$server}/js/client/gallery/jquery.lightbox-0.5.js" type="text/javascript"></script>


{literal}
 <script type="text/javascript">
    $(function() 
    {
        $('#gallery a').lightBox();
    });
    </script>
   
{/literal}
{literal}
<script language="javascript">
	
	$(document).ready(function(){
	     
		{/literal}
        {if $status_active_trang_chu=="active"}
            {literal}
              $('#sp_t_trang_chu').show();
            {/literal}
        {else}
            {literal}
            $('#sp_t_trang_chu').hide();
            {/literal}
        {/if}
        
        {if $status_active_moi_ngay_lam_gi=="active"}
            {literal}
                $('#sp_t_moi_ngay_lam_gi').show();
            {/literal}
        {else}
            {literal}
                $('#sp_t_moi_ngay_lam_gi').hide();
            {/literal}
        {/if}
        
        {if $status_active_lop_hoc=="active"}
            {literal}
                $('#sp_t_lop_hoc').show();
            {/literal}
        {else}
            {literal}
                $('#sp_t_lop_hoc').hide();
            {/literal}
        {/if}
        
        {if $status_active_dinh_duong_cho_tre=="active"}
            {literal}
                $('#sp_t_dinh_duong_cho_be').show();
            {/literal}
        {else}
            {literal}
                $('#sp_t_dinh_duong_cho_be').hide();
            {/literal}
        {/if}
        
        {if $status_active_nhap_hoc=="active"}
            {literal}
                $('#sp_t_nhap_hoc').show();
            {/literal}
        {else}
            {literal}
                $('#sp_t_nhap_hoc').hide();
            {/literal}
        {/if}
        
        {if $status_active_thu_vien_anh=="active"}
            {literal}
                $('#sp_t_thu_vien_anh').show();
            {/literal}
        {else}
            {literal}
                $('#sp_t_thu_vien_anh').hide();
            {/literal}
        {/if}
        
        {if $status_active_video_online=="active"}
            {literal}
                	$('#sp_t_video_online').show();
            {/literal}
        {else}
            {literal}
                	$('#sp_t_video_online').hide();
            {/literal}
        {/if}
        
        {if $status_active_thong_tin_lien_he=="active"}
            {literal}
                	$('#sp_t_lien_he').show();
            {/literal}
        {else}
            {literal}
                	$('#sp_t_lien_he').hide();
            {/literal}
        {/if}
        
        {literal}
		
		$('.ft_len_dau_trang').click(function(){
		$('html,body').animate({scrollTop:0},500);
			});
	});
    
    function show_trang_chu()
    {
    	$('#sp_t_trang_chu').show();
       
    }
    function hide_trang_chu()
    {
    	$('#sp_t_trang_chu').hide();
    }
    
    
    function show_moi_ngay_lam_gi()
    {
    	$('#sp_t_moi_ngay_lam_gi').show();
    }
    function hide_moi_ngay_lam_gi()
    {
    	$('#sp_t_moi_ngay_lam_gi').hide();
    }
    function show_lop_hoc()
    {
    	$('#sp_t_lop_hoc').show();
    }
    function hide_lop_hoc()
    {
    	$('#sp_t_lop_hoc').hide();
    }
		
	function show_dinh_duong_cho_be()
	{
		$('#sp_t_dinh_duong_cho_be').show();
	}
    
	function hide_dinh_duong_cho_be()
	{
		$('#sp_t_dinh_duong_cho_be').hide();
	}
		
	function show_nhap_hoc()
	{
		$('#sp_t_nhap_hoc').show();
	}
	function hide_nhap_hoc()
	{
		$('#sp_t_nhap_hoc').hide();
	}
		
	function show_thu_vien_anh()
	{
		$('#sp_t_thu_vien_anh').show();
	}
	function hide_thu_vien_anh()
	{
		$('#sp_t_thu_vien_anh').hide();
	}
		
		
		
	function show_video_online()
		{
			$('#sp_t_video_online').show();
		}
	function hide_video_online()
		{
			$('#sp_t_video_online').hide();
		}
		
		
		
		function show_lien_he()
		{
			$('#sp_t_lien_he').show();
		}
	function hide_lien_he()
		{
			$('#sp_t_lien_he').hide();
		}
		
</script>
{/literal}
</head>
<body>

	<div id="wrapper">
    
		<div id="header">			
             {include file="client/header.tpl"}
        </div> <!--end header-->
        
        <div class="wrapper1">
        
	       <div id="body_main">
                
                <div id="body_left">
                    {include file="client/leftmenu.tpl"}
                </div>
                
                <div id="body_content">      
                    {include file="$body_main"}
                     
                </div>
                
                {if $page_index=="thong-tin-lien-he"}
                    <div id="ban_do">
                        <img src="{$server}/images/icon/so_do_duong_di.png"/>
                    </div> 
                {/if}
                    {include file="client/footer.tpl"}

                <div class="vide1"> </div>
    	   </div>
           
           
           
        </div> <!--end wrapper1-->
            
        <div id="footer">
            Copyright &copy; 2014 Tr&#432;&#7901;ng M&#7847;m Non Hải Âu
            <br />
            <a href="http://3nss.net" target="_blank" title="">&#272;&#432;&#7907;c thi&#7871;t k&#7871; v&#224; ph&#225;t tri&#7875;n b&#7903;i NewSunSoft</a>
        </div>
        
    </div> <!--end wrapper-->
</body>
</html>
