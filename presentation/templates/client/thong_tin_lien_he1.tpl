<link href="{$server}/css/client/thong_tin_lien_he.css" rel="stylesheet" type="text/css" />
<script src="{$server}/js/client/ajax_captcha.js" type="text/javascript"></script>

{literal}
   <script language="javascript">
        function sendMail()
        {
            
            var rs = true;
            var ten_phu_huynh = document.getElementById("ten_phu_huynh");
            var email = document.getElementById("email");
            var so_dien_thoai = document.getElementById("so_dien_thoai");
            var dia_chi = document.getElementById("dia_chi");
            var ma_xac_nhan = document.getElementById("ma_xac_nhan");
            var noi_dung = document.getElementById("noi_dung");
            var imgCaptcha = document.getElementById("imgCaptcha");
            
            if(ten_phu_huynh.value=="")
            {
               
                rs = false;
                document.getElementById("error_ten_phu_huynh").innerHTML = "(*)";
                ten_phu_huynh.focus();
            }
            else
                document.getElementById("error_ten_phu_huynh").innerHTML = "";
                
                
            if(email.value=="")
            {
                
                document.getElementById("error_email").innerHTML = "(*)";
                if(rs==true)
                    email.focus();
                rs = false;
            }
            else
                document.getElementById("error_email").innerHTML = "";
          
            if(ma_xac_nhan.value=="")
            {
                
                document.getElementById("error_ma_xac_nhan").innerHTML = "(*)";
                if(rs==true)
                    ma_xac_nhan.focus();
                rs = false;
            }
            else
            {
                
                document.getElementById("error_ma_xac_nhan").innerHTML = "";
            }
              
            if(ten_phu_huynh.value!="" && email.value!="" && ma_xac_nhan.value!="")
            {
                if(so_dien_thoai.value=="")
                    so_dien_thoai.value="0";
                    
                if(dia_chi.value=="")
                    dia_chi.value="0";                    
                     
                if(noi_dung.value=="")
                    noi_dung.value="0";
                    
            }
            
            if(rs==true)
            {
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Browser does not support HTTP Request");
                    return;
                } 

                var url="home/"+ten_phu_huynh.value                       
                        
                        +"/"+email.value
                        +"/"+dia_chi.value
                        +"/"+so_dien_thoai.value
                        +"/"+ma_xac_nhan.value
                        +"/"+noi_dung.value
                        +"/index.lienhe";
               
                document.getElementById("loading").style.display = "block";
                
                xmlhttp.onreadystatechange=function()
                {
                    
                    if (xmlhttp.readyState==4)
                    {
                       
                        if(xmlhttp.responseText!="")
                        {
                          
                            var message  = "";
                            message = xmlhttp.responseText;
                          
                            alert(message);
                            switch(message)
                            {
                                
                                case "e":
                                    document.getElementById("email").focus();
                                    document.getElementById("error_email").innerHTML = "(*)";
                                    break;
                                    
                                case "-c":
                                    document.getElementById("ma_xac_nhan").value = '';
                                    document.getElementById("ma_xac_nhan").focus();
                                    document.getElementById("error_ma_xac_nhan").innerHTML = "(*)";
                                    break;
                                    
                                case "e-c":
                                    document.getElementById("email").focus();
                                    document.getElementById("error_email").innerHTML = "(*)";
                                    document.getElementById("ma_xac_nhan").value = '';
                                    document.getElementById("error_ma_xac_nhan").innerHTML = "(*)";
                                    break;
                                    
                                case "1":
                                    document.getElementById('messages').innerHTML="G&#7917;i Th&#224;nh C&#244;ng";
                                    ten_phu_huynh.value='';
                                    email.value = '';
                                    dia_chi.value = '';
                                    so_dien_thoai.value = '';
                                    ma_xac_nhan.value = '';
                                    noi_dung.value= '';   
                                    
                                
                            }
    
                            document.getElementById("loading").style.display = "none";

                             
                        }
                       
                    }
                   
                } 
                
                xmlhttp.open("GET",url,true);
                xmlhttp.send(null);
            }
            
        }
       
       
           
        function GetXmlHttpObject()
        {
            var objXMLHttp=null;
            if (window.XMLHttpRequest)
            {
                objXMLHttp=new XMLHttpRequest();
            }
            else if (window.ActiveXObject)
            {
                objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            return objXMLHttp;
        } 
        
        function GetXmlHttpObjectEmail()
        {
            var objXMLHttp=null;
            if (window.XMLHttpRequest)
            {
                objXMLHttp=new XMLHttpRequest();
            }
            else if (window.ActiveXObject)
            {
                objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            return objXMLHttp;
        }
    </script>
{/literal}


<div class="thong_tin_lien_he">
		<div class="ttlh_tieu_de">
		</div>
		<div class="ttlh_noi_dung">
			<div class="ttlh_nd_text1">
				<div class="ttlh_nd_text">
						<p>Tr&#432;&#7901;ng M&#7847;m Non Chim C&#225;nh C&#7909;t</p>
                        <p>&#272;&#7883;a Ch&#7881; : 7/280 Qu&#7889;c l&#7897; 13 Khu ph&#7889; 5 <br /> Ph&#432;&#7901;ng Hi&#7879;p B&#236;nh Ph&#432;&#7899;c Qu&#7853;n Th&#7911; &#272;&#7913;c TP HCM</p>
                        <p>&#272;i&#7879;n tho&#7841;i : 08 2242 5757 - 0989 072 062 <br /> Fax: 08 5422 5839</p>
                        <p>Email : info@chimcanhcut.edu.vn</p>
				</div>
			</div>
		</div>
</div>

<div class="form_lien_he">
	<div class="flh_tieu_de">
		<div class="flh_td_icon"><img src="{$server}/images/icon/icon.png" /></div>
		<div class="flh_td_text"></div>
	</div>
	<div class="flh_noi_dung" >
	 <form method="post" name="frmLienHe" id="frmLienHe" >
            <div class="flh_nd_label_input">
               <div class="thong_bao_gui"><label id="messages"></label></div> 
            </div>
	  		<div class="flh_nd_label_input">
                
				<div class="flh_nd_label">
                    <label>T&#234;n ph&#7909; huynh</label>
                    <label class="error" id="error_ten_phu_huynh"></label>
                </div>
                
                <div class="flh_nd_input1">
                    <div class="flh_nd_input">
                        <input type="text" value="" name="ten_phu_huynh" id="ten_phu_huynh" />
                    </div> 	
                </div>			
		   </div>
           
			<div class="flh_nd_label_input">
				<div class="flh_nd_label">
                    <label>Email</label>
                    <label class="error" id="error_email"></label>
                </div>
				<div class="flh_nd_input1">
                    <div class="flh_nd_input">
                        <input type="text" value="" name="email" id="email" />
                    </div>
                </div>
			</div>
            
			<div class="flh_nd_label_input">
				<div class="flh_nd_label">&#272;i&#7879;n tho&#7841;i</div>
				<div class="flh_nd_input1"><div class="flh_nd_input"><input type="text" value="" name="so_dien_thoai" id="so_dien_thoai" /></div></div>
			</div>
            
			<div class="flh_nd_label_input">
				<div class="flh_nd_label">&#272;&#7883;a ch&#7881;</div>
				<div class="flh_nd_input1">
                    <div class="flh_nd_input">
                        <input type="text" value="" name="dia_chi" id="dia_chi" />
                    </div>
                </div>
			</div>
          
			<div class="flh_nd_label_input">
				<div class="flh_nd_label">
                    <label>M&#227; x&#225;c nh&#7853;n</label>
                    <label class="error" id="error_ma_xac_nhan"></label>
                </div>
                <div class="flh_nd_input1">
                    <div class="flh_nd_input_maxn">
                        <input type="text" value="" name="ma_xac_nhan" id="ma_xac_nhan" />
                    </div>
                    <div class="flh_nd_capcha1">
                        <img id="imgCaptcha" src="{$server}/include/client/create_image.php"  />
                    </div>
                </div>
				<div class="refresh_capcha"></div>
			</div>
         
			<div class="flh_nd_textarea">
				<div class="flh_nd_label">N&#7897;i dung</div>
				<div class="flh_nd_input">
				  <textarea name="noi_dung" id="noi_dung"></textarea>
				</div>
			</div>
			<div class="flh_button">
				<div class="flh_bt_xoa"><input type="reset" class="reset" value="" name="bt_xoa" /></div>
				<div class="flh_bt_gui">
                    <input type="button" value="" name="bt_gui" id="bt_gui"   onclick="sendMail();"/>
                    <input id="btnCaptcha" type="hidden" value="Captcha Test" 
                            name="btnCaptcha" onclick="getParam(document.frmLienHe)" />
                 </div>
			      
            </div>
		</form>
	</div>
</div>

<div class="loading" style="display: none;" id="loading">
    loading....
</div>
