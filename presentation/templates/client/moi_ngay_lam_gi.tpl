<link href="{$server}/css/client/moi_ngay_lam_gi.css" rel="stylesheet" type="text/css" />
<script src="{$server}/js/client/jsHinhAnhHoatDong.js"></script>
<div class="mnlg_mot_ngay_cua_be">
	<div class="mglg_mgcb_tieu_de">
		<div class="mglg_td_icon"><img src="{$server}/images/icon/moi-ngay-lam-gi/icon.png"/></div>
		<div class="mglg_td_text"></div>
	</div>
	<div class="mglg_table">
		<div class="tieu_de_thoi_gian">Th&#7901;i gian</div>
		<div class="mnlg_tieu_de_noi_dung">Ho&#7841;t &#273;&#7897;ng</div>
		<!-- -->
        {section name=i loop=$cs_1_ngay_cua_be->id}                
        <div class="mnlg_tr">
			<div class="mnlg_td_left">
                {$cs_1_ngay_cua_be->news[i]}	
            </div>
			<div class="mnlg_td_right">
			    {$cs_1_ngay_cua_be->short_description[i]}
            </div>
		</div>
        {/section}
		<!-- -->
	</div>
</div>

<div class="hoat_dong_ngoai_khoa">

	<div class="hdnk_tieu_de">
		<div class="hdnk_td_icon"></div>
		<div class="hdnk_td_text">
			<div class="hdnk_td_t_image"><img src="{$server}/images/icon/moi-ngay-lam-gi/anhhoatdongcuabe.png" />					</div>
		</div>
	</div>
    
	<div class="hdnk_hinh_anh" id="divHinhAnhHoatDong">
        
        {if count($obj_news_2hinh->id)==1}
        <div class="hdnk_ha_left_picture">
			<div class="hdnk_picture">
				<img src="{$server}/uploads/news/{$obj_news_2hinh->small_image[0]}" height="195px" width="275" />					</div>
			<div class="hdnk_ha_text_chu_thich">{$obj_news_2hinh->news[0]}					</div>
		</div>
        {/if}
        
        {if count($obj_news_2hinh->id)>1}
        <div class="hdnk_ha_left_picture">
			<div class="hdnk_picture">
				<img src="{$server}/uploads/news/{$obj_news_2hinh->small_image[0]}" height="195px" width="275" />					</div>
			<div class="hdnk_ha_text_chu_thich">{$obj_news_2hinh->news[0]}					</div>
		</div>
		<div class="hdnk_ha_left_picture">
			<div class="hdnk_picture">
				<img src="{$server}/uploads/news/{$obj_news_2hinh->small_image[1]}" height="195px" width="275" />					</div>
			<div class="hdnk_ha_text_chu_thich">{$obj_news_2hinh->news[1]}					</div>
		</div>
        {/if}       
               
	</div>
    
    {if count($obj_news_2hinh->id)>2}
	<div class="hdnk_next_preview" id="divLinkPageHinhAnhHoatDong">
		<div class="hdnk_preview">
			<div class="preview_picture">
				<input type="button" id="btPre" class="buttonChimCutTruoc"  value="" name="btPre" onclick="fnLoadImage('anh-hoat-dong-cua-be',0,'2','divHinhAnhHoatDong','divLinkPageHinhAnhHoatDong','{$iCountImage}');"/>
                    	
            </div>
		</div>
		<div class="hdnk_next">
			<div class="next_picture">
                <input type="button" id="btNext" class="buttonChimCutSau" value="" name="btNext" onclick="fnLoadImage('anh-hoat-dong-cua-be',2,'2','divHinhAnhHoatDong','divLinkPageHinhAnhHoatDong','{$iCountImage}');" /> 
		        				
            </div>
		</div>
	</div>
    {/if}
    
	<div class="space"></div>
</div>
	 