Event.observe(window, 'load', function() {
	var locSearch=document.location.search;
	var shingleNo=0;
	var activeDrawer=null;
	if(locSearch.length>1){
		var aN_V=locSearch.substr(1).split('&');
		for(var k=0; k<aN_V.length; k++){
			if(aN_V[k].split('=')[0]=='shingleNo'){shingleNo=parseInt(aN_V[k].split('=')[1]); break;}
		}
	}
	var arrShinglesHolders=document.getElementsByClassName('ShinglesMenu');
	for(var n=0; n<arrShinglesHolders.length; n++){
		var ShinglesHolderX = null;
		var ShinglesHolder=arrShinglesHolders[n];
		var UL_ID=n+'-shingles';
		ShinglesHolder.getElementsByTagName('UL')[0].id=UL_ID;
		var HeaderHeight=0;
		var HandleHeight=72;
		var PicHeight=parseInt(ShinglesHolder.getElementsByTagName('LI')[0].getElementsByTagName('IMG')[0].height);
		ShinglesHolderX = new AC.ShingleBureau(UL_ID);
		var challenges = $(UL_ID).getElementsByTagName('li');
		var ShinglesCount=challenges.length;
		var ShinglesHeight=HandleHeight*ShinglesCount+PicHeight;
		ShinglesHolder.style.height=HeaderHeight+ShinglesHeight+'px';
		ShinglesHolder.getElementsByTagName('UL')[0].style.height=ShinglesHeight+'px';
		for (var i = 0; i < challenges.length; i++) {
			var arrA=challenges[i].getElementsByTagName('a');
			for(var j=0; j<arrA.length; j++){
				var sHref=arrA[j].href;
				arrA[j].href=(sHref.indexOf('?')==-1?sHref+'?shingleNo='+i:sHref+'&shingleNo='+i)
			}
			challenges[i].style.position='absolute';
			var handle = Element.getElementsByClassName(challenges[i], 'handle')[0];
			var drawer = new AC.ShingleDrawer(challenges[i], handle, ShinglesHolderX, {
				triggerEvent: 'mouseover',
				triggerDelay: 50});
			ShinglesHolderX.addDrawer(drawer);
			if(shingleNo==i) activeDrawer=drawer;
		}
		if(activeDrawer!=null) {activeDrawer.open('true');}
	}
}, false);
