
function fnLoadImage(pCategory, pStart, pEnd, pdivContent, pdivLink)
{
    xmlhttp=GetXmlHttpObject();
    if (xmlhttp==null)
    {
        alert ("Browser does not support HTTP Request");
        return;
    } 
    
    var url= "hinh-anh-hoat-dong-click"+ "/" + pCategory + "/"+ pStart + "/" + pEnd + "/index.hinhanhhoatdong";
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {
            if(xmlhttp.responseText!="")
            {
                document.getElementById(pdivContent).innerHTML=xmlhttp.responseText;
            }
        }
    } 
    
    var start1 = 0;
    if(pStart>pEnd-1)
        start1 = pStart - 2;
    var start2 = pStart + 2;
    
    var strLink = "";
    strLink += "<div class=\"hdnk_preview\"><div class=\"preview_picture\"><input type=\"button\" class=\"buttonChimCutTruoc\"  id=\"btPre\" value=\"\" name=\"btPre\" onclick=\"fnLoadImage('"+pCategory+"',"+ start1 +",'2','divHinhAnhHoatDong','divLinkPageHinhAnhHoatDong');\"/></div></div>";
    strLink += "<div class=\"hdnk_next\"><div class=\"next_picture\"><input type=\"button\" class=\"buttonChimCutSau\" id=\"btNext\" value=\"\" name=\"btNext\" onclick=\"fnLoadImage('"+pCategory+"'," + start2 + ",'2','divHinhAnhHoatDong','divLinkPageHinhAnhHoatDong');\" /></div></div>";
    document.getElementById(pdivLink).innerHTML = strLink;
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function GetXmlHttpObject()
{
    var objXMLHttp=null;
    if (window.XMLHttpRequest)
    {
        objXMLHttp=new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    return objXMLHttp;
} 