function checkfrminsert()
{
    var frm=document.getElementById("frmAddImage");
    var title=document.getElementById("txtNews").value;
    var shortdes=document.getElementById("txtShortDescription").value;
    var small_image=document.getElementById("fileSmall").value;
    var large_image=document.getElementById("fileLarge").value;
    var seo=document.getElementById("txtTitleSEO").value;
   // var meta=document.getElementById("txtDescriptionMeta").value;
    var keyword=document.getElementById("txtKeywordMeta").value;
    //handle error
  
      if(title=="")
      {
           
        document.getElementById("error_title").innerHTML="Nh&#7853;p ti&#234;u &#273;&#7873;";
        return false;
      }else{
         document.getElementById("error_title").innerHTML="";
      }
  
  
      if(shortdes=="")
      {       
        document.getElementById("error_short_des").innerHTML="Nh&#7853;p t&#243;m t&#7855;t";
        return false;
      }
      else{
         document.getElementById("error_short_des").innerHTML="";
      }
      if(small_image!="")
      {
           if(!checkfile(small_image))
           {
             document.getElementById("error_small_image").innerHTML="Nh&#7853;p v&#224;o h&#236;nh &#7843;nh";
            return false;
           }
           else
            document.getElementById("error_small_image").innerHTML=" ";
      }
      
      if(large_image!="")
      {
             if(!checkfile(large_image))
             {
                 document.getElementById("error_large_image").innerHTML="Nh&#7853;p v&#224;o h&#236;nh &#7843;nh";
                 return false;
             }
             else
             document.getElementById("error_large_image").innerHTML="";
      }
    
      if(seo=="")
      {
          document.getElementById("error_seo").innerHTML="Nh&#7853;p v&#224;o SEO";
            return false;
      }
      else
      {
            if(CheckUnicodeChar(seo)!=1)
            {
                document.getElementById("error_seo").innerHTML = "Page title co d?ng format(aa-bb)";
                return false;
            }
            else
            {
                document.getElementById("error_seo").innerHTML="";
            }
         
      }
    
      if(keyword.length==0)
      {
            document.getElementById("err_key").innerHTML="Nh&#7853;p v&#224;o keyword";
         return false;
      }
      else{
           document.getElementById("err_key").innerHTML="";
      }
      
     return true;
}

 function checkfile(strfile)
    {  
        var ext = strfile.substring(strfile.lastIndexOf('.') + 1); 
        if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png"||ext=="PNG")
        {          
             return true;
        }
        else
        {
            return false;
        }
    }