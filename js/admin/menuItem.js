<!--

/*
Configure menu styles below
NOTE: To edit the link colors, go to the STYLE tags and edit the ssm2Items colors
*/
YOffset=150; // no quotes!!
XOffset=0;
staticYOffset=30; // no quotes!!
slideSpeed=20 // no quotes!!
waitTime=100; // no quotes!! this sets the time the menu stays out for after the mouse goes off it.
menuBGColor="#fe4819";
menuIsStatic="yes"; //this sets whether menu should stay static on the screen
menuWidth=200; // Must be a multiple of 10! no quotes!!
menuCols=2;
hdrFontFamily="verdana";
hdrFontSize="2";
hdrFontColor="white";
hdrBGColor="#fe4819";
hdrAlign="left";
hdrVAlign="center";
hdrHeight="15";
linkFontFamily="Verdana";
linkFontSize="2";
linkBGColor="white";
linkOverBGColor="#ffffff";
linkTarget="_top";
linkAlign="Left";
barBGColor="#fe4819";
barFontFamily="Verdana";
barFontSize="2";
barFontColor="white";
barVAlign="center";
barWidth=30; // no quotes!!
barText="MENU"; // <IMG> tag supported. Put exact html for an image to show.

///////////////////////////

// ssmItems[...]=[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
ssmItems[0]=["Orders"] //create header
ssmItems[1]=["&nbsp;List orders", "admin.php?func=orders", ""]

ssmItems[2]=["Catalog", "", ""] //create header
ssmItems[3]=["&nbsp;Categories", "admin.php?func=categories.manage", ""]
ssmItems[4]=["&nbsp;Products", "admin.php?func=products.manage", ""]
ssmItems[5]=["&nbsp;Supplier", "admin.php?func=products.supplier", ""]


ssmItems[6]=["Users", "", ""] //create header
ssmItems[7]=["&nbsp;Users", "admin.php?func=user&type=U", ""]
ssmItems[8]=["&nbsp;Administrator", "admin.php?func=user&type=A&id=1&link=reset", ""]
ssmItems[9]=["&nbsp;Customer", "admin.php?func=user&type=C", ""]
ssmItems[10]=["", "#", ""]

ssmItems[11]=["Administration", "", ""] //create header
ssmItems[12]=["&nbsp;Payment methods", "admin.php?func=payment", ""]
ssmItems[13]=["&nbsp;Credit cards", "admin.php?func=cards", ""]
ssmItems[14]=["&nbsp;Currencies", "admin.php?func=currencies", ""]
ssmItems[15]=["", "admin.php?func=title", ""]

ssmItems[16]=["Content", "", ""] //create header
ssmItems[17]=["&nbsp;Site News", "admin.php?func=news&type=n", ""]
ssmItems[18]=["&nbsp;Service-Dịch vụ", "admin.php?func=news&type=s", ""]
ssmItems[19]=["&nbsp;FAQ", "admin.php?func=news&type=f", ""]
ssmItems[20]=["&nbsp;Objects need help", "admin.php?func=news&type=o", ""]

ssmItems[21]=["Pages", "", ""] //create header
ssmItems[22]=["&nbsp;About our company", "admin.php?func=about&id=0", ""]
ssmItems[23]=["&nbsp;Contact us", "admin.php?func=contact", ""]
ssmItems[24]=["&nbsp;Supplier", "admin.php?func=contact&type=su", ""]
ssmItems[25]=["&nbsp;Payment", "admin.php?func=about&id=4", ""]

ssmItems[26]=["Shipping/Country", "", ""] //create header
ssmItems[27]=["&nbsp;Shipping methods", "admin.php?func=shipping", ""]
ssmItems[28]=["&nbsp;District", "admin.php?func=district", ""]
ssmItems[29]=["&nbsp;States", "admin.php?func=states", ""]
ssmItems[30]=["&nbsp;Countries", "admin.php?func=countries", ""]

ssmItems[31]=["Agent", "", ""] //create header
ssmItems[32]=["&nbsp;View Agent", "admin.php?func=agent", ""]

ssmItems[33]=["Music", "", ""] //create header
ssmItems[34]=["&nbsp;Background music", "admin.php?func=music", ""]
ssmItems[35]=["Gift", "", ""] //create header
ssmItems[36]=["&nbsp;Gift", "admin.php?func=gift", ""]
/*
ssmItems[30]=["Advertising", "", ""] //create header
ssmItems[31]=["&nbsp;Advertising menu", "admin.php?func=advertising", ""]
*/
buildMenu();

//-->