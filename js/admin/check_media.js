function check_insert_media()
{
    var media=document.getElementById("txtNews");
    var short_des=document.getElementById("txtShortDescription");
    var fileSmall=document.getElementById("fileSmall").value;
    var fileLarge=document.getElementById("fileLarge").value;
    var urlvideo=document.getElementById("txtUrlVideo");
    var seo =document.getElementById("txtTitleSEO");
    if(media.value=="")
    {
         media.focus();
         document.getElementById("error_name").innerHTML  ="T&#234;n video";
         return false;
    }else
    {
         document.getElementById("error_name").innerHTML  ="";
    }
    if(short_des.value="")
    {
         short_des.focus();
         document.getElementById("error_short_des").innerHTML ="T&#243;m t&#7855;t";
         return false;
    }
    else{
         document.getElementById("error_short_des").innerHTML ="";
    }
    if(fileSmall=="")
    {
         document.getElementById("error_file_small").innerHTML ="Nh&#7853;p h&#236;nh &#7843;nh";
        return false;
    }else{
          if(!checkfile(fileSmall))
          {
             document.getElementById("error_file_small").innerHTML ="Nh&#7853;p h&#236;nh &#7843;nh";
            return false;
          }
          else{
             document.getElementById("error_file_small").innerHTML ="";
          }
    }
    if(fileLarge=="")
    {
         document.getElementById("error_file_large").innerHTML ="Nh&#7853;p h&#236;nh &#7843;nh";
        return false;
    }else{
          if(!checkfile(fileLarge))
          {
             document.getElementById("error_file_large").innerHTML ="Nh&#7853;p h&#236;nh &#7843;nh";
            return false;
          }
          else{
             document.getElementById("error_file_large").innerHTML ="";
          }
    }
    if(urlvideo.value=="")
    {
    urlvideo.focus();
    document.getElementById("error_url_video").innerHTML ="Nh&#7853;p t&#234;n file video";
    return false;
    }
    if(seo.value=="")
    {
        seo.focus();
        document.getElementById("error_seo").innerHTML ="Nh&#7853;p SEO";
        return false;
    }else
    {
        document.getElementById("error_seo").innerHTML ="";
    }
    
    return true;
}



 function checkfile(strfile)
{  
    var ext = strfile.substring(strfile.lastIndexOf('.') + 1); 
    if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png"||ext=="PNG")
    {          
         return true;
    }
    else
    {
        return false;
    }
}
