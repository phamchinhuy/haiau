/*
 * 
 * Type:	function
 * Name:	js common
 * Date:	30/6/2010
 * @author:	haithe1988 
 */
function trim(val)
{ 
	return val.replace(/^\s+|\s+$/g,"");
}

function RefreshCity()
{			
	var f 			= document.frmRegisterForm;		
	var listCountry = f.slcountry;
	var listCity 	= f.slcity;		
	
	var countryID 	= listCountry.options[listCountry.selectedIndex].value;	
	var arrChildren;
	var arrTemp	;
	var i;
	var x = 1;
	listCity.length = 0 ;				
	var strChildCityList = f.city.value;
	arrChildren = strChildCityList.split("|");
	listCity.options[0] = new Option('[Lựa chọn]', 0);	
	for (i = 0; i < arrChildren.length; i++)
	{
		arrTemp = arrChildren[i].split("~");
		if (countryID == arrTemp[0]) 
		{
			listCity.options[x] = new Option(arrTemp[2], arrTemp[1]);
			x++
		}			
	}
}

//cho phep nhung ky tu tu A->Z, a->z, 0->9,va cac ky tu _-
function CheckUsernameChar(stringIn)
{
	retval = false
     var i;
     for ( i=0; i <= stringIn.length-1; i++) 
	 {
         if ( ( ( stringIn.charCodeAt(i) >= 48)&&(stringIn.charCodeAt(i) <= 57)) || ((stringIn.charCodeAt(i) > 64)&&(stringIn.charCodeAt(i) <= 90)) || ((stringIn.charCodeAt(i) >= 97)&&(stringIn.charCodeAt(i) <= 122)) || (stringIn.charCodeAt(i) == 95) || (stringIn.charCodeAt(i) == 45) )
		 {
           	retval = true;
         }
		 else
		 {
			retval = false;
			break;
         }
     }
     return retval;
}

// kiem tra xem user nhap vao username co tren 6 ky tu va duoi 20 ky tu khong? de hien thi button check len
function CheckStatus()
{
	var frm 	= document.frmRegisterForm;
	var strUsername = frm.username.value;
	
	if ( strUsername != '' && ( strUsername.length >= 6 && strUsername.length <= 20 ) )
	{
		document.getElementById('btCheck').disabled	= false;
		bolUsernameChecked = false;
		return true;
	}
	else
	{
		document.getElementById('btCheck').disabled	= true;
		return false;
	}

}

//kiem tra unicode cho password Checkchar1
function CheckCharPassword(stringIn)
{
	retval = false
     var i;
     for (i=0;i<=stringIn.length-1;i++) 
	 {
         if (((stringIn.charCodeAt(i) >= 8)&&(stringIn.charCodeAt(i) <= 127)) && (stringIn.charCodeAt(i)!=34) && (stringIn.charCodeAt(i)!=39) && (stringIn.charCodeAt(i)!=32))
		 {
           	retval = true;
         }
		 else
		 {
			retval = false;
			break;
         }
     }
     return retval;
}

//kiem tra cac ky tu dat biet @,<,>,!,$,%,(,),=,#,{,},[,],",^,~,`,,/,\,|,*,.,+,: cho fullname CheckChar
function CheckCharFullName(stringIn) 
{
	if ((stringIn.indexOf("@") >= 0)||(stringIn.indexOf("<") >= 0)||(stringIn.indexOf(">") >= 0)||(stringIn.indexOf("!") >= 0)||(stringIn.indexOf("$") >= 0)||(stringIn.indexOf("%") >= 0)||(stringIn.indexOf("(") >= 0)||(stringIn.indexOf(")") >= 0)||(stringIn.indexOf("=") >= 0)||(stringIn.indexOf("#") >= 0)||(stringIn.indexOf("{") >= 0)||(stringIn.indexOf("}") >= 0)||(stringIn.indexOf("[") >= 0)||(stringIn.indexOf("]") >= 0)||(stringIn.indexOf("|") >= 0)||(stringIn.indexOf('"') >= 0) ||(stringIn.indexOf(".") >= 0) ||(stringIn.indexOf(";") >= 0) ||(stringIn.indexOf("?") >= 0) ||(stringIn.indexOf(",") >= 0) ||(stringIn.indexOf("+") >= 0) ||(stringIn.indexOf("&") >= 0) ||(stringIn.indexOf(":") >= 0) ||(stringIn.indexOf("\\") >= 0) ||(stringIn.indexOf("/") >= 0) ||(stringIn.indexOf("*") >= 0) ||(stringIn.indexOf("`") >= 0) ||(stringIn.indexOf("~") >= 0) ||(stringIn.indexOf("^") >= 0) ||(stringIn.indexOf("-") >= 0)||(stringIn.indexOf("_") >= 0))
	{
		return false;
	}
	return true;
}

// kiem tra co dung la dia chi email.
function CheckEmail(stringIn)
{
	var re = /^([A-Za-z0-9\_\-]+\.)*[A-Za-z0-9\_\-]+@[A-Za-z0-9\_\-]+(\.[A-Za-z0-9\_\-]+)+$/;
	if ( stringIn.search(re) == -1 )
	{
		return false;
	}
	else
	{
		return true;
	}
}

// kiem tra so dien thoai
function CheckTel(str)
{
	var pattern = "0123456789-+() ";
	if (str.length > 0) {
		if (str.length < 5) {
			return false;
		} else {
			for (var a=0; a<pattern.length; a++) {
				if (pattern.indexOf(str.charAt(a),0) == -1) return false;
			}
		}
	}
	return true;	
}
//
// kiem tra tien
function CheckPrice(str)
{
if(!is_numeric(str))
    {
       return false;  
    }
	return true;	
}
function Redirect(strUrl)
{
	window.location = strUrl;
}