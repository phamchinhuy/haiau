<?php

session_start(); 
ob_start();

require_once 'include/app_top.php';
require_once 'include/global.php';
require_once 'include/lib.db.common.inc.php';
require_once 'include/lib.util.common.inc.php';
require_once 'include/class/cs_account.php';
require_once 'include/class/cs_date.php';
require_once 'include/db.php';
include_once("ckeditor/ckeditor.php");

$page = new Page();
$main_user ="";
$main_content_template = "admin/admin_home.tpl";
$headerTitle = "";
$main_category_tree="admin/default.tpl";
$admin_subMenu = "admin/default.tpl";
$server = STR_SERVER;
$page->assign("server",$server);
$func = "";
//ckeditor
$ckeditor = new CKEditor();
$ckeditor->basePath = '/ckeditor/';
$ckeditor->config['filebrowserBrowseUrl'] = 'ckfinder/ckfinder.html';
$ckeditor->config['filebrowserImageBrowseUrl'] = 'ckfinder/ckfinder.html?type=Images';
$ckeditor->config['filebrowserFlashBrowseUrl'] = 'ckfinder/ckfinder.html?type=Flash';
$ckeditor->config['filebrowserUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
$ckeditor->config['filebrowserImageUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
$ckeditor->config['filebrowserFlashUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
$page->assign("ckeditor",$ckeditor);
//$seo=new Seo();
//echo $seo->get_beauty_url_string("h?i h?i th?");
$_SESSION["username_admin"]=isset($_SESSION["username_admin"])?$_SESSION["username_admin"]:"";
if(isset($_GET['view'])&&$_GET['view']=='logout'){
	include('user_logout.php');
}

if(isset($_GET['view'])&&$_GET['view']=='forgotpass')
{
    include('forgotpassword.php');
}
//include_once("include/admin/admin_check_online.php");
$blogin  = true;
$typeAdmin = "";
$_SESSION['views'] = "aa";

if(isset($_SESSION['id_account_admin']))
{
    $blogin == true;
    $typeAdmin =  isset($_SESSION['user_type_admin'])? $_SESSION['user_type_admin']:"";
}
else
{
    $blogin = false;
    $main_user  = "admin/empty.tpl"; 
    $main_content_template = "admin/admin_login.tpl";
}

if(isset($_POST["login"]))
{
    $admin_subMenu = "admin/admin_submenu.tpl";
    $mess="";
    $messp=" ";
    $txtUsername="";
    $txtPasword="";
    $txtUsername = isset($_POST["user_username"])?$_POST["user_username"]:"";
    $txtPasword = isset($_POST["user_password"])?$_POST["user_password"]:"";
    $txtUsername = trim($txtUsername);
    $txtPasword = trim($txtPasword);
    $txtPasword = md5($txtPasword);
   
    if($txtUsername!="" && $txtPasword!="")
    {
         
        $flag=0;
        $cs_account = new cs_account();
        $cs_account->select_user_by_username_password($txtUsername,$txtPasword);
        $flag=count($cs_account->accountId);
        if($flag>0)
        {
            $user_type = "";
            $username = "";
            $id_account = "";
            $status="";
            
            $user_type = $cs_account->type[0];
            $username = $cs_account->username[0];
            $id_account = $cs_account->accountId[0];
            $status=$cs_account->status[0];
        
            $_SESSION["id_account_admin"]=$id_account;
            $_SESSION["username_admin"]=$username;
            $_SESSION["user_type_admin"]=$user_type;  
     
     
            $str= navigateLink("admin.php?3nss=news");
            echo $str;
        }
    }
    
}
$admin_account = "admin/empty.tpl";
if($blogin==true)
{
    $admin_account = "admin/admin_account.tpl";
    
    //TYPE USER
    if($blogin==true&&$typeAdmin=="user")
    {
        $admin_subMenu = "admin/admin_submenu.tpl";
        $main_category_tree="admin/main_category_tree.tpl";
        include("include/admin/main_category_tree.php");   
        if (isset($_GET["3nss"]) || isset($_POST["3nss"]))
        {
            
            $func= isset($_GET["3nss"]) ? $_GET["3nss"] : $_POST["3nss"];
            $b = false;
            switch($func)
            {
                case "news":
                    include("include/admin/admin_news.php");
                    $b = true;
                    break;
                case "online":
                    include("include/admin/admin_online.php");
                    $b = true;
                    break;    
                case "image":
                    include("include/admin/admin_image.php");
                    $b = true;
                    break;
                case "media":
                    include("include/admin/admin_media.php");
                    $b = true;
                    break;
                case "clientcontact":
                    include("include/admin/admin_contact.php");
                    $b = false;
                    break;    
                case "product":
                    include("include/admin/admin_product.php");
                    $b = false;
                    break;
                case "faq":
                    include("include/admin/admin_faq.php");
                    $b = true;
                    break;
                case "change.pass":
                    include("include/admin/admin_changePass.php");
                    $b = false;
                    break;
                case "orders":
                    include_once("include/admin/admin_orders.php");
                    $b = false;
				    break; 
                case "member":
                    include_once("include/admin/admin_members.php");
                    $b = false;
				    break; 
                 case "website":
                    include_once("include/admin/admin_website.php");
                    $b = true;
				    break; 
                 case "currency":
                    include("include/admin/admin_currency.php");
                    $b = false;
                    break;        
                default:
                    include("include/admin/admin_news.php");
                    $b = true;
                    break;
            }
        }
       
        if($blogin==true)
        {
            $main_user = "admin/admin_category_treeview_user.tpl";
            include("include/admin/admin_category_treeview_user.php");
        }
        
        
    }
    //END
    
    //TYPE ADMIN
    if($blogin==true&&$typeAdmin=="admin")
    {
        $main_category_tree = "admin/admin_management_left.tpl";
        if (isset($_GET["3nss"]) || isset($_POST["3nss"]))
        {   
         
            $func= isset($_GET["3nss"]) ? $_GET["3nss"] : $_POST["3nss"];
            switch($func)
            {
                case "permsion":
                    include("include/admin/admin_permsion.php");
                    break;
                case "categories.manage":
                    include("include/admin/admin_category.php");
                    break;
                case "categories":
                    include("include/admin/admin_type_category.php");
                    break;
                case "user":
                    include("include/admin/admin_users.php");
                    break;
                case "change.pass":
                    include("include/admin/admin_changePass.php");
                    break;
                default:
                    include("include/admin/admin_type_category.php");
                    break;
            }
        }
    }
    //END TYPE ADMIN
    
    //TYPE SUBADMIN
    if($blogin==true&&$typeAdmin=="subadmin")
    {
         $main_category_tree = "admin/admin_management_left.tpl";
        if (isset($_GET["3nss"]) || isset($_POST["3nss"]))
        {   
         
            $func= isset($_GET["3nss"]) ? $_GET["3nss"] : $_POST["3nss"];
            switch($func)
            {
                case "permsion":
                    include("include/admin/admin_permsion.php");
                    break;
                case "categories.manage":
                    include("include/admin/admin_category.php");
                    break;
                case "user":
                    include("include/admin/admin_users.php");
                    break;
                case "change.pass":
                    include("include/admin/admin_changePass.php");
                    break;
                default:
                    include("include/admin/admin_category.php");
                    break;
            }
        }
    }
    //END

}



$page->assign("user_admin",$_SESSION["username_admin"]);
$page->assign("main_category_tree",$main_category_tree);
$page->assign("main_user",$main_user);
$page->assign("admin_account",$admin_account);
$page->assign("admin_subMenu",$admin_subMenu);
$page->assign("typeAdmin",$typeAdmin);
$page->assign("main_content_template",$main_content_template);
$page->assign("headerTitle",$headerTitle);
$page->display("admin/admin.tpl");

?>
