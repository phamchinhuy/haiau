<?php
//class cs_topic created by tran van tam 24/4/2010
class cs_topic
{
    //name of table
    public $tbl="topic";
    
    //properties of class topic
    public $id                  = array();
    public $topic               = array();
    public $status              = array();
    public $date_created        = array();
    public $id_user_created     = array();
    public $date_updated        = array();
    public $id_user_updated     = array();
    public $date_deleted        = array();
    public $id_user_deleted     = array();
    public $small_image         = array();
    public $large_image         = array();
    //fields name
    
    public $id_fieldname                    = "id";
    public $topic_fieldname                 = "topic";
    public $status_fieldname                = "status";
    public $date_created_fieldname          = "date_created";
    public $id_user_created_fieldname       = "id_user_created";
    public $date_updated_fieldname          = "date_updated";
    public $id_user_updated_fieldname       = "id_user_updated";
    public $date_deleted_fieldname          = "date_deleted";
    public $id_user_deleted_fieldname       = "id_user_deleted";
    public $small_image_fieldname           = "small_image";
    public $large_image_fieldname           = "large_image";
    
    protected $DataObject;
    function _construct()
    {
        $this->DataObject = new cs_topic();
    }
   public function select_topic()
    {
        try
        {
            $connect  = sql_connect_default();
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." order by id"; 
                    
                            
            $result = null;
            $result = mysql_query($sql,$connect);
            $countRow = 0;
            $countRow = @mysql_num_rows($result);
            if($countRow>0)
            {
                for($i=0;$i<$countRow;$i++)
                {
                    $this->id[$i]=mysql_result($result, $i, $this->id_fieldname);
                    $this->topic[$i]=mysql_result($result, $i, $this->topic_fieldname);
                    $this->status[$i]=mysql_result($result, $i, $this->status_fieldname);
                    $this->date_created[$i]=mysql_result($result, $i, $this->date_created_fieldname);
                    $this->id_user_created[$i]=mysql_result($result, $i, $this->id_user_created_fieldname);
                    $this->date_updated[$i]=mysql_result($result, $i, $this->date_updated_fieldname);
                    $this->id_user_updated[$i]=mysql_result($result, $i, $this->id_user_updated_fieldname);
                    $this->date_deleted[$i]=mysql_result($result, $i, $this->date_deleted_fieldname);
                    $this->id_user_deleted[$i]=mysql_result($result, $i, $this->id_user_deleted_fieldname);
                    $this->small_image[$i]=mysql_result($result, $i, $this->small_image_fieldname);
                    $this->large_image[$i]=mysql_result($result, $i, $this->large_image_fieldname);
                    
                }
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        mysql_free_result($result);
        mysql_close($connect);
    }     
        
        
}
?>