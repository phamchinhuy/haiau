<?php
	class cs_library{
	
		//upload. ex: $image   = do_upload( $_FILES['image'], 'images/');
        // get radon name 
          function getrandom()
	      	    {
		    	$len = 10; //length of text
				$base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz123456789';
				$text = "";
				for ($i=1; $i<=$len; $i++)
				{
				   $text .= $base[rand(0,strlen($base)-1)];
				}
                    return strtoupper($text);

	      	    }

		function doUpload( $userfile, $url ) { 
			$site_name= $_SERVER['HTTP_HOST']; 
			$url_dir  = 'http://'. $_SERVER['HTTP_HOST']. dirname($_SERVER['PHP_SELF']); 
			$url_this = 'http://'. $_SERVER['HTTP_HOST']. $_SERVER['PHP_SELF']; 
		     //get random file name
             $name=$this->getrandom();
			$upload_dir= ''; 
			$upload_url= $url_dir. '/'; 
			$dir       = opendir('.'); 

			$message  = '';
			$temp_name= $userfile['tmp_name']; 
			$file_name= $userfile['name']; 
			$file_type= $userfile['type']; 
			$file_size= $userfile['size']; 
			$result   = $userfile['error']; 
			$file_url = $upload_url. $url. $file_name; 
            //add random name in to file images name
			$file_path= $upload_dir. $url.$file_name; 
		//	echo 	$file_path;
			if ( $file_name =='') { 
				//$message = 'Filename invalid !';
				$message = '';
				return false; 
			} 
			else{ 
				if ( $file_size > 2000000) { 
					$message = 'The file size is over 2mb !'; 
					return $false; 
				} 
				else{
					if ( $file_type == 'text/plain' ) { 
						$message = 'Sorry, you can not upload file script !' ; 
						return false; 
					}
				}
                return true;
			} 
			$result = move_uploaded_file($temp_name, $file_path);
			if($result){
				return $file_name;
			}
			else{
				$message = 'There are errors in the upload file !'; 
				return $message;
			} 
		} 
	
	//page
	function doPage( $total, $cur, $div, $rows, $url ){
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
		if($currentDiv){
			$sPage .= '&nbsp;<a href="'.$url.'&p=0" >First</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.($currentDiv*$div - 1).'" >Prev</a>&nbsp;&nbsp;';
		}
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= ' <a href="'.$url.'&p='.($currentDiv*$div + $i ).'">'.($page+1).'</a> ';
			}
			else{
				$sPage .= ' <span style="text-decoration:none; font-weight:bold; color:#000000;">'.($page+1).'</span> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= '&nbsp;&nbsp;<a href="'.$url.'&p='.(($currentDiv+1)*$div).'" >Next</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.(($nDiv-1)*$div ).'" >Last</a>&nbsp;';
		}
		$opage='
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:8px 0px 0px 0px;">
					<tr>
						<td width="75%">
							Displaying <b> ' . $nPage. '/' . $total .'</b> - Pages <span class="current" >'. $sPage. '</span>	
						</td>
						<td width="35%" align="right" >
							<a href="#" onclick="javascript:docheck(true,1);" class="checkAll">Check All</a>
                            |
							<a href="#" onclick="javascript:docheck(false,2);" class="checkAll">Uncheck All</a>
							| 
                            <a href="#" onclick="javascript:oop_delete_all333('. ($cur+1). ');" class="checkAll">Delete</a>
						</td>
					</tr>
				</table>
				';
		return $opage;
	}
    //phan trang moi
    
    //catogory child
    	function doPageM( $total, $cur, $div, $rows, $url,$t )
        {
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
		if($currentDiv){
			$sPage .= '&nbsp;<a href="'.$url.'&t=0" >First</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&t='.($currentDiv*$div - 1).'" >Prev</a>&nbsp;&nbsp;';
		}
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= ' <a href="'.$url.'&'.$t.'='.($currentDiv*$div + $i ).'">'.($page+1).'</a> ';
			}
			else{
				$sPage .= ' <span style="text-decoration:none; font-weight:bold; color:#000000;">'.($page+1).'</span> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= '&nbsp;&nbsp;<a href="'.$url.'&'.$t.'='.(($currentDiv+1)*$div).'" >Next</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&'.$t.'='.(($nDiv-1)*$div ).'" >Last</a>&nbsp;';
		}
		$opage='
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:8px 0px 0px 0px;">
					<tr>
						<td width="75%">
							Displaying <b> ' . $nPage. '/' . $total .'</b> - Pages <span class="current" >'. $sPage. '</span>	
						</td>
					</tr>
				</table>
				';
		return $opage;
	}
    	function dopaging( $total, $cur, $div, $rows, $url )
        {
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
	
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= '   <div class="list_page_pages"><a href="'.$url.''.($currentDiv*$div + $i ).'">'.($page+1).'</a> </div>';
			}
			else{
				$sPage .= '  <div class="list_page_pages" style="text-decoration:none; font-weight:bold; color:red;">'.($page+1).'</div> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= ' <div class="list_page_Previous"><a href="'.$url.(($currentDiv+1)*$div).'" >Next</a></div>';
			$sPage .= ' <div class="list_page_next"><a href="'.$url.(($nDiv-1)*$div ).'" >Last</a></div>';
		}
		return $sPage;
	}
	//user
   	function doPageUser( $total, $cur, $div, $rows, $url ){
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
		if($currentDiv){
			$sPage .= '&nbsp;<a href="'.$url.'&p=0" >First</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.($currentDiv*$div - 1).'" >Prev</a>&nbsp;&nbsp;';
		}
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= ' <a href="'.$url.'&p='.($currentDiv*$div + $i ).'">'.($page+1).'</a> ';
			}
			else{
				$sPage .= ' <span style="text-decoration:none; font-weight:bold; color:#000000;">'.($page+1).'</span> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= '&nbsp;&nbsp;<a href="'.$url.'&p='.(($currentDiv+1)*$div).'" >Next</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.(($nDiv-1)*$div ).'" >Last</a>&nbsp;';
		}
		$opage='
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:8px 0px 0px 0px;">
					<tr>
						<td width="55%">
							Displaying <b> ' . $nPage. '/' . $total .'</b> - Pages <span class="current" >'. $sPage. '</span>	
						</td>
						<td width="45%" align="left" >
                            	<a href="#"   onclick="javascript:oop_reset_all333('. ($cur+1). ');"  class="checkAll">Reset PassWord</a>
                            |
							<a href="#" onclick="javascript:docheck(true,1);" class="checkAll">Check All</a>
                            |
							<a href="#" onclick="javascript:docheck(false,2);" class="checkAll">Uncheck All</a>
							| 
                            <a href="#" onclick="javascript:oop_delete_all333('. ($cur+1). ');" class="checkAll">Delete</a>
						</td>
					</tr>
				</table>
				';
		return $opage;
	}
	function doPageB( $total, $cur, $div, $rows, $url ){
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
		if($currentDiv){
			$sPage .= '&nbsp;<a href="'.$url.'&p=0" >First</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.($currentDiv*$div - 1).'" >Prev</a>&nbsp;&nbsp;';
		}
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= ' <a href="'.$url.'&p='.($currentDiv*$div + $i ).'">'.($page+1).'</a> ';
			}
			else{
				$sPage .= ' <span style="text-decoration:none; font-weight:bold; color:#000000;">'.($page+1).'</span> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= '&nbsp;&nbsp;<a href="'.$url.'&p='.(($currentDiv+1)*$div).'" >Ti&#7871;p</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.(($nDiv-1)*$div ).'" >Cu&#7889;i</a>&nbsp;';
		}
		$opage='
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:8px 0px 0px 25px;">
					<tr>
						<td>
							Hi&#7875;n th&#7883; <b>'. ($cur+1). '</b> &#273;&#7871;n <b>'. $nPage. '</b> (c&#7911;a <b>'. $total .'</b> tin) &nbsp;&nbsp;&nbsp; Sau: <span class="current" >'. $sPage. '</span>	
						</td>
					</tr>
				</table>
				';
		return $opage;
	}
    function doPageC( $total, $cur, $div, $rows, $url ){
		$nPage      = floor($total/$rows) + (($total%$rows)?1:0);
		$nDiv       = floor($nPage/$div) + (($nPage%$div)?1:0);
		$currentDiv = floor($cur/$div) ;
		$sPage      = '';
		
		if($currentDiv){
			$sPage .= '&nbsp;<a href="'.$url.'&p=0" >First</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.($currentDiv*$div - 1).'" >Prev</a>&nbsp;&nbsp;';
		}
		$count =($nPage<=($currentDiv+1)*$div)?($nPage-$currentDiv*$div):$div;
		
		for( $i=0; $i<$count; $i++ ){
			$page= ( $currentDiv* $div+ $i );
			if( $page!=$cur ){
				$sPage .= ' &nbsp <a href="'.$url.'&p='.($currentDiv*$div + $i ).'">'.($page+1).'</a> ';
			}
			else{
				$sPage .= ' &nbsp <span style="text-decoration:none; font-weight:bold; color:#000000;">'.($page+1).'</span> ';
			}
		}
		
		if($currentDiv < $nDiv - 1){
			$sPage .= '&nbsp;&nbsp;<a href="'.$url.'&p='.(($currentDiv+1)*$div).'" >Ti&#7871;p</a>&nbsp;';
			$sPage .= '&nbsp;<a href="'.$url.'&p='.(($nDiv-1)*$div ).'" >Cu&#7889;i</a>&nbsp;';
		}
		$opage='
				<table border="0" cellspacing="0" cellpadding="0" class="pageService">
					<tr>
						<td class="td1">
							Trang: <span class="current" >'. $sPage. '</span>
						</td>
                        <td class="td2">
                            &nbsp; Hi&#7875;n th&#7883; <b>'. ($cur+1). '</b> / <b>'. $nPage. '</b> trang ( S&#7889; l&#432;&#7907;ng tin: <b>'. $total .'</b>  )
                        </td>
					</tr>
				</table>
				';
		return $opage;
	}
    function getPageNum($urls,$num,$cp)
    {
            //khai bao so s?n ph?m tr�n m�t trang catogory
           $arraypage=array(array("5"),array("10"),array("15"),array("20"),array("25"),array("30")
         );
        $com="";   
        for($i=0;$i<count($arraypage);$i++)
           for($j=0;$j<1;$j++)
        {     
            ///url
  			  if($arraypage[$i][0]==$num)
             {
            $com.="<option  value='".$urls."&".$cp."=".$arraypage[$i][0]."'  selected='selected'>".$arraypage[$i][0]."</option>";    
             } 
					 
		       else{
		         	 $com.="<option  value='".$urls."&".$cp."=".$arraypage[$i][0]."'>".$arraypage[$i][0]."</option>";    
		       }
              
        }
        return $com;
    }
             function getRealIpAddr()
        {
                if (!empty($_SERVER['HTTP_CLIENT_IP']))  
                {      
                    $ip=$_SERVER['HTTP_CLIENT_IP'];
                }      
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))    
                //to check ip is pass from proxy      
                    {         
                    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                   }       
                    else        
                    {          
                    $ip=$_SERVER['REMOTE_ADDR'];           
                    }
        return $ip; 
        
        }
        function RemoveSign($str)
            {
            $coDau=array("�","�","?","?","�","�","?","?","?","?","?","a","?","?"
            ,"?","?","?","�","�","?","?","?","�","?","?","?","?","?","�","�","?","?","i",
            "�","�","?","?","�","�","?","?","?","?","?","o"
            ,"?","?","?","?","?",
            "�","�","?","?","u","u","?","?","?","?","?",
            "?","�","?","?","?",
            "d",
            "�","�","?","?","�","�","?","?","?","?","?","A"
            ,"?","?","?","?","?",
            "�","�","?","?","?","�","?","?","?","?","?",
            "�","�","?","?","I",
            "�","�","?","?","�","�","?","?","?","?","?","O"
            ,"?","?","?","?","?",
            "�","�","?","?","U","U","?","?","?","?","?",
            "?","�","?","?","?",
            "�","�","�","�");
            $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
            ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
            ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
            ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
            ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
            return str_replace($coDau,$khongDau,$str);
            }
      
}
	//page 
    
?>