<?php //create by tran van tam 28/4/2010
    class cs_date
    {
        public $date;
        
        private function convertMMDDYYYY($dateTime)
        {
          $a=Array();
          $str="";
          $a = explode('/',$dateTime);
          if(count($a)==1) $a = explode('-',$dateTime);
          else
          {
            if(count($a)>=2)
            {
              $tmp=$a[0];
              $a[0]=$a[1];
              $a[1]=$tmp;
              $str=implode('/',$a);
            }
          }
          
          return $str;  
        }
        function formatDDMMYYYY($dDate)
        {
            $dDate= $this->convertMMDDYYYY($dDate);
            $dNewDate = strtotime($dDate);
            return date('d/m/Y',$dNewDate);
        }
        function formatYYYYDDMM($dDate)
        {
            $dDate= $this->convertMMDDYYYY($dDate);
            $dNewDate = strtotime($dDate);
            return date('Y/d/m',$dNewDate);
        }
        function formatYYYYMMDD($dDate)
        {
            $dDate= $this->convertMMDDYYYY($dDate);
            $dNewDate = strtotime($dDate);
            return date('Y/m/d',$dNewDate);
        }
        function formatMMDDYYYY($dDate)
        {
            $dDate= $this->convertMMDDYYYY($dDate);
            $dNewDate = strtotime($dDate);
            return date('m/d/Y',$dNewDate);
        }
        
    }
?>