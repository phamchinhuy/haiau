<?php
    class cs_client_news
    {
        
        //table name
        public $tbl = "news";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $news                = array();
        public $small_image         = array();
        public $large_image         = array();
        public $short_description   = array();
        public $long_description    = array();
        public $status              = array();
        public $url                 = array();
        public $target              = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $id_topic            = array();
        public $show_page           = array();
        public $url_video           = array();
        public $keyword             = array();
        public $page_title          = array();
        public $meta_description    = array();
        public $meta_keyword        = array();
        public $is_comment          = array();
        public $count_view          = array();
        public $begin_date          = array();
        public $begin_end           = array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $news_fieldname              = "news";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $short_description_fieldname = "short_description";
        public $long_description_fieldname  = "long_description";
        public $status_fieldname            = "status";
        public $url_fieldname               = "url";
        public $target_fieldname            = "target";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $id_topic_fieldname          = "id_topic";
        public $show_page_fieldname         = "show_page";
        public $url_video_fieldname         = "url_video";
        public $keyword_fieldname           = "keyword";
        public $page_title_fieldname        = "page_title";
        public $meta_description_fieldname  = "meta_description";
        public $meta_keyword_fieldname      = "meta_keyword";
        public $count_view_fieldname        = "count_view";
        public $begin_date_fieldname        = "begin_date";
        public $begin_end_fieldname         = "begin_end";
        public $is_comment_fieldname        = "is_comment";
        //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_client_news();
        }
        function getsource($rs)
        {
                   if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = $row[$this->news_fieldname];
                        //if(strlen($this->news[$i])>70)
                        $this->news[$i]=$this->news[$i];
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        
                        $this->short_description[$i]    = $this->short_description[$i];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
        }
        
        function convert_string($p_string)
        {
            $arr_string = array();
            
            $str = array();
            $str =  str_split($p_string);
            $i = 0;
            for($n=0; $n<count($str); $n++)
            {
                $strChar = "";
                $strChar = $str[$n];
                echo $strChar;
                
                for($c=0; $c<strlen($strChar); $c++)
                {
                    //echo $strChar[$c]."<br>";
                    /*
                    $arr_char = array();
                    $arr_char = $this->convert_character_to_array_character($strChar[$c]);
                    if(count($arr_char)>0)
                    {
                        for($m=0; $m<count($arr_char); $m++)
                        {
                            $strA = "";    
                            $strA = str_replace($strChar[$c], $arr_char[$m], $strChar);
                            $arr_string[$i] = $strA;
                            echo  $arr_string[$i]."<br/>";
                            $i++;
                            
                        }
                    }
                    */
                }
                
            }
            
            return $arr_string;
            
        }
        
        //public $arr_a = array("a","a","�","?","?","�","?","?","?","?","?","a","?","?","?","?","?");
        
        public $arr_a = array("a","&#225;","&#224;","&#7843;","&#7841;","&#226;","&#7847;","&#7845;","&#7849;","&#7851;","&#7853;","&#7855;","&#7857;","&#7855;","&#7859;","&#7861;","&#7841;");
        
        public $arr_e = array("e", "&#233;", "&#232;", "&#7867;", "&#7869;", "&#7865;", "&#234;", "&#7871;", "&#7873;", "&#7875;", "&#7877;", "&#7879;");
        
        function convert_character_to_array_character($p_character)
        {
            $arr_char = array();
            switch($p_character)
            {
                case "a":
                    for($iA=0; $iA<count($this->arr_a); $iA++)
                    {
                        $arr_char[$iA] = $this->arr_a[$iA];
                    }
                    break;
                case "e":
                    for($iE=0; $iE<count($this->arr_e); $iE++)
                    {
                        $arr_char[$iE] = $this->arr_e[$iE];
                    }
                    break;
            }
            return $arr_char;
        }
        
        function convert_character($p_character)
        {
            
            $char = "";
            if($p_character!="")
            {
                switch($p_character)
                {
                    
                    case "a":
                    case "�":
                    case "�":
                    case "?":
                    case "�":
                    case "?":
                    case "�":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                    case "a":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                        $char = "a";
                        break;
                        
                    case "e":
                    case "�":
                    case "�":
                    case "?":
                    case "?":
                    case "?":
                    case "�":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                        $char = "e";
                        break;
                    
                    case "y":
                    case "�":
                    case "?":
                    case "?":
                    case "?":
                        $char = "y";
                        break;
                        
                    case "u":
                    case "�":
                    case "�":
                    case "?":
                    case "u":
                    case "?":
                    case "u":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                    case "?":
                        $char = "u";
                        break;
                    
                     case "i":
                     case "�":
                     case "�":
                     case "?":
                     case "i":
                     case "?":
                        $char = "i";
                        break;
                        
                     
                        
                    
                        
                }
            }
            return $char;
            
        }
        
        public function search($p_string, $p_start, $p_end)
        {
            try
            {
                $sql = "";
                $p_string = strtolower($p_string);
                $sql="select *  
                        from news 
                        where   (LOWER(".$this->news_fieldname.") LIKE '%".$p_string."%')
                                or (LOWER(".$this->news_fieldname.") LIKE '".$p_string."%')
                                or (LOWER(".$this->news_fieldname.") LIKE '%".$p_string."')
                                or (LOWER(".$this->short_description_fieldname.") LIKE '%".$p_string."%')";
                                
                $arr_string_search = array();
                $arr_string_search = explode(" ", $p_string, strlen($p_string));
                
                for($s=0; $s<count($arr_string_search); $s++)
                {
               
                    $sql.=" or (LOWER(".$this->news_fieldname.") LIKE '%".$arr_string_search[$s]."%')
                                or (LOWER(".$this->short_description_fieldname.") LIKE '%".$arr_string_search[$s]."%')";
                     
                }
                
                
                $sql.= " and ".$this->status_fieldname."='1'";
                                
                $sql.= " order by ".$this->id_fieldname." desc"; 
                
                if($p_start!="" && $p_end!="")
                    $sql.=" limit ".$p_start.", ".$p_end;
                
                //echo $sql;
                        
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                    $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        
        //xuan
        //3/5/2010
        public function select_news_by_id($p_id)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_fieldname."=".$p_id."
                                and ".$this->status_fieldname."='1' 
                        order by `id`"; 
                        
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //end
           //3/5/2010
        public function select_news_by_id_category($p_id_category)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_category_fieldname."=".$p_id_category."
                                and ".$this->status_fieldname."='1' 
                        order by ".$this->id_fieldname." desc
                        ";
          
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //end
        public function select_news_by_id_diffrent($p_id,$p_cate)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_fieldname."<>".$p_id."
                        and ".$this->id_category_fieldname."=".$p_cate." 
                        and ".$this->status_fieldname."='1' 
                        order by `id` limit 0,2"; 
                        
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                 if($rs!="")
                   $this->getsource($rs);    
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
          public function select_news_by_id_diffrent_1($p_id,$p_cate)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_fieldname."<".$p_id."
                        and ".$this->id_category_fieldname."=".$p_cate." 
                        
                        and ".$this->status_fieldname."='1' 
                        order by `id` limit 0,2"; 
                        
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                 if($rs!="")
                   $this->getsource($rs);    
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        ///
        
        public function select_news_by_page_title($p_page_title)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->page_title_fieldname."='".$p_page_title."'
                                and ".$this->status_fieldname."='1'";
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                  $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //end
        
        
        
        
        //xuan
        //3/5/2010
        public function select_news_by_id_category_status($p_id_category, $p_status , $p_start, $p_count)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        
        
        
        public function select_news_by_id_category_status_not_id($p_id_category, $notid, $p_status , $p_start, $p_count)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."!=".$notid." and ".$this->id_fieldname."<".$notid."
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!="")
                  $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
        }
        
        
        public function select_news_by_id_category_status_not_id2_not_limit($p_id_category, $notid, $p_status)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."!=".$notid." 
                        order by `id` desc";
    
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = $row[$this->news_fieldname];
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
        }
        
        public function select_news_by_id_category_status_pagetitle($p_id_category, $notid, $p_status , $p_start, $p_count)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->page_title_fieldname."!='".$notid."'
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                //echo $sql;
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->news[$i] = mysql_result($result,$i,$this->news_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->target[$i] = mysql_result($result,$i,$this->target_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->id_topic[$i] = mysql_result($result,$i,$this->id_topic_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->url_video[$i] = mysql_result($result,$i,$this->url_video_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
            mysql_close($connect);
        }
        
        
        //xuan
        //3/5/2010
        public function select_news_by_id_category_not_limit($p_id_category, $p_status)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc";
                // echo $sql;
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = isset($row[$this->news_fieldname])?$row[$this->news_fieldname]:"";
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        
        
        public function select_news_by_id_category_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc 
                        limit ".$p_start.", ".$p_end;
                //echo $sql;
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = isset($row[$this->news_fieldname])?$row[$this->news_fieldname]:"";
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        
        ///  and ".$this->tbl.".".$this->status_fieldname."='".$p_status."'
          public function select_news_by_id_parent_category_status_limit($p_id_category, $p_status,$start,$end)
          {
            try
            {   
            $obj= new cs_client_category();
                $sql = "";
                $sql = "select ".$this->tbl.".*  
                        from ".$this->tbl.",".$obj->tbl." 
                        where   ".$this->tbl.".".$this->id_category_fieldname."=".$obj->tbl.".".$obj->id_fieldname." 
                                  and ".$obj->tbl.".".$obj->id_parent_fieldname."='".$p_id_category."'
                                  and ".$this->tbl.".".$this->status_fieldname."='".$p_status."'
                                  order by `id` desc limit ".$start.",".$end;
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = $row[$this->news_fieldname];
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          }
         public function select_news_by_id_parent_category_not_limit($p_id_category, $p_status)
        {
            try
            {   
                $obj= new cs_client_category();
                $sql = "";
                $sql = "select ".$this->tbl.".*  
                        from ".$this->tbl.",".$obj->tbl." 
                        where   ".$this->tbl.".".$this->id_category_fieldname."=".$obj->tbl.".".$obj->id_fieldname." 
                                  and ".$obj->tbl.".".$obj->id_fieldname."='".$p_id_category."'
                                  and ".$this->tbl.".".$this->status_fieldname."='".$p_status."'
                                  order by `id` desc";
                //echo $sql;
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->news[$i]                 = $row[$this->news_fieldname];
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i]             = $row[$this->id_topic_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $this->is_comment[$i]           = $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //
        public function select_news_by_id_category_not_limit_different($p_id_category, $p_status, $pid)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."<'".$pid."'
                        order by `id` desc limit 0, 9";
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null && $rs->rowCount()>0)
                {
                    $this->getsource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        public function select_news_by_id_category_limit_different($p_id_category, $p_status,$pid,$start,$end)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."<'".$pid."'
                        order by `id` desc limit ".$start.",".$end;
               
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                  $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //xuan
        //3/5/2010
        public function select_news_by_id_category_status_id($p_id_category, $p_status, $p_id , $p_start, $p_count)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."<".$p_id."
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->news[$i] = mysql_result($result,$i,$this->news_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->target[$i] = mysql_result($result,$i,$this->target_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->id_topic[$i] = mysql_result($result,$i,$this->id_topic_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->url_video[$i] = mysql_result($result,$i,$this->url_video_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
            mysql_close($connect);
        }
        
        public function select_news_by_id_category_status_id2($p_id_category, $p_status, $p_id , $p_start, $p_count)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                                and ".$this->id_fieldname."!=".$p_id."
                                and ".$this->id_fieldname."<".$p_id."
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->news[$i] = mysql_result($result,$i,$this->news_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->target[$i] = mysql_result($result,$i,$this->target_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->id_topic[$i] = mysql_result($result,$i,$this->id_topic_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->url_video[$i] = mysql_result($result,$i,$this->url_video_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
            mysql_close($connect);
        }
        
    //Thuy Nguyen
    //18/02/2011
    public function select_news_by_id_category_asc_date_create($p_id_category)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_category_fieldname."=".$p_id_category."
                                and ".$this->status_fieldname."='1' 
                                order by `date_created` ASC
                        ";
            
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
       //Thuy Nguyen
    //18/02/2011
    public function select_news_by_id_category_desc_date_create($p_id_category)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_category_fieldname."=".$p_id_category."
                                and ".$this->status_fieldname."='1' 
                                order by ".$this->id_fieldname." DESC
                        ";
            
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }   
        public function select_news_by_id_category_asc_date_create1($p_id_category)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_category_fieldname."=".$p_id_category."
                                and ".$this->status_fieldname."='1' 
                                order by ".$this->id_fieldname." asc
                        ";
            
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }   
    public function select_news_by_id_category_desc_date_create_limit($p_id_category, $pStart, $pEnd)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from news 
                    where   ".$this->id_category_fieldname."=".$p_id_category."
                            and ".$this->status_fieldname."='1' 
                            order by ".$this->id_fieldname." DESC
                            limit ".$pStart.", ".$pEnd."
                    ";
        
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            if($rs!=null)
             $this->getsource($rs);
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }   
    //Thuy Nguyen
    //21/02/2011
    public function select_news_birthday_by_id_parent_category($p_id_parent)
        {
          
            $today = date("Y-m-d h:m:s");  
          
            try
            {
                $sql = "";
               
                 $sql = "select *  
                        from news 
                        where   ".$this->id_category_fieldname." in (select category.id  
                                                                    from `category`  
                                                                    where `status`=1 
                                                                    and  `id_parent`='".$p_id_parent."')
                                                                   
                                and ".$this->status_fieldname."='1' 
                                and month(`".$this->begin_date_fieldname."`)=month('".$today."')
                                
                                order by `date_created` ASC
                        ";
             
             
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                 $this->getsource($rs);
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }

    //Thuy Nguyen
    //22/02/2011
       //3/5/2010
    public function select_news_list_by_id_category_id_parent($p_id_parent)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from news 
                    where   ".$this->id_category_fieldname." in (select category.id  
                                                                    from `category`  
                                                                    where `status`=1 
                                                                    and  `id_parent`='".$p_id_parent."')
                                                                   
                            and ".$this->status_fieldname."='1' 
                    ";
           
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            if($rs!=null)
             $this->getsource($rs);
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
   } 
?>