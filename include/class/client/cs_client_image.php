<?php
class cs_client_images
{
        public $tbl                     ="images";
        public $id_object               =array();
        public $id                      =array();
        public $object_type             =array();
        public $status                  =array();
        public $path_image_thumb        =array();
        public $path_image_large        =array();
        public $alt                     =array();
        //
        private $db="";        
        //column name
        public $id_object_fieldname               ="id_object";
        public $id_fieldname                      ="id";
        public $object_type_fieldname             ="object_type";
        public $status_fieldname                  ="status";
        public $path_image_thumb_fieldname        ="path_image_thumb";
        public $path_image_large_fieldname        ="path_image_large";
        public $alt_fieldname                     ="alt";       	
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_images();
        }
        function  cs_client_images()
        {
            
        }
        
    /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    public function getSource($rs)
    {
         $do_nick_chat="";
         if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_object[$i]            = $row[$this->id_object_fieldname];
                        $this->object_type[$i]          = $row[$this->object_type_fieldname];
                        
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->path_image_large[$i]     = $row[$this->path_image_large_fieldname];
                        $this->path_image_thumb[$i]     = $row[$this->path_image_thumb_fieldname];
                        $this->alt[$i]                  = $row[$this->alt_fieldname];
                    $i++;
                }
            }
    }
    /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:xuan.pham
     * Ngay tao: 04/08/2010
     */
    public function select_image_by_idObject_object_type_status($p_id_object, $p_object_type, $p_status)
    {
        $db = new db;
        $sql = "";
        $sql = " select * 
                 from  ".$this->tbl." 
                 where ".$this->id_object_fieldname."=".$p_id_object." 
                 and ".$this->object_type_fieldname."='".$p_object_type."' 
                 and ".$this->status_fieldname."='".$p_status."' ";
        $rs = null;
        $rs = $db->sql_execute($sql,$db->isSelect);
        if($rs!=null)
            $this->getSource($rs);
    } 	
    
}

?>