<?php


class cs_client_product
{
    
    //table name
    public $tbl = "product";
    
    //value field
    public $id                  = array();
    public $id_category         = array();
    public $product             = array();
    public $product_code        = array();
    public $short_description   = array();
    public $long_description    = array();
    public $small_image         = array();
    public $large_image         = array();
    public $status              = array();
    public $date_created        = array();
    public $id_user_created     = array();
    public $date_updated        = array();
    public $id_user_updated     = array();
    public $date_deleted        = array();
    public $id_user_deleted     = array();
    public $is_comment          = array();
    public $price               = array();
    public $priceSaleOf         = array();
    public $re_urls             = array();
    public $page_title            = array();
    public $meta_keyword          = array();
    public $meta_description      = array();
    

    //end value field
    
    
    //field name
    public $id_fieldname                = "id";
    public $id_category_fieldname       = "id_category";
    public $product_fieldname           = "product";
    public $product_code_fieldname      = "product_code";
    public $short_description_fieldname = "short_description";
    public $long_description_fieldname  = "long_description";
    public $small_image_fieldname       = "small_image";
    public $large_image_fieldname       = "large_image";
    public $status_fieldname            = "status";
    public $date_created_fieldname      = "date_created";
    public $id_user_created_fieldname   = "id_user_created";
    public $date_updated_fieldname      = "date_updated";
    public $id_user_updated_fieldname   = "id_user_updated";
    public $date_deleted_fieldname      = "date_deleted";
    public $id_user_deleted_fieldname   = "id_user_deleted";
    public $is_comment_fieldname        = "is_comment";
    public $price_fieldname             = "price";
    public $priceSaleOf_fieldname       = "priceSaleOf";
    public $re_urls_fieldname           = "re_urls";
    public $page_title_fieldname                = "page_title";
    public $meta_keyword_fieldname              = "meta_keyword";
    public $meta_description_fieldname          = "meta_description";
   
    //end field name
    
    protected $DataObject;
    function _construct()
    {
        $this->DataObject = new cs_client_product();
    }
    
    /**
     * DESCRIBE FUNCTION:  select_product_by_id_category_status
     * select product by id category and status  = 1
     * 
     * parameter:
     * $p_id_category: id category
     * $p_status: status
     * 
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
    public function select_product_by_id_category_status($p_id_category, $p_status)
    {
        try
        {
            $db = new db();
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where   ".$this->id_category_fieldname."=".$p_id_category." 
                            and ".$this->status_fieldname." = ".$p_status."
                            order by ".$this->id_fieldname." desc";
            $rs = null;                
            $rs = $db->sql_execute($sql, $db->isSelect);
            if($rs != null)
            {
                if($rs->rowCount()>0)
                {
                    $i = 0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] =$row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $i++;
                    }
                }
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
    //END
    
    
    /**
     * DESCRIBE FUNCTION:  select_product_by_id_category_status_limit
     * select product by id category and status  = 1 and limit
     * 
     * parameter:
     * $p_id_category, 
     * $p_status, 
     * $p_start, 
     * $p_end
     * 
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
    public function select_product_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
    {
        try
        {
            $db = new db();
            $sql = "";
            $sql = "select *  
                    from product
                    where   id_category=".$p_id_category." 
                            and status = ".$p_status."
                            order by `id` desc
                            limit ".$p_start.", ".$p_end;
            $rs = null;                
            $rs = $db->sql_execute($sql, $db->isSelect);
            if($rs != null)
            {
                if($rs->rowCount()>0)
                {
                    $i = 0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] =$row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $i++;
                    }
                }
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
    //END
    
    
    
    public function select_product_by_id_category($p_id_category)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from product
                    where   id_category=".$p_id_category." 
                            and status < 2
                            order by `id` desc";
            $db = new db();
            $result = null;
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $i++;
                    }
                    
                }
            }   
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    public function select_product_new($p_site, $p_status, $p_template)
    {
        try
        {
            $sql = "";
            $sql = "select p.*  
                    from product as p, category as c, type_category as tc
                    where c.id_site=".$p_site." 
                            and tc.template='".$p_template."' 
                            and c.id_type_category=tc.id 
                            and c.id=p.id_category 
                            and p.status=".$p_status."  and c.status='1'
                            order by p.id desc";
            $db = new db();
            $result = null;
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $i++;
                    }
                }
            }   
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    public function select_product_new_limit($p_site, $p_status, $p_template, $p_start, $p_count)
    {
        try
        {
            $sql = "";
            $sql = "select p.*  
                    from product as p, category as c, type_category as tc
                    where c.id_site=".$p_site." 
                            and tc.template='".$p_template."' 
                            and c.id_type_category=tc.id 
                            and c.id=p.id_category 
                            and p.status=".$p_status." and c.status='1'
                            order by p.id desc
                            limit ".$p_start.", ".$p_count." ";
            $db = new db();
            $result = null;
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $i++;
                    }
                }
            }   
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    
    //27-04-2010
    //Tam
    public function select_product_by_id_category_limit($p_id_category, $p_start, $p_end)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from product 
                    where   id_category=".$p_id_category." 
                            and status < 2
                            order by `id` desc
                            limit ".$p_start.", ".$p_end;
                            
            $result = null;
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                    }
                }
            }   
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    /**
     * DESCRIBE FUNCTION: 
     * select_product_by_id
     * 
     * parameter:
     * $p_id
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
    public function select_product_by_id($p_id)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from product
                    where   ".$this->id_fieldname."=".$p_id." and ".$this->status_fieldname."='1' order by `id`";
           
            $result = null;
            $db = new db();
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                    }
                }
            }   
            
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    
    /**
     * DESCRIBE FUNCTION: 
     * select_product_by_id
     * 
     * parameter:
     * $p_id
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
    public function select_product_by_Page_Title($p_Page_Title)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from product
                    where   ".$this->page_title_fieldname."='".$p_Page_Title."' and ".$this->status_fieldname."='1' order by `id`";
           
            $result = null;
            $db = new db();
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                    }
                }
            }   
            
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    
    //27-04-2010
    //Tam
    public function select_product_by_re_url($p_id)
    {
        try
        {
            $connect  = sql_connect_default();
            $sql = "";
            $sql = "select *  
                    from product
                    where   ".$this->re_urls_fieldname."='".$p_id."' and ".$this->status_fieldname."='1' order by `id`";
           // echo $sql; 
            $result = null;
            $result = $db->sql_execute($sql,$db->isSelect);
            if($result!=null)
            {
                $countRow = $result->rowCount();
                if($countRow>0)
                {
                    $i = 0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->product[$i] = $row[$this->product_fieldname];
                        $this->product_code[$i] = $row[$this->product_code_fieldname];
                        $this->short_description[$i] = $row[$this->short_description_fieldname];
                        $this->long_description[$i] = $row[$this->long_description_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->price[$i] = $row[$this->price_fieldname];
                        $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                        $this->re_urls[$i] = $row[$this->re_urls_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                    }
                }
            }   
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    
    
}
?>