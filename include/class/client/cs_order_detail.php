<?php

class cs_client_order_detail
{
    public $tbl = "order_detail";
    
    public $id                  = array();
    public $id_order            = array();
    public $id_product          = array();
    public $product_code        = array();
    public $count               = array();
    public $price               = array();
    public $size                = array();
    public $color               = array();
    
    
    public $id_fieldname                = "id";
    public $id_order_fieldname          = "id_order";
    public $id_product_fieldname        = "id_product";
    public $product_code_fieldname      = "product_code";
    public $count_fieldname             = "count";
    public $price_fieldname             = "price";
    public $size_fieldname              = "size ";
    public $color_fieldname             = "color";
   
    
    public function insert_order_detail()
    {
        $db = new db();
        $sql = "";
        $sql = " insert into order_detail 
                (
                    ". $this->id_order_fieldname."
                    ,". $this->id_product_fieldname."
                    ,". $this->product_code_fieldname."
                    ,". $this->count_fieldname."
                    ,". $this->price_fieldname."
                    ,". $this->size_fieldname."
                    ,". $this->color_fieldname."
                )
                values
                (
                    ". $this->id_order[0]."
                    ,". $this->id_product[0]."
                    ,'". $this->product_code[0]."'
                    ,". $this->count[0]."
                    ,". $this->price[0]."
                    ,'". $this->size[0]."'
                    ,'". $this->color[0]."'
                )";
        $rs=null;
        $rs = $db->sql_execute($sql, $db->isInsert);
        return $rs;
    }
}

?>