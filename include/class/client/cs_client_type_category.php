<?php

    class cs_client_type_category
    {
        public $tbl               ="type_category";
        public $type_categoryId   =array();
        public $type              =array();
        public $template          =array();
        public $status            =array();
        public $key_type          =array();
        public $date_updated      =array();
        public $date_created      =array();
        public $id_user_created  	=array();
        public $id_user_updated   =array();
        public $date_deleted      =array();
        public $id_user_deleted   =array();
        public $position          =array();
      
        public $icon=array();
        private $_connection = '';
        private $_cursor = null;
        private $_sql = '';
        
        
        //value
        public $id  = array();
        
        //field name
        public $id_fieldname                = "id";
        public $type_fieldname              = "type";
        public $template_fieldname          = "template";
        public $status_fieldname            = "status";
        public $key_type_fieldname          = "key_type";
        public $icon_fieldname              = "icon";
        public $date_created_fieldname      = "date_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_created_fieldname   = "id_user_created";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $position_fieldname          ="position";
        private $db=null;
        
        protected $DataObject;
        
        function _construct()
        {
            $this->DataObject = new cs_client_type_category();
                $this->db=new db();  
        }
        function cs_client_type_category()
        {
            $this->db=new db();
        }
         public function select_id_by_position($p)
    {
        $position =0;
        $sql ="select ".$this->id_fieldname." from  ".$this->tbl  ;
        $sql.="  where  ".$this->position_fieldname."=".$p;
        //echo $sql;
        if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $position  =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"1";      
                    }
            }
        }
        return   $position;
    }
          public function swap_two_position($p1,$p2)
       {
        try{   
             $id_gallery1="";
             $id_gallery2="";
             $id_gallery1=$this->select_id_by_position($p1);
             $id_gallery2=$this->select_id_by_position($p2);
             $rs=false;
             $rs=$this->doUpdate_position($p1,$id_gallery2);
             if($rs)
             {
                $rs=false;
                $rs=$this->doUpdate_position($p2,$id_gallery1);
                 return $rs;          
             }
        }
         catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
       }
          public  function doUpdate_position($p,$id)
    {
       $sql="";
       $sql.="update ".$this->tbl." ";
       $sql.="set ".$this->position_fieldname."=".$p;
       $sql.="  where ".$this->id_fieldname."=".$id;
       // echo $sql;
        if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            return 0;      
    }
        //PHAM THI THU XUAN
        //21/04/2010
        public function select_type_category_by_id_status_1($p_id)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from type_category 
                        where id=".$p_id." 
                                and status=1";
                        
                $result = null;
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
        public function create_combox_position($position)
    {
        //echo $position;
         $str="";
         $cs_type=new cs_type_category();
         $cs_type->selectall_typecateogory();
         $cb="'cbhd".$position."'";
         $str.='<select width="20" name="cb'.$position.'" id="cb'.$position.'" onchange="change_type(
         this.value,document.getElementById('.$cb.').value
         )">
          ';
        
            for($i=0;$i<count($cs_type->type_categoryId);$i++)
            {
                if( $cs_type->position[$i]==$position)
               
               $str.="<option value='".$cs_type->position[$i]."' selected='selected'>".($i+1)."</option>";
               else
               $str.="<option value='".$cs_type->position[$i]."'  >".($i+1)."</option>";
            }
           $str.="</select>";
           $str.='<input type="hidden" value="'.$position.'" name="cbhd'.$position.'" id="cbhd'.$position.'">';
          // echo $str;
           return $str;
    }
         //PHAM THI THU XUAN
        //21/04/2010
        public function select_max_category_by_id()
        {
            try
            {
               
                $sql = "";
                $sql = "select max(".$this->id_fieldname.") as id ,".$this->template_fieldname."
                        from   ".$this->tbl."
          
                               where  status < 2";
                   //   echo $sql;        
                $result = "";
                $max="";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                     if($result!="")
                     foreach($result as $row)
                     {
                      
                       $max=$row[$this->template_fieldname]; 
                     }
                     return $max;
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
      
        public function select_max_position()
        {
            try
            {
               
                $sql = "";
                $sql = "select max(".$this->position_fieldname.") as ".$this->position_fieldname." ,".$this->template_fieldname."
                        from   ".$this->tbl."
          
                               where  status < 3";
                   //   echo $sql;        
                $result = "";
                $max=0;
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                     if($result!="")
                     foreach($result as $row)
                     {
                      
                       $max=$row[$this->position_fieldname]; 
                     }
                     return $max;
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
         public function select_category_id_by_template($template)
        {
            try
            {
               
                $sql = "";
                $sql = "select *
                        from   ".$this->tbl."
                        where ".$this->template_fieldname."='".$template."'
                                and status < 2";
             
                $result = "";
                 if($sql!="")
                 {
                      
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
        
        public function select_category_id_by_key_type($p_key_type)
        {
            try
            {
               
                $sql = "";
                $sql = "select *
                        from   ".$this->tbl."
                        where ".$this->key_type_fieldname."='".$p_key_type."'
                                and status =1";
                
                $result = "";
               
                 if($sql!="")
                 {
                      
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
        //END
        
          //Phan thi thuy nguyen
        //17/02/2011
        public function select_type_category_by_id_status_1_key_type($keytype)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from type_category 
                        where key_type='".$keytype."' 
                                and status=1";
                    // echo $sql;           
                $result = null;
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        
        }
        //end
        
        
      
       function setQuery($sql) 
	{
		$this->_sql = $sql;
	}

	function query() 
	{	
        $this->database();
		$this->_cursor = mysql_query($this->_sql,$this->_connection);
		return $this->_cursor;
	}
    function disconnect() 
	{
		mysql_close( $this->_connection);
	}	
    function database() 
	{
		$this->_connection =  sql_connect_default();
	}
      function do_selectall($rsSQL)
      {  
      	if($rsSQL!=null)
        {                
            $i=0;
            foreach($rsSQL  as $rowSQL )
            { 
                //category
                $this->id[$i]=$rowSQL[$this->id_fieldname];
               $this->type_categoryId[$i]=$rowSQL[$this->id_fieldname];
               $this->type[$i]=$rowSQL[$this->type_fieldname];
               $this->template[$i]=$rowSQL[$this->template_fieldname];
               //echo $this->template[$i];
               $this->position[$i]=$rowSQL[$this->position_fieldname];
               $this->status[$i]=$rowSQL[$this->status_fieldname];
               $this->key_type[$i]=$rowSQL[$this->key_type_fieldname];
               $this->date_created[$i]=$rowSQL[$this->date_created_fieldname];
               $this->id_user_created[$i]=$rowSQL[$this->id_user_created_fieldname];
               $this->date_updated[$i]=$rowSQL[$this->date_updated_fieldname];
               $this->date_deleted[$i]=$rowSQL[$this->date_deleted_fieldname];
               $this->id_user_deleted[$i]=$rowSQL[$this->id_user_created_fieldname];
               $this->id_user_updated[$i]=$rowSQL[$this->id_user_updated_fieldname];
               $this->icon[$i]=$rowSQL[$this->icon_fieldname];                            
                $i++;
            }    		      
	    
        }
           
      }
      function do_selectall_typecateogory($orderby)
      {
            try{
            $sql="select * from ".$this->tbl." where status=1  order by  ".$orderby ." ";
            //echo $sql;
            if($sql!="")
             {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($rs);
                
             }
         }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
         
          
      }
       function selectall_typecateogory()
      {
            try{
            $sql="select * from ".$this->tbl."  where  ".$this->status_fieldname." <3  order by ".$this->position_fieldname. "  ";
          //  echo $sql;
            if($sql!="")
             {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                if($rs!="")
                $this->do_selectall($rs);
                
             }
         }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
         
          
      }
       function do_selectall_typecateogory_id($id)
      {
        try{
             $sql="select * from ".$this->tbl." where  id=".$id;
             if($sql!="")
             {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($rs);
             }
        }
         catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
         
      }
      //check template 
       function check_template_exist()
      {
        try{
             $sql="select * from ".$this->tbl." where  ".$this->template_fieldname."='".$this->template[0]."'";
             if($sql!="")
             {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($rs);
                $count=0;
                $count=count($this->type_categoryId);
              
                if($count>0)
                return 0;
                else
                return 1;
             }
        }
         catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
         
      }
      
      ///
      function donum()
      {
        try{
              $num=0;
              $sql="select * from  ".$this->tbl." where ".$this->status_fieldname." <>3 ";
              if($sql!="")
              {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                 $num=$rs->rowCount();
              }
              return $num;
        }
        catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
          
      }
      	function doSelectL($l1, $l2,$orderby){
	     try{	       
                $sql="select * from ".$this->tbl."  where ".$this->status_fieldname." <>3  order by  ".$this->position_fieldname ." limit ".$l1.",".$l2;
             if($sql!="")
             {
                $rs="";
                $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($rs);
             }
	     }
         catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
     	}
        	function doNumL( $l1, $l2){
        	   try
               {
        	     $sql="select * from   ".$this->tbl."  where status <>3 limit ".$l1.",".$l2;   
                 $num=0;
                 $rs="";
                 $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                 $num=$rs->rowCount() ;
                 return $num; 
        	   }
                catch(exception $e)
            {
                var_dump($e->getMessage());
            }
	     }
     	function doActive($id, $status){   		
            try{
                
                if($status){
    			$status= 0;
    		}
    		else{
    			$status= 1;	
    		}
    		
            $query  = 'update  '.$this->tbl.'  set status="'.$status.'" where id="'.$id.'"';
    	    if($query!="")
                {
                    $rs=false;
                    $rs=$this->db->sql_execute($query,$this->db->isUpdate);
                    return $rs;
                }
            }
                catch(exception $e)
            {
                var_dump($e->getMessage());
            }
	   }
       	function doDelete($id){
       	    try{
       	        if(isset($this->id_user_deleted[0])&&$this->id_user_deleted[0]!="")
                    $query  = 'update '.$this->tbl.'   set status="3" 
                    ,date_deleted 	=now(),id_user_deleted='.$this->id_user_deleted[0].'
                    where id="'.$id.'"';
                else
                    $query  = 'update '.$this->tbl.'   set status="3" 
                    ,date_deleted 	=now()
                    where id="'.$id.'"';   
                    $rs=false;
                    if($query!="")
                    {
                        $rs=$this->db->sql_execute($query,$this->db->isUpdate);
                    }
                    return $rs;
                
       	    }
                catch(exception $e)
            {
                var_dump($e->getMessage());
            }
	   }
          	function doDeletex($id){
       	    try{
       	        $flag=false;
       	        if(isset($this->id_user_deleted[0])&&$this->id_user_deleted[0]!="")
                    $query  = 'delete  from  '.$this->tbl.'  
                   
                    where id="'.$id.'"';
                    
                    if($query!="")
                    { 
                        $rs=$this->db->sql_execute($query,$this->db->isDelete);
                        
                         $result="";
                         if($rs)
                         {
                        $sql="select  * from category where id_type_category=".$id ;
                       
                         $result=$this->db->sql_execute($sql,$this->db->isSelect);
                         if($result!="")
                           {
                           if($result->rowCount()>0)
                              $flag=true;
                           else
                               $flag=false;
                            }
                            else
                            $flag=false; 
                          }else
                          $flag=false; 
                    }
             $rs=true;       
             if($flag)
             {
                $sql=""; 
                $sql="delete from category where id_type_category=".$id;
                $rs= $this->db->sql_execute($sql,$this->db->isDelete);
             }
             return $rs;
                
       	    }
                catch(exception $e)
            {
                var_dump($e->getMessage());
            }
	   }
        function doinsert()
       {
        if($this->icon[0]!="")
        {
             if($this->insertFile($this->icon[0]))
             {
                $sql="";
              	$sql.= 'insert into '.$this->tbl. '(type ,template ,status,key_type ,icon,date_created,
                  id_user_created
                  ,'.$this->position_fieldname.') values( ';
                $sql.="'".$this->type[0]."'";
                $sql.=",'".$this->template[0]."'"; 
                $sql.=",'".$this->status[0]."'"; 
                $sql.=",'".$this->key_type[0]."'"; 
                $sql.=",'".$this->icon[0]."'"; 
                $sql.=",".$this->date_created[0].""; 
                $sql.=",'".$this->id_user_created[0]."'"; 
                $sql.=",".$this->position[0].""; 
                $sql.=') ';
    		    $rs=false;
                $rs=$this->db->sql_execute($sql,$this->db->isInsert);
                return $rs;  
             }
        }  
         else{
                $sql="";
              	$sql.= 'insert into '.$this->tbl. '(type ,template ,status,key_type ,date_created,
                  id_user_created,'.$this->position_fieldname.'
                  ) values( ';
                $sql.="'".$this->type[0]."'";
                $sql.=",'".$this->template[0]."'"; 
                $sql.=",'".$this->status[0]."'"; 
                $sql.=",'".$this->key_type[0]."'"; 
                $sql.=",".$this->date_created[0].""; 
                $sql.=",'".$this->id_user_created[0]."'"; 
                $sql.=",".$this->position[0].""; 
                $sql.=') ';
                $rs=false;
                $rs=$this->db->sql_execute($sql,$this->db->isInsert);
                return $rs;
            }    
       } 
	     function doupdate($id)
       {
        try{
                $rs=false;
                if($this->icon[0]!="")
                {
                    if($this->insertFile($this->icon[0]))
                     {
                        $sql="";
                      	$sql.= 'update  '.$this->tbl. ' set  ';
                        $sql.="type='".$this->type[0]."'";
                        $sql.=",template='".$this->template[0]."'"; 
                        $sql.=",status='".$this->status[0]."'"; 
                        $sql.=",key_type='".$this->key_type[0]."'"; 
                        $sql.=",icon='".$this->icon[0]."'"; 
                        $sql.=",date_updated 	=".$this->date_updated[0].""; 
                        $sql.=",id_user_created='".$this->id_user_updated[0]."'"; 
                        $sql.='  where  id='.$id;
                        $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                         return $rs;
                    }
                    else{
                        return $rs;
                    }
                 }   
            else
                {
                        $sql="";
                      	$sql.= 'update  '.$this->tbl. ' set  ';
                        $sql.="type='".$this->type[0]."'";
                        $sql.=",template='".$this->template[0]."'"; 
                        $sql.=",status='".$this->status[0]."'"; 
                        $sql.=",key_type='".$this->key_type[0]."'"; 
                        $sql.=",date_updated 	=".$this->date_updated[0].""; 
                        $sql.=",id_user_created='".$this->id_user_updated[0]."'"; 
                        $sql.='  where  id='.$id;
                		$rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                        return $rs;
                }
             
             }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
       }
      function insertFile($file)
      {
        $newname =  uniqid("").".gif";
        $newname = "icon"."_".$newname;
        $image_path= $newname;
        $result="";
         if(move_uploaded_file($file,$this->savefolder.$newname))
         {  
            $this->icon[0]=$newname;
            return true;
          } 
          return false;
        
      } 
    }
    
  


?>