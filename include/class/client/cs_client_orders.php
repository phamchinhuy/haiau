<?php

class cs_client_orders
{
    public $tbl = "orders";
    
    public $id                  = array();
    public $id_user             = array();
    public $total               = array();
    public $subtotal            = array();
    public $status              = array();
    public $date_created        = array();
    public $notes               = array();
    public $detail              = array();
    public $ip_address          = array();
    public $validation_code     = array();
    public $id_account          = array();
    public $date_updated        = array();
    public $id_user_deleted     = array();
    public $date_deleted        = array();
    
    public $id_fieldname                = "id";
    public $id_user_fieldname           = "id_user";
    public $total_fieldname             = "total";
    public $subtotal_fieldname          = "subtotal";
    public $status_fieldname            = "status";
    public $date_created_fieldname      = "date_created";
    public $notes_fieldname             = "notes";
    public $detail_fieldname            = "detail";
    public $ip_address_fieldname        = "ip_address";
    public $validation_code_fieldname   = "validation_code";
    public $id_account_fieldname        = "id_account";
    public $date_updated_fieldname      = "date_updated";
    public $id_user_deleted_fieldname   = "id_user_deleted";
    public $date_deleted_fieldname      = "date_deleted";
    
    public function insert_order()
    {
        $db = new db();
        $sql = "";
        $sql = " insert into orders 
                (
                    ".$this->id_user_fieldname."
                    ,". $this->total_fieldname."
                    ,". $this->subtotal_fieldname."
                    ,". $this->status_fieldname."
                    ,". $this->date_created_fieldname."
                    ,". $this->notes_fieldname."
                    ,". $this->ip_address_fieldname."
                )
                values
                (
                    ".$this->id_user[0]."
                    ,". $this->total[0]."
                    ,". $this->subtotal[0]."
                    ,'". $this->status[0]."'
                    ,now()
                    ,'". $this->notes[0]."'
                    ,'". $this->ip_address[0]."'
                )";
        $rs=null;
        $strIDOrderMax = "";
        $rs = $db->sql_execute($sql, $db->isInsert);
        {
            $strIDOrderMax = $this->getMaxIDOrder();
        }
        return $strIDOrderMax;
    }
    
    public function getMaxIDOrder()
    {
        $str = "";
        $db = new db();
        $sql = " select max(id) as id from ".$this->tbl;
        $rs = null; 
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            foreach($rs as $row)
            {
                $str = $row[$this->id_fieldname];
            }
        }
        return $str;
    }
    
    
}

?>