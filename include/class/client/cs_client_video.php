<?php
    class cs_client_video //create by tran van tam 27/4/2010
    {
        
        //table name
        public $tbl = "video";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $video               = array();
        public $short_description   = array();
        public $long_description    = array();
        public $small_image         = array();
        public $large_image         = array();
        public $status              = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $url                 = array();
        public $url_video           = array();
        public $file_extention      = array();
        public $show_page           = array();
        public $count_view          = array();
        public $is_comment          = array();
        public $keyword             = array();
        public $page_title          = array();
        public $meta_description    = array();
        public $meta_keyword        = array();
        public $begin_date          = array();
        public $begin_end           = array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $video_fieldname             = "video";
        public $short_description_fieldname = "short_description";
        public $long_description_fieldname  = "long_description";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $status_fieldname            = "status";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $url_fieldname               = "url";
        public $url_video_fieldname         = "url_video";
        public $file_extention_fieldname    = "file_extention";
        public $show_page_fieldname         = "show_page";
        public $count_view_fieldname        = "count_view";
        public $is_comment_fieldname        = "is_comment";
        public $keyword_fieldname           = "keyword";
        public $page_title_fieldname        = "page_title";
        public $meta_description_fieldname  = "meta_description";
        public $meta_keyword_fieldname      = "meta_keyword";
        public $begin_date_fieldname        = "begin_date";
        public $begin_end_fieldname         = "begin_end";
       
        //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_client_video();
           
        }
        
        //23-02-2011
        //thuy nguyen
         function getsource($rs)
        {
                   if($rs!=null && $rs->rowCount()>0)
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                       
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_category[$i]          = $row[$this->id_category_fieldname];
                        $this->video[$i]                 = $row[$this->video_fieldname];
                        if(strlen($this->video[$i])>70)
                            $this->video[$i]=$this->video[$i];
                        $this->small_image[$i]          = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->short_description[$i]    = $row[$this->short_description_fieldname];
                       
                        $this->short_description[$i]    = $this->short_description[$i];
                        $this->long_description[$i]     = $row[$this->long_description_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->file_extention[$i]       = $row[$this->file_extention_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->url_video[$i]            = $row[$this->url_video_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i]           = $row[$this->count_view_fieldname];
                        $this->begin_date[$i]           = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i]            = $row[$this->begin_end_fieldname];
                        $i++;
                    }
                }
        }
        //23/2/2011
        //thuy nguyen
         public function select_video_by_id_category_status_nguyen($p_id_category, $p_status)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc
                        "; 
               //echo $sql;
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                $this->getsource($rs);
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //27-04-2010
        //Tam
        public function select_video_by_id_category_status($p_id_category, $p_status)
        {
            try
            {
                //$connect  = sql_connect_default();
                
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."='".$p_id_category."' 
                                and ".$this->status_fieldname." = '".$p_status."'
                                order by ".$this->id_fieldname." desc";
                 echo $sql;           
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname); 
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->video[$i] = mysql_result($result,$i,$this->video_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->file_extention[$i] = mysql_result($result,$i,$this->file_extention_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                        
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from video
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname); 
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->video[$i] = mysql_result($result,$i,$this->video_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->file_extention[$i] = mysql_result($result,$i,$this->file_extention_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category1($p_id_category)
        {
            try
            {
                $connect  = sql_connect_default();
                echo "huhuhuhu";
                $sql = "";
                $sql = "select *  
                        from video 
                        where   id_category=".$p_id_category." 
                                and status < 2
                                order by `id` desc";
                  echo $sql;              
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname); 
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->video[$i] = mysql_result($result,$i,$this->video_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->file_extention[$i] = mysql_result($result,$i,$this->file_extention_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from video 
                        where   id_category=".$p_id_category." 
                                and status < 2
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname); 
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->video[$i] = mysql_result($result,$i,$this->video_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->file_extention[$i] = mysql_result($result,$i,$this->file_extention_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_video_by_id($p_id)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from video
                        where   ".$this->id_fieldname."=".$p_id." order by `id`"; 
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname); 
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->video[$i] = mysql_result($result,$i,$this->video_fieldname);
                        $this->short_description[$i] = mysql_result($result,$i,$this->short_description_fieldname);
                        $this->long_description[$i] = mysql_result($result,$i,$this->long_description_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->file_extention[$i] = mysql_result($result,$i,$this->file_extention_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        //27-04-2010
        public function update_video_set_status($p_status,$p_id)
        {
            try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
        }
        //END
         public function update_video_set_comment($status_comment,$p_id)
        {
            try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->is_comment_fieldname."='".$status_comment."' 
                            where ".$this->id_fieldname."=".$p_id;
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
        }
        //END
        //27-04-2010 created by tran van tam
        public function delete_video_by_id($p_id)
        {
            try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = " delete from ".$this->tbl." where ".$this->id_fieldname."=".$p_id;
                                              
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
        }
        //END
    }
?>