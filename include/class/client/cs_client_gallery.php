<?php
    class cs_client_gallery
    {
        
        //table name
        public $tbl = "gallery";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $title               = array();
        public $small_image         = array();
        public $large_image         = array();
        public $position            = array();
        public $status              = array();
        public $url                 = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $image_note          = array();
        public $image_x             = array();
        public $image_y             = array();
        public $alt                 = array();
        public $keyword             = array();
        public $page_title          = array();
        public $meta_description    = array();
        public $meta_keyword        = array();
        public $show_page           = array();
        public $is_comment          = array();
        public $count_view          = array();
        public $begin_date          = array();
        public $begin_end           = array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $title_fieldname             = "title";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $position_fieldname          = "position";
        public $status_fieldname            = "status";
        public $url_fieldname               = "url";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $image_note_fieldname        = "image_note";
        public $image_x_fieldname           = "image_x";
        public $image_y_fieldname           = "image_y";
        public $alt_fieldname               = "alt";
        public $keyword_fieldname           = "keyword";
        public $page_title_fieldname        = "page_title";
        public $meta_description_fieldname  = "meta_description";
        public $meta_keyword_fieldname      = "meta_keyword";
        public $show_page_fieldname         = "show_page";
        public $is_comment_fieldname        = "is_comment";
        public $count_view_fieldname        = "count_view";
        public $begin_date_fieldname        = "begin_date";
        public $begin_end_fieldname         = "begin_end";
        //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_client_gallery();
        }
        
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id($p_id)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_fieldname."=".$p_id." 
                        and ".$this->status_fieldname."='1'";
                        
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->title[$i] = mysql_result($result,$i,$this->title_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->position[$i] = mysql_result($result,$i,$this->position_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->image_note[$i] = mysql_result($result,$i,$this->image_note_fieldname);
                        $this->image_x[$i] = mysql_result($result,$i,$this->image_x_fieldname);
                        $this->image_y[$i] = mysql_result($result,$i,$this->image_y_fieldname);
                        $this->alt[$i] = mysql_result($result,$i,$this->alt_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_not_limit($p_id_category, $p_status)
        {
            try
            {
               $db=new db();
                $sql = "";
                 $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc";
                        
                $result = null;
                $result = $db->sql_execute($sql,$db->isSelect);
                $countRow = 0;
                if($result!="")
                if($result->rowCount()>0)
                {
                    $i=0;
                    foreach($result as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] =$row[$this->id_category_fieldname];
                        $this->title[$i] = $row[$this->title_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->position[$i] =$row[$this->position_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->url[$i] = $row[$this->url_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] =$row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] =$row[$this->id_user_deleted_fieldname];
                        $this->image_note[$i] = $row[$this->image_note_fieldname];
                        $this->image_x[$i] = $row[$this->image_x_fieldname];
                        $this->image_y[$i] = $row[$this->image_y_fieldname];
                        $this->alt[$i] =$row[$this->alt_fieldname];
                        $this->keyword[$i] = $row[$this->keyword_fieldname];
                        $this->page_title[$i] =$row[$this->page_title_fieldname];
                        $this->meta_description[$i] =$row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $this->show_page[$i] =$row[$this->show_page_fieldname];
                        $this->is_comment[$i] =$row[$this->is_comment_fieldname];
                        $this->count_view[$i] =$row[$this->count_view_fieldname];
                        $this->begin_date[$i] = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i] = $row[$this->begin_end_fieldname];
                        $i++;
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
         
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_limit($p_id_category, $p_status , $p_start, $p_count)
        {
           
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                
                $db = new db();
                $rs = null;
                $rs = $db->sql_execute($sql, $db->isSelect);
                if($rs!=null)
                {
                    $i = 0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname];
                        $this->id_category[$i] = $row[$this->id_category_fieldname];
                        $this->title[$i] = $row[$this->title_fieldname];
                        $this->small_image[$i] = $row[$this->small_image_fieldname];
                        $this->large_image[$i] = $row[$this->large_image_fieldname];
                        $this->position[$i] = $row[$this->position_fieldname];
                        $this->status[$i] = $row[$this->status_fieldname];
                        $this->url[$i] = $row[$this->url_fieldname];
                        $this->date_created[$i] = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname];
                        $this->image_note[$i] = $row[$this->image_note_fieldname];
                        $this->image_x[$i] = $row[$this->image_x_fieldname];
                        $this->image_y[$i] = $row[$this->image_y_fieldname];
                        $this->alt[$i] = $row[$this->alt_fieldname];
                        $this->keyword[$i] = $row[$this->keyword_fieldname];
                        $this->page_title[$i] = $row[$this->page_title_fieldname];
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                        $this->show_page[$i] = $row[$this->show_page_fieldname];
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                        $this->count_view[$i] = $row[$this->count_view_fieldname];
                        $this->begin_date[$i] = $row[$this->begin_date_fieldname];
                        $this->begin_end[$i] = $row[$this->begin_end_fieldname];
                        $i++;
                    }
                }
        
                
                  
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_status($p_id_category, $p_status , $p_start, $p_count)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname."='".$p_status."'
                        order by `id` desc
                        limit ".$p_start.",".$p_count.""; 
                //echo $sql;
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->id_category[$i] = mysql_result($result,$i,$this->id_category_fieldname);
                        $this->title[$i] = mysql_result($result,$i,$this->title_fieldname);
                        $this->small_image[$i] = mysql_result($result,$i,$this->small_image_fieldname);
                        $this->large_image[$i] = mysql_result($result,$i,$this->large_image_fieldname);
                        $this->position[$i] = mysql_result($result,$i,$this->position_fieldname);
                        $this->status[$i] = mysql_result($result,$i,$this->status_fieldname);
                        $this->url[$i] = mysql_result($result,$i,$this->url_fieldname);
                        $this->date_created[$i] = mysql_result($result,$i,$this->date_created_fieldname);
                        $this->id_user_created[$i] = mysql_result($result,$i,$this->id_user_created_fieldname);
                        $this->date_updated[$i] = mysql_result($result,$i,$this->date_updated_fieldname);
                        $this->id_user_updated[$i] = mysql_result($result,$i,$this->id_user_updated_fieldname);
                        $this->date_deleted[$i] = mysql_result($result,$i,$this->date_deleted_fieldname);
                        $this->id_user_deleted[$i] = mysql_result($result,$i,$this->id_user_deleted_fieldname);
                        $this->image_note[$i] = mysql_result($result,$i,$this->image_note_fieldname);
                        $this->image_x[$i] = mysql_result($result,$i,$this->image_x_fieldname);
                        $this->image_y[$i] = mysql_result($result,$i,$this->image_y_fieldname);
                        $this->alt[$i] = mysql_result($result,$i,$this->alt_fieldname);
                        $this->keyword[$i] = mysql_result($result,$i,$this->keyword_fieldname);
                        $this->page_title[$i] = mysql_result($result,$i,$this->page_title_fieldname);
                        $this->meta_description[$i] = mysql_result($result,$i,$this->meta_description_fieldname);
                        $this->meta_keyword[$i] = mysql_result($result,$i,$this->meta_keyword_fieldname);
                        $this->show_page[$i] = mysql_result($result,$i,$this->show_page_fieldname);
                        $this->is_comment[$i] = mysql_result($result,$i,$this->is_comment_fieldname);
                        $this->count_view[$i] = mysql_result($result,$i,$this->count_view_fieldname);
                        $this->begin_date[$i] = mysql_result($result,$i,$this->begin_date_fieldname);
                        $this->begin_end[$i] = mysql_result($result,$i,$this->begin_end_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        //END
        
        
    }
?>