
<?php 
//created by tran van tam 6/5/2010
class cs_client_faq 
{
    //name of table
    public $tbl="faq";
    //name of properties of class
    public $id                      = Array();
    public $fullname                = Array();
    public $address                 = Array();
    public $email                   = Array();
    public $phone                   = Array();
    public $question                = Array();
    public $dateCreatedQuestion     = Array();
    public $reply                   = Array();
    public $dateCreatedReply        = Array();
    public $status                  = Array();
    //name of fields
    public $id_fieldname            = "id";
    public $fullname_fieldname      ="fullname";
    public $address_fieldname       ="address";
    public $email_fieldname         ="email";
    public $phone_fieldname         ="phone";
    public $question_fieldname      ="question";
    public $dateCreatedQuestion_fieldname="dateCreatedQuestion";
    public $reply_fieldname         ="reply";
    public $dateCreatedReply_fieldname="dateCreatedReply";
    public $status_fieldname        ="status";
    
    protected $DataObject;
    function _construct()
    {
        $this->DataObject = new cs_client_faq();
    }
    public function insertFAQ()
    {
        try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->fullname_fieldname."
                                ,".$this->address_fieldname."
                                ,".$this->email_fieldname."
                                ,".$this->phone_fieldname."
                                ,".$this->question_fieldname."
                                ,".$this->dateCreatedQuestion_fieldname."
                                ,".$this->reply_fieldname."
                                ,".$this->dateCreatedReply_fieldname."
                                ,".$this->status_fieldname."
                            
                            ) 
                            values 
                            ('".$this->fullname[0]."'
                                , '".$this->address[0]."'
                                , '".$this->email[0]."'
                                , '".$this->phone[0]."'
                                , '".$this->question[0]."'
                                , '".$this->dateCreatedQuestion[0]."'
                                , '".$this->reply[0]."'
                                , '".$this->dateCreatedReply[0]."'
                                , '".$this->status[0]."'
                            )";
                //echo $sql;
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
    }
    public function updateFAQ()
    {
        try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->fullname_fieldname."='".$this->fullname[0]."'
                                , ".$this->address_fieldname."='".$this->address[0]."'
                                , ".$this->email_fieldname."='".$this->email[0]."'
                                , ".$this->phone_fieldname."='".$this->phone[0]."'
                                , ".$this->question_fieldname."='".$this->question[0]."'
                                , ".$this->dateCreatedQuestion_fieldname."='".$this->dateCreatedQuestion[0]."'
                                , ".$this->reply_fieldname."='".$this->reply[0]."'
                                , ".$this->dateCreatedReply_fieldname."='".$this->dateCreatedReply[0]."'
                                , ".$this->status_fieldname."='".$this->status[0]."'
                            where ".$this->id_fieldname."=".$this->id[0];
               
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
    }
    
        public function selectFAQ()
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from faq where status=1
                        order by `dateCreatedQuestion`"; 
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->fullname[$i] = mysql_result($result,$i,$this->fullname_fieldname); 
                        $this->address[$i] = mysql_result($result,$i,$this->address_fieldname);
                        $this->email[$i] = mysql_result($result, $i, $this->email_fieldname);
                        $this->phone[$i] = mysql_result($result,$i,$this->phone_fieldname);
                        $this->question[$i] = mysql_result($result, $i, $this->question_fieldname);
                        $this->dateCreatedQuestion[$i] = mysql_result($result, $i, $this->dateCreatedQuestion_fieldname);
                        $this->reply[$i] = mysql_result($result, $i, $this->reply_fieldname);
                        $this->dateCreatedReply[$i] = mysql_result($result, $i, $this->dateCreatedReply_fieldname);
                        $this->status[$i] = mysql_result($result, $i, $this->status_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        
        public function selectFAQLimit($p_start, $p_end)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from faq  where status=1
                        order by `dateCreatedQuestion` desc
                        limit ".$p_start.", ".$p_end;
                                
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->fullname[$i] = mysql_result($result,$i,$this->fullname_fieldname); 
                        $this->address[$i] = mysql_result($result,$i,$this->address_fieldname);
                        $this->email[$i] = mysql_result($result, $i, $this->email_fieldname);
                        $this->phone[$i] = mysql_result($result,$i,$this->phone_fieldname);
                        $this->question[$i] = mysql_result($result, $i, $this->question_fieldname);
                        $this->dateCreatedQuestion[$i] = mysql_result($result, $i, $this->dateCreatedQuestion_fieldname);
                        $this->reply[$i] = mysql_result($result, $i, $this->reply_fieldname);
                        $this->dateCreatedReply[$i] = mysql_result($result, $i, $this->dateCreatedReply_fieldname);
                        $this->status[$i] = mysql_result($result, $i, $this->status_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        
        public function selectFAQById($p_id)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from faq
                        where id='".$p_id."'"; 
                $result = null;
                $result = mysql_query($sql,$connect);
                $countRow = 0;
                $countRow = @mysql_num_rows($result);
                if($countRow>0)
                {
                    for($i=0;$i<$countRow;$i++)
                    {
                        $this->id[$i] = mysql_result($result,$i,$this->id_fieldname);
                        $this->fullname[$i] = mysql_result($result,$i,$this->fullname_fieldname); 
                        $this->address[$i] = mysql_result($result,$i,$this->address_fieldname);
                        $this->email[$i] = mysql_result($result, $i, $this->email_fieldname);
                        $this->phone[$i] = mysql_result($result,$i,$this->phone_fieldname);
                        $this->question[$i] = mysql_result($result, $i, $this->question_fieldname);
                        $this->dateCreatedQuestion[$i] = mysql_result($result, $i, $this->dateCreatedQuestion_fieldname);
                        $this->reply[$i] = mysql_result($result, $i, $this->reply_fieldname);
                        $this->dateCreatedReply[$i] = mysql_result($result, $i, $this->dateCreatedReply_fieldname);
                        $this->status[$i] = mysql_result($result, $i, $this->status_fieldname);
                    }
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_free_result($result);
            mysql_close($connect);
        }
        public function setFAQidStatus($p_status, $p_id)
        {
             try
            {
                $rs = false;
                $connect  = sql_connect_default();
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
                  
                $result = null;
                $result = mysql_query($sql,$connect);
                if($result!=null)
                    $rs = true;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
        }
        //check email is existed in faq table
        public function existEmail($email)
        {
            try
            {
                $rs =true;
                $connect  = sql_connect_default(); 
                $sql = "Select * from ".$this->tbl." where ".$this->email_fieldname."= '$email'";
                           
                $result = null;
                $result = mysql_query($sql,$connect);
                
                if(mysql_num_rows($result)!=0)
                    $rs = false;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            mysql_close($connect);
            return $rs;
        }
        //END
}
?>
