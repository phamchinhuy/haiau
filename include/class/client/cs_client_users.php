<?php
class cs_client_users
{   
    //table name
    public $tbl = "users";
    
    public $id                  = array();
    public $username            = array();
    public $password            = array();
    public $name                = array();
    public $address             = array();
    public $email               = array();
    public $phone               = array();
    public $birthday            = array();
    public $code_authentication = array();
    public $status              = array();
    public $last_login          = array();
    public $ip                  = array();
    public $date_register       = array();
    public $date_update       = array();
    
    
    public $id_fieldname                    = "id";
    public $username_fieldname              = "username";
    public $password_fieldname              = "password";
    public $name_fieldname                  = "name";
    public $address_fieldname               = "address";
    public $email_fieldname                 = "email";
    public $phone_fieldname                 = "phone";
    public $birthday_fieldname              = "birthday";
    public $code_authentication_fieldname   = "code_authentication";
    public $status_fieldname                = "status";
    public $last_login_fieldname            = "last_login";
    public $ip_fieldname                    = "ip";
    public $date_register_fieldname         = "date_register";
    public $date_update_fieldname           = "date_update";
    
    
    public function insert_user()
    {
        $db = new db();
        $sql = "";
        $sql.=" insert into users 
                (
                    ".$this->username_fieldname."
                    ,".$this->password_fieldname."
                    ,".$this->name_fieldname."
                    ,".$this->address_fieldname."
                    ,".$this->phone_fieldname."
                    ,".$this->birthday_fieldname."
                    ,".$this->code_authentication_fieldname."
                    ,".$this->status_fieldname."
                    ,".$this->last_login_fieldname."
                    ,".$this->ip_fieldname."
                    ,".$this->date_register_fieldname."
                )
                values
                (
                    '".$this->username[0]."'
                    ,'".$this->password[0]."'
                    ,'".$this->name[0]."'
                    ,'".$this->address[0]."'
                    ,'".$this->phone[0]."'
                    ,'".$this->birthday[0]."'
                    ,'".$this->code_authentication[0]."'
                    ,'".$this->status[0]."'
                    ,'".$this->last_login[0]."'
                    ,'".$this->ip[0]."'
                    ,'".$this->date_register[0]."'
                )";
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isInsert);
        return $rs;
    }
    
    public function select_user_by_username_password($p_username, $p_password, $p_status)
    {
        $db = new db();
        $sql = "";
        $sql .= "   select * 
                    from users 
                    where ".$this->username_fieldname."='".$p_username."' 
                            and ".$this->password_fieldname."='".$p_password."' 
                            and ".$this->status_fieldname."=".$p_status;
        $rs = null;
        $rs=$db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            if($rs->rowCount()>0)
            {
                $i = 0;
                foreach($rs as $row)
                {
                    $this->id[$i]                   = $row[$this->id_fieldname];
                    $this->username[$i]             = $row[$this->username_fieldname];
                    $this->password[$i]             = $row[$this->password_fieldname];
                    $this->name[$i]                 = $row[$this->name_fieldname];
                    $this->address[$i]              = $row[$this->address_fieldname];
                    $this->email[$i]                = $row[$this->email_fieldname];
                    $this->phone[$i]                = $row[$this->phone_fieldname];
                    $this->birthday[$i]             = $row[$this->birthday_fieldname];
                    $this->code_authentication[$i]  = $row[$this->code_authentication_fieldname];
                    $this->status[$i]               = $row[$this->status_fieldname];
                    $this->last_login[$i]           = $row[$this->last_login_fieldname];
                    $this->ip[$i]                   = $row[$this->ip_fieldname];
                    $this->date_register[$i]        = $row[$this->date_register_fieldname];
                    $i++;
                }
            }
        }
    }
    
    public function update_user()
    {
        $db = new db();
        $sql = "";
        $sql.="update users 
                set ".$this->name_fieldname."='".$this->name[0]."'
                    , ".$this->address_fieldname."='".$this->address[0]."'
                    , ".$this->phone_fieldname."='".$this->phone[0]."'
                    , ".$this->date_update_fieldname."='".$this->date_update[0]."'
                where ".$this->id_fieldname."=".$this->id[0];
        echo $sql;
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isUpdate);
        return $rs;
    }
    
    public function select_user_by_id($p_user_id, $p_status)
    {
        $db = new db();
        $sql = "";
        $sql .= "   select * 
                    from users 
                    where ".$this->id_fieldname."=".$p_user_id." 
                            and ".$this->status_fieldname."='".$p_status."'";
        $rs = null;
        $rs=$db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            if($rs->rowCount()>0)
            {
                $i = 0;
                foreach($rs as $row)
                {
                    $this->id[$i]                   = $row[$this->id_fieldname];
                    $this->username[$i]             = $row[$this->username_fieldname];
                    $this->password[$i]             = $row[$this->password_fieldname];
                    $this->name[$i]                 = $row[$this->name_fieldname];
                    $this->address[$i]              = $row[$this->address_fieldname];
                    $this->email[$i]                = $row[$this->email_fieldname];
                    $this->phone[$i]                = $row[$this->phone_fieldname];
                    $this->birthday[$i]             = $row[$this->birthday_fieldname];
                    $this->code_authentication[$i]  = $row[$this->code_authentication_fieldname];
                    $this->status[$i]               = $row[$this->status_fieldname];
                    $this->last_login[$i]           = $row[$this->last_login_fieldname];
                    $this->ip[$i]                   = $row[$this->ip_fieldname];
                    $this->date_register[$i]        = $row[$this->date_register_fieldname];
                    $i++;
                }
            }
        }
    }
    
    
    public function select_user_by_username($p_user_name, $p_status)
    {
        $db = new db();
        $sql = "";
        $sql .= "   select * 
                    from users 
                    where ".$this->username_fieldname."='".$p_user_name."'
                            and ".$this->status_fieldname."='".$p_status."'";
        $rs = null;
        $rs=$db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            if($rs->rowCount()>0)
            {
                $i = 0;
                foreach($rs as $row)
                {
                    $this->id[$i]                   = $row[$this->id_fieldname];
                    $this->username[$i]             = $row[$this->username_fieldname];
                    $this->password[$i]             = $row[$this->password_fieldname];
                    $this->name[$i]                 = $row[$this->name_fieldname];
                    $this->address[$i]              = $row[$this->address_fieldname];
                    $this->email[$i]                = $row[$this->email_fieldname];
                    $this->phone[$i]                = $row[$this->phone_fieldname];
                    $this->birthday[$i]             = $row[$this->birthday_fieldname];
                    $this->code_authentication[$i]  = $row[$this->code_authentication_fieldname];
                    $this->status[$i]               = $row[$this->status_fieldname];
                    $this->last_login[$i]           = $row[$this->last_login_fieldname];
                    $this->ip[$i]                   = $row[$this->ip_fieldname];
                    $this->date_register[$i]        = $row[$this->date_register_fieldname];
                    $i++;
                }
            }
        }
    }
    
    public function update_code_authentication($p_UserID, $p_CodeAuthentication)
    {
        $sql = "";
        $sql = "update users 
                   set code_authentication ='".$p_CodeAuthentication."'
                   where id=".$p_UserID;
        $db = new db();
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isUpdate);
        return $rs;
    }
    
     public function select_user_by_username_codeAuthentication($p_user_name, $p_code_authentication, $p_status)
    {
        $db = new db();
        $sql = "";
        $sql .= "   select * 
                    from users 
                    where ".$this->username_fieldname."='".$p_user_name."'
                            and ".$this->code_authentication_fieldname."='".$p_code_authentication."'
                            and ".$this->status_fieldname."='".$p_status."'";
        $rs = null;
        $rs=$db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            if($rs->rowCount()>0)
            {
                $i = 0;
                foreach($rs as $row)
                {
                    $this->id[$i]                   = $row[$this->id_fieldname];
                    $this->username[$i]             = $row[$this->username_fieldname];
                    $this->password[$i]             = $row[$this->password_fieldname];
                    $this->name[$i]                 = $row[$this->name_fieldname];
                    $this->address[$i]              = $row[$this->address_fieldname];
                    $this->email[$i]                = $row[$this->email_fieldname];
                    $this->phone[$i]                = $row[$this->phone_fieldname];
                    $this->birthday[$i]             = $row[$this->birthday_fieldname];
                    $this->code_authentication[$i]  = $row[$this->code_authentication_fieldname];
                    $this->status[$i]               = $row[$this->status_fieldname];
                    $this->last_login[$i]           = $row[$this->last_login_fieldname];
                    $this->ip[$i]                   = $row[$this->ip_fieldname];
                    $this->date_register[$i]        = $row[$this->date_register_fieldname];
                    $i++;
                }
            }
        }
    }
    
    public function update_user_with_password()
    {
        $db = new db();
        $sql = "";
        $sql.="update users 
                set ".$this->password_fieldname."='".$this->password[0]."'  
                    , date_update = now()
                    , code_authentication  = ''
                where ".$this->id_fieldname."=".$this->id[0];
                
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isUpdate);
        return $rs;
    }
}
?>