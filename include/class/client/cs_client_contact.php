<?php

class cs_client_contact
{
        public $tbl                     ="contact";
        public $id                      =array();
        public $full_name               =array();
        public $company_name  	        =array();
        public $email                   =array();
        public $content                 =array();
        public $date_create             =array();
        public $phone                   =array();
        public $address                 =array();
        //
        private $db="";        
        //column name
  
        public $id_fieldname                                ="id";
        public $phone_fieldname                             ="phone";
        public $full_name_fieldname                         ="full_name";
        public $company_name_fieldname                      ="company_name";
        public $email_fieldname                             ="email";
        public $content_fieldname                           ="content";
        public $date_create_fieldname                       ="date_create";
        public $address_fieldname                           ="address";
               	
        protected $DataObject;
        function  cs_client_contact()
        {
            $this->db=new db();
        }
        
         /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    public function getSource($rs)
    {
         $do_nick_chat="";
         if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->full_name[$i]            = $row[$this->full_name_fieldname];
                        $this->company_name[$i]         = $row[$this->company_name_fieldname];
                        $this->phone[$i]                =$row[$this->phone_fieldname]
  ;                     $this->email[$i]               = $row[$this->email_fieldname];
                        $this->content[$i]              = $row[$this->content_fieldname];
                        $this->date_create[$i]          = $row[$this->date_create_fieldname];
                        $this->address[$i]          = $row[$this->address_fieldname];
                     
                    $i++;
                }
            }
    }
    
    //PHAM THI THU XUAN
    //21/04/2010
    public function select_contact_by_id($p_id_object)
    {
        try
        {   $num=0;   
            $rs="";       
           $sql = "";
            $sql = "select *  
                    from ".$this->tbl."
                    where  ".$this->id_object_fieldname."=".$p_id_object." 
                  ";                        
          if($sql!="")
             {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $this->getSource($rs);
         
           // return $rs->rowCount();
             }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
       
    }
        
    public function select_imgages_by_id_limit($p_id_object,$start,$end)
    {
        try
        {   
            $rs="";
            $num=0;
            $sql = "";
            $sql = "select *  
                    from   ".$this->tbl."                          
                    order by   ".$this->date_create_fieldname." ";   
            $sql.= "  limit  ".$start.",".$end;       
            
             if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);                 
    
                 }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
       
    }         
      //tong so category co status <>3
      
    function do_insert()
    {
        $sql="insert into ".$this->tbl." 
        (
            ".$this->full_name_fieldname."
            , ".$this->company_name_fieldname."
            , ".$this->content_fieldname."
            , ".$this->email_fieldname."
            , ".$this->date_create_fieldname."
            , ".$this->phone_fieldname."
            , ".$this->address_fieldname."
        ) 
        values
        (
            '".$this->full_name[0]."'
            , '".$this->company_name[0]."'
            , '".$this->content[0]."'
            , '".$this->email[0]."'
            , now()
            , '".$this->phone[0]."'
            , '".$this->address[0]."'
        )
        ";
        
       $rs=0;
       $db=new db();
       if($sql!="")
       {
            $rs = $this->db->sql_execute($sql,$this->db->isInsert);
       }
       return $rs;    
    }        	                       
}        
    

?>