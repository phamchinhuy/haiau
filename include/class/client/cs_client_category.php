<?php

class cs_client_category
{
    public $tbl                 ="category";
    public $categoryId          =array();
    public $id                  =array();
    //public $postion             =0;
    public $id_type_category    =array();
    public $id_site             =array();
    public $category            =array();
    public $status              =array();
    public $id_parent           =array();
    public $small_images        =array();
    public $large_images        =array();
    public $music               =array();
    public $order               =array();
    public $url                 =array();
    public $target              =array();
    public $show_subCategory    =array();
    public $date_created        =array();
    public $id_user_created     =array();
    public $date_updated        =array();
    public $date_deleted        =array();
    public $id_user_deleted     =array();
    public $id_user_updated     =array();
    public $keyword             =array();
    public $page_title          =array();
    public $meta_description    =array();
    public $meta_keyword        =array();
    public $show_page           =array();
    public $id_music_background =array();
    public $template            =array();
    public $re_urls             =array();
    public $small_image = array();
    public $large_image = array();
    public $positons=array();
    private $_connection = '';
    private $_cursor = null;
    private $_sql = '';
    public $categoryType=array();
  
    public $id_parent_sected=array();
    
    //file
    private $tbfile="file";
    private $savefolder="uploads/";
    private $filealt=array();
    private $tbfile_link="file_link";
    private $id_object=array();
    private $object_type=array();
    private $id_file=array();
    private  $id_file_detail =array();
    private $file_status=array();
    
    //xu ly
    public $file_type=array();	
    public $start="";
    public $end="";
    
    
    //column name
    public $id_fieldname                    = "id";
    public $id_type_category_fieldname      = "id_type_category";
    public $id_site_fieldname               = "id_site";
    public $category_fieldname              = "category";
    public $status_fieldname                = "status";
    public $id_parent_fieldname             = "id_parent";
    public $small_image_fieldname           = "small_image";
    public $large_image_fieldname           = "large_image";
    public $order_fieldname                 = "`order`";
    public $url_fieldname                   = "url";
    public $target_fieldname                = "target";
    public $show_subCategory_fieldname      = "show_subCategory";
    public $date_created_fieldname          = "date_created";
    public $id_user_created_fieldname       = "id_user_created";
    public $date_updated_fieldname          = "date_updated";
    public $id_user_updated_fieldname       = "id_user_updated";
    public $date_deleted_fieldname          = "date_deleted";
    public $id_user_deleted_fieldname       = "id_user_deleted";
    public $keyword_fieldname               = "keyword";
    public $page_title_fieldname            = "page_title";
    public $meta_description_fieldname      = "meta_description";
    public $meta_keyword_fieldname          = "meta_keyword";
    public $show_page_fieldname             = "show_page";
    public $id_music_background_fieldname   = "id_music_background";
    public $template_fieldname              = "template";
    public $position_fielname               = "position";
    

    
    protected $DataObject;
    function _construct()
    {
        $this->DataObject = new cs_client_category();
    }
    
    
     /**
     * DESCRIBE FUNCTION: 
     * select category theo id site va id category type
     * 
     * parameter:
     * $p_id_site : id site 
     * $p_key_cateType: key category type
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
     public function select_category_by_idSite_idCategoryType($p_id_site, $p_key_cateType, $p_parent_id)
     {
        $sql = "";
        $sql = "select c.* 
                from category as c, type_category as tc, sites as site
                where tc.key_type = '".$p_key_cateType."' 
                        and c.id_type_category = tc.id
                        and site.id = '".$p_id_site."' 
                        and site.status = '1'
                        and c.status='1'
                        and c.id_parent=".$p_parent_id."
                        and c.id_site = site.id 
                order by  c.order asc";
      
        $db = new db();
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            $this->getsource($rs);
        }
     } 
    
     /**
     * DESCRIBE FUNCTION:  select_category_by_template
     * select category theo template
     * 
     * parameter:
     * $p_page_title : page title category 
     * 
     * 
     * information of coder:
     * Company : NewSunSoft
     * coder: xuan.pham
     * date created: 17/07/2010
     */
    public function select_category_by_page_title($p_page_title)
    {
        try
        {
            $db = new db();
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where ".$this->page_title_fieldname."='".$p_page_title."'
                            and status=1";
            //echo $sql;
            $rs = null;
            $rs =$db->sql_execute($sql,$db->isSelect);
            $countRow = 0;
            
            if($rs!=null)
            {
                $this->getsource($rs);
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    public function select_category_by_page_title_site($p_page_title, $p_site)
    {
        try
        {
            $db = new db();
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where ".$this->page_title_fieldname."='".$p_page_title."' 
                            and id_site=".$p_site." ";
           
            $rs = null;
            $rs =$db->sql_execute($sql,$db->isSelect);
            $countRow = 0;
            
            if($rs!=null)
            {
                $this->getsource($rs);
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
    }
    //END
    
    //PHAM THI THU XUAN
    //21/04/2010
    public function select_category_by_id($p_id)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where ".$this->id_fieldname."='".$p_id."'
                   ";
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!=null)
            {
               $this->getsource($rs);
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
    //END
    
    
    //PHAM THI THU XUAN
    //21/04/2010
    public function getsource($rs)
    {
          if($rs!=null)
            {
                if($rs->rowCount()>0)
                {
                    $i = 0;
                    foreach($rs as $row)
                    {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_type_category[$i]     = $row[$this->id_type_category_fieldname];
                        $this->id_site[$i]              = $row[$this->id_site_fieldname];
                        $this->category[$i]             = $row[$this->category_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->id_parent[$i]            = $row[$this->id_parent_fieldname];
                        $this->small_images[$i]         = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->order[$i]                = $row["order"];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->show_subCategory[$i]     = $row[$this->show_subCategory_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->keyword[$i]              = $row[$this->keyword_fieldname];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         = $row[$this->meta_keyword_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->id_music_background[$i]  = $row[$this->id_music_background_fieldname];
                        $this->template[$i]             = $row[$this->template_fieldname];
                        $this->positons[$i]             = $row[$this->position_fielname];
                        $i++;
                    }
                }
            }
    }
    
    public function select_category_by_parent_id($p_id_parent)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where  ".$this->id_parent_fieldname."='".$p_id_parent."'
                            and ".$this->status_fieldname."=1 
                    order by  ".$this->order_fieldname." asc";
            
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!="")
            {
                $this->getsource($rs);
            }
           
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
      
    }
    
    public function select_category_by_parent_id_not_order_limit($p_id_parent, $p_order, $p_start, $p_end)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where  ".$this->id_parent_fieldname."='".$p_id_parent."'
                            and ".$this->status_fieldname."=1 
                            and ".$this->order_fieldname.">".$p_order."
                            
                    order by  ".$this->order_fieldname." asc
                    limit ".$p_start.",".$p_end."
                    
                    ";
            
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!="")
            {
                $this->getsource($rs);
            }
           
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
      
    }
    
    public function select_category_by_parent_id_limit($p_id_parent, $p_start, $p_end)
    {
        try
        {
            $sql = "";
            $sql = "
                        select *  
                        from ".$this->tbl." 
                        where  ".$this->id_parent_fieldname."='".$p_id_parent."'
                                and ".$this->status_fieldname."=1 
                        order by  ".$this->order_fieldname." asc
                        limit ".$p_start.",".$p_end."
                    ";
                    
            
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!="")
            {
                $this->getsource($rs);
            }
           
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
      
    }
    
    
    public function select_category_by_parent_id_active($p_id_parent)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where status=1 and  ".$this->id_parent_fieldname."='".$p_id_parent."' order by  ".$this->order_fieldname;
           //echo $sql;
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!="")
            {
                $this->getsource($rs);
            }
           
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
      
    }
    
    public function select_category_by_id_site_id_type_category($p_id_site, $p_id_type_category)
     {
        $sql = "";
        $sql = "select * 
                from ".$this->tbl."
                where ".$this->id_site_fieldname."=".$p_id_site."
                        and ".$this->id_type_category_fieldname."=".$p_id_type_category."
                        and ".$this->status_fieldname."=1
                order by position asc";
        
        $db = new db();
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            $this->getsource($rs);
        }
     } 
    //END
    
    
     //thuy nguyen
    //22/02/2011
    public function select_category_by_id_type_category($p_idtype)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where ".$this->id_type_category_fieldname ."='".$p_idtype."'
                    and status= 1
                   ";
                   //echo $sql;
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!=null)
            {
               $this->getsource($rs);
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
     //thuy nguyen
    //28/02/2011
    public function select_category_by_id_type_category_id_parent($p_idtype,$p_idparent)
    {
        try
        {
            $sql = "";
            $sql = "select *  
                    from ".$this->tbl." 
                    where ".$this->id_type_category_fieldname ."='".$p_idtype."'
                    and ".$this->id_parent_fieldname."=".$p_idparent."
                    and status= 1
                    
                    order by ".$this->order_fieldname." asc
                   ";
                   //echo $sql;
            $db = new db();
            $rs = null;
            $rs = $db->sql_execute($sql, $db->isSelect);
            $countRow = 0;
            if($rs!=null)
            {
               $this->getsource($rs);
            }
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
        
    }
}
?>
