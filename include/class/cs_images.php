<?php

class cs_images
{
        public $tbl                     ="images";
        public $id_object               =array();
        public $id                      =array();
        public $object_type             =array();
        public $status                  =array();
        public $path_image_thumb        =array();
        public $path_image_large        =array();
        public $alt                     =array();
        //
        private $db="";        
        //column name
        public $id_object_fieldname               ="id_object";
        public $id_fieldname                      ="id";
        public $object_type_fieldname             ="object_type";
        public $status_fieldname                  ="status";
        public $path_image_thumb_fieldname        ="path_image_thumb";
        public $path_image_large_fieldname        ="path_image_large";
        public $alt_fieldname                     ="alt";       	
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_images();
            $this->db=new db();  
        }
        function  cs_images()
        {
            $this->db=new db();
        }
        
         /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    public function getSource($rs)
    {
         $do_nick_chat="";
         if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_object[$i]            = $row[$this->id_object_fieldname];
                        $this->object_type[$i]          = $row[$this->object_type_fieldname];
                        
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->path_image_large[$i]     = $row[$this->path_image_large_fieldname];
                        $this->path_image_thumb[$i]     = $row[$this->path_image_thumb_fieldname];
                        $this->alt[$i]                  = $row[$this->alt_fieldname];
                    $i++;
                }
            }
    }
        //PHAM THI THU XUAN
        //21/04/2010
          public function select_imgages_by_id_object($p_id_object)
        {
            try
            {   $num=0;   
                $rs="";       
               $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where  ".$this->id_object_fieldname."=".$p_id_object." and ".$this->status_fieldname." <3                 
                        order by ".$this->id_fieldname." ";                        
              if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
             
               // return $rs->rowCount();
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
                 public function select_imgages_by_id_($p_id)
        {
            try
            {   $num=0;   
                $rs="";       
               $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where  ".$this->id_fieldname."=".$p_id." and ".$this->status_fieldname." <3                 
                        order by ".$this->id_fieldname." ";                        
              if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
             
               // return $rs->rowCount();
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
            public function select_imgages_by_id_object_limit($p_id_object,$start,$end)
        {
            try
            {   
                $rs="";
                $num=0;
                $sql = "";
                $sql = "select *  
                        from   ".$this->tbl."
                        where  ".$this->id_object_fieldname."=".$p_id_object." and ".$this->status_fieldname."  <3                 
                        order by   ".$this->id_fieldname." ";   
                $sql.= "  limit  ".$start.",".$end;       
                
                 if($sql!="")
                     {
                    $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getSource($rs);                 
 
                     }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }         
      //tong so category co status <>3
          function donum()
          {
          $sql="select * from ".$this->tbl." where ".$this->status_fieldname." < 3
           ";
           $num=0;
           if($sql!="")
                {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                    return $rs->rowCount();
                }
        
          }
         
        //lay tong so category theo limit
 	      function doNumL( $l1, $l2){
            $sql="select * from ".$this->tbl." where  id_parent=0  and status <> 3  limit ".$l1.",".$l2;
        $num=0;
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $num=$rs->numRows();
        }
        return $num;
        }     
	   function doinsert()
       {
        $sql="";
      	$sql.= 'insert  into '.$this->tbl.'';
        $sql.='('.$this->id_object_fieldname.',
        '.$this->object_type_fieldname.',
        '.$this->status_fieldname.',
        '.$this->path_image_thumb_fieldname.',
        '.$this->path_image_large_fieldname.', 
        '.$this->alt_fieldname.') 
        values(';
        $sql.="".$this->id_object[0]."";
        $sql.=",'".$this->object_type[0]."'"; 
        $sql.=",'".$this->status[0]."'"; 
        $sql.=",'".$this->path_image_thumb[0]."'"; 
        $sql.=",'".$this->path_image_large[0]."'"; 
        $sql.=",'".$this->alt[0]."')";       
	    if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isInsert);
        else
            return 0;
       }
	    function doupdate($id)
       {
            $sql="";
          	$sql.= 'update  '.$this->tbl. ' set  ';
            $sql.=$this->id_object_fieldname."= ".$this->id_object[0]." ";
            $sql.=",".$this->object_type_fieldname."='".$this->object_type[0]."'  "; 
            $sql.=",".$this->status_fieldname."='".$this->status[0]."'   "; 
            if(isset($this->path_image_large[0])&&$this->path_image_large[0]!="")
            $sql.=",".$this->path_image_large_fieldname."='".$this->path_image_large[0]."'  "; 
            if(isset($this->path_image_thumb[0])&&$this->path_image_thumb[0]!="")
            $sql.=",".$this->path_image_thumb_fieldname."='".$this->path_image_thumb[0]."'   "; 
            $sql.=",".$this->alt_fieldname."='".$this->alt[0]."'   "; 
            $sql.=' where '.$this->id_fieldname.'='.$id;
            
    		if($sql!="")
                 return $this->db->sql_execute($sql, $this->db->isUpdate);
            else
                return 0;
       }
         public function update_images_set_status($p_status,$p_id)
        {
            try
            {
                $rs = false;
               
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
              
                 if($sql!="")
                 {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                 }
                 return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }

        }
           function update_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
            if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
                ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
                ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
             {
                    if($_FILES[$resource]['size']<$size)
                    {
                         $filename= $_FILES[$resource]["type"];
                         
                     switch($filename)
                         {
                            case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                        break;
                             case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                        break;                        
                            case "image/gif"      :   $file    =uniqid("").".gif";
                                                                        break;
                            case "image/png"      :     $file    =uniqid("").".png";
                                                                         break;         
                         }
                        if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
                        {
                             if($f==1)
                             {
                                $this->path_image_large[0]=$file;
                             }
                             else
                             {
                                $this->path_image_thumb[0]=$file;
                             }
                            $flag=true;
                        }
                            
                    }
                     else
                     $flag = false;
                
             }
         return $flag;
            
          }
              catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
          
       }
    
   
    
       
    
}        
    

?>