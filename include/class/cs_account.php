<?php

    class cs_account
    {
        public $tbl="account";
        public $accountId=array();
        public $username=array();
        public $password=array();
        public $fullname=array();
        public $address=array();
        public $email=array();
        public $phone=array();
        public $nick_yahoo =array();
        public $nick_skype=array();
        public $status=array();
        public $date_updated=array();
        public $date_last_login =array();
        
        
        //type user
        public $type_id=array();
        public $type=array();
        public $tbtype="type_user";
        private $_connection = '';
        private $_cursor = null;
        private $_sql = '';
        
        //
        public $user_type =  array();
        public $id =  array();
        
        //field name
        public $id_fieldname = "id";
        public $username_fieldname = "username";
        public $password_fieldname = "password";
        public $user_type_fieldname = "user_type";
        public $fullname_fieldname = "fullname";
        public $address_fieldname = "address";
        public $email_fieldname = "email";
        public $phone_fieldname = "phone";
        public $nick_yahoo_fieldname = "nick_yahoo";
        public $nick_skype_fieldname = "nick_skype";
        public $status_fieldname = "status";
        public $date_updated_fieldname = "date_updated";
        public $date_last_login_fieldname = "date_last_login";
        private $db=null;
   	  
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_account();
               $this->db=new db();
        }
        function cs_account()
        {
            
            $this->db=new db();
        }
        
        
        //23-04-2010
        //XUAN
        public function select_user_by_id_user($p_id_user)
        {
            try
            {         
                $rs = "";
                $sql = "select *  from ".$this->tbl."
                        where  ".$this->id_fieldname." = ".$p_id_user;
                      
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($rs);
                }         
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_user_by_username_password($p_username, $p_password)
        {
            try
            {
                $sql = "";
                $sql = "
                        select *
                        from account
                        where ".$this->username_fieldname."='".$p_username."' 
                                and ".$this->password_fieldname."='".$p_password."' 
                                and status=1";
           
                $rs=""; 
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    if($rs!="")
                    {
                        $this->do_selectall($rs);
                    }
                }      
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        
        
  
          function do_selectall($rsSQL)
          {  
              	 if($rsSQL!=null)
                {            
                    $i=0;
                    foreach($rsSQL as $rowSQL)               
                    { 
                        //category
                       $this->accountId[$i]=$rowSQL[$this->id_fieldname];
                       $this->type[$i]=$rowSQL[$this->user_type_fieldname];
                       $this->username[$i]=$rowSQL[$this->username_fieldname];
                       $this->password[$i]=$rowSQL[$this->password_fieldname];
                       $this->fullname[$i]=$rowSQL[$this->fullname_fieldname];
                       $this->address[$i]=$rowSQL[$this->address_fieldname];
                       $this->email[$i]=$rowSQL[$this->email_fieldname];
                       $this->phone[$i]=$rowSQL[$this->phone_fieldname];
                       $this->nick_yahoo[$i]=$rowSQL[$this->nick_yahoo_fieldname];
                       $this->nick_skype[$i]=$rowSQL[$this->nick_skype_fieldname];
                       $this->status[$i]=$rowSQL[$this->status_fieldname];
                       $this->date_updated[$i]=$rowSQL[$this->date_updated_fieldname];
                       $this->date_last_login[$i]=$rowSQL[$this->date_last_login_fieldname];                            
                        $i++;
                    }    		      
                }
               
          }
          function do_selectall_users($orderby)
          {
             
            try{
                 $sql="select * from ".$this->tbl." order by  ".$orderby;
                 $rs=false;
                 if($sql!="")
                    {
                        $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                        $this->do_selectall($rs);
                    }
             
                }
            catch(exception $e)
                {
                    var_dump($e->getMessage());
                }             
          }
           function do_select_users_id($id)
          {
            try{
                  $sql="select * from ".$this->tbl." where  id=".$id;
                  //echo $sql;
                  $rs="";
                if($sql!="")
                 {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    if($rs!="")
                    $this->do_selectall($rs);
                 }

                }
                catch(exception $e)
                {
                    var_dump($e->getMessage());
                }                        
          }   
           function do_select_Type_id($id)
          {
            
             $sql="select * from ".$this->tbtype." where  id=".$id;
             $result="";
             if($sql!="")
             {
                 $result=$this->db->sql_execute($sql,$this->db->isSelect);
                 foreach($result as $row)
                $this->type[0]=$row["name"];
             }
             
          }
           function do_select_user_name($username)
          {
            
             $sql="select * from account  where  ".$this->username_fieldname."='".$username."'";
           
             $result="";
             if($sql!="")
             {
                 $result=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($result);
                if(count($this->accountId)>0)
                return 1;
                else
                return 0;
             }
             
          }
           function do_select_email($email)
          {
            
             $sql="select * from account  where  ".$this->email_fieldname."='".$email."'";
             
             $result="";
             if($sql!="")
             {
                 $result=$this->db->sql_execute($sql,$this->db->isSelect);
                $this->do_selectall($result);
                if(count($this->accountId)>0)
                return 1;
                else
                return 0;
             }
             
          }
          function donum($type)
          {   
            try
            {
                if($type=="")
                $sql="select * from  ".$this->tbl."  where 	username!='admin' and  ".$this->status_fieldname." < 3 ";
                else
                $sql="select * from  ".$this->tbl."   where user_type='".$type."'   and    ".$this->status_fieldname." <3";
                $rs="";
                $value=0;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    if($rs!="")
                    {
                        $value=$rs->rowCount();
                    }
                }
                return $value;                
            }
             catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
          }
          	function doSelectL($l1,$l2,$orderby,$type)
          {
            try{
                 if($type=="")
    	           $sql="select * from ".$this->tbl." 
                        where username!='admin'
                                and ".$this->status_fieldname." < 3
                        order by  ".$orderby."
                        limit ".$l1.",".$l2;
                else
                    $sql="select * from ".$this->tbl."  where user_type='".$type."' 
                      and    ".$this->status_fieldname." <3
                       order by  ".$orderby."    limit ".$l1.",".$l2;
                if($sql!="")
                 {
                    $rs="";
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->do_selectall($rs);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          	
    	  }
        	function doNumL($l1,$l2,$type)
            {
  	       
             try{
             if($type=="")
  	             $sql="select * from   ".$this->tbl."  limit ".$l1.",".$l2;
             else
                $sql="select * from   ".$this->tbl." where 	user_type='".$type."' 
              and    ".$this->status_fieldname." <3
                 limit ".$l1.",".$l2;
             $num=0;
             $rs="";
             $rs=$this->db->sql_execute($sql,$this->db->isSelect);
             if($rs!="")
                 {
                     $num=$rs->rowCount();   
                 }
              return $num;
             }
             catch(exception $e)
            {
                var_dump($e->getMessage());
            }
	     }
         	function doActive($id, $status)
         {
                try{
                if($status){
			     $status= 0;
		          }
        		else
                {
        			$status= 1;	
        		}
        		$query  = 'update  '.$this->tbl.'  set status="'.$status.'" where id="'.$id.'"';
                $rs=false;
                if($query!="")
                {
                  
                    $rs=$this->db->sql_execute($query,$this->db->isUpdate);
                    return $rs;
                }
    		  return $rs;
                
            }
                catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
	   }
       	function doDelete($id)
        {
            try{
           	    $query  = ' delete from '.$this->tbl.'    where id="'.$id.'"';
                $rs=false;
                if($query!="")
                {
                  
                    $rs=$this->db->sql_execute($query,$this->db->isDelete);
                     return $rs;
                }
    		  return $rs;
                
            }
            catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
	   }
        function doinsert()
       {
        try{
            $sql="";
          	$sql.= 'insert into '.$this->tbl. '(username ,password,	user_type 	 ,fullname,email,nick_yahoo ,
            nick_skype ,status  
            ) values( ';
            $sql.="'".$this->username[0]."'";
            $sql.=",'".$this->password[0]."'"; 
             $sql.=",'".$this->type[0]."'"; 
            $sql.=",'".$this->fullname[0]."'"; 
      
            $sql.=",'".$this->email[0]."'"; 
         
            $sql.=",'".$this->nick_yahoo[0]."'"; 
            $sql.=",'".$this->nick_skype[0]."'"; 
            $sql.=",'".$this->status[0]."'";       
            $sql.=') ';
            $rs=false;
            if($sql!="")
            {
                return $rs=$this->db->sql_execute($sql,$this->db->isInsert);
            }
        }
        catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
       } 
	    function doupdate($id)
       {
        try{
                $sql="";
              	$sql.= ' update '.$this->tbl. '  set  ';
                $sql.="  fullname='".$this->fullname[0]."'";
                $sql.="	,user_type='".$this->type[0]."'";
              
                //$sql.=",email='".$this->email[0]."'"; 
                $sql.=", nick_yahoo='".$this->nick_yahoo[0]."'";  
                $sql.=", nick_skype='".$this->nick_skype[0]."'";
                 $sql.=", status=".$this->status[0]."";  
                $sql.=", date_updated=now()";  
                $sql.='  where  id= '.trim($id);
              
                $rs=false;
                if($sql!="")
                {
                    return $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                } 
                return $rs;      
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
		
    }
    function createTypeUser($id)
    {
         $sql="select * from   ".$this->tbtype;
         $this->setQuery($sql);
         $rsSQL="";
         $rsSQL=$this->query();
         $this->disconnect();
         $com="";
         $com.="<select name='cbtype'>";
         	if($rsSQL!=null)
            {  
                $i=0;
                while($rowSQL =  mysql_fetch_array($rsSQL))
                {
                if($rowSQL['name']==$id)
                    $com.="<option value='".$rowSQL['name']."' selected='selected'>".$rowSQL['name']."</option>";
                 else
                    $com.="<option value='".$rowSQL['name']."' >".$rowSQL['name']."</option>";    
                 $i++;   
                }
            }
         $com.="</select>"; 
         return $com;
    }
    //nguoi viet:hathe1988
    //ngay viet:22042010
    function createTypeUserID($id,$url)
    {
         $sql="select * from   ".$this->tbtype;
         $this->setQuery($sql);
         $rsSQL="";
         $rsSQL=$this->query();
         $this->disconnect();
         $com="";
         $com.="<select name='cbtype' onchange='ClickToURL(this.value)'>";
         	if($rsSQL!=null)
            {  
                $i=0;
                while($rowSQL =  mysql_fetch_array($rsSQL))
                {
                if($rowSQL['id']==$id)
                    $com.="<option value='".$url."&pr=".$rowSQL['id']."' selected='selected'>".$rowSQL['name']."</option>";
                 else
                    $com.="<option value='".$url."&pr=".$rowSQL['id']."' >".$rowSQL['name']."</option>";    
                 $i++;   
                }
            }
         $com.="</select>"; 
         return $com;
    }
     /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong locations
     * 
     * Tham so:$status :trang thai can update cua col status (0,1,2)
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    function doStatus($status,$p_id)
    {
      
       //$do_location_i= $p_do_location;
       $sql="update ".$this->tbl."  set ";
       $sql.=$this->status_fieldname."=".$status;
       $sql.="  where  ".$this->id_fieldname."=".$p_id;
       $result = "";
       //echo $sql;
        if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            return 0;
       
    }
    function do_update_pass($id)
    {
         $sql="";
         $sql.=" update ".$this->tbl. " set ";
         $sql.=" ".$this->password_fieldname."='".$this->password[0]."'";
         $sql.=" ,".$this->date_updated_fieldname."=now()";
         $sql.=" where ".$this->id_fieldname."=".$this->id[0];
         if($sql!="")
           return $this->db->sql_execute($sql,$this->db->isUpdate);
    }
}
?>