<?php

class cs_contact
{
        public $tbl                     ="contact";
        public $id                      =array();
        public $full_name             =array();
        public $company_name  	         =array();
        public $email        =array();
        public $content        =array();
        public $phone        =array();
        public $date_create                     =array();
        public $address                    =array();
        //
        private $db="";        
        //column name
  
        public $id_fieldname                              ="id";
        public $full_name_fieldname                       ="full_name";
        public $phone_fieldname                           ="phone";
        public $company_name_fieldname                    ="company_name";
        public $email_fieldname                           ="email";
        public $content_fieldname                         ="content";
        public $date_create_fieldname                     ="date_create";
        public $address_fieldname                     ="address";  
             	
        protected $DataObject;
        function _construct()
        {
            $this->db=new db();  
        }
        function  cs_contact()
        {
            $this->db=new db();
        }
        
         /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    public function getSource($rs)
    {
         $do_nick_chat="";
         if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->full_name[$i]            = $row[$this->full_name_fieldname];
                        $this->company_name[$i]         = $row[$this->company_name_fieldname];
                        $this->phone[$i]                = $row[$this->phone_fieldname];
                        $this->email[$i]                = $row[$this->email_fieldname];
                        $this->content[$i]              = $row[$this->content_fieldname];
                        $this->date_create[$i]          = $row[$this->date_create_fieldname];
                        $this->address[$i]          = $row[$this->address_fieldname];
                     
                    $i++;
                }
            }
    }
        //PHAM THI THU XUAN
        //21/04/2010
          public function select_contact_by_id($p_id_object)
        {
            try
            {   $num=0;   
                $rs="";       
               $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where  ".$this->id_fieldname."=".$p_id_object." 
                      ";                        
              if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
             
               // return $rs->rowCount();
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
        
            public function select_contact_by_id_limit($start,$end)
        {
            try
            {   
                $rs="";
                $num=0;
                $sql = "";
                $sql = "select *  
                        from   ".$this->tbl."                          
                        order by   ".$this->id_fieldname." desc ";   
                $sql.= "  limit  ".$start.",".$end;       
         
                 if($sql!="")
                     {
                    $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getSource($rs);                 
 
                     }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }         
      //tong so category co status <>3
          function donum()
          {
          $sql="select * from ".$this->tbl." 
           ";
           $num=0;
           $rs="";
           if($sql!="")
                {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                   if($rs!="")
                    return $rs->rowCount();
                    else
                    return 0;
                }
                return 0;
        
          }
         
        //lay tong so category theo limit
 	      function doNumL( $l1, $l2){
            $sql="select * from ".$this->tbl."     limit ".$l1.",".$l2;
        $num=0;
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $num=$rs->numRows();
        }
        return $num;
        }     
	        function dodele( $id){
            $sql="delete  from   ".$this->tbl."  where     id=".$id;
        
          $flag=0;
       if($sql!="")
        {
           $flag= $this->db->sql_execute($sql,$this->db->isDelete);
           
        }
         return $flag;
        }     
    
   
    
       
    
}        
    

?>