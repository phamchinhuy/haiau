<?php

class cs_orders
{
    public $tbl = "orders";
    
    public $id                  = array();
    public $id_user             = array();
    public $total               = array();
    public $subtotal            = array();
    public $status              = array();
    public $date_created        = array();
    public $notes               = array();
    public $detail              = array();
    public $ip_address          = array();
    public $validation_code     = array();
    public $id_account          = array();
    public $date_updated        = array();
    public $id_user_deleted     = array();
    public $date_deleted        = array();
    
    public $id_fieldname                = "id";
    public $id_user_fieldname           = "id_user";
    public $total_fieldname             = "total";
    public $subtotal_fieldname          = "subtotal";
    public $status_fieldname            = "status";
    public $date_created_fieldname      = "date_created";
    public $notes_fieldname             = "notes";
    public $detail_fieldname            = "detail";
    public $ip_address_fieldname        = "ip_address";
    public $validation_code_fieldname   = "validation_code";
    public $id_account_fieldname        = "id_account";
    public $date_updated_fieldname      = "date_updated";
    public $id_user_deleted_fieldname   = "id_user_deleted";
    public $date_deleted_fieldname      = "date_deleted";
    
    public function insert_order()
    {
        $db = new db();
        $sql = "";
        $sql = " insert into orders 
                (
                    ".$this->id_user_fieldname."
                    ,". $this->total_fieldname."
                    ,". $this->subtotal_fieldname."
                    ,". $this->status_fieldname."
                    ,". $this->date_created_fieldname."
                    ,". $this->notes_fieldname."
                    ,". $this->ip_address_fieldname."
                )
                values
                (
                    ".$this->id_user[0]."
                    ,". $this->total[0]."
                    ,". $this->subtotal[0]."
                    ,'". $this->status[0]."'
                    ,now()
                    ,'". $this->notes[0]."'
                    ,'". $this->ip_address[0]."'
                )";
        $rs=null;
        $strIDOrderMax = "";
        $rs = $db->sql_execute($sql, $db->isInsert);
        {
            $strIDOrderMax = $this->getMaxIDOrder();
        }
        return $strIDOrderMax;
    }
    
    public function getMaxIDOrder()
    {
        $str = "";
        $db = new db();
        $sql = " select max(id) as id from ".$this->tbl;
        $rs = null; 
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            foreach($rs as $row)
            {
                $str = $row[$this->id_fieldname];
            }
        }
        return $str;
    }
    
    public function select_orders_by_status($p_status, $p_start, $p_end, $p_field_order, $p_sort)
    {
        $sql = "";
        if($p_start!="" && $p_end!="")
        {
            $sql = "select *
                    from orders
                    where status=".$p_status." 
                    order by ".$p_field_order." ".$p_sort." 
                    limit ".$p_start.", ".$p_end;    
        }
        else
        {
            $sql = "select *
                    from orders
                    where status=".$p_status." 
                    order by ".$p_field_order." ".$p_sort.""; 
                    
        }
        
        $db = new db();
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            $i = 0;
            foreach($rs as $row)
            {
                $this->id[$i] = $row[$this->id_fieldname];
                $this->id_user[$i] = $row[$this->id_user_fieldname];
                $this->total[$i] = $row[$this->total_fieldname];
                $this->subtotal[$i] = $row[$this->subtotal_fieldname];
                $this->status[$i] = $row[$this->status_fieldname];
                $this->date_created[$i] = $row[$this->date_created_fieldname];
                $this->notes[$i] = $row[$this->notes_fieldname];
                $this->ip_address[$i] = $row[$this->ip_address_fieldname];
                $this->validation_code[$i] = $row[$this->validation_code_fieldname];
                $this->id_account[$i] = $row[$this->id_account_fieldname];
                $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname];
                $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                $i++;
            }   
        }
    }
    
    public function select_orders_by_id($p_id)
    {
        $sql = "";
        
        $sql = "select *
                from orders
                where id=".$p_id;
                    
        $db = new db();
        $rs = null;
        $rs = $db->sql_execute($sql, $db->isSelect);
        if($rs!=null)
        {
            $i = 0;
            foreach($rs as $row)
            {
                $this->id[$i] = $row[$this->id_fieldname];
                $this->id_user[$i] = $row[$this->id_user_fieldname];
                $this->total[$i] = $row[$this->total_fieldname];
                $this->subtotal[$i] = $row[$this->subtotal_fieldname];
                $this->status[$i] = $row[$this->status_fieldname];
                $this->date_created[$i] = $row[$this->date_created_fieldname];
                $this->notes[$i] = $row[$this->notes_fieldname];
                $this->detail[$i] = $row[$this->detail_fieldname];
                $this->ip_address[$i] = $row[$this->ip_address_fieldname];
                $this->validation_code[$i] = $row[$this->validation_code_fieldname];
                $this->id_account[$i] = $row[$this->id_account_fieldname];
                $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname];
                $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                $i++;
            }   
        }
    }
    
    public function update_orders_by_status()
    {
        $sql = "";
        $sql = " update orders
                    set id_account=".$this->id_account[0]."
                        , status=".$this->status[0]."
                        , date_updated=now()
                    where id=".$this->id[0];
        $rs = null;
        $db = new db();
        $rs = $db->sql_execute($sql, $db->isUpdate);
        return $rs;

    }
    
    public function update_orders_by_detail()
    {
        $sql = "";
        $sql = " update orders
                    set id_account=".$this->id_account[0]."
                        , detail='".$this->detail[0]."'
                        , date_updated=now()
                    where id=".$this->id[0];
        $rs = null;
        $db = new db();
        $rs = $db->sql_execute($sql, $db->isUpdate);
        return $rs;

    }
    
    
}

?>