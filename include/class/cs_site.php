<?php
    class cs_site
    {
        
        //table name
        public $tbl = "sites";
        
        //value field
        public $id                  = array();
        public $site_name           = array();
        public $status              =array();
       
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $site_name_fieldname       = "site_name";
        public $status_fieldname              = "status";
        private $db="";
       
        //end field name
        private $db="";
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_category();
            $this->db=new db();
        }
        function cs_news()
        {
            $this->db=new db();
        }
        public function insert_news()
        {
            try
            {
                $rs = false;
                
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->id_category_fieldname."
                                ,".$this->news_fieldname."
                                ,".$this->small_image_fieldname."
                                ,".$this->large_image_fieldname."
                                ,".$this->short_description_fieldname."
                                ,".$this->long_description_fieldname."
                                ,".$this->status_fieldname."
                                ,".$this->url_fieldname."
                                ,".$this->target_fieldname."
                                ,".$this->date_created_fieldname."
                                ,".$this->id_user_created_fieldname."
                                ,".$this->date_updated_fieldname."
                                ,".$this->id_user_updated_fieldname."
                                ,".$this->date_deleted_fieldname."
                                ,".$this->id_user_deleted_fieldname."
                                ,".$this->id_topic_fieldname."
                                ,".$this->show_page_fieldname."
                                ,".$this->url_video_fieldname."
                                ,".$this->keyword_fieldname."
                                ,".$this->page_title_fieldname."
                                ,".$this->meta_description_fieldname."
                                ,".$this->meta_keyword_fieldname."
                                ,".$this->count_view_fieldname."
                                ,".$this->begin_date_fieldname."
                                ,".$this->begin_end_fieldname."
                                ,".$this->is_comment_fieldname."
                                
                            ) 
                            values 
                            ('".$this->id_category[0]."'
                                , '".$this->news[0]."'
                                , '".$this->small_image[0]."'
                                , '".$this->large_image[0]."'
                                , '".$this->short_description[0]."'
                                , '".$this->long_description[0]."'
                                , '".$this->status[0]."'
                                , '".$this->url[0]."'
                                , '".$this->target[0]."'
                                , '".$this->date_created[0]."'
                                , '".$this->id_user_created[0]."'
                                , '".$this->date_updated[0]."'
                                , '".$this->id_user_updated[0]."'
                                , '".$this->date_deleted[0]."'
                                , '".$this->id_user_deleted[0]."'
                                , '".$this->id_topic[0]."'
                                , '".$this->show_page[0]."'
                                , '".$this->url_video[0]."'
                                , '".$this->keyword[0]."'
                                , '".$this->page_title[0]."'
                                , '".$this->meta_description[0]."'
                                , '".$this->meta_keyword[0]."'
                                , '".$this->count_view[0]."'                                  
                                , '".$this->begin_date[0]."'
                                , '".$this->begin_end[0]."'
                                , '".$this->is_comment[0]."'
                            )";
                //echo $sql;
                $res=$this->db->sql_execute($sql,$this->db->isInsert);
                return $res;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        public function update_news()
        {
            try
            {
                $rs = false;
          
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->news_fieldname."='".$this->news[0]."'";
                              if(isset($this->small_image[0])&&$this->small_image[0]!="")
                             $sql.= " , ".$this->small_image_fieldname."='".$this->small_image[0]."'";
                               if(isset($this->large_image[0])&&$this->large_image[0]!="")
                               $sql.=" , ".$this->large_image_fieldname."='".$this->large_image[0]."'";
                               
                                $sql.=", ".$this->short_description_fieldname."='".$this->short_description[0]."'
                                , ".$this->long_description_fieldname."='".$this->long_description[0]."'
                                , ".$this->status_fieldname."='".$this->status[0]."' 
                                , ".$this->url_fieldname."='".$this->url[0]."'
                                , ".$this->target_fieldname."='".$this->target[0]."'
                                , ".$this->date_updated_fieldname."='".$this->date_updated[0]."'
                                , ".$this->id_user_updated_fieldname."='".$this->id_user_updated[0]."'
                                , ".$this->id_topic_fieldname."='".$this->id_topic[0]."'
                                , ".$this->show_page_fieldname."='".$this->show_page[0]."'
                                , ".$this->url_video_fieldname."='".$this->url_video[0]."'
                                , ".$this->keyword_fieldname."='".$this->keyword[0]."'
                                , ".$this->page_title_fieldname."='".$this->page_title[0]."'
                                , ".$this->meta_description_fieldname."='".$this->meta_description[0]."'
                                , ".$this->meta_keyword_fieldname."='".$this->meta_keyword[0]."'
                                , ".$this->count_view_fieldname."='".$this->count_view[0]."'
                                , ".$this->begin_date_fieldname."='".$this->begin_date[0]."'
                                , ".$this->begin_end_fieldname."='".$this->begin_end[0]."'
                                , ".$this->is_comment_fieldname."='".$this->is_comment[0]."' 
                                
                                
                            where ".$this->id_fieldname."=".$this->id[0];
                    $rs = true;
                  
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function  get_source($rs)
        {
            try{
                if($rs!="")
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname]; 
                        $this->id_category[$i] =$row[$this->id_category_fieldname];
                        $this->news[$i] = $row[$this->news_fieldname];
                        $this->small_image[$i] =$row[$this->small_image_fieldname] ;
                        $this->large_image[$i] =$row[$this->large_image_fieldname]; ;
                        $this->short_description[$i] =$row[$this->short_description_fieldname] ;
                        $this->long_description[$i] =$row[$this->long_description_fieldname];
                        $this->status[$i] =$row[$this->status_fieldname];;
                        $this->url[$i] =$row[$this->url_fieldname]; ;
                        $this->target[$i] =$row[$this->target_fieldname]; ;
                        $this->date_created[$i] =$row[$this->date_created_fieldname]; ;
                        $this->id_user_created[$i] =$row[$this->id_user_created_fieldname] ;
                        $this->date_updated[$i] =$row[$this->date_updated_fieldname]; ;
                        $this->id_user_updated[$i] =$row[$this->id_user_updated_fieldname];;
                        $this->date_deleted[$i] =$row[$this->date_deleted_fieldname]; ;
                        $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname];
                        $this->id_topic[$i] =  $row[$this->id_topic_fieldname];
                        $this->show_page[$i] =  $row[$this->show_page_fieldname];
                        $this->url_video[$i] =  $row[$this->url_video_fieldname];
                        $this->keyword[$i] =  $row[$this->keyword_fieldname];
                        $this->page_title[$i] =  $row[$this->page_title_fieldname];
                        $this->meta_description[$i] =  $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i] =  $row[$this->meta_keyword_fieldname];
                        $this->count_view[$i] =  $row[$this->count_view_fieldname];
                        $this->begin_date[$i] =  $row[$this->begin_date_fieldname];
                        $this->begin_end[$i] =  $row[$this->begin_end_fieldname];
                        $this->is_comment[$i] =  $row[$this->is_comment_fieldname];
                        $i++;
                    }
                }
               
            }
             catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        public function select_news_by_id_category_status($p_id_category, $p_status)
        {
            try
            {
              
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname." = ".$p_status."
                                order by ".$this->id_fieldname." desc";
                $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                }                
                
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_news_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from news
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $result = "";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_news_by_id_category($p_id_category)
        {
            try
            {
            
                $sql = "";
                $sql = "select *  
                        from news 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by `id` desc";
                                
                $result = "";
              
                if($sql!="")
                {
                   $result=$this->db->sql_execute($sql,$this->db->isSelect);
                   $this->get_source($result); 
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_news_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from news 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by ".$this->date_created_fieldname."  desc
                                limit ".$p_start.", ".$p_end;
                //echo $sql;                
                $result ="";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }              
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_news_by_id($p_id)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from news 
                        where   ".$this->id_fieldname."=".$p_id." and ".$this->status_fieldname." < 3  order by ".$this->id_fieldname; 
                $result = "";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        public function update_news_set_status($p_status,$p_id)
        {
            try
            {
                $rs = "";
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
                //echo $sql;
                $result =false;
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $result;
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
        }
        //END
         public function update_news_set_comment($status_comment,$p_id)
        {
            try
            {
                $rs = false;
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->is_comment_fieldname."='".$status_comment."' 
                            where ".$this->id_fieldname."=".$p_id;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $rs;
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        //24-04-2010 created by tran van tam
        public function delete_news_by_id($p_id)
        {
            try
            {
                $rs = false;
             
                $sql = "";
                $sql = " update ".$this->tbl." set   ".$this->status_fieldname."=3 
                      where    ".$this->id_fieldname."=".$p_id;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isDelete);
                    return $rs;
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
            function update_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
            if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
                ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
                ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
             {
                    if($_FILES[$resource]['size']<$size)
                    {
                         $filename= $_FILES[$resource]["type"];
                         
                     switch($filename)
                         {
                            case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                        break;
                             case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                        break;                        
                            case "image/gif"      :   $file    =uniqid("").".gif";
                                                                        break;
                            case "image/png"      :     $file    =uniqid("").".png";
                                                                         break;         
                         }
                        if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
                        {
                             if($f==1)
                             {
                                $this->large_image[0]=$file;
                             }
                             else
                             {
                                $this->small_image[0]=$file;
                             }
                            $flag=true;
                        }
                            
                    }
                     else
                     $flag = false;
                
             }
         return $flag;
            
          }
              catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
          
       }
    
    }
?>