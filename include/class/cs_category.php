<?php

class cs_category
{
        public $tbl                 ="category";
        public $categoryId          =array();
        public $id                  =array();
        public $id_site             =array();
        public $postion             =0;
        public $id_type_category    =array();
        public $category            =array();
        public $status              =array();
        public $id_parent           =array();
        public $small_images        =array();
        public $large_images        =array();
        public $music               =array();
        public $order               =array();
        public $url                 =array();
        public $target              =array();
        public $show_subCategory    =array();
        public $date_created        =array();
        public $id_user_created     =array();
        public $date_updated        =array();
        public $date_deleted        =array();
        public $id_user_deleted     =array();
        public $id_user_updated     =array();
        public $keyword             =array();
        public $page_title          =array();
        public $meta_description    =array();
        public $meta_keyword        =array();
        public $show_page           =array();
        public $id_music_background =array();
        public $template            =array();
        public $re_urls             =array();
        public $history             ="";
        public $id_s="";
        //
        public $db="";
        private $_connection = '';
        private $_cursor = null;
        private $_sql = '';
        public $categoryType=array();
        public $positons=array();
        public $id_parent_sected=array();
        public $flags="";
      //
        public $pageAction=array();
        //file
        private $tbfile="file";
        private $savefolder="uploads/";
        private $filealt=array();
        private $tbfile_link="file_link";
        private $id_object=array();
        private $object_type=array();
        private $id_file=array();
        private  $id_file_detail =array();
        private $file_status=array();
        
        //xu ly
      //flag
        public $subpermision=array();
        public $idtb=array();
        public $flag=array();
        public $file_type=array();	
        public $start="";
        public $end="";
        
        
        //column name
        public $id_fieldname                    = "id";
        public $id_site_fieldname                    = "id_site";
        public $id_type_category_fieldname      = "id_type_category";
        public $category_fieldname              = "category";
        public $status_fieldname                = "status";
        public $id_parent_fieldname             = "id_parent";
        public $small_image_fieldname           = "small_image";
        public $large_image_fieldname           = "large_image";
        public $order_fieldname                 = "order";
        public $url_fieldname                   = "url";
        public $target_fieldname                = "target";
        public $show_subCategory_fieldname      = "show_subCategory";
        public $date_created_fieldname          = "date_created";
        public $id_user_created_fieldname       = "id_user_created";
        public $date_updated_fieldname          = "date_updated";
        public $id_user_updated_fieldname       = "id_user_updated";
        public $date_deleted_fieldname          = "date_deleted";
        public $id_user_deleted_fieldname       = "id_user_deleted";
        public $keyword_fieldname               = "keyword";
        public $page_title_fieldname            = "page_title";
        public $meta_description_fieldname      = "meta_description";
        public $meta_keyword_fieldname          = "meta_keyword";
        public $show_page_fieldname             = "show_page";
        public $id_music_background_fieldname   = "id_music_background";
        public $template_fieldname              = "template";
        public $re_urls_fieldname               ="re_urls"; 
        //
        public $small_image = array();
        public $large_image = array();
        
        
          	
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_category();
           
        }
        function  cs_category()
        {
            $this->db=new db();
        }
        
         /**
     * MO TA CHUC NANG: 
     * Load tat ca record trong cs_category
     * 
     * Tham so:$rs :ket qua tra ve tu truy van sql
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
    public function getSource($rs)
    {
         $do_nick_chat="";
         if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i]                   = $row[$this->id_fieldname];
                        $this->id_site[$i]              = $row[$this->id_site_fieldname];
                        $this->id_type_category[$i]     = $row[$this->id_type_category_fieldname];
                        
                        $this->category[$i]             = $row[$this->category_fieldname];
                        $this->status[$i]               = $row[$this->status_fieldname];
                        $this->id_parent[$i]            = $row[$this->id_parent_fieldname];
                        $this->small_images[$i]         = $row[$this->small_image_fieldname];
                        $this->large_image[$i]          = $row[$this->large_image_fieldname];
                        $this->order[$i]                = $row[$this->order_fieldname];
                        $this->url[$i]                  = $row[$this->url_fieldname];
                        $this->target[$i]               = $row[$this->target_fieldname];
                        $this->show_subCategory[$i]     = $row[$this->show_subCategory_fieldname];
                        $this->date_created[$i]         = $row[$this->date_created_fieldname];
                        $this->id_user_created[$i]      = $row[$this->id_user_created_fieldname];
                        $this->date_updated[$i]         = $row[$this->date_updated_fieldname];
                        $this->id_user_updated[$i]      = $row[$this->id_user_updated_fieldname];
                        $this->date_deleted[$i]         = $row[$this->date_deleted_fieldname];
                        $this->id_user_deleted[$i]      = $row[$this->id_user_deleted_fieldname];
                        $this->keyword[$i]              = $row['keyword'];
                        $this->page_title[$i]           = $row[$this->page_title_fieldname];
                        $this->meta_description[$i]     = $row[$this->meta_description_fieldname];
                        $this->meta_keyword[$i]         =$row[$this->meta_keyword_fieldname];
                        $this->show_page[$i]            = $row[$this->show_page_fieldname];
                        $this->id_music_background[$i]  =$row[$this->id_music_background_fieldname];
                        $this->template[$i]             = $row[$this->template_fieldname];
                        $this->re_urls[$i]              = $row[$this->re_urls_fieldname];
                         $this->pageAction=explode(",",$this->show_page[$i]);                
                    $i++;
                }
            }
    }
        //PHAM THI THU XUAN
        //21/04/2010
          public function select_category_by_tree_id_parent_order_by_desc($p_id_parent)
        {
            try
            {             
               $sql = "";
                $sql = "select *  
                        from category 
                        where  id_parent=".$p_id_parent." and status = 1                 
                        order by `order` asc";                        
              if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
        public function check_page_title_extis()
        {
            try{
                
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where  ".$this->page_title_fieldname."='".$this->page_title[0]."'
                       ";
                
                $flag=false;
                if($sql!="")
                {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                if($rs->rowCount()>0)
                  $flag=true;
                } 
                return $flag;       
            }
            catch(exception $e)
            {
               var_dump($e->getMessage()); 
            }
        }
        public function select_category_by_id_parent_order_by_desc($p_id_parent)
        {
            try
            {           
                $sql = "";
                $sql = "select *  
                        from category 
                        where  id_parent=".$p_id_parent." and status = 1
                        and id_type_category=4 	
                        order by `order` asc";
              if($sql!="")
                {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
                }                                  
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
             public function select_category_by_id_parent_order_by_desc_2($p_id_parent)
        {
            try
            {              
                $sql = "";
                $sql = "select *  
                        from category 
                        where  id_parent=".$p_id_parent." and status = 1                      
                        order by `order` asc";                     
              if($sql!="")
                {
                    $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getSource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
         public function select_category_by_urls_order_by_desc($p_id_parent)
        {
             $temp="";
            try
            {        
                $sql = "";
                $sql = "select *  
                        from category 
                        where  url='".$p_id_parent."'  and status = 1
                        and id_type_category=4
                              ";
                   if($sql!="")
                {
                    $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                    if($rs!="")
                    {
                            $i=0;
                            foreach($rs as $row)
                            {
                                 $temp =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"0";      
                            }
                    }
                }              
              
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            //echo $sql;
         
            return $temp;
        }
        
        //PHAM THI THU XUAN
        //21/04/2010
        public function select_category_by_id($p_id)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from category 
                        where id=".$p_id ."";
                $sql.="  and status =1" ;       
                        
                if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
        //END
        
        //Phan Thi Thuy Nguyen
        //18/02/2011
        public function select_category_by_page_title($p_pagetitle)
        {
            try
            {
                $connect  = sql_connect_default();
                $sql = "";
                $sql = "select *  
                        from category 
                        where page_title=".$p_pagetitle ."";
                $sql.="  and status =1" ;       
                        
                if($sql!="")
                 {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
        }
        //END
         //Thiet lap ket noi 
   	  function setQuery($sql) 
	{
		$this->_sql = $sql;
	}
    
    //set query
	function query() 
	{	
        $this->database();
    	$this->_cursor = mysql_query($this->_sql, $this->_connection );
        $this->disconnect();
    	return $this->_cursor;
	}
    function disconnect() 
	{
		mysql_close( $this->_connection );
	}
    
    //	ten :ho hai the
    //ngay tao:20/04/2010
    //ngay cap nhat
    function database() 
	{
		$this->_connection =  sql_connect_default();
	}
      function do_selectall($rsSQL)
      {  
  	if($rsSQL!=null)
    {                
        $i=0;
        while($rowSQL =  mysql_fetch_array($rsSQL))
        { 
            //category
            $this->categoryId[$i]=$rowSQL['id'];
           $this->id_type_category[$i]=$rowSQL['id_type_category'];
           $this->category[$i]=$rowSQL['category'];
           $this->status[$i]=$rowSQL['status'];
           $this->id_parent[$i]=$rowSQL['id_parent'];
           $this->small_images[$i]=$rowSQL['small_image'];
           $this->large_images[$i]=$rowSQL['large_image'];
           $this->order[$i]=$rowSQL['order'];
           $this->url[$i]=$rowSQL['url'];
           $this->target[$i]=$rowSQL['target'];
           $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
           $this->date_created[$i]=$rowSQL['date_created'];
           $this->id_user_created[$i]=$rowSQL['id_user_created'];
           $this->date_updated[$i]=$rowSQL['date_updated'];
           $this->date_deleted[$i]=$rowSQL['date_deleted'];
           $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
           $this->keyword[$i]=$rowSQL['keyword'];
           $this->page_title[$i]=$rowSQL['page_title'];
           $this->meta_description[$i]=$rowSQL['meta_description'];
           $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
           $this->show_page[$i]=$rowSQL['show_page'];
           $this->id_music_background[$i] =$rowSQL['id_music_background'];
           $this->template[$i]=$rowSQL['template'];
           $this->re_urls[$i]=$rowSQL['re_urls']; 
           $this->positons[$i]=$rowSQL['position'];
           $this->id_parent_sected[$i]=$rowSQL['id_parent_sected'];
           $this->pageAction=explode(",",$this->show_page[$i]);                                              
            $i++;
        }   
        
		mysql_free_result($rsSQL);
    }
           
      }
      //lat tat ca catogory
      function do_selectall_cateogory($orderby)
      {
        $sql="select * from category order by ".$orderby;
        if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $this->getSource($rs);
        }
      }
      //l?y category theo id
       function do_selectall_cateogory_id($id)
      {
            $sql="select * from category where  id=".$id;
            if($sql!="")
            {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                $this->getSource($rs);
            }
      }
      //tong so category co status <>3
      function donum()
      {
        $sql="select * from category where  status <> 3
        and id_parent=0
         ";
       $num=0;
       if($sql!="")
            {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $num=$rs->rowCount();
            }
        return $num;
      }
        function donum_by_site($id)
      {
        $sql="select * from category where  status <> 3
        and id_parent=0 and id_site=".$id."
        ";
        $num=0;
        if($sql!="")
            {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $num=$rs->rowCount();
            }
        return $num;
      }   
        //lay tong so category theo limit
 	  function doNumL( $l1, $l2){
        $sql="select * from ".$this->tbl." where  id_parent=0  and status <> 3  limit ".$l1.",".$l2;
       $num=0;
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $num=$rs->numRows();
        }
            return $num;
     }
     	function doActive($id, $status){
    		if($status){
    			$status= 0;
    		}
    		else{
    			$status= 1;	
    		}
		
		$sql  = 'update '.$this->tbl.' set status="'.$status.'" where id="'.$id.'"';
		 //echo $sql;
         if($sql!="")
            return $this->db->sql_execute($sql,$this->db->isUpdate);
        else
            return 0;
	   }
       //xoa mot cateogory
       	function doDelete($id){
	   	$sql  = 'update '.$this->tbl.' set '.$this->status_fieldname.'="3" where  '.$this->id_fieldname.'="'.$id.'"';
	    if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            return 0;
	   }
	   function doinsert()
       {
        $sql="";
      	$sql.= 'insert  into 
        category(`id_type_category`,`category`,`status`,`id_parent`,`small_image`, 
        `large_image`,`order`,`url`,`target`,`show_subCategory`,`date_created`, 
        `id_user_created`,
        `keyword`,page_title,meta_description, 
        meta_keyword,show_page,id_music_background, template,re_urls,position, id_parent_sected ,id_site) 
        values(';
        $sql.="".$this->id_type_category[0]."";
        $sql.=",'".$this->category[0]."'"; 
        $sql.=",'".$this->status[0]."'"; 
        $sql.=",'".$this->id_parent[0]."'"; 
        $this->small_image[0]=isset($this->small_image[0])?$this->small_image[0]:"";
         $sql.=",'".$this->small_image[0]."'";
         $this->large_image[0]=isset($this->large_image[0])?$this->large_image[0]:"";
          $sql.=",'".$this->large_image[0]."'";
        $sql.=",'".$this->order[0]."'"; 
        $sql.=",'".$this->url[0]."'"; 
        $sql.=",'".$this->target[0]."'"; 
        $sql.=",'".$this->show_subCategory[0]."'"; 
        $sql.=",".$this->date_created[0]."";
        $sql.=",'".$_SESSION['id_account_admin']."'"; 
        $sql.=",'".$this->keyword[0]."'"; 
        $sql.=",'".$this->page_title[0]."'"; 
        $sql.=",'".$this->meta_description[0]."'"; 
        $sql.=",'".$this->meta_keyword[0]."'"; 
        $sql.=",'".$this->show_page[0]."'"; 
        $sql.=",'".$this->id_music_background[0]."'";
        $sql.=",'".$this->template[0]."'";  
        $sql.=",'".$this->re_urls[0]."'";  
        $sql.=",'".$this->positons[0]."'";
        $sql.=",'".$this->id_parent_sected[0]."'";   
         $sql.=",'".$this->id_site[0]."'";        
        $sql.=') ';
        //echo $sql;
	    if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isInsert);
        else
            return 0;
       }
	    function doupdate($id)
       {
            $sql="";
          	$sql.= 'update  '.$this->tbl. ' set  ';
            $sql.="id_type_category= ".$this->id_type_category[0]." ";
            $sql.=",category='".$this->category[0]."'  "; 
            $sql.=",status='".$this->status[0]."'   "; 
            $sql.=",url='".$this->url[0]."'  "; 
            $sql.=",`target`='".$this->target[0]."'   "; 
            $sql.=",show_subCategory='".$this->show_subCategory[0]."'  "; 
            
            if($this->small_images[0]!="")
               $sql.=",small_image='".$this->small_images[0]."'"; 
            
            if($this->large_images[0]!="")
                $sql.=",large_image='".$this->large_images[0]."'"; 
            
            $sql.=",date_updated=".$this->date_updated[0].""; 
            $sql.=",id_user_updated='".$this->id_user_updated[0]."'  "; 
            $sql.=",keyword='".$this->keyword[0]."'  "; 
            $sql.=",page_title='".$this->page_title[0]."'  "; 
            $sql.=",meta_description='".$this->meta_description[0]."'  "; 
            $sql.=", meta_keyword='".$this->meta_keyword[0]."'  "; 
            $sql.=",show_page='".$this->show_page[0]."'  "; 
            $sql.=",id_music_background='".$this->id_music_background[0]."'  ";
            $sql.=",template='".$this->template[0]."'  ";  
            $sql.=",re_urls='".$this->re_urls[0]."'  "; 
            $sql.=",position='".$this->positons[0]."'";
            $sql.=",id_parent_sected='".$this->id_parent_sected[0]."'"; 
            $sql.=",id_site='".$this->id_site[0]."'";         
            $sql.=' where id ='.$id;
             //echo $sql;
    		if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
           else
            return 0;
       }
       
       function swap($id1,$id2)
       {
       $id_1=$this->getCategory_by_order($id1);
       $id_2=$this->getCategory_by_order($id2);
       $sql="update ".$this->tbl." set `order` =".$id1 ." where id=". $id_2;
       $rs=0;
      
       if($sql!="")
            $rs= $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            $rs=0;
        if($rs==1)
        {
            $sql="";
            $sql="update ".$this->tbl." set `order` =".$id2 ." where id=". $id_1;
            if($sql!="")
                return $this->db->sql_execute($sql, $this->db->isUpdate);
            else
                return 0;
        }
        return 0;
       }
       function swapOrder($id,$order,$position_category_id,$flag)
       { 
        $sql="SELECT *  FROM ".$this->tbl." where  ".$this->status_fieldname."<>3   and   ".$this->id_parent_fieldname."=".$id."  order by `order` ";
            if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $this->getSource($rs);
        }
        //$this->disconnect()
        global $vt;
        global $res;
       
        for($i=0;$i<count($this->id);$i++)
         { 
             if($this->order[$i]==$order)
             {    
                 if($flag==1)
                    $vt=$i;
                 if($flag==0)
                    $vt=$i+1;
                 
             }
             if($this->order[$i]==$position_category_id)
             $res=$i;
         }
        $this->swap($this->order[$vt],$position_category_id);
        for($i=$res;$i>$vt+1;$i--)
            {    
            $this->swap($this->order[$i],$this->order[$i-1]);
            }         
       }
       function getMaxCategory()
       {
          $sql="SELECT max(`order`) as position  FROM `category`  ";
          $max="";
        if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row['position'])?$row['position']:"0";      
                    }
            }
        }
      //  echo $max;
        return $max;
       }
         function getMinCategory()
       {
          $sql="SELECT min(`order`) as position  FROM `category`  ";
           if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row['position'])?$row['position']:"0";      
                    }
            }
        }
        return $max;
       }
        function getCategory_parent_id($id)
       {
          $sql="SELECT  id_parent 	  FROM `category`  where   id =".$id;
                      $max=0;
          // echo $sql;
           if($sql!="")
        {

            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row[$this->id_parent_fieldname])?$row[$this->id_parent_fieldname]:"0";      
                    }
            }
        }
        return $max;
       }
         function getCategory_child_id($id)
       {
          $sql="SELECT   *	  FROM `category`  where   id_parent =".$id;
        
            if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            $this->getSource($rs);
        }
       }
       function checkParent($id)
       {
        $sql="SELECT  id_parent 	  FROM `category`  where   id =".$id;
        $rs = $this->db->sql_execute($sql,$this->db->isSelect);
          // echo $sql;
          if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row[$this->id_parent_fieldname])?$row[$this->id_parent_fieldname]:"0";      
                    }
            }
        return $max;
       }
       function checkChild($id)
       {
        $rs="";
        $max="";
        $sql="SELECT  id	  FROM `category`  where   id_parent  =".$id;
        $rs = $this->db->sql_execute($sql,$this->db->isSelect);
          if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"0";      
                    }
            }
        
        return $max;
       }
       //nguoi viet:Haithe1988
       //ngayviet:23042010
         function getCategory_by_order($id)
       {
          $rs="";
          $sql="SELECT  id	  FROM `category`  where  `order` =".$id;
          $rs = $this->db->sql_execute($sql,$this->db->isSelect);
          $max="";
          if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"0";      
                    }
            }
        
        return $max;
       }
        function getCategory_urls_by_id($id)
       {
          $rs="";
          $sql="SELECT  re_urls	  FROM `category`  where  id =".$id;
          
          if($sql="")
            {
                $rs = $this->db->sql_execute($sql,$this->db->isSelect);
                if($rs!="")
                {
                     foreach($rs as $row)
                    {
                        $max =!empty($row[$this->re_urls_fieldname])?$row[$this->re_urls_fieldname]:"";      
                    }
                }
                   
            }
        return $max;
       }
        function getOrder_parent_id($id)
       {
          $rs="";
          $sql="SELECT  `order`  FROM `category`  where   id =".$id;
           $rs = $this->db->sql_execute($sql,$this->db->isSelect);
           $max = 0;
          if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row[$this->order_fieldname])?$row[$this->order_fieldname]:"";      
                    }
            }
        
            return $max;
        }
        function getMaxId()
       {
         $rs="";
          $sql="SELECT max(`id`) as position  FROM `category`  ";
           $rs = $this->db->sql_execute($sql,$this->db->isSelect);
         if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $max =!empty($row['position'])?$row['position']:"0";      
                    }
            }
        
         return $max;
       }
        function get_id_site_by_id($id)
       {
            $rs="";
            $sql="SELECT ".$this->id_site_fieldname."  
                FROM   ".$this->tbl ."  where ".$this->id_fieldname."
          =".$id;

            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
             if($rs!="")
                {
                        $i=0;
                        foreach($rs as $row)
                        {
                            $max =!empty($row[$this->id_site_fieldname])?$row[$this->id_site_fieldname]:"";      
                        }
                }
            
         return $max;
       }
       function RemoveSign($str)
        {
        // B?ng d?ch k� t? kh�ng d?u bao g?m 2 b?ng m� cho unicode v� windows cp 1258
        $trans = array ('�' => 'a', '�' => 'a', '?' => 'a', '�' => 'a', '?' => 'a', '�' => 'a', '?' => 'a', '?' => 'a', '?' => 'a', '?' => 'a', '?' => 'a', '�' => 'a', '�' => 'u', '?' => 'u', 'u' => 'u', '?' => 'u', '�' => 'a', '�' => 'a', '�' => 'o', '?' => 'o', '?' => 'o', '?' => 'o', '?' => 'o', '?' => 'o', '�' => 'o', '�' => 'o', '?' => 'o', '�' => 'o', '?' => 'o', '�' => 'e', '?' => 'e', '?' => 'e', '?' => 'e', '?' => 'e', '?' => 'e', '�' => 'i', '�' => 'i', '?' => 'i', 'i' => 'i', '?' => 'i', 'o' => 'o', '?' => 'o', '�' => 'y', '?' => 'y', '?' => 'y', '?' => 'y', '?' => 'y', '?' => 'o', '?' => 'o', '?' => 'o', '?' => 'o', 'u' => 'u', '?' => 'u', '?' => 'u', '?' => 'u', '?' => 'u', '?' => 'u', 'd' => 'd', '�' => 'A', '�' => 'A', '?' => 'A', '�' => 'A', '?' => 'A', '�' => 'A', '?' => 'A', '�' => 'A', '?' => 'A', '?' => 'A', '?' => 'A', '�' => 'U', '�' => 'U', '?' => 'U', 'U' => 'U', '?' => 'U', '�' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', 

        '�' => 'E', '?' => 'E', '?' => 'E', '?' => 'E', '?' => 'E', '?' => 'E', '�' => 'I', '�' => 'I', '?' => 'I', 'I' => 'I', '?' => 'I', 'O' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', '?' => 'O', 'U' => 'U', '?' => 'U', '?' => 'U', '?' => 'U', '?' => 'U', '?' => 'U', '�' => 'D', '�' => 'Y', '?' => 'Y', '?' => 'Y', '?' => 'Y', '?' => 'Y', 

        'a�' => 'a', 'a`' => 'a', 'a?' => 'a', 'a~' => 'a', 'a?' => 'a', 'a' => 'a', 'a�' => 'a', 'a`' => 'a', 'a?' => 'a', 'a~' => 'a', 'a?' => 'a', '�' => 'a', '�' => 'a', '�`' => 'a', '�?' => 'a', '�~' => 'a', '�?' => 'a', 'u�' => 'u', 'u`' => 'u', 'u?' => 'u', 'u~' => 'u', 'u?' => 'u', 'u' => 'u', 'u�' => 'u', 'u`' => 'u', 'u?' => 'u', 'u~' => 'u', 'u?' => 'u', 'i�' => 'i', 'i`' => 'i', 'i?' => 'i', 'i~' => 'i', 'i?' => 'i', 'o�' => 'o', 'o`' => 'o', 'o?' => 'o', 'o~' => 'o', 'o?' => 'o', '�' => 'o', '��' => 'o', '�`' => '�', '�?' => 'o', '�~' => 'o', '�?' => 'o', 'o' => 'o', 'o�' => 'o', 'o`' => 'o', 'o?' => 'o', 'o~' => 'o', 'o?' => 'o', 'd' => 'd', '�' => 'D', 'y�' => 'y', 'y`' => 'y', 'y?' => 'y', 'y~' => 'y', 'y?' => 'y', 'A�' => 'A', 'A`' => 'A', 'A?' => 'A', 'A~' => 'A', 'A?' => 'A', 'A' => 'A', 'A�' => 'A', 'A?' => 'A', 'A~' => 'A', 'A?' => 'A', '�' => 'A', '´' => 'A', '�?' => 'A', '�~' => 'A', '�?' => 'A', 'E�' => 'E', 'E`' => 'E', 'E?' => 'E', 'E~' => 'E', 'E?' => 'E', 'ʴ' => 'E', '�`' => 'E', '�?' => 'E', '�~' => 'E', '�?' => 'E', 'U�' => 'U', 'U`' => 'U', 'U?' => 'U', 'U~' => 'U', 'U?' => 'U', 'U' => 'U', 'U�' => 'U', 'U`' => 'U', 'U?' => 'U', 'U~' => 'U', 'U?' => 'U', 'I�' => 'I', 'I`' => 'I', 'I?' => 'I', 'I~' => 'I', 'I?' => 'I', 'O�' => 'O', 'O`' => 'O', 'O?' => 'O', 'O~' => 'O', 'O?' => 'O', '�' => 'O', 'Դ' => 'O', '�?' => 'O', '�~' => 'O', '�?' => 'O', 'O' => 'O', 'O�' => 'O', 'O`' => 'O', 'O?' => 'O', 'O~' => 'O', 'O?' => 'O', 'Y�' => 'Y', 'Y`' => 'Y', 'Y?' => 'Y', 'Y~' => 'Y', 'Y?' => 'Y', ' ' => '-' )

        ;   
        // D?ch Ti?ng Vi?t c� d?u th�nh kh�ng d?u theo 2 b?ng m� Unicode va window cp 1258
       $str = strtr($str,$trans); // Chu?i d� du?c b? d?u  
        $value=trim($str);
        $valuemoi=str_replace(" ", "-",$str);
        return $valuemoi;
        }
          function insertImagesLarge($file)
        {
        $newname =  uniqid("").".jpg";
        $newname ="Large"."_".$newname;
        $image_path= $newname;
        $this->savefolder="uploads/images/images_large/";
        //$catogory_id=$this->category_id[0]; 
             if(move_uploaded_file($file,$this->savefolder.$newname))
             {  
               
              $this->large_images[0]=$newname;
                return true;
             }
           return false;      
          }
        function insertImagesThum($file)
        {
        
            $newname =  uniqid("").".jpg";
            $newname = "thum"."_".$newname;
            $image_path= $newname;
            $result="";
            $this->savefolder="uploads/images/images_thum/";
            if(move_uploaded_file($file,$this->savefolder.$newname))
             {  
                $this->small_images[0]=$newname;
                    return true;
             }     
            return false;   
        }
     //hien thi dang da cap trong 
     function select_multilevel($parent_id='0', $insert_text='<img src="images/admin/gach.gif"/>&nbsp;')
     {
      if($this->postion==0)
      { 
         if($this->id_s=="")
        $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3   order by `order` ".  "
      limit ".$this->start.",".$this->end;
      else
          $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3   order by `order` ".  "
        and 	id_parent<>".$this->id_s."
      limit ".$this->start.",".$this->end;
     // echo $sql;
      }
      else{
         if($this->id_s=="")
      $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3   order by `order` ".  "";
         else
         $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3 
            and 	id_parent<>".$this->id_s."
           order by `order` ".  ""; 
      }
     // echo $sql;
      if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $rowSQL)
                    {
                        $i=$this->postion;
                //category        
              $objcategory=new cs_type_category();
              $objcategory->do_selectall_typecateogory_id($rowSQL['id_type_category']);
              $this->categoryId[$i]=$rowSQL['id'];
              $this->id_type_category[$i]=$rowSQL['id_type_category'];    
                 if($rowSQL['id_parent']!="0")        
               $this->category[$i]=$insert_text.$rowSQL['category'];
                 else
               $this->category[$i]=$rowSQL['category'];  
               
               
               $this->status[$i]=$rowSQL['status'];
               $this->id_parent[$i]=$rowSQL['id_parent'];
               $this->small_images[$i]=$rowSQL['small_image'];
               $this->large_images[$i]=$rowSQL['large_image'];
               $this->order[$i]=$rowSQL['order'];
               $this->url[$i]=$rowSQL['url'];
               $this->categoryType[$i]= isset($objcategory->type[0])?$objcategory->type[0]:"";
               $this->target[$i]=$rowSQL['target'];
               $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
               $this->date_created[$i]=$rowSQL['date_created'];
               $this->id_user_created[$i]=$rowSQL['id_user_created'];
               $this->date_updated[$i]=$rowSQL['date_updated'];
               $this->date_deleted[$i]=$rowSQL['date_deleted'];
               $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
               $this->keyword[$i]=$rowSQL['keyword'];
               $this->page_title[$i]=$rowSQL['page_title'];
               $this->meta_description[$i]=$rowSQL['meta_description'];
               $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
               $this->show_page[$i]=$rowSQL['show_page'];
               $this->id_music_background[$i] =$rowSQL['id_music_background'];
               $this->template[$i]=$rowSQL['template'];
               $this->re_urls[$i]=$rowSQL['re_urls'];        
               $this->positons[$i]=$rowSQL['position'];
               $this->id_parent_sected[$i] =$rowSQL['id_parent_sected'];                                      
                $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                $this->id_site[$i]=$rowSQL['id_site'];
               $this->postion=$i+1;  
               $this->select_multilevel($this->categoryId[$i], $insert_text.$insert_text);
                    }
            }
        }
     // $total= mysql_num_rows($rsSQL);
                 
        }  
        //select multi by id_site
        
    function select_multilevel_by_site($parent_id='0', $insert_text='&nbsp;&nbsp;')
     {
      if($this->postion==0)
      { 
      
         if($this->id_s=="")
        $sql=   "SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3  
       	        and ".$this->id_site_fieldname."=".$this->id_site[0]."
                order by `order` ".  "
      
                limit ".$this->start.",".$this->end;
      else
        $sql=   "SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3
                and ".$this->id_site_fieldname."=".$this->id_site[0]."
        
                and 	id_parent<>".$this->id_s."
                order by `order` ".  "
                limit ".$this->start.",".$this->end;
     // echo $sql;
      }
      else{
         if($this->id_s=="")
      $sql=     "SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3 
                and id_site=".$this->id_site[0]."
                order by `order` ".  "";
         else
         $sql=  "SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3 
                and 	id_parent<>".$this->id_s."
                and id_site=".$this->id_site[0]."
            
                order by `order` ".  ""; 
      }
     
     $id_site=$this->id_site[0];
      if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $rowSQL)
                    {
                        $i=$this->postion;
                //category        
              $objcategory=new cs_type_category();
              $objcategory->do_selectall_typecateogory_id($rowSQL['id_type_category']);
              $this->categoryId[$i]=$rowSQL['id'];
              $this->id_type_category[$i]=$rowSQL['id_type_category'];
               $this->category[$i]=$insert_text.$rowSQL['category'];
               $this->status[$i]=$rowSQL['status'];
               $this->id_parent[$i]=$rowSQL['id_parent'];
               $this->small_images[$i]=$rowSQL['small_image'];
               $this->large_images[$i]=$rowSQL['large_image'];
               $this->order[$i]=$rowSQL['order'];
               $this->url[$i]=$rowSQL['url'];
               $this->categoryType[$i]= $objcategory->type[0];
               $this->target[$i]=$rowSQL['target'];
               $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
               $this->date_created[$i]=$rowSQL['date_created'];
               $this->id_user_created[$i]=$rowSQL['id_user_created'];
               $this->date_updated[$i]=$rowSQL['date_updated'];
               $this->date_deleted[$i]=$rowSQL['date_deleted'];
               $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
               $this->keyword[$i]=$rowSQL['keyword'];
               $this->page_title[$i]=$rowSQL['page_title'];
               $this->meta_description[$i]=$rowSQL['meta_description'];
               $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
               $this->show_page[$i]=$rowSQL['show_page'];
               $this->id_music_background[$i] =$rowSQL['id_music_background'];
               $this->template[$i]=$rowSQL['template'];
               $this->re_urls[$i]=$rowSQL['re_urls'];        
               $this->positons[$i]=$rowSQL['position'];
               $this->id_site[$i]=$rowSQL['id_site'];
               $this->id_parent_sected[$i] =$rowSQL['id_parent_sected'];                                      
                $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
               $this->postion=$i+1;  
              
               $this->select_multilevel_by_site($this->categoryId[$i], $insert_text.'&nbsp;'.'&nbsp;'.'&nbsp;');
                    }
            }
        }
     // $total= mysql_num_rows($rsSQL);
                 
        }  
        ///
            //select combox level
     function select_multilevel_combox($parent_id='0', $insert_text='&nbsp;')
    {  
        $this->id_site[0]=isset($this->id_site[0])?$this->id_site[0]:"";
        if($this->id_site[0]!="")
            $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3  
                and ".$this->id_site_fieldname."=".$this->id_site[0]."
                order by `order` ".  "";
                
         else
            $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3  
                order by `order` ".  "";
          
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                 $i=0;
                 foreach($rs as $rowSQL)
                 {
                      $i=$this->postion;
                    //category        
                  $objcategory=new cs_type_category();
                  $objcategory->do_selectall_typecateogory_id($rowSQL['id_type_category']);
                  $this->categoryId[$i]=$rowSQL['id'];
                  $this->id_type_category[$i]=$rowSQL['id_type_category'];
                   $this->category[$i]=$insert_text.$rowSQL['category'];
                   $this->status[$i]=$rowSQL['status'];
                   $this->id_parent[$i]=$rowSQL['id_parent'];
                   $this->small_images[$i]=$rowSQL['small_image'];
                   $this->large_images[$i]=$rowSQL['large_image'];
                   $this->order[$i]=$rowSQL['order'];
                   $this->url[$i]=$rowSQL['url'];
                   $this->categoryType[$i]= $objcategory->type[0];
                   $this->target[$i]=$rowSQL['target'];
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   $this->date_created[$i]=$rowSQL['date_created'];
                   $this->id_user_created[$i]=$rowSQL['id_user_created'];
                   $this->date_updated[$i]=$rowSQL['date_updated'];
                   $this->date_deleted[$i]=$rowSQL['date_deleted'];
                   $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
                   $this->keyword[$i]=$rowSQL['keyword'];
                   $this->page_title[$i]=$rowSQL['page_title'];
                   $this->meta_description[$i]=$rowSQL['meta_description'];
                   $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
                   $this->show_page[$i]=$rowSQL['show_page'];
                   $this->id_music_background[$i] =$rowSQL['id_music_background'];
                   $this->template[$i]=$rowSQL['template'];
                   $this->re_urls[$i]=$rowSQL['re_urls'];        
                   $this->positons[$i]=$rowSQL['position'];
                   $this->id_parent_sected[$i] =$rowSQL['id_parent_sected'];                                      
                   //echo $i."-";
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   //echo $parent_id."--";
                   $this->postion=$i+1;  
                     $this->select_multilevel($this->categoryId[$i], $insert_text.'&nbsp;'.'&nbsp;'.'&nbsp;');
                 }
            }
       }                       
    }  
    ///tre view 
     function select_multilevel_tree_view($parent_id='0', $insert_text='&nbsp;')
    {  
            $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <2 ";
            if($this->id_site[0]!="")
            $sql.="and   ".$this->id_site_fieldname."='".$this->id_site[0]."'    ";
            $sql.="and   ".$this->id_type_category_fieldname."='".$this->id_type_category[0]."' 
                  order by `order` ".  "";
      
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                 $i=0;
                 foreach($rs as $rowSQL)
                 {
                      $i=$this->postion;
                    //category        
                  $objcategory=new cs_type_category();
                  $objcategory->do_selectall_typecateogory_id($rowSQL['id_type_category']);
                  $this->categoryId[$i]=$rowSQL['id'];
                  $this->id_type_category[$i]=$rowSQL['id_type_category'];
                   $this->category[$i]=$insert_text.$rowSQL['category'];
                   $this->status[$i]=$rowSQL['status'];
                   $this->id_parent[$i]=$rowSQL['id_parent'];
                   $this->small_images[$i]=$rowSQL['small_image'];
                   $this->large_images[$i]=$rowSQL['large_image'];
                   $this->order[$i]=$rowSQL['order'];
                   $this->url[$i]=$rowSQL['url'];
                   $this->categoryType[$i]= $objcategory->type[0];
                   $this->target[$i]=$rowSQL['target'];
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   $this->date_created[$i]=$rowSQL['date_created'];
                   $this->id_user_created[$i]=$rowSQL['id_user_created'];
                   $this->date_updated[$i]=$rowSQL['date_updated'];
                   $this->date_deleted[$i]=$rowSQL['date_deleted'];
                   $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
                   $this->keyword[$i]=$rowSQL['keyword'];
                   $this->page_title[$i]=$rowSQL['page_title'];
                   $this->meta_description[$i]=$rowSQL['meta_description'];
                   $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
                   $this->show_page[$i]=$rowSQL['show_page'];
                   $this->id_music_background[$i] =$rowSQL['id_music_background'];
                   $this->template[$i]=$rowSQL['template'];
                   $this->re_urls[$i]=$rowSQL['re_urls'];        
                   $this->positons[$i]=$rowSQL['position'];
                   $this->id_parent_sected[$i] =$rowSQL['id_parent_sected'];                                      
                   //echo $i."-";
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   //echo $parent_id."--";
                   $this->postion=$i+1;  
                     $this->select_multilevel_tree_view($this->categoryId[$i], $insert_text.'&nbsp;'.'&nbsp;'.'&nbsp;');
                 }
            }
       }                       
    }  
    ///
     function select_multilevel_combox_selected($parent_id='0', $insert_text='&nbsp;')
    {
      $sql="SELECT * FROM category WHERE id_parent=".$parent_id." and status <> 3   order by `order` ".  "";
       if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                 $i=0;
                 foreach($rs as $rowSQL)
                 {
                     $i=$this->postion;
                    //category        
                  $objcategory=new cs_type_category();
                  $objcategory->do_selectall_typecateogory_id($rowSQL['id_type_category']);
                  $this->categoryId[$i]=$rowSQL['id'];
                  $this->id_type_category[$i]=$rowSQL['id_type_category'];
                   $this->category[$i]=$insert_text.$rowSQL['category'];
                   $this->status[$i]=$rowSQL['status'];
                   $this->id_parent[$i]=$rowSQL['id_parent'];
                   $this->small_images[$i]=$rowSQL['small_image'];
                   $this->large_images[$i]=$rowSQL['large_image'];
                   $this->order[$i]=$rowSQL['order'];
                   $this->url[$i]=$rowSQL['url'];
                   $this->categoryType[$i]= $objcategory->type[0];
                   $this->target[$i]=$rowSQL['target'];
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   $this->date_created[$i]=$rowSQL['date_created'];
                   $this->id_user_created[$i]=$rowSQL['id_user_created'];
                   $this->date_updated[$i]=$rowSQL['date_updated'];
                   $this->date_deleted[$i]=$rowSQL['date_deleted'];
                   $this->id_user_deleted[$i]=$rowSQL['id_user_deleted'];
                   $this->keyword[$i]=$rowSQL['keyword'];
                   $this->page_title[$i]=$rowSQL['page_title'];
                   $this->meta_description[$i]=$rowSQL['meta_description'];
                   $this->meta_keyword[$i]=$rowSQL['meta_keyword'];
                   $this->show_page[$i]=$rowSQL['show_page'];
                   $this->id_music_background[$i] =$rowSQL['id_music_background'];
                   $this->template[$i]=$rowSQL['template'];
                   $this->re_urls[$i]=$rowSQL['re_urls'];        
                   $this->positons[$i]=$rowSQL['position'];
                   $this->id_parent_sected[$i] =$rowSQL['id_parent_sected'];                                      
                   $this->show_subCategory[$i]=$rowSQL['show_subCategory'];
                   $this->postion=$i+1;  
                   $this->select_multilevel($this->categoryId[$i], $insert_text.'&nbsp;'.'&nbsp;'.'&nbsp;');	
                 }
            }
         }        
                     
    }  
    //funcion hien thi combox menu da cap
    //nguoi tao:haithe1988
    //ngaytao:23/04/2010
    function createMutilevelCombox($id)
    {
         $com="";
         $com.="<SELECT name='ddlParent' id='ddlParent' style='width:300px' onchange='return checksite();'>";
         $com.='<option value="0" selected="selected" >--Root--</option>';
         $this->flags=$this->id_s;
         $this->select_multilevel_combox();
         $temp="";
            for($i=0;$i<count($this->categoryId);$i++)
                {        //echo $this->flag;                                    
                   if($this->categoryId[$i]!=$this->flags)
                   {                    
                    $com.='<option value="'.$this->categoryId[$i].'" ';
                       if($this->categoryId[$i]==$id)
                            $com.="  selected='selected'";                      
                    $com.=' >'.$this->category[$i].'</option>';                                      
                    }    
                }
        $com.="</SELECT>";
        return $com;
    }
    //23-04-2010
      function update_category_with_url()
    {
        try
        {
            $sql = "";
            $sql = " update ".$this->tbl."
                        set ".$this->url_fieldname."='".$this->url[0]."'
                        where ".$this->id_fieldname."=".$this->id[0];
           if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            return 0;
        }
        catch(exception $e)
        {
            var_dump($e->getMessage());
        }
   
        return $rs;
    }
    //END
      /**
     * MO TA CHUC NANG: 
     * Create combox position
     * 
     * Tham so:$id :vi tri position tuong ung
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
  public function create_combox_site($site_id,$javacript)
    {
        //echo $position;
       
        $rs="";
        $str="";
        $sql="select * from sites where 	status <2";
        if($javacript!="")
            $str.='<select width="20" name="site" id="site" onchange="'.$javacript.'">';    
        else
           $str.="<select width='20' name='site' id='site'>";    
   
        if($sql!="")
        {   
                $str.="<option value=''  selected='selected'>---</option>";
                $rs=$this->db->sql_execute($sql, $this->db->isSelect);
                $i=0;
                foreach($rs as $row)
                {
                    if($site_id!="")
                    {
                        if($row['id']==$site_id)
                            $str.="<option value='".$row['id']."' selected='selected'>".$row['site_name']."</option>";
                        else
                            $str.="<option value='".$row['id']."'  >".$row['site_name']."</option>";
                    }
                    else
                    {
                        if($i==$rs->rowCount()-1)
                        {
                            $str.="<option value='".$row['id']."' selected='selected'>".$row['site_name']."</option>";
                            $_SESSION["site"] = $row['id'];
                        }
                        else
                            $str.="<option value='".$row['id']."'  >".$row['site_name']."</option>";
                        
                    }
                    $i++;
                }
        }
        
        $str.="</select>";
        return $str;
        
    }
    public function update_position($txtparent,$id_site)
    {
         
         $query="update category  set id_parent =".$txtparent. ""; 
            if($txtparent!=0)
            {  
                $id_site_by_parent=$this->get_id_site_by_id($txtparent);
                 if($id_site_by_parent!=$id_site)
                $query.=",".$this->id_site_fieldname."=".$id_site_by_parent."  ";
            }       
             $id=isset($_GET['id'])?$_GET['id']:"";
             $type=isset($_GET['type'])?$_GET['type']:"";
             if($type=="add")
             {
                     $query.=" where  	 id=".$this->getMaxId();
                        if($txtparent==0)
                        return true;
             }
             else{
                 if($id!="")
            $query.=" where  	 id=".$id;
            else
            $query.=" where  	 id=".$this->getMaxId();
             }
            if($id!="")
            {
               if($txtparent==$this->getCategory_parent_id($id))
               return true;
            }

      
         if($query!="")
               return $this->db->sql_execute($query,$this->db->isUpdate);
          else
            return 0;
           
    }
      //END
            function update_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
            if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
                ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
                ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
             {
                    if($_FILES[$resource]['size']<$size)
                    {
                         $filename= $_FILES[$resource]["type"];
                         
                     switch($filename)
                         {
                            case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                        break;
                             case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                        break;                        
                            case "image/gif"      :   $file    =uniqid("").".gif";
                                                                        break;
                            case "image/png"      :     $file    =uniqid("").".png";
                                                                         break;         
                         }
                        if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
                        {
                             if($f==1)
                             {
                                $this->large_image[0]=$file;
                             }
                             else
                             {
                                $this->small_image[0]=$file;
                             }
                            $flag=true;
                        }
                            
                    }
                     else
                     $flag = false;
                
             }
         return $flag;
            
          }
              catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
          
       }
            function get_max_id_site()
       {
            $rs="";
            $sql="SELECT max(id) from   site ";

            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
             if($rs!="")
                {
                        $i=0;
                        foreach($rs as $row)
                        {
                            $max =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"";      
                        }
                }
            
         return $max;
       }
    
}        


?>