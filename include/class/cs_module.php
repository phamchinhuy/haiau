<?php

/**
 * @author MINHLOI.NET
 * @copyright 2010
 */

 class module
 {
      public $tbl="module";
      public $id=array();
      public $module_name=array();
      ///
      public $id_fieldname="id";
      public $module_name_fieldname="module_name"; 	
      private $db="";
      function module()
      {
        $this->db=new db();
      }
      function getsource($rs)
      {
         if($rs!="")
         {
             $i=0;
             if($rs->rowCount()>0)
             {
                 foreach($rs as $row)
                 {
                 $this->id[$i]=$row[$this->id_fieldname];
                 $this->module_name[$i]=$row[$this->module_name_fieldname];
                 $i++;
                 }
             }
         }
      }
      function getall()
      {
          $sql="";
          $sql.=" select * from ".$this->tbl;
          $rs="";
          if($sql!="")
          {
             $rs=$this->db->sql_execute($sql,$this->db->isSelect);
             if($rs!="")
              {
                 $this->getsource($rs);
              }
          }
      }
      function create_cb($template)
      {
         $str="";
         $this->getall();
         for($i=0;$i<count($this->id);$i++)
         {
             $str.='<option value="'.trim($this->module_name[$i]).'" ';
             if($template==trim($this->module_name[$i]))
              $str.='  selected="selected" ';
             
             $str.= ' >'.$this->module_name[$i].'</option>';
         }
         return $str;
      }
 }

?>