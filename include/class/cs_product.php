<?php

    class cs_product //create by tran van tam 27/4/2010
    {
        
        //table name
        public $tbl = "product";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $product             = array();
        public $product_code        = array();
        public $short_description   = array();
        public $long_description    = array();
        public $small_image         = array();
        public $large_image         = array();
        public $status              = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $is_comment          = array();
        public $price               = array();
        public $priceSaleOf         = array();
        public $re_urls             = array();
        public $size                = array();
        public $color               = array();
        public $page_title          =array();
        public $meta_keyword        =array();
        public $meta_description    =array();
        
        
       
        
        
        
        
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $product_fieldname           = "product";
        public $product_code_fieldname      = "product_code";
        public $short_description_fieldname = "short_description";
        public $long_description_fieldname  = "long_description";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $status_fieldname            = "status";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $is_comment_fieldname        = "is_comment";
        public $price_fieldname             = "price";
        public $priceSaleOf_fieldname       = "priceSaleOf";
        public $re_urls_fieldname           = "re_urls";
        public $color_fieldname             = "color";
        public $size_fieldname              = "size";
        public $page_title_fieldname          ="page_title";
        public $meta_keyword_fieldname        ="meta_keyword";
        public $meta_description_fieldname    ="meta_description";
         
         //
        private $db=null;
        //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_product();
            
        }
        public function cs_product()
        {
            $this->db=new db();
        }
        public function insert_product()
        {
            try
            {             
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->id_category_fieldname."
                                ,".$this->product_fieldname."
                                ,".$this->product_code_fieldname."
                                ,".$this->short_description_fieldname."
                                ,".$this->long_description_fieldname."
                                ,".$this->small_image_fieldname."
                                ,".$this->large_image_fieldname."
                                ,".$this->status_fieldname."
                                ,".$this->date_created_fieldname."
                                ,".$this->id_user_created_fieldname."
                                ,".$this->date_updated_fieldname."
                                ,".$this->id_user_updated_fieldname."
                                ,".$this->date_deleted_fieldname."
                                ,".$this->id_user_deleted_fieldname."
                                ,".$this->is_comment_fieldname."
                                ,".$this->price_fieldname."
                                ,".$this->priceSaleOf_fieldname."
                                ,".$this->re_urls_fieldname."
                                 ,".$this->color_fieldname."
                                 ,".$this->size_fieldname."                                
                                 ,".$this->page_title_fieldname."
                                 ,".$this->meta_keyword_fieldname."
                                 ,".$this->meta_description_fieldname."
                            ) 
                            values 
                            ('".$this->id_category[0]."'
                                , '".$this->product[0]."'
                                , '".$this->product_code[0]."'
                                , '".$this->short_description[0]."'
                                , '".$this->long_description[0]."'
                                , '".$this->small_image[0]."'
                                , '".$this->large_image[0]."'
                                , '".$this->status[0]."'
                                , '".$this->date_created[0]."'
                                , '".$this->id_user_created[0]."'
                                , '".$this->date_updated[0]."'
                                , '".$this->id_user_updated[0]."'
                                , '".$this->date_deleted[0]."'
                                , '".$this->id_user_deleted[0]."'
                                , '".$this->is_comment[0]."'
                                , '".$this->price[0]."'
                                , '".$this->priceSaleOf[0]."'
                                , '".$this->re_urls[0]."'  
                                , '".$this->color[0]."'  
                                , '".$this->size[0]."'
                                , '".$this->page_title[0]."'  
                                , '".$this->meta_keyword[0]."'  
                                , '".$this->meta_description[0]."'    
                            )";             
              $rs=false;
              if($sql!="")
              {
                $rs= $this->db->sql_execute($sql,$this->db->isInsert);
              }
             
                return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function update_product()
        {
            try
            {
                $rs = false;
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->product_fieldname."='".$this->product[0]."'
                                , ".$this->product_code_fieldname."='".$this->product_code[0]."'
                                , ".$this->short_description_fieldname."='".$this->short_description[0]."'
                                , ".$this->long_description_fieldname."='".$this->long_description[0]."'";
                                
                                if(isset($this->small_image[0])&&$this->small_image[0]!="")
                                {
                                    $sql.= " , ".$this->small_image_fieldname."='".$this->small_image[0]."'";
                                }
                
                                   if(isset($this->large_image[0])&&$this->large_image[0]!="") 
                                   {
                                         $sql.= ", ".$this->large_image_fieldname."='".$this->large_image[0]."'";
                                   }
           
                               
                $sql.=          ", ".$this->status_fieldname."='".$this->status[0]."'
                              
                          
                                , ".$this->date_updated_fieldname."='".$this->date_updated[0]."'
                                , ".$this->id_user_updated_fieldname."='".$this->id_user_updated[0]."'
                        
                                , ".$this->is_comment_fieldname."='".$this->is_comment[0]."' 
                                 , ".$this->color_fieldname."='".$this->color[0]."'
                                    , ".$this->size_fieldname."='".$this->size[0]."'
                                , ".$this->price_fieldname."='".$this->price[0]."'
                                , ".$this->priceSaleOf_fieldname."='".$this->priceSaleOf[0]."'
                                , ".$this->re_urls_fieldname."='".$this->re_urls[0]."'
                                , ".$this->color_fieldname."='".$this->color[0]."'
                                , ".$this->size_fieldname."='".$this->size[0]."'
                                 , ".$this->page_title_fieldname."='".$this->page_title[0]."'
                                 , ".$this->meta_keyword_fieldname."='".$this->meta_keyword[0]."'
                                 , ".$this->meta_description_fieldname."='".$this->meta_description[0]."'
                             
                            where ".$this->id_fieldname."=".$this->id[0];
              ////echo $sql;
             if($sql!="")
             {
                $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
             }
             return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_product_by_id_category_status($p_id_category, $p_status)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname." = ".$p_status."
                                order by ".$this->id_fieldname." desc";
                                
                $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getsource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_product_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
                
                $sql = "";
                $sql = "select *  
                        from product
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                  $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getsource($rs);
                }               
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_product_by_id_category($p_id_category)
        {
            try
            {
                $num=0;
                $sql = "";
                $sql = "select *  
                        from product
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by `id` desc";
                          
               
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getsource($rs);
                    return $num=$rs->rowCount();
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
 
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_product_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from product 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getsource($rs);
                    return $rs->rowCount();
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
  
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_product_by_id($p_id)
        {
            try
            {
              
                $sql = "";
                $sql = "select *  
                        from product
                        where   ".$this->id_fieldname."=".$p_id." order by `id`"; 
                 $rs="";
               
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->getsource($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        //27-04-2010
        public function update_product_set_status($p_status,$p_id)
        {
            try
            {
                $rs = false;
               
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
               //echo $sql;
                 if($sql!="")
                 {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                 }
                 return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }

        }
        //END
         public function update_product_set_comment($status_comment,$p_id)
        {
            try
            {
                $rs = false;
              
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->is_comment_fieldname."='".$status_comment."' 
                            where ".$this->id_fieldname."=".$p_id;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                }
                return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        //27-04-2010 created by tran van tam
        public function delete_product_by_id($p_id)
        {
            try
            {
                $rs = false;
                $sql = "";
                $sql = " delete from ".$this->tbl." where ".$this->id_fieldname."=".$p_id;                    
                $rs=false;
                $rs=$this->db->sql_execute($sql,$this->db->isDelete);
                return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        function RewriteURLs($str)
        {
        $marTViet=array("�","�","?","?","�","�","?","?","?","?","?","a",
       "?","?","?","?","?","�","�","?","?","?","�","?"
        	,"?","?","?","?",
        	"�","�","?","?","i",
        	"�","�","?","?","�","�","?","?","?","?","?","o"
        	,"?","?","?","?","?",
        	"�","�","?","?","u","u","?","?","?","?","?",
        	"?","�","?","?","?",
        	"d",
        	"�","�","?","?","�","�","?","?","?","?","?","A"
        	,"?","?","?","?","?",
        	"�","�","?","?","?","�","?","?","?","?","?",
        	"�","�","?","?","I",
        	"�","�","?","?","�","�","?","?","?","?","?","O"
        	,"?","?","?","?","?",
        	"�","�","?","?","U","U","?","?","?","?","?",
        	"?","�","?","?","?",
        	"�");
        	 
        	$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
        	,"a","a","a","a","a","a",
        	"e","e","e","e","e","e","e","e","e","e","e",
        	"i","i","i","i","i",
        	"o","o","o","o","o","o","o","o","o","o","o","o"
        	,"o","o","o","o","o",
        	"u","u","u","u","u","u","u","u","u","u","u",
        	"y","y","y","y","y",
        	"d",
        	"A","A","A","A","A","A","A","A","A","A","A","A"
        	,"A","A","A","A","A",
        	"E","E","E","E","E","E","E","E","E","E","E",
        	"I","I","I","I","I",
        	"O","O","O","O","O","O","O","O","O","O","O","O"
        	,"O","O","O","O","O",
        	"U","U","U","U","U","U","U","U","U","U","U",
        	"Y","Y","Y","Y","Y",
        	"D");        
            $result= str_replace($marTViet,$marKoDau,$str);
             $result= str_replace(" ","-",$result);
            return $result;
        } 
        //END
        function getsource($rs)
        {
              if($rs!=null)
            {            
                $i=0;
                foreach($rs as $row)
                {
                $this->id[$i] =$row[$this->id_fieldname]; 
                $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                $this->product[$i] = $row[$this->product_fieldname];
                $this->product_code[$i] = $row[$this->product_code_fieldname];
                $this->short_description[$i] = $row[$this->short_description_fieldname];
                $this->long_description[$i] = $row[$this->long_description_fieldname];
                $this->small_image[$i] = $row[$this->small_image_fieldname];
                $this->large_image[$i] = $row[$this->large_image_fieldname];
                $this->status[$i] = $row[$this->status_fieldname];
                $this->date_created[$i] = $row[$this->date_created_fieldname];
                $this->id_user_created[$i] = $row[$this->id_user_created_fieldname];
                $this->date_updated[$i] = $row[$this->date_updated_fieldname];
                $this->id_user_updated[$i] =$row[$this->id_user_updated_fieldname];
                $this->date_deleted[$i] = $row[$this->date_deleted_fieldname];
                $this->id_user_deleted[$i] = $row[$this-> id_user_deleted_fieldname];
                $this->is_comment[$i] = $row[$this->is_comment_fieldname];
                $this->price[$i] = $row[$this->price_fieldname];
                $this->priceSaleOf[$i] = $row[$this->priceSaleOf_fieldname];
                $this->color[$i] = $row[$this->color_fieldname];
                $this->size[$i] = $row[$this->size_fieldname];
                $this->re_urls[$i] =$row[$this->re_urls_fieldname];
                $this->page_title[$i] = $row[$this->page_title_fieldname];
                $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname];
                $this->meta_description[$i] =$row[$this->meta_description_fieldname];
                $i++;
                }
            }
       } 
       function update_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
         if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
            ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
            ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
         {
            if($_FILES[$resource]['size']<$size)
            {
                 $filename= $_FILES[$resource]["type"];
                 
             switch($filename)
                 {
                    case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                break;
                     case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                break;                        
                    case "image/gif"      :   $file    =uniqid("").".gif";
                                                                break;
                    case "image/png"      :     $file    =uniqid("").".png";
                                                                 break;         
                 }
            if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
            {
                 if($f==1)
                 {
                    $this->large_image[0]=$file;
                 }
                 else
                 {
                    $this->small_image[0]=$file;
                 }
                $flag=true;
            }
                    
            }
            else
                 $flag = false;
            
         }
         return $flag;
            
          }
          catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
       }
           function check_page_title_extis()
           {
              try
                {
                  
                    $sql = "";
                    $sql = "select *  
                            from ".$this->tbl."
                            where   ".$this->page_title_fieldname."='".$this->page_title[0]."'"; 
                     $rs="";
                   
                    if($sql!="")
                    {                  
                        $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                        if($rs->rowCount()>0)
                        return true;
                        else
                        return false;
                    }
                }
                catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
           }
    }
?>