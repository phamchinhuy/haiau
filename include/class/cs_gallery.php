<?php
    class cs_gallery
    {
        
        //table name
        public $tbl = "gallery";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $title               = array();
        public $small_image         = array();
        public $large_image         = array();
        public $position            = array();
        public $status              = array();
        public $url                 = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $image_note          = array();
        public $image_x             = array();
        public $image_y             = array();
        public $alt                 = array();
        public $keyword             = array();
        public $page_title          = array();
        public $meta_description    = array();
        public $meta_keyword        = array();
        public $show_page           = array();
        public $is_comment          = array();
        public $count_view          = array();
        public $begin_date          = array();
        public $begin_end           = array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $title_fieldname             = "title";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $position_fieldname          = "position";
        public $status_fieldname            = "status";
        public $url_fieldname               = "url";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $image_note_fieldname        = "image_note";
        public $image_x_fieldname           = "image_x";
        public $image_y_fieldname           = "image_y";
        public $alt_fieldname               = "alt";
        public $keyword_fieldname           = "keyword";
        public $page_title_fieldname        = "page_title";
        public $meta_description_fieldname  = "meta_description";
        public $meta_keyword_fieldname      = "meta_keyword";
        public $show_page_fieldname         = "show_page";
        public $is_comment_fieldname        = "is_comment";
        public $count_view_fieldname        = "count_view";
        public $begin_date_fieldname        = "begin_date";
        public $begin_end_fieldname         = "begin_end";
        private $db=null;
        //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_gallery();
        }
        function cs_gallery()
        {
            $this->db=new db();
        }
        
        public function insert_gallery()
        {
            try
            {
                $rs = false;
             
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->id_category_fieldname."
                                ,".$this->title_fieldname."
                                ,".$this->small_image_fieldname."
                                ,".$this->large_image_fieldname."
                                ,".$this->position_fieldname."
                                ,".$this->status_fieldname."
                                ,".$this->url_fieldname."
                                ,".$this->date_created_fieldname."
                                ,".$this->id_user_created_fieldname."
                                ,".$this->date_updated_fieldname."
                                ,".$this->id_user_updated_fieldname."
                                ,".$this->date_deleted_fieldname."
                                ,".$this->id_user_deleted_fieldname."
                                ,".$this->image_note_fieldname."
                                ,".$this->image_x_fieldname."
                                ,".$this->image_y_fieldname."
                                ,".$this->alt_fieldname."
                                ,".$this->keyword_fieldname."
                                ,".$this->page_title_fieldname."
                                ,".$this->meta_description_fieldname."
                                ,".$this->meta_keyword_fieldname."
                                ,".$this->show_page_fieldname."
                                ,".$this->is_comment_fieldname."
                                ,".$this->count_view_fieldname."
                                ,".$this->begin_date_fieldname."
                                ,".$this->begin_end_fieldname."
                                
                            ) 
                            values 
                            ('".$this->id_category[0]."'
                                , '".$this->title[0]."'
                                , '".$this->small_image[0]."'
                                , '".$this->large_image[0]."'
                                , '".$this->position[0]."'
                                , '".$this->status[0]."'
                                , '".$this->url[0]."'
                                , '".$this->date_created[0]."'
                                , '".$this->id_user_created[0]."'
                                , '".$this->date_updated[0]."'
                                , '".$this->id_user_updated[0]."'
                                , '".$this->date_deleted[0]."'
                                , '".$this->id_user_deleted[0]."'
                                , '".$this->image_note[0]."'
                                , '".$this->image_x[0]."'
                                , '".$this->image_y[0]."'
                                , '".$this->alt[0]."'
                                , '".$this->keyword[0]."'
                                , '".$this->page_title[0]."'
                                , '".$this->meta_description[0]."'
                                , '".$this->meta_keyword[0]."'
                                , '".$this->show_page[0]."'
                                , '".$this->is_comment[0]."'
                                , '".$this->count_view[0]."'
                                , '".$this->begin_date[0]."'
                                , '".$this->begin_end[0]."'
                            )";
                //echo $sql;
               if($sql!="")
               {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    return $rs;
               }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
            return $rs;
        }
        //END
        
        
        //23-04-2010
        public function update_gallery()
        {
            try
            {
                $rs = false;
             
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->title_fieldname."='".$this->title[0]."'
                                , ".$this->small_image_fieldname."='".$this->small_image[0]."'
                                , ".$this->large_image_fieldname."='".$this->large_image[0]."'
                                , ".$this->url_fieldname."='".$this->url[0]."'
                                , ".$this->image_note_fieldname."='".$this->image_note[0]."'
                                , ".$this->status_fieldname."='".$this->status[0]."'
                                , ".$this->is_comment_fieldname."='".$this->is_comment[0]."'
                                , ".$this->keyword_fieldname."='".$this->keyword[0]."'
                                , ".$this->page_title_fieldname."='".$this->page_title[0]."'
                                , ".$this->meta_description_fieldname."='".$this->meta_description[0]."'
                                , ".$this->meta_keyword_fieldname."='".$this->meta_keyword[0]."'
                                , ".$this->date_updated_fieldname."='".$this->date_updated[0]."'
                                , ".$this->id_user_updated_fieldname."='".$this->id_user_updated[0]."' 
                            where ".$this->id_fieldname."=".$this->id[0];
                //echo $sql;
                  if($sql!="")
                  {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                  }
                  return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_status($p_id_category, $p_status)
        {
            try
            {
             
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname." = ".$p_status."
                                order by ".$this->position_fieldname." desc";
                 $rs="";               
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
             
                $sql = "";
                $sql = "select *  
                        from gallery 
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                order by ".$this->position_fieldname." 
                                limit ".$p_start.", ".$p_end;
                  $rs="";              
                 if($sql!="")
                 {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category($p_id_category)
        {
            try
            {
             
                $sql = "";
                $sql = "select *  
                        from gallery 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by ".$this->position_fieldname." ";
                $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
             
                $sql = "";
                $sql = "select *  
                        from gallery 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by ".$this->position_fieldname."  
                                limit   "." ".$p_start.", ".$p_end;
                                
                $result ="";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_gallery_by_id($p_id)
        {
            try
            {
             
                $sql = "";
                $sql = "select *  
                        from gallery 
                        where   ".$this->id_fieldname."=".$p_id; 
                $result ="";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
          public function select_max_postion_gallery_by_id($p_id)
        {
            try
            {
                $max=0;
                $sql = "";
                $sql = "select max(".$this->position_fieldname.") as ".$this->position_fieldname."  
                        from ".$this->tbl."
                        where   ".$this->id_category_fieldname."=".$p_id; 
                $result ="";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    foreach($result as $row)
                    $max=isset($row[$this->position_fieldname])?$row[$this->position_fieldname]:"0";
                 }
         
                 return $max;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
            public function select_gallery_position_by_id($p_id)
        {
            try
            {
                $max=0;
                $sql = "";
                $sql = "select max(".$this->position_fieldname.") as ".$this->position_fieldname."  
                        from ".$this->tbl."
                        where   ".$this->id_category_fieldname."=".$p_id; 
                $result ="";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    foreach($result as $row)
                    $max=isset($row[$this->position_fieldname])?$row[$this->position_fieldname]:"0";
                 }
         
                 return $max;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
           
          
        }
        //END
        
        //23-04-2010
        public function update_gallery_set_status($p_status,$p_id)
        {
            try
            {
                $rs = false;
             
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
                $result = "";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $result;
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
            
        }
        //END
        function get_source($rs)
        {
            if($rs!="")
            {
                 $i=0;
                 foreach($rs as $row)
                 {
                       $this->id[$i] =$row[$this->id_fieldname]; 
                        $this->id_category[$i] = $row[$this->id_category_fieldname]; 
                        $this->title[$i] = $row[$this->title_fieldname]; 
                        $this->small_image[$i] = $row[$this->small_image_fieldname]; 
                        $this->large_image[$i] = $row[$this->large_image_fieldname]; 
                        $this->position[$i] = $row[$this->position_fieldname]; 
                        $this->status[$i] = $row[$this->status_fieldname]; 
                        $this->url[$i] = $row[$this->url_fieldname]; 
                        $this->date_created[$i] = $row[$this->date_created_fieldname]; 
                        $this->id_user_created[$i] = $row[$this->id_user_created_fieldname]; 
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname]; 
                        $this->id_user_updated[$i] = $row[$this->id_user_updated_fieldname]; 
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname]; 
                        $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname]; 
                        $this->image_note[$i] = $row[$this->image_note_fieldname]; 
                        $this->image_x[$i] =$row[$this->image_x_fieldname]; 
                        $this->image_y[$i] = $row[$this->image_y_fieldname]; 
                        $this->alt[$i] = $row[$this->alt_fieldname]; 
                        $this->keyword[$i] = $row[$this->keyword_fieldname]; 
                        $this->page_title[$i] = $row[$this->keyword_fieldname]; 
                        $this->meta_description[$i] = $row[$this->meta_description_fieldname]; 
                        $this->meta_keyword[$i] =$row[$this->meta_keyword_fieldname]; 
                        $this->show_page[$i] = $row[$this->show_page_fieldname]; 
                        $this->is_comment[$i] = $row[$this->is_comment_fieldname]; 
                        $this->count_view[$i] = $row[$this->count_view_fieldname]; 
                        $this->begin_date[$i] =$row[$this->begin_date_fieldname]; 
                        $this->begin_end[$i] = $row[$this->begin_end_fieldname]; 
                        $i++;
                 }
            }
        }
            /**
     * MO TA CHUC NANG: 
     * Load advertise_id  trong tb advertise theo position
     * 
     * Tham so:$p_advertise :object gold_type truyen vao
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 09/07/2010
     */
   public function select_id_by_position($p)
    {
        $position =0;
        $sql ="select ".$this->id_fieldname." from  ".$this->tbl  ;
        $sql.="  where  ".$this->position_fieldname."=".$p;
        //echo $sql;
        if($sql!="")
        {
            $rs = $this->db->sql_execute($sql,$this->db->isSelect);
            if($rs!="")
            {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $position  =!empty($row[$this->id_fieldname])?$row[$this->id_fieldname]:"1";      
                    }
            }
        }
        return   $position;
    }
           /**
     * MO TA CHUC NANG: 
     * swap position of two record  
     * 
     * Tham so:$p_advertise :object advertise truyen vao
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 09/07/2010
     */
     public function swap_two_position($p1,$p2)
       {
        try{   
             $id_gallery1="";
             $id_gallery2="";
             $id_gallery1=$this->select_id_by_position($p1);
             $id_gallery2=$this->select_id_by_position($p2);
             $rs=false;
             $rs=$this->doUpdate_position($p1,$id_gallery2);
             if($rs)
             {
                $rs=false;
                $rs=$this->doUpdate_position($p2,$id_gallery1);
                 return $rs;          
             }
        }
         catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
       }
        /**
     * MO TA CHUC NANG: 
     * thay doi position trong $do_advertise
     * 
     * Tham so:$status :trang thai can update cua col status (0,1,2)
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 28/06/2010
     */
   public  function doUpdate_position($p,$id)
    {
       $sql="";
       $sql.="update ".$this->tbl." ";
       $sql.="set ".$this->position_fieldname."=".$p;
       $sql.="  where ".$this->id_fieldname."=".$id;
       // echo $sql;
        if($sql!="")
            return $this->db->sql_execute($sql, $this->db->isUpdate);
        else
            return 0;      
    }
           /**
     * MO TA CHUC NANG: 
     * Create combox position
     * 
     * Tham so:$id :vi tri position tuong ung
     * 
     * 
     * Thong tin nguoi tao:
     * Cong ty : NewSunSoft
     * Nguoi tao:Haithe1988
     * Ngay tao: 23/07/2010
     */
    public function create_combox_position($position,$id_cate)
    {
        //echo $position;
        $str="";
        $cs_gallery=new cs_gallery();
        if($id_cate!="")    
         $cs_gallery->select_gallery_by_id_category($id_cate);        
        //echo  $gold_type->position[1];
         $str.="<select width='20' name='cb".$position."' id='cb".$position."' onchange='change_gallary(myform.cb".$position.".value,myform.cbhd".$position.".value)'>";
        for($i=0;$i<count($cs_gallery->id);$i++)
        {
            if( $cs_gallery->position[$i]==$position)
           
           $str.="<option value='".$cs_gallery->position[$i]."' selected='selected'>".($i+1)."</option>";
           else
           $str.="<option value='".$cs_gallery->position[$i]."'  >".($i+1)."</option>";
        }
           $str.="</select>";
           return $str;
    }
        function update_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
            if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
                ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
                ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
             {
                    if($_FILES[$resource]['size']<$size)
                    {
                         $filename= $_FILES[$resource]["type"];
                         
                     switch($filename)
                         {
                            case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                        break;
                             case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                        break;                        
                            case "image/gif"      :   $file    =uniqid("").".gif";
                                                                        break;
                            case "image/png"      :     $file    =uniqid("").".png";
                                                                         break;         
                         }
                        if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
                        {
                             if($f==1)
                             {
                                $this->large_image[0]=$file;
                                $this->small_image[0]="temp";
                             }
                             else
                             {
                                $this->small_image[0]=$file;
                                $this->large_image[0]="";
                             }
                            $flag=true;
                        }
                            
                    }
                     else
                     $flag = false;
                
             }
         return $flag;
            
          }
              catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
          
       }
    
}
?>