<?php
    class cs_website
    {
        
        //table name
        public $tbl = "website";
        
        //value field
    
        public $id         = array();
        public $fullname                  =array();
        public $address                = array();
        public $email         = array();
        public $phone         = array();
        public $title   = array();
        public $status              = array();
        public $href                 = array();
        public $target              = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $id_cateogry         =array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $fullname_fieldname          = "fullname";
        public $address_fieldname           = "address";
        public $email_fieldname             = "email";
        public $phone_fieldname             = "phone";
        public $title_fieldname             = "title";
        public $status_fieldname            = "status";
        public $url_fieldname               = "url";
        public $target_fieldname            = "target";
        public $href_fieldname              = "href";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_created_fieldname      = "date_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $id_cateogry_fieldname       = "id_category";
        //end field name
        private $db="";
        protected $DataObject;
      
        function cs_website()
        {
            $this->db=new db();
        }
        public function insert_website()
        {
            try
            {
                $rs = false;
                
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->title_fieldname."
                              
                                ,".$this->status_fieldname."
                                 ,".$this->id_cateogry_fieldname."
                                ,".$this->href_fieldname."
                                ,".$this->target_fieldname."
                                ,".$this->date_created_fieldname."
                                ,".$this->id_user_created_fieldname."
                                ,".$this->date_updated_fieldname."
                                ,".$this->id_user_updated_fieldname."                               
                            ) 
                            values 
                            ('".$this->title[0]."'
                              , '".$this->status[0]."'
                                , ".$this->id_cateogry[0]."
                                
                                , '".$this->href[0]."'
                                , '".$this->target[0]."'
                                , '".$this->date_created[0]."'
                                , '".$this->id_user_created[0]."'
                                , '".$this->date_updated[0]."'
                                , '".$this->id_user_updated[0]."'
                            )";
                //echo $sql;
                $res=$this->db->sql_execute($sql,$this->db->isInsert);
                return $res;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        public function update_website()
        {
            try
            {
                $rs = false;
          
                $sql = "";
                $sql = " update ".$this->tbl."
                set ".$this->status_fieldname."='".$this->status[0]."'";                                   
                $sql.=", ".$this->title_fieldname."='".$this->title[0]."'
                 , ".$this->id_cateogry_fieldname."=".$this->id_cateogry[0]."
                , ".$this->status_fieldname."='".$this->status[0]."' 
          
                
                , ".$this->target_fieldname."='".$this->target[0]."'
                , ".$this->date_updated_fieldname."=now()
                , ".$this->id_user_updated_fieldname."='".$this->id_user_updated[0]."'
                , ".$this->href_fieldname."='".$this->href[0]."'                              
                where ".$this->id_fieldname."=".$this->id[0];
               // echo $sql;
                $rs = true;
              
                $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function  get_source($rs)
        {
            try{
                if($rs!="")
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname]; 
                     
                        $this->title[$i] =$row[$this->title_fieldname] ;
                        $this->target[$i] =$row[$this->target_fieldname]; 
                         $this->id_cateogry[$i] =$row[$this->id_cateogry_fieldname]; 
                        $this->href[$i] =$row[$this->href_fieldname] ;
                        $this->date_created[$i] =$row[$this->date_created_fieldname];
                        $this->id_user_created[$i] =$row[$this->id_user_created_fieldname];
                        $this->date_updated[$i] =$row[$this->date_updated_fieldname]; 
                        $this->id_user_updated[$i] =$row[$this->id_user_updated_fieldname]; 
                        $this->date_deleted[$i] =$row[$this->date_deleted_fieldname]; 
                        $this->id_user_deleted[$i] =$row[$this->id_user_deleted_fieldname] ;
                        $this->status[$i] =$row[$this->status_fieldname]; 
              
                        $i++;
                    }
                }
               
            }
             catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        public function select_webiste_by_id($p_id)
        {
            try
            {
              
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_fieldname."=".$p_id."
                                order by ".$this->id_fieldname." desc";
                $rs="";
               // echo $sql;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                }                
                
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_website_limit($p_cate, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where   status <3
                                and ".$this->id_cateogry_fieldname."=".$p_cate."
                                order by ".$this->date_created_fieldname." desc
                                limit ".$p_start.", ".$p_end;
               // echo $sql;                
                $result = "";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        public function do_select_website($p_idcate)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from      ".$this->tbl."
                        where     ".$this->tbl.".status <3
                        and       ".$this->id_cateogry_fieldname."=".$p_idcate."
                                  order by ".$this->date_created_fieldname." desc
                            ";
             
                $num=0;           
                $result = ""; 
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    if($result!="")
                        $num=$result->rowCount();
                        return $num;
                 }
             
                 return $num;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //23-04-2010
        //XUAN
       
        //END
        
        
        //23-04-2010
        //XUAN
       
  
        
        //23-04-2010
        public function update_website_set_status($p_status,$p_id)
        {
            try
            {
                $rs = "";
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
               
                $result =false;
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $result;
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
        }
      
     
       
    }
?>