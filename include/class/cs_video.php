<?php
    class cs_video //create by tran van tam 27/4/2010
    {
        
        //table name
        public $tbl = "video";
        
        //value field
        public $id                  = array();
        public $id_category         = array();
        public $video               = array();
        public $short_description   = array();
        public $long_description    = array();
        public $small_image         = array();
        public $large_image         = array();
        public $status              = array();
        public $date_created        = array();
        public $id_user_created     = array();
        public $date_updated        = array();
        public $id_user_updated     = array();
        public $date_deleted        = array();
        public $id_user_deleted     = array();
        public $url                 = array();
        public $file_extention      = array();
        public $show_page           = array();
        public $count_view          = array();
        public $is_comment          = array();
        public $keyword             = array();
        public $page_title          = array();
        public $meta_description    = array();
        public $meta_keyword        = array();
        public $begin_date          = array();
        public $begin_end           = array();
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $video_fieldname             = "video";
        public $short_description_fieldname = "short_description";
        public $long_description_fieldname  = "long_description";
        public $small_image_fieldname       = "small_image";
        public $large_image_fieldname       = "large_image";
        public $status_fieldname            = "status";
        public $date_created_fieldname      = "date_created";
        public $id_user_created_fieldname   = "id_user_created";
        public $date_updated_fieldname      = "date_updated";
        public $id_user_updated_fieldname   = "id_user_updated";
        public $date_deleted_fieldname      = "date_deleted";
        public $id_user_deleted_fieldname   = "id_user_deleted";
        public $url_fieldname               = "url";
        public $file_extention_fieldname    = "file_extention";
        public $show_page_fieldname         = "show_page";
        public $count_view_fieldname        = "count_view";
        public $is_comment_fieldname        = "is_comment";
        public $keyword_fieldname           = "keyword";
        public $page_title_fieldname        = "page_title";
        public $meta_description_fieldname  = "meta_description";
        public $meta_keyword_fieldname      = "meta_keyword";
        public $begin_date_fieldname        = "begin_date";
        public $begin_end_fieldname         = "begin_end";
        private $db="";
                //end field name
        
        protected $DataObject;
        function _construct()
        {
            $this->DataObject = new cs_video();
        }
        public function cs_video()
        {
          $this->db=new db();    
        }
        public function insert_video()
        {
            try
            {
                $rs = false;              
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                            (".$this->id_category_fieldname."
                                ,".$this->video_fieldname."
                                ,".$this->short_description_fieldname."
                                ,".$this->long_description_fieldname."
                                ,".$this->small_image_fieldname."
                                ,".$this->large_image_fieldname."
                                ,".$this->status_fieldname."
                                ,".$this->date_created_fieldname."
                                ,".$this->id_user_created_fieldname."
                                ,".$this->date_updated_fieldname."
                                ,".$this->id_user_updated_fieldname."
                                ,".$this->date_deleted_fieldname."
                                ,".$this->id_user_deleted_fieldname."
                                ,".$this->url_fieldname."
                                ,".$this->file_extention_fieldname."
                                ,".$this->show_page_fieldname."
                                ,".$this->count_view_fieldname."
                                ,".$this->is_comment_fieldname."
                                ,".$this->keyword_fieldname."
                                ,".$this->page_title_fieldname."
                                ,".$this->meta_description_fieldname."
                                ,".$this->meta_keyword_fieldname."
                                ,".$this->begin_date_fieldname."
                                ,".$this->begin_end_fieldname."
                               
                                
                            ) 
                            values 
                            ('".$this->id_category[0]."'
                                , '".$this->video[0]."'
                                , '".$this->short_description[0]."'
                                , '".$this->long_description[0]."'
                                , '".$this->small_image[0]."'
                                , '".$this->large_image[0]."'
                                , '".$this->status[0]."'
                                , '".$this->date_created[0]."'
                                , '".$this->id_user_created[0]."'
                                , '".$this->date_updated[0]."'
                                , '".$this->id_user_updated[0]."'
                                , '".$this->date_deleted[0]."'
                                , '".$this->id_user_deleted[0]."'
                                , '".$this->url[0]."'
                                , '".$this->file_extention[0]."'
                                , '".$this->show_page[0]."'
                                , '".$this->count_view[0]."'
                                , '".$this->is_comment[0]."'  
                               
                                , '".$this->keyword[0]."'
                                , '".$this->page_title[0]."'
                                , '".$this->meta_description[0]."'
                                , '".$this->meta_keyword[0]."'
                                , '".$this->begin_date[0]."'
                                , '".$this->begin_end[0]."'
                                
                            )";
                //echo $sql;
                $result =false;
               if($sql!="")
               {
                  $result=$this->db->sql_execute($sql,$this->db->isInsert);
                  return $result;
               }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
           
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function update_video()
        {
            try
            {
                $rs = false;
               
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->video_fieldname."='".$this->video[0]."'
                                , ".$this->short_description_fieldname."='".$this->short_description[0]."'
                                , ".$this->long_description_fieldname."='".$this->long_description[0]."'
                                , ".$this->small_image_fieldname."='".$this->small_image[0]."'
                                , ".$this->large_image_fieldname."='".$this->large_image[0]."'
                                , ".$this->status_fieldname."='".$this->status[0]."'
                                , ".$this->url_fieldname."='".$this->url[0]."'
                                , ".$this->date_created_fieldname."='".$this->date_created[0]."'
                                , ".$this->id_user_created_fieldname."='".$this->id_user_created[0]."'
                                , ".$this->date_updated_fieldname."='".$this->date_updated_fieldname[0]."'
                                , ".$this->id_user_updated_fieldname."='".$this->id_user_updated[0]."'
                                , ".$this->date_deleted_fieldname."='".$this->date_deleted[0]."'
                                , ".$this->id_user_deleted_fieldname."='".$this->id_user_deleted[0]."'
                                , ".$this->url_fieldname."='".$this->url[0]."'
                                , ".$this->file_extention_fieldname."='".$this->file_extention[0]."'
                                , ".$this->show_page_fieldname."='".$this->show_page[0]."'
                                , ".$this->count_view_fieldname."='".$this->count_view[0]."'
                                , ".$this->is_comment_fieldname."='".$this->is_comment[0]."' 
                                , ".$this->keyword_fieldname."='".$this->keyword[0]."'
                                , ".$this->page_title_fieldname."='".$this->page_title[0]."'
                                , ".$this->meta_description_fieldname."='".$this->meta_description[0]."'
                                , ".$this->meta_keyword_fieldname."='".$this->meta_keyword[0]."'
                                , ".$this->begin_date_fieldname."='".$this->begin_date[0]."'
                                , ".$this->begin_end_fieldname."='".$this->begin_end[0]."'
                               
                               
                            where ".$this->id_fieldname."=".$this->id[0];
               
                $result = false;
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $result;
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
            return $rs;
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category_status($p_id_category, $p_status)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname." = ".$p_status."
                                order by ".$this->id_fieldname." desc";
                                
                $result ="";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
          
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from video
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                 and status < 3
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                 $result="";               
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
         
          
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category($p_id_category)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from video 
                        where   id_category=".$p_id_category." 
                         and status < 3
                                order by `id` desc";
                  $result="";              
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        
          
        }
        //END
        
        
        //27-04-2010
        //Tam
        public function select_video_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from video 
                        where   id_category=".$p_id_category." 
                        and status < 3
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $result = "";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
                   
        }
        //END
        
        //27-04-2010
        //Tam
        public function select_video_by_id($p_id)
        {
            try
            {               
                $sql = "";
                $sql = "select *  
                        from video
                        where   ".$this->id_fieldname."=".$p_id." order by `id`"; 
                $result="";
                if($sql!="")
                {
                      $result=$this->db->sql_execute($sql,$this->db->isSelect);
                      $this->get_source($result);
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }    
        }
        //END
        
        //27-04-2010
        public function update_video_set_status($p_status,$p_id)
        {
            try
            {
                $rs = false;
               
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."='".$p_status."' 
                            where ".$this->id_fieldname."=".$p_id;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $rs;
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
            return $rs;
        }
        //END
         public function update_video_set_comment($status_comment,$p_id)
        {
            try
            {
                $rs = false;              
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->is_comment_fieldname."='".$status_comment."' 
                            where ".$this->id_fieldname."=".$p_id;
                 if($sql!="")
                 {
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $rs;
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        //27-04-2010 created by tran van tam
        public function delete_video_by_id($p_id)
        {
            try
            {
                $rs = false;
               
                $sql = "";
                $sql = " delete from ".$this->tbl." where ".$this->id_fieldname."=".$p_id;
                                              
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isDelete);
                }
                return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
            return $rs;
        }
        //END
        function get_source($rs)
        {
            if($rs!="")
            {
                $i=0;
                foreach($rs as $row)
                {
                        $this->id[$i] = $row[$this->id_fieldname]; 
                        $this->id_category[$i] =$row[$this->id_category_fieldname]; 
                        $this->video[$i] = $row[$this->video_fieldname]; 
                        $this->short_description[$i] = $row[$this->short_description_fieldname]; 
                        $this->long_description[$i] = $row[$this->long_description_fieldname]; 
                        $this->small_image[$i] = $row[$this->small_image_fieldname]; 
                        $this->large_image[$i] = $row[$this->large_image_fieldname]; 
                        $this->status[$i] = $row[$this->status_fieldname]; 
                        $this->date_created[$i] = $row[$this->date_created_fieldname]; 
                        $this->id_user_created[$i] =$row[$this->id_user_created_fieldname]; 
                        $this->date_updated[$i] = $row[$this->date_updated_fieldname]; ;
                        $this->id_user_updated[$i] =$row[$this->id_user_updated_fieldname]; 
                        $this->date_deleted[$i] = $row[$this->date_deleted_fieldname]; 
                        $this->id_user_deleted[$i] = $row[$this->id_user_deleted_fieldname]; 
                        $this->url[$i] = $row[$this->url_fieldname]; 
                        $this->file_extention[$i] = $row[$this->file_extention_fieldname]; 
                        $this->show_page[$i] = $row[$this->show_page_fieldname]; 
                        $this->count_view[$i] = $row[$this->count_view_fieldname]; 
                        $this->is_comment[$i] =$row[$this->is_comment_fieldname]; 
                        $this->keyword[$i] = $row[$this->keyword_fieldname]; 
                        $this->page_title[$i] = $row[$this->page_title_fieldname]; 
                        $this->meta_description[$i] =$row[$this->meta_description_fieldname]; 
                        $this->meta_keyword[$i] = $row[$this->meta_keyword_fieldname]; 
                        $this->begin_date[$i] = $row[$this->begin_date_fieldname]; 
                        $this->begin_end[$i] = $row[$this->begin_end_fieldname]; 
                    $i++;
                }
            }
        }
             function upload_Imges($resource,$size,$path,$f)
       {
          try
          {
                $flag=false;
                $filename="";
                $file="";
            if($_FILES[$resource]["tmp_name"]!=""&&($_FILES[$resource]["type"] == "image/gif"
                ||$_FILES[$resource]["type"] == "image/jpeg"||$_FILES[$resource]["type"] == "image/pjpeg"
                ||$_FILES[$resource]["type"] == "image/pjpeg"||$_FILES[$resource]["type"] == "image/png"))
             {
                    if($_FILES[$resource]['size']<$size)
                    {
                         $filename= $_FILES[$resource]["type"];
                         
                     switch($filename)
                         {
                            case "image/jpeg"     :    $file     =uniqid("").".jpg";
                                                                        break;
                             case "image/pjpeg"   :  $file     =uniqid("").".jpg";
                                                                        break;                        
                            case "image/gif"      :   $file    =uniqid("").".gif";
                                                                        break;
                            case "image/png"      :     $file    =uniqid("").".png";
                                                                         break;         
                         }
                        if(move_uploaded_file($_FILES[$resource]["tmp_name"],$path.$file ))
                        {
                             if($f==1)
                             {
                                $this->large_image[0]=$file;
                             }
                             else
                             {
                                $this->small_image[0]=$file;
                             }
                            $flag=true;
                        }
                            
                    }
                     else
                     $flag = false;
                
             }
         return $flag;
            
          }
              catch(exception $e)
                {
                    var_dump($e->getMessage());
                }
          
       }
    
   
    }
?>