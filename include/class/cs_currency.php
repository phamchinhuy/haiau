<?php
    class cs_currency
    {
        
        //table name
        public $tbl = "currency";
        
        //value field
        public $currency            = array();
        public $id_category         = array();
        public $id                  =array();
        public $money                = array();      
        public $date_created        = array();
        public $date_updated        = array();
        public $status              =array();
      
        //end value field
        
        
        //field name
        public $id_fieldname                = "id";
        public $id_category_fieldname       = "id_category";
        public $date_created_fieldname      = "date_created"; 
        public $date_updated_fieldname      = "date_updated";
        public $status_fieldname            = "status";
        public $currency_fieldname          ="currency";
        public $money_fieldname             = "money";   
        //end field name
        private $db="";
        protected $DataObject;
        function cs_currency()
        {
            $this->db=new db();
        }
        public function insert_currency()
        {
            try
            {
                $rs = false;
                
                $sql = "";
                $sql = "insert into 
                            ".$this->tbl." 
                                (".$this->id_category_fieldname."
                                ,".$this->currency_fieldname."
                              
                                ,".$this->date_created_fieldname." 
                                 ,".$this->status_fieldname."  
                                  ,".$this->money_fieldname."                                                 
                            ) 
                            values 
                            (".$this->id_category[0]."
                                , '".$this->currency[0]."'
                               
                                
                                , now()
                                   ,".$this->status[0]."
                                    ,".$this->money[0]." 
                            )";
               //  echo $sql;           
                //echo $sql;
                $res=$this->db->sql_execute($sql,$this->db->isInsert);
                return $res;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        public function update_currency()
        {
            try
            {
                $rs = false;
          
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->id_category_fieldname."='".$this->id_category[0]."'";
                          
                                 $sql.=", ".$this->currency_fieldname."='".$this->currency[0]."'
                            
                                 , ".$this->date_updated_fieldname."=now()
                                 , ".$this->status_fieldname."=".$this->status[0]."
                                  , ".$this->money_fieldname."=".$this->money[0]." 
                            where ".$this->id_fieldname."=".$this->id[0];
                    $rs = true;
                  //  echo $sql;
                    $rs=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $rs;
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function  get_source($rs)
        {
            try{
                if($rs!="")
                {
                    $i=0;
                    foreach($rs as $row)
                    {
                        $this->id[$i] = $row[$this->id_fieldname]; 
                       // echo $this->id[$i];
                        $this->id_category[$i] =$row[$this->id_category_fieldname];
                        $this->money[$i] = $row[$this->money_fieldname];
                        $this->status[$i] =$row[$this->status_fieldname] ;
                       
                        $this->date_created[$i] =$row[$this->date_created_fieldname]; 
                  
                        $this->date_updated[$i] =$row[$this->date_updated_fieldname]; 
                    
                         $this->currency[$i] =  $row[$this->currency_fieldname];
        
                        $i++;
                    }
                }
               
            }
             catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        public function select_currency_id_category_status($p_id_category, $p_status)
        {
            try
            {
              
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_category_fieldname."=".$p_id_category." 
                                and ".$this->status_fieldname." = ".$p_status."
                                order by ".$this->id_fieldname." desc";
                $rs="";
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($rs);
                }                
                
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_currency_category_status_limit($p_id_category, $p_status, $p_start, $p_end)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where   id_category=".$p_id_category." 
                                and status = ".$p_status."
                                order by `id` desc
                                limit ".$p_start.", ".$p_end;
                                
                $result = "";
                 if($sql!="")
                 {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                 }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_currency_by_id_category($p_id_category)
        {
            try
            {
            
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl."
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by `id` desc";
                                
                $result = "";
              
                if($sql!="")
                {
                   $result=$this->db->sql_execute($sql,$this->db->isSelect);
                   $this->get_source($result); 
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
          
        }
        //END
        
        
        //23-04-2010
        //XUAN
        public function select_currency_by_id_category_limit($p_id_category, $p_start, $p_end)
        {
            try
            {
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   id_category=".$p_id_category." 
                                and status < 3
                                order by id desc
                                limit ".$p_start.", ".$p_end;
                              
                $result ="";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }              
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        //XUAN
        public function select_currency_by_id($p_id)
        {
            try
            {
               
                $sql = "";
                $sql = "select *  
                        from ".$this->tbl." 
                        where   ".$this->id_fieldname."=".$p_id." and ".$this->status_fieldname." < 3  order by ".$this->id_fieldname; 
               // echo $sql;
                $result = "";
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isSelect);
                    $this->get_source($result);
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END
        
        //23-04-2010
        public function update_currency_set_status($p_status,$p_id)
        {
            try
            {
                $rs = "";
                $sql = "";
                $sql = " update ".$this->tbl."
                            set ".$this->status_fieldname."=".$p_status."
                            , ".$this->date_updated_fieldname."=now()
                                where ".$this->id_fieldname."=".$p_id;
               // echo $sql;
                $result =false;
                if($sql!="")
                {
                    $result=$this->db->sql_execute($sql,$this->db->isUpdate);
                    return $result;
                }
                
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
            
        }
     
        //END
        //24-04-2010 created by tran van tam
        public function delete_currency_by_id($p_id)
        {
            try
            {
                $rs = false;
             
                $sql = "";
                $sql = " update ".$this->tbl." set   ".$this->status_fieldname."=3 
                      where    ".$this->id_fieldname."=".$p_id;
                if($sql!="")
                {
                    $rs=$this->db->sql_execute($sql,$this->db->isDelete);
                    return $rs;
                }
            }
            catch(exception $e)
            {
                var_dump($e->getMessage());
            }
        }
        //END

    }
?>