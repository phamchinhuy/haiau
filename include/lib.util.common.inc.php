<?php
/**
   
    Cac ham dung chung

	01. function get_param($param_name);
	02. function set_Cookie($param_name, $param_value, $time);
	03. function get_Cookie($param_name);
	04. function get_session($param_name);
	05. function set_session($param_name, $param_value);
	06. function destroy_session($param_name);
	07. function convertnumpage($Total, $Each );
	08. function load_session() ;
	09. function delete_session() ;
	10. function save_session($array2save) ;


*/
if(!defined("_UTIL_FUNCTION_PHP"))
{
	define("_UTIL_FUNCTION_PHP", 1 );
}

/***********************************************************************************
        lay gia tri tu form HTML
*/
function get_param($param_name)
{
	global $HTTP_POST_VARS;
	global $HTTP_GET_VARS;
	$param_value = "";
	if(isset($HTTP_POST_VARS[$param_name]))
		$param_value = $HTTP_POST_VARS[$param_name];
	else 
		if(isset($HTTP_GET_VARS[$param_name]))
			$param_value = $HTTP_GET_VARS[$param_name];

	return $param_value;
}
/***********************************************************************************
        ham         function set_Cookie($param_name, $param_value)
*/
function set_Cookie($param_name, $param_value, $time)
{
	$ret = setcookie (${$param_name}, $param_value,time() + $time );
	return $ret;
}


/***********************************************************************************
        ham         function get_Cookie($param_name)
*/
function get_Cookie($param_name)
{
	$ret = isset($HTTP_COOKIE_VARS[${$param_name}])?$HTTP_COOKIE_VARS[${$param_name}]:"";
	return $ret;
}
/***********************************************************************************
        ham function get_session($param_name)
*/
function get_session($param_name)
{
	session_start();
	global $HTTP_POST_VARS;
	global $HTTP_GET_VARS;
	global ${$param_name};
	$param_value = "";
	if(!isset($HTTP_POST_VARS[$param_name]) && !isset($HTTP_GET_VARS[$param_name]) && session_is_registered($param_name))
		$param_value = ${$param_name};

	return $param_value;
}
/***********************************************************************************
        ham function set_session($param_name, $param_value)
*/


function set_session($param_name, $param_value)
{
	session_start();
	global ${$param_name};
	if(session_is_registered($param_name))
		session_unregister($param_name);
	${$param_name} = $param_value;
	session_register($param_name);
}
/***********************************************************************************
        ham function set_session($param_name, $param_value)
*/
function destroy_session($param_name)
{
	session_start();
	${$param_name} = "";
	session_unregister($param_name);
	session_destroy();
}
/*
	create:5/25/2002
	by : hocnv@ssp.com.vn
	chuyen tu tong so record /so record tren 1 trang => so trang co the 
*/
function  convertnumpage($Total, $Each )
{
	
	$numpage = $Total/$Each;
	$CheckPage = $Total%$Each;
	if($CheckPage!=0)
	{
		$numpage = $numpage+1;
	}
	$numpage = intval ($numpage) ;

	return $numpage;
}

// util session info
$sid = session_id ();
// save session info
function save_session($array2save) 
{
	global $sid;
//	echo "<hr> sid : " . $sid ; 
//	$temporary_directory=session_save_path();
	$temporary_directory = realpath ("./"). "/session";
	
	$content = ~serialize($array2save);

	if(!is_writable($temporary_directory)) die("<h3>The folder \"$temporary_directory\" do not exists or the webserver don't have permissions to write</h3>");

	$sessiondir = $temporary_directory;
	if(!file_exists($sessiondir)) mkdir($sessiondir,0777);
	$f = fopen("$sessiondir/$sid.usf","wb+") or die("<h3>Could not open session file</h3>");
	fwrite($f,$content);
	fclose($f);

	return 1;

}

//load_session()
function load_session() {
	global $sid;
	//$temporary_directory=session_save_path();
	$temporary_directory = realpath ("./"). "/session";
	
	$sessionfile = $temporary_directory."/$sid.usf";
	$result      = Array();
	if(file_exists($sessionfile)) {
		$result = file($sessionfile);
		$result = join("",$result);
		$result = unserialize(~$result);
	}
	return $result;
}

// delete an session (logout)
function delete_session() {
	global $sid;
	// $temporary_directory=session_save_path();
	$temporary_directory = realpath ("./"). "/session";
	
	$sessionfile = $temporary_directory."/$sid.usf";
//	echo "sessionfile : " . $sessionfile ;
//	unlink($sessionfile);
	return @unlink($sessionfile);
}
function save_session_ex($array2save,$path) 
{
	global $sid;
//	echo "<hr> sid : " . $sid ; 
//	$temporary_directory=session_save_path();
	$temporary_directory = realpath($path);
	
	$content = ~serialize($array2save);

	if(!is_writable($temporary_directory)) die("<h3>The folder \"$temporary_directory\" do not exists or the webserver don't have permissions to write</h3>");

	$sessiondir = $temporary_directory;
	if(!file_exists($sessiondir)) mkdir($sessiondir,0777);
	$f = fopen("$sessiondir/$sid.usf","wb+") or die("<h3>Could not open session file</h3>");
	fwrite($f,$content);
	fclose($f);

	return 1;

}

//load_session()
function load_session_ex($path) {
	global $sid;
	//$temporary_directory=session_save_path();
	$temporary_directory = realpath($path);
	
	$sessionfile = $temporary_directory."/$sid.usf";
	$result      = Array();
	if(file_exists($sessionfile)) {
		$result = file($sessionfile);
		$result = join("",$result);
		$result = unserialize(~$result);
	}
	return $result;
}

// delete an session (logout)
function delete_session_ex($path) {
	global $sid;
	// $temporary_directory=session_save_path();
	$temporary_directory = realpath($path);
	
	$sessionfile = $temporary_directory."/$sid.usf";
//	echo "sessionfile : " . $sessionfile ;
//	unlink($sessionfile);
	return @unlink($sessionfile);
}

function get_new_id($table, $fieldGetMax)
{
 
 $select = 'select max('.$fieldGetMax.') from `'.$table.'`  where `id` != <some big id>' ;
 $query = mysql_query($select);
 $obj = mysql_fetch_object($query);
 return $obj->$fieldGetMax;
 
}

function get_new_id_table($table, $fieldGetMax)
{
     $connect = sql_connect_default();
     $select = "select ".$fieldGetMax." from ".$table." limit 0,1";
     $query = mysql_query($select,$connect);
     $row = @msql_num_rows($query);
     mysql_free_result($query);
     mysql_close($connect);
     return $row[$fieldGetMax];
}

function previous_id($table, $id_column) 
{
  if ($table && $id_column) 
  {
  	$sql = "select ".$id_column. " from " . $table . " order by " . $id_column . " desc limit 1";
    $resultSQL = mysql_query($sql);
    $stuff = mysql_fetch_assoc($resultSQL);
    return $stuff[$id_column];
  } 
  else 
  {
    return false;
  }
}

function getMessageJavascript($message,$link)
{
	$messageError = "<script language='javascript'>alert('".$message."');
					window.location='".$link."'</script>";
	return $messageError;
}
function navigateLink($link)
{
	$messageError = "<script language='javascript'>window.location='".$link."'</script>";
	return $messageError;
}
function navigateLinkHeader($link)
{
    header("Location: $link");
}
function checkEmail($email) {
  if (ereg("[[:alnum:]]+@[[:alnum:]]+\.[[:alnum:]]+", $email)) {
    return true;
  } else {
    return false;
  }
}

function getIdentity($tbl)
{
	$rsID="0";
    $sqlid = ""; 
	$connect = sqlConnect();
	$sqlid.="select @@IDENTITY ";
	$sqlid.="select IDENT_CURRENT('".$tbl."')";
	$sqlid.="select SCOPE_IDENTITY() ";
	$resultSQLID = mysql_query($sqlid,$connect);
	if($resultSQLID!=null)
	{
		$row = mysql_fetch_assoc($resultSQLID);
		$rsID=$row["@@IDENTITY"];
    }
    
	return $rsID;
}

function createCookie($name, $value='', $maxage=0, $path='',$domain='', $secure=false, $HTTPOnly=false)
{
    if(is_array($name))
    {
        list($k,$v)    =    each($name);

            $name    =    $k.'['.$v.']';

    }
    $ob = ini_get('output_buffering');
    // Abort the method if headers have already been sent, except when output buffering has been enabled
    if ( headers_sent() && (bool) $ob === false || strtolower($ob) == 'off' )
        return false;
    if ( !empty($domain) )
    {
        // Fix the domain to accept domains with and without 'www.'.
        if ( strtolower( substr($domain, 0, 4) ) == 'www.' ) $domain = substr($domain, 4);
        // Add the dot prefix to ensure compatibility with subdomains
        if ( substr($domain, 0, 1) != '.' ) $domain = '.'.$domain;
        // Remove port information.
        $port = strpos($domain, ':');
        if ( $port !== false ) $domain = substr($domain, 0, $port);
    }
    // Prevent "headers already sent" error with utf8 support (BOM)
    //if ( utf8_support ) header('Content-Type: text/html; charset=utf-8');
    if(is_array($name))
    {
        header('Set-Cookie: '.$name.'='.rawurlencode($value)
                                .(empty($domain) ? '' : '; Domain='.$domain)
                                .(empty($maxage) ? '' : '; Max-Age='.$maxage)
                                .(empty($path) ? '' : '; Path='.$path)
                                .(!$secure ? '' : '; Secure')
                                .(!$HTTPOnly ? '' : '; HttpOnly'), false);
    }else{
        header('Set-Cookie: '.rawurlencode($name).'='.rawurlencode($value)
                                .(empty($domain) ? '' : '; Domain='.$domain)
                                .(empty($maxage) ? '' : '; Max-Age='.$maxage)
                                .(empty($path) ? '' : '; Path='.$path)
                                .(!$secure ? '' : '; Secure')
                                .(!$HTTPOnly ? '' : '; HttpOnly'), false);
    }
    return true;
} 

function sendMail($from,$to,$subject,$body)
{
	$rs = false;
	$header = "Content-type: text/html; charset=utf-8\r\nFrom: ".$from."\r\nReply-to: ".$from;
	if (mail($to, $subject, $body,$header)) 
		$rs = true;
	return $rs;
	
}

function make_safe($variable) 
{
 $variable = mysql_real_escape_string(trim($variable));
 return $variable;
}
function getRealIPAddr()

   {

   if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from 
   {

   $ip=$_SERVER['HTTP_CLIENT_IP'];

   }

   elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 //to check ip is pass from proxy

   {

   $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

   }

   else

   {

   $ip=$_SERVER['REMOTE_ADDR'];

   }

   return $ip;

   } 

///get ip
function getip() {
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $realip = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        if (getenv("HTTP_X_FORWARDED_FOR")) {
            $realip = getenv( "HTTP_X_FORWARDED_FOR");
        } elseif (getenv("HTTP_CLIENT_IP")) {
            $realip = getenv("HTTP_CLIENT_IP");
        } else {
            $realip = getenv("REMOTE_ADDR");
        }
    }
    return $realip;
} 

function getFileExtension($filename)
{
  return substr($filename, strrpos($filename, '.'));
}


function get_message($p_strMessage)
{
    $script = '';
    $script .= '<div style="display: none; " class="divMessage"  id="divMessage">
                    <div class="divMessage_top"><img src="../../../images/client/hoa/khung_message/khung_message_top.gif" /> </div>
                    <div class="divMessage_middle">
                        <table cellpadding="5" width="90%">
                            <tr>
                                <td align="right">
                                    <a href="#" title="&#273;&#243;ng l&#7841;i" onclick="setVisibility(\'divMessage\', \'none\');">
                                        <img src="../../../images/delete.png" border="0">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    '.$p_strMessage.'
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="divMessage_bottom"><img src="../../../images/client/hoa/khung_message/khung_message_bottom.gif" /> </div>
                </div>';
    $script .= '<script language="JavaScript">setVisibility("divMessage","inline")</script>';
    return $script;
}

function currentPage() 
{
     $pageURL = 'http';
     if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
     $pageURL .= "://";
     if ($_SERVER["SERVER_PORT"] != "80") 
     {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
     } 
     else 
     {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
     }
     return $pageURL;
}
function formatDate($format,$dateStr) {
  if (trim($dateStr) == '' || substr($dateStr,0,10) == '0000-00-00') {
    return '';
  }
  $ts = strtotime($dateStr);
  if ($ts === false) {
    return '';
  }
  return date($format,$ts);
} 
?>