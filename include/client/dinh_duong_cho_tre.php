<?php

require_once("include/class/client/cs_client_category.php");
require_once("include/class/client/cs_client_type_category.php");

$key_dinh_duong_cho_be = "dinh-duong-cho-be";
$key_danh_sach_che_do_dd = "danh-sach-che-do-dinh-duong";
$page->assign("key",$key_danh_sach_che_do_dd);

if($func=="chim-canh-cut")
{
     $cs_category_gioithieuddchobe = new cs_client_category();
        $cs_category_gioithieuddchobe->select_category_by_page_title_site($key_dinh_duong_cho_be, "1");
        
        if(count($cs_category_gioithieuddchobe->id)>0)
        {
            $strIDCategory_gioithieuddchobe = "";
            $strIDCategory_gioithieuddchobe = $cs_category_gioithieuddchobe->id[0];// lay id category
            
            $strID_Type_Category_gioithieuddchobe = "";
            $strID_Type_Category_gioithieuddchobe = $cs_category_gioithieuddchobe->id_type_category[0];//lay id cua loai category
            
            $cs_client_type_category_gioithieuddchobe = new cs_client_type_category();
            $cs_client_type_category_gioithieuddchobe->select_type_category_by_id_status_1($strID_Type_Category_gioithieuddchobe);
            
            if(count($cs_client_type_category_gioithieuddchobe->id)>0)
            { 
              
                $strCategory_gioithieuddchobe = "";
                $strCategory_gioithieuddchobe = $key_dinh_duong_cho_be;
                
                $strTypeCategory_gioithieuddchobe = "";
                $strTypeCategory_gioithieuddchobe = $cs_client_type_category_gioithieuddchobe->key_type[0];//lay ten loai category
                
                $cs_dinh_duong_cho_be = new cs_client_news();        
                $cs_dinh_duong_cho_be->select_news_by_id_category_desc_date_create($strIDCategory_gioithieuddchobe);// lay news cua category theo ma
               
                $page->assign("strCategory_gioithieuddchobe", $strCategory_gioithieuddchobe);
                $page->assign("strTypeCategory_gioithieuddchobe", $strTypeCategory_gioithieuddchobe);
                $page->assign("cs_dinh_duong_cho_be", $cs_dinh_duong_cho_be);// parste qua .tpl
            }
          
        }
        
        //load  detail dinh duong cho tre
        
        $cs_client_type_category = new cs_client_type_category();
        $cs_client_category = new cs_client_category();
        $key_intro = $page_index;
        $cs_client_type_category->select_category_id_by_key_type($key_intro);
        $page->assign("cs_client_type_category", $cs_client_type_category);
        $page->assign("key_intro", $key_intro);
        $key_ds_dinh_duong = "danh-sach-che-do-dinh-duong";
        $strIDTypeCategory = "";
        
          if(count($cs_client_type_category->id)>0)
          {
              $strIDTypeCategory = $cs_client_type_category->id[0];
          }
         
        
        if($strIDTypeCategory!="")
        {
            $cs_category_ds = new cs_client_category();
            $cs_category_ds->select_category_by_page_title_site($key_ds_dinh_duong, "1");
            if(count($cs_category_ds->id)>0)
            {
                 //lay danh sach cac che do dinh duong
                $cs_ds_chedodd_limit = new cs_client_news(); 
                $cs_ds_chedodd_limit->select_news_by_id_category_limit($cs_category_ds->id[0],"1","0","1");// lay news cua category theo ma
                
                $cs_ds_chedodd = new cs_client_news(); 
                //$cs_ds_chedodd->select_news_by_id_category_limit_different($cs_category_ds->id[0],"1",$cs_ds_chedodd_limit->id[0],"0","5");
                //echo $cs_ds_chedodd->id[0];
                $cs_ds_chedodd->select_news_by_id_category_desc_date_create($cs_category_ds->id[0]);
                $page->assign("cs_ds_chedodd", $cs_ds_chedodd);
               
                $keywords = $cs_ds_chedodd->meta_keyword[0];
                $description = $cs_ds_chedodd->meta_description[0];
                
                if($func!="" && $func!="chim-canh-cut")
                {
                    $link = "";
                    $link = $server."/".$key_intro."/".$cs_ds_chedodd->page_title[0]."/";
                    echo navigateLinkHeader($link);
                   
                } 
                      
              
            }
        }
        //load chi tiet 1 bang dinh duong
        $cs_bang_dinh_duong_mota =$cs_ds_chedodd->long_description[0];
        $cs_bang_dinh_duong_news =$cs_ds_chedodd->news[0];
        $cs_bang_dinh_duong_link_down =$cs_ds_chedodd->url[0];
        $page->assign("cs_bang_dinh_duong_link_down", $cs_bang_dinh_duong_link_down);
        $page->assign("cs_bang_dinh_duong_mota", $cs_bang_dinh_duong_mota);
        $page->assign("cs_bang_dinh_duong_news", $cs_bang_dinh_duong_news);
        
}

else
{
    
    //lay bang dinh duong 
    $cs_bang_dinh_duong1 = new cs_client_news();        
    $cs_bang_dinh_duong1->select_news_by_page_title($cate);// lay news cua category theo ma
    
    $headerTitle=$cs_bang_dinh_duong1->news[0];          
    $cs_bang_dinh_duong_mota =$cs_bang_dinh_duong1->long_description[0];
    $cs_bang_dinh_duong_news =$cs_bang_dinh_duong1->news[0];
    
    $page->assign("cs_bang_dinh_duong_mota", $cs_bang_dinh_duong_mota);
    $page->assign("cs_bang_dinh_duong_news", $cs_bang_dinh_duong_news);
    
    $cs_bang_dinh_duong_link_down =$cs_bang_dinh_duong1->url[0];
    $page->assign("cs_bang_dinh_duong_link_down", $cs_bang_dinh_duong_link_down);
        
    $cs_category_ds = new cs_client_category();
    $cs_category_ds->select_category_by_page_title_site($key_danh_sach_che_do_dd, "1");
   
    if(count($cs_category_ds->id)>0)
    {
         //lay danh sach cac che do dinh duong
        $cs_ds_chedodd_limit = new cs_client_news(); 
        $cs_ds_chedodd_limit->select_news_by_id_category_limit($cs_category_ds->id[0],"1","0","1");// lay news cua category theo ma
        
        $cs_ds_chedodd = new cs_client_news(); 
        $cs_ds_chedodd->select_news_by_id_category_limit_different($cs_category_ds->id[0],"1",$cs_bang_dinh_duong1->id[0],"0","5");
        $page->assign("cs_ds_chedodd", $cs_ds_chedodd);
        
       
    }
   
    
              
               
}




//load list dinh duong cho tre

?>