<?php
ob_start();

require_once("../../include/config.php");
require_once("../../include/db.php");
require_once("../../include/class/client/cs_client_category.php");
require_once("../../include/class/client/cs_client_type_category.php");
require_once("../../include/class/client/cs_client_news.php");


$server = "";
$server = STR_SERVER;
$folder_upload_news = "/uploads/news/";
$type = "";
$type = isset($_REQUEST["type"])?$_REQUEST["type"]:"";

$cate = "";
$cate = isset($_REQUEST["cate"])?$_REQUEST["cate"]:"";

$cate_title = "";
$cate_title = isset($_REQUEST["cate_title"])?$_REQUEST["cate_title"]:"";

$obj_news = new cs_client_news();
$obj_news->select_news_by_id_category_limit($cate,1, 0, 100);  

$count = 0;
$count=count($obj_news->id); 

$str = '';
//echo $count."-".$type."-".$cate;
if($count>0)
{
     for($i=0;$i<$count;$i++)
     {
        
        $str_short_content = "";
        if(strlen($obj_news->short_description[$i])>250)
            $str_short_content = substr($obj_news->short_description[$i],0, 250)." ...";
        else
            $str_short_content = $obj_news->short_description[$i];
        
        $link = "";
        $link = $server."/".$type."/".$cate_title."/".$obj_news->page_title[$i];
        
        $str.='<div class="ct_content_item">
                    <div class="image">
                         <a href="#">
                            <img src="'.$server.$folder_upload_news.''.$obj_news->small_image[$i].'"
                                  width="142" height="91" alt="'.$obj_news->news[$i].'" />
                        </a>
                    </div>
                    <div class="title">
                        <a href="#">
                            '.$obj_news->news[$i].'
                        </a>
                    </div>
                </div>';
        
     }
}
else
{
    $str = 'D&#7919; li&#7879;u &#273;ang  c&#7853;p nh&#7853;t';
}

echo $str;

?>