<?php
$folder_upload_news = "/uploads/news/";
require_once("include/class/client/cs_client_category.php");
require_once("include/class/client/cs_client_type_category.php");

$headerTitle = "M&#7895;i Ng&#224;y L&#224;m G&#236;";
$key_1_ngay_cua_be = "1-ngay-cua-be";
$cs_category_s = new cs_client_category();
$cs_category_s->select_category_by_page_title_site($key_1_ngay_cua_be, "1");
if(count($cs_category_s->id)>0)
{
    $strIDCategory = "";
    $strIDCategory = $cs_category_s->id[0];// lay id category
    
    $strID_Type_Category = "";
    $strID_Type_Category = $cs_category_s->id_type_category[0];//lay id cua loai category
    
    $cs_client_type_category_s = new cs_client_type_category();
    $cs_client_type_category_s->select_type_category_by_id_status_1($strID_Type_Category);
    
    if(count($cs_client_type_category_s->id)>0)
    {
       
        $strCategory = "";
        $strCategory = $key_1_ngay_cua_be;
        
        $strTypeCategory = "";
        $strTypeCategory = $cs_client_type_category_s->key_type[0];//lay ten loai category
        
        $keywords = $cs_category_s->meta_keyword[0];
         $description = $cs_category_s->meta_description[0];
         $headerTitle=$cs_category_s->category[0];
         
        $cs_1_ngay_cua_be = new cs_client_news();
        $cs_1_ngay_cua_be->select_news_by_id_category_asc_date_create($strIDCategory);// lay news cua category theo ma
        
        $page->assign("strCategory", $strCategory);
        $page->assign("strTypeCategory", $strTypeCategory);
        $page->assign("cs_1_ngay_cua_be", $cs_1_ngay_cua_be);// parste qua .tpl
        
         
    }
  
}
//lay anh hoat dong cua be
$key_anh_hoat_dong_cua_be = "anh-hoat-dong-cua-be";
$cs_category_anhhoatdong = new cs_client_category();
$cs_category_anhhoatdong->select_category_by_page_title_site($key_anh_hoat_dong_cua_be, "1");
if(count($cs_category_s->id)>0)
{
    $strIDCategory_anhhoatdong = "";
    $strIDCategory_anhhoatdong = $cs_category_anhhoatdong->id[0];// lay id category
    
    $strID_Type_Category_anhhoatdong = "";
    $strID_Type_Category_anhhoatdong = $cs_category_anhhoatdong->id_type_category[0];//lay id cua loai category
    
    $cs_client_type_category_anhhoatdong = new cs_client_type_category();
    $cs_client_type_category_anhhoatdong->select_type_category_by_id_status_1($strID_Type_Category_anhhoatdong);
    
    if(count($cs_category_anhhoatdong->id)>0)
    {
       
        $strCategory_anhhoatdong = "";
        $strCategory_anhhoatdong = $key_anh_hoat_dong_cua_be;
       
        $strTypeCategory_anhhoatdong = "";
        $strTypeCategory_anhhoatdong = $cs_client_type_category_anhhoatdong->key_type[0];//lay ten loai category
        
        $cs_anh_hoat_dong_cua_be = new cs_client_news();
        $cs_anh_hoat_dong_cua_be->select_news_by_id_category($strIDCategory_anhhoatdong);// lay news cua category theo ma
       
        $page->assign("strCategory_anhhoatdong", $strCategory_anhhoatdong);
        $page->assign("strTypeCategory_anhhoatdong", $strTypeCategory_anhhoatdong);
        $page->assign("cs_anh_hoat_dong_cua_be", $cs_anh_hoat_dong_cua_be);// parste qua .tpl
   
        
    }
}
/*-------------------------------*/


/*begin h�nh anh*/
$cs_client_category_partner  = new cs_client_category();

$key_intro_partner= "anh-hoat-dong-cua-be";
$cs_client_category_partner->select_category_by_page_title($key_intro_partner);

$page->assign("cs_client_category_partner", $cs_client_category_partner);
$page->assign("key_intro_partner", $key_intro_partner);

 
if(count($cs_client_category_partner->id)>0)
{
    //ANH HOAT DONG CUA BE
    //load 2 hinh len 
    $obj_news_2hinh = new cs_client_news();
    $obj_news_2hinh->select_news_by_id_category_desc_date_create($cs_client_category_partner->id[0]);
    $page->assign("obj_news_2hinh",$obj_news_2hinh);
    //END ANH HOAT DONG CUA BE
    
    $iPage = 0;
    $iPage = count($obj_news_2hinh->id)/2;
    $iCountImage = 0;
    $iCountImage = count($obj_news_2hinh->id);
    $page->assign("iCountImage", $iCountImage);
    
    
    
    $obj_news = new cs_client_news();
    $obj_news->select_news_by_id_category($cs_client_category_partner->id[0]);
    if(count($obj_news->id)>0)    {
        
        $arr_partner= array();
        for($n=0; $n<count($obj_news); $n++)
        {
            $arr_partner[$n] = $n;
        }
        
        $strContentpartner = "";
        
        //$obj_news = new cs_client_news();
        //$obj_news->select_news_by_id_category_limit($cs_client_category_partner->id[0], 1, 0, 100);  
        
        $count = 0;
        $count=count($obj_news->id); 
        $str = '';
        $str .='
                  <div class="jcarousel-skin-tango">
                        <div style="position: relative; display: block;" 
                                    class="jcarousel-container jcarousel-container-horizontal"> 
                            <div style="overflow: hidden; position: relative;" 
                                    class="jcarousel-clip jcarousel-clip-horizontal">
                                <ul style="margin: 0px; padding: 0px; overflow: hidden; position: relative; top: 0px; left: -255px; width: 950px;" 
                                        id="mycarousel" class="jcarousel-list jcarousel-list-horizontal">
                                       
                                
                                
                ';
        if($count>0)
        {
             for($i=0;$i<$count;$i++)
             {
                
                $str_short_content = "";
                if(strlen($obj_news->short_description[$i])>250)
                    $str_short_content = substr($obj_news->short_description[$i],0, 250)." ...";
                else
                    $str_short_content = $obj_news->short_description[$i];
                
                $link = "";
                $link = $server."/moi-ngay-lam-gi/".$cs_client_category_partner->page_title[0]."/".$obj_news->page_title[$i];
            
                $n = $i+1;
                
                $str.=' <li jcarouselindex="'.$n.'" 
                                style="float: left; list-style-type: none; list-style-image: none; list-style-position: outside;" 
                                class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
                           
                            <div class="hdnk_ha_left_picture">
                                   <img src="'.$server.$folder_upload_news.'/'.$obj_news->small_image[$i].'" alt="" 
                                        height="195" width="275">                               
                                    <div class="hdnk_ha_text_chu_thich">'.$obj_news->news[$i].'</div>
                           </div>
                        </li>';
                
                
             }
        }
        
        $str.='
                   </ul>
                            </div>
                            <div disabled="false" style="display: block;" class="jcarousel-prev jcarousel-prev-horizontal"></div>
                            <div disabled="false" style="display: block;" class="jcarousel-next jcarousel-next-horizontal"></div>
                        </div>
                    </div>             
                ';
        $strContentpartner = $str;
        $page->assign("cs_client_category_partner", $cs_client_category_partner);
        $page->assign("arr_partner", $arr_partner);
        $page->assign("countpartner", count($cs_client_category_partner->id));
        $page->assign("strContentpartner",$strContentpartner);
       
    }
}
/*end h�nh anh*/


?>