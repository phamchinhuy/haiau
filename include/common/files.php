<?php
function upload_image($resource, $size, $path, &$file_name)
{
    try
    {
        $flag=false;
        $filename="";
        $file="";
        
        $type_file = "";
        $type_file= get_type_file($resource);
        
        $name_file = "";
        $name_file = get_name_file_extention($resource);
        
        $tmp_name_file = "";
        $tmp_name_file = get_tmp_name_file($resource);
        
        $name = "";
        $name = get_name_file_by_resource($resource);
        
        if($tmp_name_file!="" && ( $type_file == "image/gif" ||$type_file == "image/jpeg" 
            || $type_file == "image/jpg" ||$type_file == "image/pjpeg" || $type_file == "image/png"))
        {
            if(get_size_file($resource)<$size)
            {
                $file_name_upload = "";
                $file_name_upload = $name."_".date('d').date('m').date('Y')."_".time();
                
                if($file_name=="")
                {
                    $file_name_upload = "";
                    $file_name_upload = $name."_".date('d').date('m').date('Y')."_".time();
                }
                else
                {
                    $file = $file_name;
                }
                
                switch($type_file)
                {
                    case "image/jpg":    
                        $file = $file_name_upload.".jpg";
                        break;
                    case "image/jpeg":    
                        $file = $file_name_upload.".jpg";
                        break;
                    case "image/pjpeg":  
                        $file = $file_name_upload.".jpg";
                        break;                        
                    case "image/gif":   
                        $file = $file_name_upload.".gif";
                        break;
                    case "image/png":     
                        $file = $file_name_upload.".png";
                        break;         
                }
                
                if(move_uploaded_file($tmp_name_file, $path.$file ))
                {
                        $file_name=$file;  
                        $flag =  true;                      
                }
        }
        else
        {
            $flag = false;
        }
     }
     return $flag;
    }
    catch(exception $e)
    {
        var_dump($e->getMessage());
    }
}

function upload_file_pdf($resource, $size, $path, &$file_name)
{
    try
    {
        $flag=false;
        $file="";
        
        $type_file = "";
        $type_file= get_type_file($resource);
        
        $name_file = "";
        $name_file = get_name_file_extention($resource);
        
        $tmp_name_file = "";
        $tmp_name_file = get_tmp_name_file($resource);
        
        $name = "";
        $name = get_name_file_by_resource($resource);
        
        $ext = "";
        $ext = get_file_extension_by_name_file($name_file);
        
        if($tmp_name_file!="" 
            && get_file_extension_by_name_file($name_file)==".pdf" 
            && $type_file=="application/pdf")
        {
            if(get_size_file($resource)<$size)
            {
                if($file_name=="")
                {
                    $file_name_upload = "";
                    $file_name_upload = $name."_".date('d').date('m').date('Y')."_".time();
                    $file = $file_name_upload.$ext;
                }
                else
                    $file = $file_name;
                
                if(move_uploaded_file($tmp_name_file, $path.$file ))
                {
                        $file_name=$file;  
                        $flag =  true;                      
                }
        }
        else
        {
            $flag = false;
        }
     }
     return $flag;
    }
    catch(exception $e)
    {
        var_dump($e->getMessage());
    }
}

function upload_file_txt_doc_pdf($resource, $size, $path, &$file_name)
{
    try
    {
        $flag=false;
        $file="";
        
        $type_file = "";
        $type_file= get_type_file($resource);
        
        $name_file = "";
        $name_file = get_name_file_extention($resource);
        
        $tmp_name_file = "";
        $tmp_name_file = get_tmp_name_file($resource);
        
        $name = "";
        $name = get_name_file_by_resource($resource);
        
        $ext = "";
        $ext = get_file_extension_by_name_file($name_file);
        
        if($tmp_name_file!="" && 
            (($ext==".doc" && $type_file=="application/msword") 
                || ($type_file=="application/pdf" && $ext==".pdf")
                || ($type_file=="text/plain" && $ext==".txt")))
        {
            if(get_size_file($resource)<$size)
            {
                if($file_name=="")
                {
                    $file_name_upload = "";
                    $file_name_upload = $name."_".date('d').date('m').date('Y')."_".time();
                    $file = $file_name_upload.$ext;
                }
                else
                {
                    $file = $file_name;
                }
                
                if(move_uploaded_file($tmp_name_file, $path.$file ))
                {
                        $flag =  true;
                        $file_name=$file;                        
                }
        }
        else
        {
            $flag = false;
        }
     }
     return $flag;
    }
    catch(exception $e)
    {
        var_dump($e->getMessage());
    }
}

function get_name_file_extention($resource)
{
    $strFile = "";
    $strFile = $_FILES[$resource]["name"];
    $strFile = str_replace("#", "No.", $strFile);
    $strFile = str_replace("$", "Dollar", $strFile);
    $strFile = str_replace("%", "Percent", $strFile);
    $strFile = str_replace("^", "", $strFile);
    $strFile = str_replace("&", "and", $strFile);
    $strFile = str_replace("*", "", $strFile);
    $strFile = str_replace("?", "", $strFile);
    $strFile = str_replace(" ", "_", $strFile);
    return $strFile;
}

function get_file_extension_by_name_file($filename)
{
    return substr($filename, strrpos($filename, '.'));
}

function get_file_extension_by_resource($resource)
{
    $filename = get_name_file_extention($resource);
    return substr($filename, strrpos($filename, '.'));
}

function get_name_file_by_resource($resource)
{
    $strNameFile = "";
    $strNameFile = get_name_file_extention($resource);
    $strNameFile = substr($strNameFile, 0, strrpos($strNameFile, '.'));
    return $strNameFile;
}

function get_type_file($resource)
{
    $strType = "";
    $strType = $_FILES[$resource]["type"];
    return $strType;
}

function get_size_file($resource)
{
    $size = 0;
    $size = $_FILES[$resource]['size'];
    return $size;
}

function get_tmp_name_file($resource)
{
    $tmp_name = 0;
    $tmp_name = $_FILES[$resource]['tmp_name'];
    return $tmp_name;
}
?>