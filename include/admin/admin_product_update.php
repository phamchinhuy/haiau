<?php


include_once("include/class/cs_product.php");
include_once("include/class/cs_calendar.php");

$folder_upload_product = "uploads/product/";
$size_file_image = "2148576"; 	
$id_user = "1";

$p_url_cateid = "";
if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
        

$id = "";
if (isset($_GET["id"]) || isset($_POST["id"]))
    $id = isset($_GET["id"]) ? $_GET["id"] : $_POST["id"];


if(isset($_POST["btEdit"])&&$_POST["btEdit"]!="")
{
  
    if($p_url_cateid!="")
    {
        $id_Category = $p_url_cateid;
        
        $product            = "";
        $product            = isset($_POST["txtNews"])?$_POST["txtNews"]:"";
        
        $txtCodeProductt = "";
        $txtCodeProduct = isset($_POST["txtCodeProduct"])?$_POST["txtCodeProduct"]:"";
        
        $small_image        = "";
        $large_image        = "";
                
       // $isComment          =isset($_POST["isComment"])?$_POST["isComment"]:"0";
        //if($isComment=="on")
        //{
            $isComment=1;
        //}
        $shortDescription   = isset($_POST["txtShortDescription"])?$_POST["txtShortDescription"]:"";
        $longDescription    = isset($_POST["description_product"])?$_POST["description_product"]:" ";
        $active             = isset($_POST["active"])? $_POST["active"]:"2";

        $price              = isset($_POST['txtPrice'])?$_POST['txtPrice']:"";
        $priceSaleOf        = isset($_POST['txtPriceSaleOf'])?$_POST['txtPriceSaleOf']:"";
        $re_urls            = isset($_POST['txturl'])?$_POST['txturl']:"";
        $re_urls            =str_replace(" ","-",$re_urls);
         $size               =isset($_POST['txtsize'])?$_POST['txtsize']:"";
        $color              =isset($_POST['txtcolor'])?$_POST['txtcolor']:"";
        
        $page_tile          =isset($_POST['txtpagetitle'])?$_POST['txtpagetitle']:"";
        $meta_keyword       =isset($_POST['txtmeta_keyword'])?$_POST['txtmeta_keyword']:"";
        $meta_des           =isset($_POST['txtDes'])?$_POST['txtDes']:"";
        
        $date_created       = date("Y-m-d");
        $id_user_created    = $id_user;
        
        $date_updated       = $date_created;
        $id_user_updated    = $id_user;
        
        $date_deleted       = "";
        $id_user_deleted    = "";
       
        
       
        $cs_product_image   = new cs_product();
        $cs_product_image->select_product_by_id($p_url_id);
        $status= $cs_product_image->status[0];
        
        //UPLOAD IMAGE
        //check type file before update
        
        //END UPLOAD IMAGE
        if($status=="on")
            $status = "1";
        else
            $status="0";
        $rsUpload=false;    
      
       
       
        //END UPLOAD IMAGE
        //get information from form into cs_news class   
        $cs_product = new cs_product();
        $cs_product->id[0]                  = $id;
        $cs_product->id_category[0]         = $p_url_cateid;
        $cs_product->product[0]             = $product;
        $cs_product->product_code[0]        = $txtCodeProduct;
        $cs_product->short_description[0]   = $shortDescription;
        $cs_product->long_description[0]    = $longDescription;
        $cs_product->status[0]              = $active;
        $cs_product->color[0]               =$color;
        $cs_product->size[0]                =$size;
        
        $cs_product->date_created[0]        = $date_created;
        $cs_product->id_user_created[0]     = $id_user_created;
        $cs_product->date_updated[0]        = $date_updated;
        $cs_product->id_user_updated[0]     = $id_user_updated;
        $cs_product->date_deleted[0]        = $date_deleted;
        $cs_product->id_user_deleted[0]     = $id_user_deleted;
        
        $cs_product->is_comment[0]          = $isComment;
        $cs_product->price[0]               = $price;
        $cs_product->priceSaleOf[0]         = $priceSaleOf;
        $cs_product->re_urls[0]             = $re_urls;
        $cs_product->page_title[0]          =$page_tile;
        $cs_product->meta_keyword[0]        =$meta_keyword;
        $cs_product->meta_description[0]    =$meta_des;
        $typeFileSmall=true;
        $typeFileSlarge=true;
        if($_FILES['fileSmall']['name'])
        {
            $typeFileSmall=$cs_product->update_Imges('fileSmall',$size_file_image,$folder_upload_product,2);
        }
       
        if($_FILES['fileLarge']['name'])
        {
             $typeFileSlarge=$cs_product->update_Imges('fileLarge',$size_file_image,$folder_upload_product,1);
        }
          if( $typeFileSlarge==true&&$typeFileSmall==true)
        {
            $rsUpload=true;
        }   
          if($rsUpload)
          {
                $rs = false;
                $rs = $cs_product->update_product();
         if($rs==true)
                  echo navigateLink("admin.php?3nss=product&cate=".$id_Category."&action=list&complete=edit");
          else
                    echo getMessageJavascript("Please check your information , fail action","");
          }
          else
          {
                echo getMessageJavascript("Please check your information , fail action","");
                
          }
    }
}

if($p_url_id!="")
{
    $cs_product = new cs_product();
    $cs_product->select_product_by_id($p_url_id);
   
    $page->assign("cs_product",$cs_product);
    $page->assign("folder_upload_product",$folder_upload_product);
}


?>