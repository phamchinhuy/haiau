<?php

include_once("include/class/cs_gallery.php");

$p_url_cateid="";
if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
$user_id=isset($_SESSION["id_account_admin"])?strval((intval($_SESSION["id_account_admin"]))):"";             
$scriptPaging="";
$cs_gallery = new cs_gallery();
$cs_gallery->select_gallery_by_id_category($p_url_cateid);
$sumGallery = count($cs_gallery->id);

if($sumGallery>0)
{
    $countItemPage = 10;
    $countPage = 1;
    if($sumGallery>$countItemPage)
        $countPage=$sumGallery/$countItemPage;
    $modePage = $sumGallery%$countItemPage;
    for($i=0;$i<$countPage;$i++)
    {
        $j = $i + 1;
        if($i==0)
        {
            $scriptPaging.="'include/admin/admin_image_list.paging.php?page=".$j."&idcate=".$p_url_cateid."&user_id=".$user_id."'";
        }
            
        else
            $scriptPaging.=", 'include/admin/admin_image_list.paging.php?page=".$j."&idcate=".$p_url_cateid."&user_id=".$user_id."'";
    }
}

 $page->assign("scriptPaging",$scriptPaging);
        
?>