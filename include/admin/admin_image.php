<?php

//22/04/2010
//XUAN
include_once("include/class/cs_gallery.php");
include_once("include/class/cs_category.php");
include_once("include/class/cs_permisions.php");
$temp = "";
$strMessage = "";

$cate=isset($_GET['cate'])?$_GET['cate']:"";
$user_id=isset($_SESSION["id_account_admin"])?strval((intval($_SESSION["id_account_admin"]))):"";
$cs_permision=new cs_permision();
$admin_image_form="admin/default.tpl";  
if($cs_permision->check_authen_permision($user_id,$cate))
{
    $array=array();
    $array=$cs_permision->permision[0];
    if (isset($_GET["cate"]) || isset($_POST["cate"]))
    {
        //TEMPLATE CHINH 
        $main_content_template="admin/admin_image.tpl";
        
        $p_url_cateid = "";
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
        
        $p_url_id = "";
        if (isset($_GET["id"]) || isset($_POST["id"]))
            $p_url_id = isset($_GET["id"]) ? $_GET["id"] : $_POST["id"];
        
        //SHOW NAME CATEOGRY DUOC CHON
        $name_category = "";
        $cs_category = new cs_category();
        $cs_category->select_category_by_id($p_url_cateid);
        if(count($cs_category->id)>0)
            $name_category = $cs_category->category[0];
        $page->assign("name_category",$name_category);
        //END SHOW NAME CATEOGRY DUOC CHON
        
        //GET LINK MENU
        $linkList = "";
        $linkList = "admin.php?3nss=image&cate=".$p_url_cateid."&action=list";
        $linkAdd = "";
        $linkAdd = "admin.php?3nss=image&cate=".$p_url_cateid."&action=add";
        //END GET LINK MENU
      
        
        $action = "";
        if(isset($_GET["action"]) || isset($_POST["action"]))
            $action = isset($_GET["action"]) ? $_GET["action"] : $_POST["action"];
            
        
        switch($action)
        {
            case "list":
                if(isset($array)&&$array[0]==1)
                {
                    $admin_image_form = "admin/admin_image_list.tpl";
                    include("include/admin/admin_image_list.php");
                }
                break;
            case "":
                if($array[0]==1)
                {
                    $admin_image_form = "admin/admin_image_list.tpl";
                    include("include/admin/admin_image_list.php");
                } 
                break;
            case "add";
                if(isset($array)&&$array[1]==1)
                {
                    $admin_image_form = "admin/admin_image_add.tpl";
                    include("include/admin/admin_image_add.php");
                }
                break;
            case "edit";
                 if(isset($array)&&$array[3]==1)
                 {
                    $admin_image_form = "admin/admin_image_update.tpl";
                    include("include/admin/admin_image_update.php");
                 }             
                break;
            case "visible";
                $admin_image_form = "admin/admin_image_list.tpl";
                $str = "";
                $str = active("visible",$p_url_id,$p_url_cateid);
                $temp = $str;
                break;
            case "active";
                $admin_image_form = "admin/admin_image_list.tpl";
                $str = "";
                $str = active("active",$p_url_id,$p_url_cateid);
                $temp = $str;
                break;
            case "delete";
                if(isset($array)&&$array[2]==1)
                {
                                 
               $admin_image_form = "admin/admin_image_list.tpl";
                $str = "";
                $str = active("delete",$p_url_id,$p_url_cateid);
                $temp = $str;
                }
                break;
            default:
                $admin_image_form = "admin/admin_image_list.tpl";
                include("include/admin/admin_image_list.php");
                break;
        }
            
        $page->assign("array",$array);
        $page->assign("admin_image_form",$admin_image_form);
        $page->assign("linkList",$linkList);
        $page->assign("linkAdd",$linkAdd);
    }
else
{
    $main_content_template="admin/admin_home.tpl";
}
if (isset($_GET["complete"]) || isset($_POST["complete"]))
{
    $complete = isset($_GET["complete"]) ? $_GET["complete"] : $_POST["complete"];
    switch($complete)
    {
        case "add":
            $strMessage = "Th&#234;m m&#7899;i th&#224;nh c&#244;ng";
            break;
        case "edit":
            $strMessage = "C&#7853;p nh&#7853;t th&#224;nh c&#244;ng";
            break;
        case "visible":
            $strMessage = "C&#7853;p nh&#7853;t t&#236;nh tr&#7841;ng kh&#244;ng k&#237;ch ho&#7841;t th&#224;nh c&#244;ng";
            break;
        case "active":
            $strMessage = "C&#7853;p nh&#7853;t t&#236;nh tr&#7841;ng k&#237;ch ho&#7841;t th&#224;nh c&#244;ng";
            break;
        case "delete":
            $strMessage = "X&#243;a th&#224;nh c&#244;ng";
            break;
        
    }
    
}

//ASSIGN
$page->assign("temp",$temp);
$page->assign("strMessage",$strMessage);
//END ASSIGN
}
//FUNCTION
function active($p_action,$p_id,$p_id_category)
{
    $strURL = "";
    $status = 0;
    if($p_action=="visible")
        $status = "2";
    if($p_action=="active")
        $status = "1";
    if($p_action=="delete")
        $status = "3";
    
    $cs_gallery_status = new cs_gallery();
    $rs = $cs_gallery_status->update_gallery_set_status($status,$p_id);
    if($rs==true)
    {
        $strURL = navigateLink("admin.php?3nss=image&cate=".$p_id_category."&complete=".$p_action);
    }
    return $strURL;
}

?>