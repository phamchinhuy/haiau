<?php
//created by tran van tam 6-5-2010
include_once("include/class/cs_faq.php");
$temp = "";
$strMessage = "";

    //TEMPLATE CHINH 
    $main_content_template="admin/admin_faq.tpl";
    
   
    
    $action = "";
    if(isset($_GET["action"]) || isset($_POST["action"]))
        $action = isset($_GET["action"]) ? trim($_GET["action"]) : $_POST["action"];
       
     $id=isset($_GET['id'])?trim($_GET['id']):"";   
    
     //GET LINK MENU
    $linkList = "";
    $linkList = "admin.php?3nss=faq&action=list";
    $linkAdd = "";
    $linkAdd = "admin.php?3nss=faq&action=add";
   
    $admin_faq_form = "admin_faq_list.tpl";
    include("include/admin/admin_faq_list.php");
        
    switch($action)
    {
        case 'add':
             $admin_faq_form="admin/admin_faq_add.tpl";
             include("include/admin/admin_faq_add.php");
             break;
        case 'list':
             $admin_faq_form = "admin/admin_faq_list.tpl";
             include("include/admin/admin_faq_list.php"); 
             break;  
        case "edit":
            $admin_faq_form = "admin/admin_faq_update.tpl";
            include("include/admin/admin_faq_update.php");
            break;
        case "visible":
            $admin_faq_form = "admin/admin_faq_list.tpl";
            $str = "";
            $str = active("visible",$id);
            $temp = $str;
            break;
        case "active":
            $admin_faq_form = "admin/admin_faq_list.tpl";
            $str = "";
            $str = active("active",$id);
            $temp = $str;
            break;
    }    
    
    $page->assign("admin_faq_form",$admin_faq_form);
    

if (isset($_GET["complete"]) || isset($_POST["complete"]))
{
    $complete = isset($_GET["complete"]) ? $_GET["complete"] : $_POST["complete"];
    switch($complete)
    {
        case "add":
            $strMessage = "Th&#234;m m&#7899;i th&#224;nh c&#244;ng";
            break;
        case "edit":
            $strMessage = "C&#7853;p nh&#7853;t th&#224;nh c&#244;ng";
            break;
        case "visible":
            $strMessage = "C&#7853;p nh&#7853;t t&#236;nh tr&#7841;ng kh&#244;ng k&#237;ch ho&#7841;t th&#224;nh c&#244;ng";
            break;
        case "active":
            $strMessage = "C&#7853;p nh&#7853;t t&#236;nh tr&#7841;ng k&#237;ch ho&#7841;t th&#224;nh c&#244;ng";
            break;
        case "delete":
            $strMessage = "X&#243;a th&#224;nh c&#244;ng";
            break;
        
    }
    
}
function active($p_action,$p_id)
{
    $strURL = "";
    $status = 0;
    if($p_action=="visible")
        $status = "0";
    if($p_action=="active")
        $status = "1";
    
    
    $cs_faq_status = new cs_faq();
    $rs = $cs_faq_status->setFAQidStatus($status, $p_id);
    if($rs==true)
    {
        $strURL = navigateLink("admin.php?3nss=faq&complete=".$p_action);
    }
    return $strURL;
}
//ASSIGN
$page->assign('linkList',$linkList);
$page->assign('linkAdd',$linkAdd);
$page->assign("temp",$temp);
$page->assign("strMessage",$strMessage);
//END ASSIGN

//FUNCTION


?>