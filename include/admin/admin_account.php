<?php

/**
 * @author hohaithe
 * @copyright 2010
 */
include('include/business/blError.php');     	
include("include/class/cs_library.php");
$lang='vn';
$objLibrary= new cs_library();
//$objBody   = new body();
//$objStates   = new states();
$objUser=new cs_account();  
$objUserDetal=new cs_account();
$main_content_template = "admin/admin_user.tpl";
$headerTitle = "Quản lý thành viên";
if(isset($_GET["ac"])&&$_GET["ac"]=="in")
$page->assign('mess',$messinsersucess);
if(isset($_GET["ac"])&&$_GET["ac"]=="up")
$page->assign('mess',$messupdatesucess);
if(isset($_GET["ac"])&&$_GET["ac"]=="de")
$page->assign('mess',$messageDeleteSuccess);
 if(isset($_GET["ac"])&&$_GET["ac"]=="err")
$page->assign('mess', $messUnSucess);
if(isset($_GET["ac"])&&$_GET["ac"]=="re")
 $page->assign('mess',$messresset);
//khai bao bien
$linkiview=isset($_GET['link'])?$_GET['link']:"";
$linki        = array();
$no           = array();
$status_img   = array();
$status_bg=array(); 
//get value tu url
$urls=$_SERVER['PHP_SELF'].'?3nss=user';
$type=isset($_GET["type"])?$_GET["type"]:"";
$linked=isset($_GET["link"])?$_GET["link"]:"";
$id=isset($_GET['id'])?$_GET['id']:"";
$pr=isset($_GET['pr'])?$_GET['pr']:"";
$userType="";
if($pr!="")
{
  $objUser->do_select_Type_id($pr);
  $userType=$objUser->type[0];
}
$DllTyUser= $objUser->createTypeUserID($pr,$urls);
//get Post
$cbType=isset($_POST['cbtype'])?$_POST['cbtype']:"";
$username=isset($_POST['username'])?$_POST['username']:"";
$password=isset($_POST['txtpassword1'])?$_POST['txtpassword1']:"";
$fullName=isset($_POST['txtFullName'])?$_POST['txtFullName']:"";
$email=isset($_POST['txtuser_login_profile'])?$_POST['txtuser_login_profile']:"";
$status=isset($_POST['status'])?$_POST['status']:"0";
$phone=isset($_POST['txtuser_phone'])?$_POST['txtuser_phone']:"";
$yahoo=isset($_POST['txtuser_yahoo'])?$_POST['txtuser_yahoo']:"";
$sky=isset($_POST['txtuser_sky'])?$_POST['txtuser_sky']:"";
//khai bao url

       //khai bao so user tren mot trang
      //slect
      
$p=isset($_GET['p'])?$_GET['p']:"0";
$rows ='10'; 
$div  = 10;
 $userType     ="subadmin";
$start= $p*$rows;
$total= $objUser->donum($userType);
$pages=$objLibrary->doPage($total,$p, $div, $rows,$urls,$userType);
$objUser->doSelectL($start,$rows,"id  DESC",$userType);
for($i=0;$i<count($objUser->accountId);$i++)
{
    $sqldate = strtotime($objUser->date_last_login[$i]);
    $date = date('d-m-Y', $sqldate);	
    if( $date=="01-01-1970")
    {
        $date="";
    } 
    $objUser->date_last_login[$i]=$date;
    //update
    $sqldate = strtotime($objUser->date_updated[$i]);
    $date = date('d-m-Y', $sqldate);	
    if( $date=="01-01-1970")
    {
        $date="";
    } 
    $objUser->date_updated[$i]=$date;
          
     	if( $objUser->status[$i]){
		$status_img[$i]= 'visible.gif';
		$status_bg[$i]= 'bgcolor="#FFFFFF";';
            
		}
		else{
		$status_img[$i]= 'hidden.gif';
		$status_bg[$i]= 'bgcolor="#F5F5F5"';
	    }
    $no[$i]=$i+1;
    $linki[$i]=$urls.'&id='. $objUser->accountId[$i]." ";
}
      //
          //userlogin la admin
       if(isset($_POST['save'])&&$_POST['save']=="Lưu")
       {
          $objUser->username[0]=$username;
          $objUser->type[0]=$cbType;
          $objUser->password[0]=$password;
          $objUser->fullname[0]=$fullName;
          $objUser->email[0]=$email;
          $objUser->status[0]=$status;
          $objUser->phone[0]=$phone;
          $objUser->nick_yahoo[0]=$yahoo;
          $objUser->nick_skype[0]=$sky;
          if($objUser->doinsert())
          {
              echo '<script>document.location="'.$urls.'&ac=in"</script>';   
          }
          else
          {
            echo '<script>document.location="'.$urls.'&ac=err"</script>';   
          }
       }
         if(isset($_POST['save'])&&$_POST['save']=="Cập nhật")
       {
          //$objUser->username[0]=$username;
          $objUser->type[0]=$cbType;
          //$objUser->password[0]=$password;
          $objUser->fullname[0]=$fullName;
          ///$objUser->email[0]=$email;
          $objUser->status[0]=$status;
          $objUser->phone[0]=$phone;
          $objUser->nick_yahoo[0]=$yahoo;
          $objUser->nick_skype[0]=$sky;
          if($objUser->doupdate($id))
          {
              echo '<script>document.location="'.$urls.'&ac=up"</script>';   
          }
          else
          {
             echo '<script>document.location="'.$urls.'&ac=err"</script>';   
          }
       }
            if($linked!="" &&$linked=="del")
      {
          $result= $objUser->doDelete($id);
          if($result)
          {
         echo '<script>document.location="'.$urls.'&ac=de"</script>';
          }
         else{
             echo '<script>document.location="'.$urls.'&ac=err"</script>';
          }
      }
      if(isset($_GET['status'])&&$_GET['status']=="active")
      {
      
        $objUser->doActive($id,$_GET['active']);
        echo '<script>document.location="'.$urls.'"</script>';
      }
    //xoa
if(isset($_POST['chkid']))
{
       $nums=$_POST['chkid'];
       foreach($nums as $key =>$value)
  {
    $objUser->doDelete($value);
  }
  echo '<script>document.location="'.$urls.'&ac=de"</script>';    
}
if($id!=""&&$type=="edit")
{  
    $objUserDetal->do_select_users_id($id);
     $dllType=$objUser->createTypeUser($objUserDetal->type[0]);  
}
else
{
     $dllType=$objUser->createTypeUser("");  
}
$page->assign('DllTyUser', $DllTyUser);
$page->assign('dllType', $dllType);
$page->assign('objUser', $objUser);
$page->assign('type',$type);
$page->assign('objUserDetal',$objUserDetal);
$page->assign('pages',$pages);
$page->assign('status_img',$status_img);
$page->assign('status_bg',$status_bg);
$page->assign('linki',$linki);
$page->assign('no',$no);

?>