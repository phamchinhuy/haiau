<?php //create by tran van tam
require_once '../global.php';
require_once '../lib.db.common.inc.php';
require_once '../lib.util.common.inc.php';
include_once("../class/cs_news.php");
include_once("../class/cs_gallery.php");
include_once("../class/cs_video.php");
$cs_news = new cs_news();
$cs_gallery = new cs_gallery();
$cs_video   = new cs_video();
if(isset($_POST['$txtSEOAdd']))
{
    $txtSEOAdd=isset($_POST['$txtSEOAdd'])?$_POST['$txtSEOAdd']:"";
    
    if($cs_news->exist_SEO($txtSEOAdd))
    {
    	//SEO is not availble
    	echo "yes";
    } 
    else
    {
    	//SEO is available
    	echo "no";
    }
}
if(isset($_POST['$txtSEOEdit']))
{
    $txtSEOEdit=isset($_POST['$txtSEOEdit'])?$_POST['$txtSEOEdit']:"";
    $demo=$_POST['$demo'];
    if($txtSEOEdit==$demo)
    echo "yes";
    else
    {
        if($cs_news->exist_SEO($txtSEOEdit))
        {
        	//SEO is not availble
        	echo "yes";
        } 
        else
        {
        	//SEO is available
        	echo "no";
        }
    }
    
}
//images

if(isset($_POST['$txtSEOImageAdd']))
{
    $txtSEOImageAdd=isset($_POST['$txtSEOImageAdd'])?trim($_POST['$txtSEOImageAdd']):"";
    
    if($cs_gallery->exist_SEO($txtSEOImageAdd))
    {
    	//SEO is not availble
    	echo "yes";
    } 
    else
    {
    	//SEO is available
    	echo "no";
    }
}
if(isset($_POST['$txtSEOImageEdit']))
{
    $txtSEOImageEdit=isset($_POST['$txtSEOImageEdit'])?$_POST['$txtSEOImageEdit']:"";
    $demo=$_POST['$demo'];
    if($txtSEOImageEdit==$demo)
    echo "yes";
    else
    {
        if($cs_gallery->exist_SEO($txtSEOImageEdit))
        {
        	//SEO is not availble
        	echo "yes";
        } 
        else
        {
        	//SEO is available
        	echo "no";
        }
    }
    
}
//media

if(isset($_POST['$txtSEOMediaAdd']))
{
    $txtSEOMediaAdd=isset($_POST['$txtSEOMediaAdd'])?trim($_POST['$txtSEOMediaAdd']):"";
    
    if($cs_video->exist_SEO($txtSEOMediaAdd))
    {
    	//SEO is not availble
    	echo "yes";
    } 
    else
    {
    	//SEO is available
    	echo "no";
    }
}
if(isset($_POST['$txtSEOMediaEdit']))
{
    $txtSEOMediaEdit=isset($_POST['$txtSEOMediaEdit'])?$_POST['$txtSEOMediaEdit']:"";
    $demo=$_POST['$demo'];
    if($txtSEOMediaEdit==$demo)
    echo "yes";
    else
    {
        if($cs_video->exist_SEO($txtSEOMediaEdit))
        {
        	//SEO is not availble
        	echo "yes";
        } 
        else
        {
        	//SEO is available
        	echo "no";
        }
    }
    
}
?>