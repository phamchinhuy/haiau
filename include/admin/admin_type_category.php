<?php

/**
 * @author hohaithe
 * @copyright 2010
 */
$file_path=FILE_PATH; 
include_once($file_path.'/class/cs_library.php');
include_once($file_path.'/class/cs_type_category.php');
include_once($file_path.'/class/cs_module.php');
include_once($file_path.'/business/blError.php');

//action 
if(isset($_GET["ac"])&&$_GET["ac"]=="in")
    $page->assign('mess',$messinsersucess);      
if(isset($_GET["ac"])&&$_GET["ac"]=="up")
    $page->assign('mess',$messupdatesucess);
if(isset($_GET["ac"])&&$_GET["ac"]=="de")
     $page->assign('mess',$messageDeleteSuccess);
if(isset($_GET["ac"])&&$_GET["ac"]=="err")
 $page->assign('mess',$messUnSucess);
//
$module=new module();
$template="";
$main_content_template = "admin/admin_type_category.tpl";  
$headerTitle = 'QU&#7842;N L&#221; LO&#7840;I DANH M&#7908;C';
$urls="admin.php?3nss=categories";
//khai bao calss

//get Url
$id=isset($_GET['id'])?$_GET['id']:"";
$type=isset($_GET['type'])?$_GET['type']:"";
$status=isset($_GET['status'])?$_GET['status']:"";
$p=isset($_GET['p'])?$_GET['p']:"0";
//get Post 
$txtimageIcon=isset($_FILES['tmp_icon']['tmp_name'])?$_FILES['tmp_icon']['tmp_name']:"";
$txtName=isset($_POST['txtName'])?$_POST['txtName']:"";
$status=isset($_POST['status'])?$_POST['status']:"0";
$txtkeyword=isset($_POST['txtkeyWord'])?$_POST['txtkeyWord']:"";
$txttemplate=isset($_POST['txtTemplate'])?$_POST['txtTemplate']:"";
//
$objCategory=new cs_type_category();
$objLibrary=new cs_library();//khai bao thu vien catogory
$objCategoryDetail=new cs_type_category();
//
$status_bg=array();
$linki=array();
$status_img=array();
$no=array();
$savefolder = IMAGES_ADMIN_UPLOAD_PATH."/icon/"; //folder image upload
 
$css=CSS_ADMIN_PATH;
$js=JS_ADMIN_PATH;
$imges_path=IMAGES_ADMIN_PATH;
    //insert 
if(isset($_POST['btn'])||(isset($_POST['btn'])&&$_POST['btn']!=""))
{
  //path
  $objCategory->savefolder=$savefolder;  
    //insert 
  $postion=$objCategory->select_max_position()+1;  
 $objCategory->type[0]=$txtName;
 $objCategory->template[0]=$txttemplate;
 $objCategory->status[0]=$status;
 ///
 //$objCategory->small_images[0]="small".$txtimagethum;
 $objCategory->date_created[0]="now()";  
 $objCategory->id_user_created[0]=$_SESSION["id_account_admin"];
 //$objCategory->date_created[0]="now()";
 $objCategory->key_type[0]=$txtkeyword;
  $objCategory->icon[0]=$txtimageIcon;
  $objCategory->position[0]=$postion;

 if($objCategory->doinsert())
 {
       // echo  $txtposition;
    echo '<script>document.location="'.$urls.'&ac=in"</script>';   
 }
 else
 {
    echo '<script>document.location="'.$urls.'&ac=err"</script>';   
    
 } 
}      
//update
if(isset($_POST['action'])||( (isset($_POST['action'])&&$_POST['action']!="")))
{

$objCategory->savefolder=$savefolder;  
$objCategory->type[0]=$txtName;
$objCategory->template[0]=$txttemplate;
$objCategory->status[0]=$status;
$objCategory->date_updated[0]="now()";  
$objCategory->id_user_updated[0]=$_SESSION["id_account_admin"];
$objCategory->key_type[0]=$txtkeyword;
$objCategory->icon[0]=$txtimageIcon;
if($objCategory->doupdate($id))
{
    echo '<script>document.location="'.$urls.'&ac=up"</script>';   
}
else
{
    echo '<script>document.location="'.$urls.'&ac=err"</script>';   
} 
}
         
//select
$rows ='20'; 
$div  = 10;
$start= $p*$rows;
$total=    $objCategory->donum();
$arrcb= array(); 
//list ajax       
$scriptPagingNews="";
$sumNews =$total;
if($sumNews>0)
{
    $countItemPage = 10;
    $countPage = 1;
    if($sumNews>$countItemPage)
        $countPage=$sumNews/$countItemPage;
    $modePage = $sumNews%$countItemPage;
    for($i=0;$i<$countPage;$i++)
    {
        $j = $i + 1;
        if($i==0)
        {
            $scriptPagingNews.="'".$file_path."/admin/admin_type_category_list.paging.php?page=".$j."'";
        }
            
        else
            $scriptPagingNews.=", '".$file_path."/admin/admin_type_category_list.paging.php?page=".$j."'";
    }
}
 $page->assign("scriptPagingNews",$scriptPagingNews);
 
   // 
    //delete
if($type!="" &&$type=="del")
{
  $objCategory->id_user_deleted[0]=isset($_SESSION['user_loged_id'])?$_SESSION['user_loged_id']:"0"; 
  $result=$objCategory->doDelete($id);
  if($result)
  {
 echo '<script>document.location="'.$urls.'&ac=de"</script>';
  }
 else{
    echo '<script>document.location="'.$urls.'&ac=err"</script>';
  }
}
if($type!="" &&$type=="delx")
{
  $objCategory->id_user_deleted[0]=isset($_SESSION['user_loged_id'])?$_SESSION['user_loged_id']:"0"; 
  $result=$objCategory->doDeletex($id);
  if($result)
  {
    echo '<script>document.location="'.$urls.'&ac=de"</script>';
  }
 else{
    echo '<script>document.location="'.$urls.'&ac=err"</script>';
  }
}
if(isset($_GET['status'])&&$_GET['status']=="active")
{

$objCategory->doActive($id,$_GET['active']);
echo '<script>document.location="'.$urls.'"</script>';
}
//xoa
if(isset($_POST['chkid']))
{
$numr=$_POST['chkid'];
$flag=true;
   foreach($numr as $key =>$value)
{
$objCategory->doDelete($value);
}
echo '<script>document.location="'.$urls.'&ac=de"</script>';    
}
    //edit
   
    if($type=="edit"&&$id=!"") 
    {
        $objCategoryDetail->do_selectall_typecateogory_id($_GET['id']); 
        $template=$objCategoryDetail->template[0];
    }
    $cb=$module->create_cb($template);
    // ]
$page->assign("savefolder",$savefolder);
$page->assign("imges_path",$imges_path);
$page->assign("js",$js);
$page->assign("css",$css);    
$page->assign("arrcb",$arrcb);
$page->assign("cb",$cb);
$page->assign("type",$type);
$page->assign("objCategoryDetail",$objCategoryDetail);  
$page->assign("objCategory",$objCategory);


  
  
  
?>