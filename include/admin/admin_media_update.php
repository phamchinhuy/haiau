<?php


include_once("include/class/cs_video.php");
include_once("include/class/cs_date.php");

$folder_upload_media = "uploads/media/";
$size_file_image = "2148576"; 	
$id_user = "1";

$p_url_cateid = "";
if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
        

$id = "";
if (isset($_GET["id"]) || isset($_POST["id"]))
    $id = isset($_GET["id"]) ? $_GET["id"] : $_POST["id"];


if(isset($_POST["btEdit"])&&$_POST["btEdit"]!="")
{
    if($p_url_cateid!="")
    {
        $id_Category = $p_url_cateid;
        
        $news = "";
        $news = isset($_POST["txtNews"])?$_POST["txtNews"]:"";
        
        $small_image = "";
        $large_image = "";
        
        $iscomment = "";
        $isComment =isset($_POST["isComment"])?$_POST["isComment"]:"0";
        if($isComment=="on")
        {
            $isComment=1;
        }
        $shortDescription   =isset($_POST["txtShortDescription"])?$_POST["txtShortDescription"]:"";
        $longDescription    =isset($_POST["description_news"])?$_POST["description_news"]:" ";
        $small_image        =isset($_FILES['fileSmall']['name'])?$_FILES['fileSmall']['name']:"";
        $large_image        =isset($_FILES['fileLarge']['name'])?$_FILES['fileLarge']['name']:"";
        $active             =isset($_POST["active"])? $_POST["active"]:"2";
        
        $url                = "";
        $url                = isset($_POST["txtUrl"])?$_POST["txtUrl"]:"0";
        $urlVideo           = isset($_POST["txtUrlVideo"])?$_POST["txtUrlVideo"]:"0";
        $page_title         = isset($_POST['txtTitleSEO'])?$_POST['txtTitleSEO']:"";
        $meta_description   = isset($_POST['txtDescriptionMeta'])?$_POST['txtDescriptionMeta']:"";
        $meta_keyword       = isset($_POST['txtKeywordMeta'])?$_POST['txtKeywordMeta']:"";
      
        //get date 
       
        $begin_date         = isset($_POST['beginDate'])?$_POST['beginDate']:"";
        $begin_end          = isset($_POST['beginEnd'])?$_POST['beginEnd']:"";
        
        $date_created       = date("Y-m-d");
        $id_user_created    = $id_user;
        
        $date_updated       = $date_created;
        $id_user_updated    = $id_user;
        
        $date_deleted       = "";
        $id_user_deleted    = "";
       
        
       
        $cs_video_image     = new cs_video();
        $cs_video_image->select_video_by_id($p_url_id);
        $status             = $cs_video_image->status[0];
        
        if($status=="on")
            $status = "1";
        else
            $status="0";
            
        if($iscomment=="on")
            $iscomment="1";
        else
            $iscomment = "0";
            
        
        //upload image
        $typeFileSmall=false;
        $FileTypeUploadSmall=isset($_FILES["fileSmall"]['type'])?$_FILES["fileSmall"]['type']:"";
        $FileType =Array('image/jpg','image/png','image/gif','image/pjpeg','image/jpeg');
        if(in_array($FileTypeUploadSmall,$FileType))
        {
            $typeFileSmall= true;
        }
        
        $rsUpload = false;
        if($_FILES["fileSmall"]["tmp_name"]&&$FileType)
        {
            $small_image =  uniqid("").".jpg";
            $small_image = "small"."_".$small_image;
            if($_FILES['fileSmall']['size']<$size_file_image)
            {
                if(move_uploaded_file($_FILES["fileSmall"]["tmp_name"],$folder_upload_media.$small_image))
                    $rsUpload=true;
            }
            else
                $rsUpload = false;
            
        }
        else
        {
            $small_image=$cs_video_image->small_image[0];
            $rsUpload = true;
        }
        //upload large image
        
        $typeFileLarge=false;
        $FileTypeUploadLarge=isset($_FILES["fileLarge"]['type'])?$_FILES["fileLarge"]['type']:"";
        $FileType =Array('image/jpg','image/png','image/gif','image/pjpeg','image/jpeg');
        if(in_array($FileTypeUploadLarge,$FileType))
        {
            $typeFileLarge= true;
        }
        if($_FILES["fileLarge"]["tmp_name"]&&$typeFileLarge)
        {
            $large_image = uniqid("").".jpg";
            $large_image = "large"."_".$large_image;
            if($_FILES['fileLarge']['size']<$size_file_image)
            {
                if(move_uploaded_file($_FILES["fileLarge"]["tmp_name"],$folder_upload_media.$large_image))
                    $rsUpload=true;
            }
            else
                $rsUpload = false;
        }
        else
        {
            $large_image=$cs_video_image->large_image[0];
            $rsUpload = true;
        }
            
        //get information from form into cs_news class   
        $cs_date  = new cs_date();
        $cs_video = new cs_video();
        $cs_video->id[0]                  = $id;
        $cs_video->id_category[0]         = $p_url_cateid;
        $cs_video->video[0]               = $news;
        $cs_video->small_image[0]         = $small_image;
        $cs_video->large_image[0]         = $large_image;
        $cs_video->short_description[0]   = $shortDescription;
        $cs_video->long_description[0]    = $longDescription;
        $cs_video->status[0]              = $active;
        $cs_video->url[0]                 = $url;
        
        $cs_video->date_created[0]        = $date_created;
        $cs_video->id_user_created[0]     = $id_user_created;
        $cs_video->date_updated[0]        = $date_updated;
        $cs_video->id_user_updated[0]     = $id_user_updated;
        $cs_video->date_deleted[0]        = $date_deleted;
        $cs_video->id_user_deleted[0]     = $id_user_deleted;
        $cs_video->file_extention[0]      ="jpg";  
        $cs_video->show_page[0]           = "";
        
        $cs_video->keyword[0]             = "";
        $cs_video->page_title[0]          = $page_title;
        $cs_video->meta_description[0]    = $meta_description;
        $cs_video->meta_keyword[0]        = $meta_keyword;
        $cs_video->is_comment[0]          = $isComment;
        $cs_video->count_view[0]          = "";
        $cs_video->begin_date[0]          = $cs_date->formatYYYYMMDD($begin_date);
        $cs_video->begin_end[0]           = $cs_date->formatYYYYMMDD($begin_end);
        
        
        
        
        //END UPLOAD IMAGE
      
      if($rsUpload)
        
         {
            $rs = false;
            $rs = $cs_video->update_video();
            if($rs==true)
                echo navigateLink("admin.php?3nss=media&cate=".$id_Category."&action=list&complete=edit");
         }
         else
         {
            
              echo  getMessageJavascript("Please check your information , fail action","");  
              $rsUpload = false;
         }  
      }
}

if($p_url_id!="")
{
    $cs_video = new cs_video();
    $cs_date  = new cs_date();
    $cs_video->select_video_by_id($p_url_id);
    $cs_video->begin_date[0]=date('d/m/Y',strtotime($cs_video->begin_date[0]));
    $cs_video->begin_end[0]=date('d/m/Y',strtotime($cs_video->begin_end[0]));
   
    $page->assign("cs_video",$cs_video);
    $page->assign("folder_upload_media",$folder_upload_media);
}


?>