<?php

/**
 * @author hohaithe
 * @copyright 2010
 */
$savefolder = IMAGES_ADMIN_UPLOAD_PATH."/category/"; //folder image upload
$css=CSS_ADMIN_PATH;
$js=JS_ADMIN_PATH;
$imges_path=IMAGES_ADMIN_PATH;
$file_path=FILE_PATH;
//DEFINE//
include($file_path.'/class/cs_library.php');
include($file_path.'/class/cs_type_category.php');
include($file_path.'/class/cs_category.php');
include($file_path.'/business/blCategory.php');
include($file_path.'/business/blError.php');
//action 
if(isset($_GET["ac"])&&$_GET["ac"]=="in")
    $page->assign('mess',$messinsersucess);      
if(isset($_GET["ac"])&&$_GET["ac"]=="up")
    $page->assign('mess',$messupdatesucess);
if(isset($_GET["ac"])&&$_GET["ac"]=="de")
    $page->assign('mess',$messageDeleteSuccess);
if(isset($_GET["ac"])&&$_GET["ac"]=="err")
    $page->assign('mess',$messUnSucess);
 
$main_content_template = "admin/admin_category.tpl";  
$headerTitle = 'QU&#7842;N L&#221; DANH M&#7908;C';
$urls="admin.php?3nss=categories.manage";
$max_file = "2148576"; 	

$folder_upload_category = "../upload/category/";
$size_file_image = "2148576"; 
$id_user=isset($_SESSION['id_account_admin'])?$_SESSION['id_account_admin']:"";
//get Url
$id=isset($_GET['id'])?strval(intval($_GET['id'])):"";
$type=isset($_GET['type'])?$_GET['type']:"";
$status=isset($_GET['status'])?$_GET['status']:"";
$p=isset($_GET['p'])?strval(intval($_GET['p'])):"0";
$url_id_site=isset($_GET['id_site'])?strval(intval($_GET['id_site'])):"";

//get Post 
$txttype=isset($_POST['ddlType'])?$_POST['ddlType']:"";

$txtposition=isset($_POST['ddlPosition'])?$_POST['ddlPosition']:"";

$txtparent=isset($_POST['ddlParent'])?$_POST['ddlParent']:"0";

$txtimageLarge=isset($_FILES['image_large']['tmp_name'])?$_FILES['image_large']['tmp_name']:"";

$txtimagethum=isset($_FILES['image_thum']['tmp_name'])?$_FILES['image_thum']['tmp_name']:"";

$txtcategory=isset($_POST['txtcategory'])?$_POST['txtcategory']:"";

$txtUrl=isset($_POST['txturl'])?$_POST['txturl']:"";

$id_site=isset($_POST['site'])?$_POST['site']:"";

if($txtUrl!="")
    $txtUrl=trim($txtUrl);
    
$txtUrl=replaceunicode($txtUrl);

$txtTarget=isset($_POST['target'])?$_POST['target']:"";

$status=isset($_POST['status'])?$_POST['status']:"2";

$txtshowchild=isset($_POST['show'])?$_POST['show']:0;

$txtkeyword=isset($_POST['txtkeyWord'])?$_POST['txtkeyWord']:"";

//showpage
$_POST['showpage']=isset($_POST['showpage'])?$_POST['showpage']:"0";

$_POST['specialnews']=isset($_POST['specialnews'])?$_POST['specialnews']:"0";

$_POST['hotnews']=isset($_POST['hotnews'])?$_POST['hotnews']:"0";

$txtshowpage=  $_POST['showpage'].",".$_POST['specialnews'].",".$_POST['hotnews'];

$txttemplate=isset($_POST['txtTemplate'])?$_POST['txtTemplate']:"";

$txtmusic=isset($_POST['txtmusic'])?$_POST['txtmusic']:"";

//seoshow
$pagetitle=isset($_POST['txtPageTitle'])?$_POST['txtPageTitle']:"";
$txtmetaDes=isset($_POST['txtmeta_description'])?$_POST['txtmeta_description']:"";
$txtmetakeyword=isset($_POST['txtmeta_keyword'])?$_POST['txtmeta_keyword']:"";
//
$id=isset($_GET['id'])?$_GET['id']:"";
////
$iSite=isset($_GET['iSite'])?strval(intval($_GET['iSite'])):"";
$objCategory=new cs_category();
$objLibrary=new cs_library();//khai bao thu vien catogory
$objCategoryDetail=new cs_category();
$objEdit=new cs_category();
$obj=new cs_category();
$objcbsite=new cs_category();
$status_bg=array();
$linki=array();
$status_img=array();
$no=array();
$dllparent="";
$cbsite_list="";

//insert 
if(isset($_POST['btn']))
{
    
    $objCategory->id_type_category[0]=$txttype;
    $objCategory->category[0]=$txtcategory;
    $objCategory->status[0]=$status;
    
    $img_thum = "";
    $resupload=true;
    $rslarge=true;
    $rssmall=true;
    if($_FILES["image_thum"]["tmp_name"])
    {
        $rssmall=$objCategory->update_Imges("image_thum",$max_file,$savefolder,2);
    }
    if($_FILES["image_large"]["tmp_name"])
    {
        $rslarge=$objCategory->update_Imges("image_large",$max_file,$savefolder,1);
    }
 
    $objCategory->order[0]=(int)$objCategory->getMaxCategory()+1;
    $objCategory->url[0]=$txtUrl;
    $objCategory->target[0]=$txtTarget;
    $objCategory->show_subCategory[0]= $txtshowchild;
    //echo   "<br>".$objCategory->show_subCategory[0]."<Br>";
    $objCategory->date_created[0]="now()";
    $objCategory->date_deleted[0]='null';
    $objCategory->date_updated[0]='null';
    $objCategory->id_user_created[0]=$_SESSION['id_account_admin'];
    $objCategory->id_user_updated[0]=$id_user;
    $objCategory->id_user_deleted[0]=$id_user;
    $objCategory->id_parent[0]="0";
    //$objCategory->show_subCategory[0]="";
    $objCategory->keyword[0]=$txtkeyword;
    $objCategory->id_site[0]=$id_site;
    
    //seo
    $objCategory->meta_keyword[0]=$txtmetakeyword;
    $objCategory->meta_description[0]=$txtmetaDes;
    $objCategory->page_title[0]=$pagetitle;
    
    $objCategory->show_page[0]=$txtshowpage;
    $objCategory->id_music_background[0]=$txtmusic;
    $objCategory->template[0]=$txttemplate;
    $objCategory->re_urls[0]=$objCategory->RemoveSign($txtcategory);
    $objCategory->positons[0]= $txtposition;
    $objCategory->id_parent_sected[0]= $txtparent;
    $id_site_by_parent="";
    if($rssmall==true &&$rslarge==true)
    {
        $rsUpload=true;
    }
    else
        $rsUpload=false;
        
    $rs=false;
    if($rsUpload==true)
    {
          $rs=$objCategory->doinsert();
    }
    if($rs==true)
    {
         
         if($txtposition==1)
         {
              if($txtparent!="0")
              {
                    $rs=false;  
                    $rs=$objCategory->update_position($txtparent,$id_site);
                   if($rs==true)
                   {
                        echo '<script>document.location="'.$urls.'&ac=in&id_site='.$id_site.'"</script>';   
                   }
                   else
                   {
                    echo '<script>document.location="'.$urls.'&ac=err&id_site='.$id_site.'"</script>';   
                   }
              }
              else
              {
                     echo '<script>document.location="'.$urls.'&ac=in&id_site='.$id_site.'"</script>';   
              }
         }
        else
        {
            $obj=new cs_category();
            $parent=$objCategory->getCategory_parent_id($txtparent);
            $rs=false;             
            $rs=$objCategory->update_position($parent,$id_site_by_parent);
            if($rs==true)
            {
              $id_category=$obj->getMaxId();
              $position_category_id=$obj->getOrder_parent_id($id_category);
              if($txtposition==0)
                {                       
                $obj->swapOrder($obj->getCategory_parent_id($obj->getMaxId()),$obj->getOrder_parent_id($txtparent),$position_category_id,1);
                }
                if($txtposition==2)
                {  
                    
                $obj->swapOrder($obj->getCategory_parent_id($obj->getMaxId()),$obj->getOrder_parent_id($txtparent),$position_category_id,0);
}
                 echo '<script>document.location="'.$urls.'&ac=in&id_site='.$id_site.'"</script>';   
            }
        }   
    
    } 
}      

//update
if(isset($_POST['action'])||( (isset($_POST['action'])&&$_POST['action']!="")))
{
    $id_site_selected=isset($_POST['cbsited'])?$_POST['cbsited']:"";
    $parent_selected=isset($_POST['parented'])?$_POST['parented']:"";
    //echo $_POST['parented'];
    //insert 
$objCategory->id_type_category[0]=$txttype;
$objCategory->category[0]=$txtcategory;
$objCategory->status[0]=$status;
$savefolder = "uploads/category/"; //folder image upload
$img_thum = "";
    if($_FILES["image_thum"]["tmp_name"])
    {
        //$newname =  uniqid("").".jpg";
        //$img_thum = "cate_thum"."_".$newname;
        
        $filename = $_FILES['image_thum']['name'];
        $ext = getFileExtension($filename);
      
        $newname =  uniqid("").$ext;
        $img_thum = "cate_thum"."_".$newname;
        
      
        if($_FILES['image_thum']['size']<$max_file)
        {
            move_uploaded_file($_FILES["image_thum"]["tmp_name"],$savefolder.$img_thum);      
        }
    }

$image_large = "";
    if($_FILES["image_large"]["tmp_name"])
    {
        //$newname_large =  uniqid("").".jpg";
        //$image_large = "cate_large"."_".$newname_large;
        
        $filename = $_FILES['image_large']['name'];
        $ext = getFileExtension($filename);
       // $objImage=new image();
        $newname =  uniqid("").$ext;
        $image_large = "cate_large"."_".$newname;
        
        if($_FILES['image_large']['size']<$max_file)
        {
            move_uploaded_file($_FILES["image_large"]["tmp_name"],$savefolder.$image_large);      
        }
    }

    $objCategory->small_images[0]=$img_thum;
    $objCategory->large_images[0]=$image_large;
    $objCategory->order[0]=(int)$objCategory->getMaxCategory()+1;
    $objCategory->url[0]=$txtUrl;
    $objCategory->target[0]=$txtTarget;
    $objCategory->show_subCategory[0]=$txtshowchild;
    $objCategory->date_created[0]="";
    // $objCategory->date_deleted[0]="now()";
    $objCategory->date_updated[0]='now()';
    $objCategory->id_user_created[0]=$id_user;
    $objCategory->id_user_updated[0]=$_SESSION['id_account_admin'];
    $objCategory->id_user_deleted[0]="";
    $objCategory->id_parent[0]="0";
    $objCategory->show_subCategory[0]=$txtshowchild;
    $objCategory->id_user_created[0]=$id_user;
    $objCategory->keyword[0]=$txtkeyword;
    $objCategory->id_site[0]=$id_site;
    //seo
    $objCategory->meta_keyword[0]=$txtmetakeyword;
    $objCategory->meta_description[0]=$txtmetaDes;
    $objCategory->page_title[0]=$pagetitle;
    $objCategory->show_page[0]=$txtshowpage;
    $objCategory->id_music_background[0]=$txtmusic;
    $objCategory->template[0]="";
    $objCategory->re_urls[0]=$objCategory->RemoveSign($txtcategory);
    $objCategory->positons[0]= $txtposition;
    $objCategory->id_parent_sected[0]= $txtparent;
     $rsupdate=false;
     $rsupdate=$objCategory->doupdate($id);
  if($rsupdate==true)
    {    
        if($txtposition==1)
        {        
            if($parent_selected!=$txtparent||$id_site_selected!=$id_site)
            {  
                $rs=false;
                $rs=$objCategory->update_position($txtparent,$id_site);
            //echo $query;
                if($rs==true)
                {
                echo '<script>document.location="'.$urls.'&id_site='.$id_site.'&ac=up"</script>';   
                }
                else
                {
               echo '<script>document.location="'.$urls.'&id_site='.$id_site.'&ac=up"</script>'; 
                }
            }
            else
            {
                echo '<script>document.location="'.$urls.'&id_site='.$id_site.'&ac=up"</script>';   
            }            
        }
        else
        {
            $obj=new cs_category();
            $rs=false;
            $parent=$objCategory->getCategory_parent_id($txtparent);
           
            $rs=$objCategory->update_position($parent,$id_site);
        
            if($rs==true)
            {
                 $position_category_id=$obj->getOrder_parent_id($id);
                if($txtposition==0)
                {                    
                  $obj->swapOrder($obj->getCategory_parent_id($id),$obj->getOrder_parent_id($txtparent),$position_category_id,1);
                }
                if($txtposition==2)
                {  
                  
                    $obj->swapOrder($obj->getCategory_parent_id($id),$obj->getOrder_parent_id($txtparent),$position_category_id,0);
                }
                echo '<script>document.location="'.$urls.'&ac=up&id_site='.$id_site.'"</script>';   
            }
        }   
            }   else{      
                  echo '<script>document.location="'.$urls.'&ac=err&id_site='.$id_site.'"</script>';   
                }
}
 //list //////   
//select
$rows =5; 
$div  = 10;
$start= $p*$rows;
//echo  $url_id_site;
$objCategory->postion=0;
if($url_id_site=="")
{
        $total= $objCategory->donum("");
        $objCategory->id_site[0]="";
        $cbsite_list=$objCategory->create_combox_site("","");
}
else
{
        $cbsite_list=$objCategory->create_combox_site($url_id_site,"");
        $total=$objCategory->donum_by_site($url_id_site);
        //echo $total;
}
  

//list ajax       
$scriptPagingNews="";
$sumNews =$total;
if($sumNews>0)
{
    $countItemPage = 10;
    $countPage = 1;
    if($sumNews>$countItemPage)
        $countPage=$sumNews/$countItemPage;
    $modePage = $sumNews%$countItemPage;
    for($i=0;$i<$countPage;$i++)
    {
        $j = $i + 1;
        if($i==0)
        {
            $scriptPagingNews.="'".$file_path."/admin/admin_category_list.paging.php?page=".$j."&idcate=".$url_id_site."'";
        }
            
        else
            $scriptPagingNews.=", '".$file_path."/admin/admin_category_list.paging.php?page=".$j."&idcate=".$url_id_site."'";
    }
}
 $page->assign("scriptPagingNews",$scriptPagingNews);
///end list
    //delete
if($type!="" &&$type=="del")
{
    $result=$objCategory->doDelete($id);
    if($result)
    {
        if($iSite==""||$url_id_site==0)
            echo '<script>document.location="'.$urls.'&ac=de"</script>';
        else
            echo '<script>document.location="'.$urls.'&ac=de&id_site='.$url_id_site.'"</script>';
    }
    else
    {
        if($iSite==""||$url_id_site==0)
            echo '<script>document.location="'.$urls.'&ac=err"</script>';
        else
              echo '<script>document.location="'.$urls.'&ac=err&id_site='.$url_id_site.'"</script>';
    }
}
    
if(isset($_GET['status'])&&$_GET['status']=="active")
{
    $rsstatus=false;
    $rsstatus=$objCategory->doActive($id,$_GET['active']);
    if($rsstatus==true)
    {
        if($url_id_site==""||$url_id_site==0)
        echo '<script>document.location="'.$urls.'"</script>';
        else
           echo '<script>document.location="'.$urls.'&id_site='.$url_id_site.'"</script>';
    }
    else
    {
         if($url_id_site==""||$url_id_site==0)
        echo '<script>document.location="'.$urls.'&ac=err"</script>';
        else
          echo '<script>document.location="'.$urls.'&ac=err&id_site='.$url_id_site.'"</script>';
    }
}

//xoa
if(isset($_POST['chkid']))
{
  $numr=$_POST['chkid'];
  $flag=true;
  foreach($numr as $key =>$value)
  {
    $objCategory->doDelete($value);
  }
 echo '<script>document.location="'.$urls.'&ac=de"</script>';    
}

//edit
$cbsite=$objcbsite->create_combox_site("","ClickToURL('?3nss=categories.manage&type=add&iSite='+frm_catogory.site.value)");
$obj->id_s="";
$objEdit->id_site[0]=$iSite;
$dllparent=$objEdit->createMutilevelCombox("");
$DllType=createDllTypeCategory("");
if($iSite!="")
{
    $cbsite=$objcbsite->create_combox_site($iSite,"ClickToURL('?3nss=categories.manage&type=add&iSite='+frm_catogory.site.value)");
}
if($type=="edit"&&isset($_GET['id'])&&$_GET['id']!="") 
{   
    $obj->id_s=$_GET['id'];
    $objCategoryDetail->do_selectall_cateogory_id($_GET['id']);
    $cbsite=$objcbsite->create_combox_site($objCategoryDetail->id_site[0],"ClickToURL('?3nss=categories.manage&type=edit&id=".$_GET['id']."&iSite='+frm_catogory.site.value)");
    $obj->id_site[0]=$objCategoryDetail->id_site[0];
    $dllparent= $obj->createMutilevelCombox($objCategoryDetail->id_parent[0]);
     if(isset($_GET['iSite'])&&$_GET['iSite']!="") 
     { 
            $dllparent="";
           $_GET['iSite']=isset($_GET['iSite'])?strval(intval($_GET['iSite'])):"";
           // echo $site;
            $cbsite=$objcbsite->create_combox_site($_GET['iSite'],"ClickToURL('?3nss=categories.manage&id=".$_GET['id']."&type=edit&iSite='+frm_catogory.site.value)"); 
            $obcb=new cs_category();
            $obcb->id_site[0]=$_GET['iSite'];
            $obcb->id_s=$_GET['id'];
            $dllparent= $obcb->createMutilevelCombox($objCategoryDetail->id_parent[0]);
     }    
    $DllType=createDllTypeCategory($objCategoryDetail->id_type_category[0]);
}
$page->assign("savefolder",$savefolder);
$page->assign("imges_path",$imges_path);
$page->assign("js",$js);
$page->assign("css",$css);
$page->assign("url_id_site",$url_id_site);
$page->assign("cbsite_list", $cbsite_list);
$page->assign("cbsite",$cbsite);
$page->assign("type",$type);
$page->assign("objCategoryDetail",$objCategoryDetail);  
$page->assign("objCategory",$objCategory);
$page->assign("DllType", $DllType);
$page->assign("dllparent",$dllparent);
$page->assign('status_img',$status_img);
$page->assign('no', $no);
$page->assign('status_bg',$status_bg); 
$page->assign('linki',$linki);
//$page->assign('pages',$pages); 
?>