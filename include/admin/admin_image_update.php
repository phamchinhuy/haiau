<?php


include_once("include/class/cs_gallery.php");
include_once("include/class/cs_date.php");
include_once("include/class/client/cs_client_gallery.php");
$folder_upload_gallery = "uploads/gallery/";
$size_file_image = "2148576"; 	
$id_user = "1";

$p_url_cateid = "";
if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
        

$id = "";
if (isset($_GET["id"]) || isset($_POST["id"]))
    $id = isset($_GET["id"]) ? $_GET["id"] : $_POST["id"];

if(isset($_POST["btEdit"])&&$_POST["btEdit"]!="")
{
    if($p_url_cateid!="")
    {
        $id_Category = $p_url_cateid;
        
        $title = "";
        $title = isset($_POST["txtName"])?$_POST["txtName"]:"";
        
        $small_image = "";
        
        
        $large_image = "";
        
        
        $position = "0";
        
        $status = "";
        $status =isset($_POST["chkStatus"])?$_POST["chkStatus"]:"1";
        
        $iscomment = "";
        $iscomment =isset($_POST["chkComment"])?$_POST["chkComment"]:"1";
        
        $url = "";
        $url = isset($_POST["txtURL"])?$_POST["txtURL"]:"0";
        
        $date_created = date("Y-m-d");
        $id_user_created = $id_user;
        
        $date_updated = $date_created;
        $id_user_updated = $id_user;
        
        $date_deleted = "";
        $id_user_deleted = "";
        
        $image_note = "";
        $image_note = isset($_POST["description_image"])?$_POST["description_image"]:"";
        
        $image_x ="";
        $image_y = "";
        $alt = "";
        $keyword = isset($_POST["txtKeyword"])?$_POST["txtKeyword"]:"";
        $page_title = isset($_POST["txtTitleSEO"])?trim($_POST["txtTitleSEO"]):"";
        $metadescription = isset($_POST["txtDescriptionMeta"])?$_POST["txtDescriptionMeta"]:"";
        $metaKeyword =  isset($_POST["txtKeywordMeta"])?$_POST["txtKeywordMeta"]:"";
        $begin_date         = isset($_POST['beginDate'])?$_POST['beginDate']:"";
        $begin_end          = isset($_POST['beginEnd'])?$_POST['beginEnd']:"";
        
        $cs_gallery_image = new cs_gallery();
        $cs_gallery_image->select_gallery_by_id($p_url_id);
         $cs_gallery = new cs_gallery();
        //UPLOAD IMAGE
        $rsUpload = false;
        $rssmall=true;
        $rslarge=true;
      /*  if(isset($_FILES["fileSmall"]["tmp_name"])&&)
        {
           $rssmall=$cs_gallery->update_Imges("fileSmall",$size_file_image,$folder_upload_gallery,2);           
        }
        else
        {
           $cs_gallery->small_image[0] = $cs_gallery_image->small_image[0];
           
        }*/
        
        if($_FILES["fileLarge"]["tmp_name"])
        {
            $rslarge=$cs_gallery->update_Imges("fileLarge",$size_file_image,$folder_upload_gallery,1);
        }
        else
        {           
            $cs_gallery->large_image[0]= $cs_gallery_image->large_image[0];
        }
        if($rssmall&&$rslarge)
        $rsUpload=true;
        else
        $rsUpload=false;
        //END UPLOAD IMAGE
        if($status=="on")
            $status = "1";
        else
            $status="0";
            
        if($iscomment=="on")
            $iscomment="1";
        else
            $iscomment = "0";
        $cs_date    = new cs_date();    
       
        $cs_gallery->id[0] = $id;
        $cs_gallery->id_category[0] = $id_Category;
        $cs_gallery->title[0] = $title;
       // $cs_gallery->small_image[0] = $small_image;
        //cs_gallery->large_image[0] = $large_image;
        $cs_gallery->position[0] = $position;
        $cs_gallery->status[0] = $status;
        $cs_gallery->url[0] = $url;
        $cs_gallery->date_created[0] = $date_created;
        $cs_gallery->id_user_created[0] = $id_user_created;
        $cs_gallery->date_updated[0] = $date_updated;
        $cs_gallery->id_user_updated[0] = $id_user_updated;
        $cs_gallery->date_deleted[0] = $date_deleted;
        $cs_gallery->id_user_deleted[0] = $id_user_deleted;
        $cs_gallery->image_note[0] = $image_note;
        $cs_gallery->image_x[0] = $image_x;
        $cs_gallery->image_y[0] = $image_y;
        $cs_gallery->small_image[0]="";
        $cs_gallery->alt[0] = $alt; 
        $cs_gallery->keyword[0] = $keyword;
        $cs_gallery->page_title[0] = $page_title;
        $cs_gallery->meta_description[0] = $metadescription;
        $cs_gallery->meta_keyword[0] = $metaKeyword;        
        $cs_gallery->show_page[0] = "";
        $cs_gallery->is_comment[0] = $iscomment;
        $cs_gallery->count_view[0] = "";
        $cs_gallery->begin_date[0] = $cs_date->formatYYYYMMDD($begin_date);
        $cs_gallery->begin_end[0] =  $cs_date->formatYYYYMMDD($begin_end);
        
        
      if($rsUpload)
        {
            $rs = false;
            $rs = $cs_gallery->update_gallery();
            
            $file_gallery_xml = ''; 
            $file_gallery_xml =  uniqid("").".xml";
            $file_gallery_xml = "gallery_"."_".$file_gallery_xml;
            
            $cs_category = new cs_category();
            $cs_category->url[0] = $file_gallery_xml;
            $cs_category->id[0] = $id_Category;
            $cs_category->update_category_with_url();
            
            $cs_client_gallery = new cs_client_gallery();
            $cs_client_gallery->select_gallery_by_id_category_not_limit($id_Category,"1");
            
            
            $str_xml = '';
            $str_xml.='<?xml version="1.0" encoding="UTF-8" ?>
            <flash_parameters copyright="socusoftFSMTheme">
                <preferences>
                    <global>
                        <basic_property movieWidth="500" movieHeight="420" html_title="Title" loadStyle="Bar" startAutoPlay="true" backgroundColor="0xfeee9c" hideAdobeMenu="false" photoDynamicShow="true" enableURL="true" transitionArray=""/>
                        <title_property photoTitleColor="0xdb9c39" photoTitleSize="12" photoTitleFont="Verdana"/>
                        <music_property path="" stream="true" loop="true"/>
                        <photo_property topPadding="3" bottomPadding="28" leftPadding="2" rightPadding="108" photoBorder="true" photoBorderSize="1" photoBorderColor="0xfffbf0"/>
                        <button_property buttonColor="0xdb9c39"/>
                        <properties enable="true" backgroundColor="0xffffff" backgroundAlpha="30" cssText="a:link{text-decoration: underline;} a:hover{color:#ff0000; text-decoration: none;} a:active{color:#0000ff;text-decoration: none;} .blue {color:#0000ff; font-size:15px; font-style:italic; text-decoration: underline;} .body{color:#ff5500;font-size:20px;}" align="bottom"/>
                    </global>
                    <thumbnail>
                        <basic_property thumbnailWidth="90" thumbnailHeight="60" showBorder="true" borderColor="0xffffff" align="right" rowCount="1" thumbnailSpacing="5"/>
                        <dropShadow_property shadowColor="0x666666" shadowTransparency="20" shadowSize="0" shadowHoverSize="1"/>
                    </thumbnail>
                </preferences>
                <album>';
                if(count($cs_client_gallery->id>0))
                {
                    for($i=0;$i<count($cs_client_gallery->id);$i++)
                    {
                        $str_xml.='<slide jpegURL="'.$cs_client_gallery->small_image[$i].'" d_URL="'.$cs_client_gallery->large_image[$i].'" 
                                    transition="0" panzoom="1" URLTarget="0" phototime="3" url="#" title="'.$cs_client_gallery->title[0].'" width="450" height="389"/>';
                    }
                    
                }
                    
                $str_xml.='</album>
            </flash_parameters>';
            $default_dir = "uploads/gallery/";
            $default_dir .= $file_gallery_xml;
            $fp = fopen($default_dir,'w');
            $write = fwrite($fp,$str_xml);
            
            
            if($rs==true)
                echo navigateLink("admin.php?3nss=image&cate=".$id_Category."&action=list&complete=edit");
            
        }
        else
        {
          
              echo  getMessageJavascript("Please check your information , fail action","");  
              $rsUpload = false;
        }
        
        
    }
}

if($p_url_id!="")
{
    $cs_gallery = new cs_gallery();
    $cs_gallery->select_gallery_by_id($p_url_id);
    $cs_gallery->begin_date[0] = date('d/m/Y', strtotime($cs_gallery->begin_date[0]));
    $cs_gallery->begin_end[0]  = date('d/m/Y', strtotime($cs_gallery->begin_end[0]));
    $page->assign("cs_gallery",$cs_gallery);
    $page->assign("folder_upload_gallery",$folder_upload_gallery);
}


?>