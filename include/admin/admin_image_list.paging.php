<?php
require_once '../../include/db.php';
include_once("../../include/class/cs_gallery.php");
include_once("../../include/class/cs_account.php");
include_once("../../include/class/cs_category.php");
include_once("../../include/class/cs_permisions.php");
$folder_upload_gallery = "uploads/gallery/";
$idcate="";
if (isset($_GET["idcate"]) || isset($_POST["idcate"]))
        $idcate = isset($_GET["idcate"]) ? $_GET["idcate"] : $_POST["idcate"];
$user_id=isset($_GET['user_id'])?$_GET['user_id']:"";
$str = "";
$countItemPage = 10;

$_GET["page"]=isset($_GET["page"])?$_GET["page"]:"";
if ($_GET["page"]!="")
{
    $page=$_GET["page"]!=""?$_GET["page"]:"1";
    $page = $page-1;
    $start = $page*$countItemPage;
    $str.='
            <table cellpadding="0" cellspacing="0" width="100%" class="tbl">
                <tr>
                    <td colspan="8" class="tdHEADER">DANH S&#193;CH H&#204;NH &#7842;NH</td>
                </tr>
                <tr>
                    <th width="200px">Ti&#234;u &#273;&#7873;</th>
                      <th width="100">V&#7883; tr&#237;</th>
                    <th width="100">H&#236;nh</th>
                    <th width="100">T&#236;nh tr&#7841;ng</th>
                    <th width="120">Ng&#432;&#7901;i t&#7841;o</th>
                    <th width="120">Ng&#432;&#7901;i c&#7853;p nh&#7853;t</th>
                    <th width="120">Ng&#224;y c&#7853;p nh&#7853;t</th>
                    <th>Ch&#7913;c n&#259;ng</th>
                </tr>
        ';    
    $cs_gallery = new cs_gallery();
    $cs_gallery->select_gallery_by_id_category_limit($idcate,$start,$countItemPage);
    $count_gallery = count($cs_gallery->id);
    $objPermision=new cs_permision();
    $array=array();
    $objPermision->do_select_permision($user_id,$idcate);
    $array=$objPermision->permision[0];
    if($count_gallery>0)
    {
        for($i=0;$i<$count_gallery;$i++)
        {
            $id = "";
            $id = $cs_gallery->id[$i];
            
            $strActive = "";
            $strDelete = '';
            if($cs_gallery->status[$i]=="2")
            {
                $strDelete.='<td><a href="admin.php?3nss=image&cate='.$idcate.'&id='.$id.'&action=delete"" onclick="return check()" title="X&#243;a"><img src="images/admin/delete.gif" border=0 />&nbsp X&#243;a</td>';
                $strActive.='<a href="admin.php?3nss=image&cate='.$idcate.'&id='.$id.'&action=active"><img src="images/admin/hidden.gif" border=0 /></a>';
            }
            else
            {
                $strDelete = '';
                $strActive.='<a href="admin.php?3nss=image&cate='.$idcate.'&id='.$id.'&action=visible"><img src="images/admin/visible.gif" border=0 /></a>';
            }
             if($array[2]==0)
            $strDelete="";
            if($array[3]!=1)
             $strActive="";
            //user created
            $id_user_created ='';
            $id_user_created = $cs_gallery->id_user_created[$i];
            $username_created = '';
            $cs_account_created = new cs_account();
            $cs_account_created->select_user_by_id_user($id_user_created);
            if(count($cs_account_created->id)>0)
                $username_created = $cs_account_created->username[0];
            
            //user update
            $id_user_updated = '';
            $id_user_updated = $cs_gallery->id_user_updated[$i];
            $username_updated = '';
            $cs_account_updated = new cs_account();
            $cs_account_updated->select_user_by_id_user($id_user_updated);
            if(count($cs_account_updated->id)>0)
                $username_updated = $cs_account_updated->username[0];
                
            //date updated
            $date_updated = "";
            $date_updated =$cs_gallery->date_updated[$i];
           
           
            
            $str.='
                     <tr>
                        <td>'.$cs_gallery->title[$i].'</td>
                        <Td>'.$cs_gallery->create_combox_position($cs_gallery->position[$i],$idcate).'';
                       $str.='<input type="hidden" value="'.$cs_gallery->position[$i].'" name="cbhd'.$cs_gallery->position[$i].'">';
                     $str.= '</td>
                        <td><img src="'.$folder_upload_gallery.$cs_gallery->large_image[$i].'" width=80 height=80 /></td>
                        <td align=center>'.$strActive.'</td>
                        <td>'.$username_created.'</td>
                        <td>'.$username_updated.'</td>
                        <td>'.$date_updated.'</td>
                        <td>
                            <table cellpadding=5 cellpacing=5 border=0 class="tblFunction">
                                <td>';
                                   if($array[3]==1)
                                    $str.='<a href="admin.php?3nss=image&cate='.$idcate.'&id='.$id.'&action=edit" title="S&#7917;a"><img src="images/admin/edit_action.gif" border=0 align="left"  />&nbsp;S&#7917;a</a>';
                                
                                $str.='</td>
                                '.$strDelete.'
                            </table>
                        </td>
                     </tr>
                  ';
        }
    }
    
    
    $str.='  </table>';

}
echo $str;
?>