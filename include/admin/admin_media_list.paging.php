<?php
require_once '../../include/db.php';
include_once("../../include/class/cs_video.php");
include_once("../../include/class/cs_account.php");
include_once("../../include/class/cs_category.php");
include_once("../../include/class/cs_permisions.php");

$folder_upload_media = "uploads/media/";
$idcate="";
if (isset($_GET["idcate"]) || isset($_POST["idcate"]))
        $idcate = isset($_GET["idcate"]) ? $_GET["idcate"] : $_POST["idcate"];
$user_id=isset($_GET['user_id'])?$_GET['user_id']:"";
$str = "";
$countItemPage =5;

$_GET["page"]=isset($_GET["page"])?$_GET["page"]:"";
if ($_GET["page"]!="")
{
    $page=$_GET["page"]!=""?$_GET["page"]:"1";
    $page = $page-1;
    $start = $page*$countItemPage;
    $str.='
            <table cellpadding="0" cellspacing="0" width="100%" class="tbl">
                 <tr>
                    <td colspan="6" class="tdHEADER">DANH S&#193;CH MEDIA</td>
                </tr>
                <tr>
                   
                    <th width="25%">Tiêu &#273;&#7873; </th>
                    <th width="6%" align="center">Hình</th>
                    <th width="10%" align="center">Tình trạng</th>
                    <th width="8%" align="center">Bình luận</th>
                    <th width="10%" align="center">Ch&#7913;c n&#259;ng</th>
                    <th width="5%" align="center">Check</th>
                </tr>
        ';
   
    $cs_video = new cs_video();
    $objP=new cs_permision();
    $array=array();
    $objP->do_select_permision($user_id,$idcate);
    $array=$objP->permision[0];
    $cs_video->select_video_by_id_category_limit($idcate,$start,$countItemPage);
    $count_video = count($cs_video->id);
 
    if($count_video>0)
    {
        for($i=0;$i<$count_video;$i++)
        {
            $id = "";
            $id = $cs_video->id[$i];
            $strcheck="";
            $strActive = "";
            $strDelete = '';
            if($cs_video->status[$i]=="2")
            {
                $strDelete.='<td><a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&action=delete"" onclick="return check()" title="X&#243;a"><img src="images/admin/delete.gif" border=0 />&nbsp X&#243;a</td>';
                $strActive.='<a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&action=active"><img src="images/admin/hidden.gif" border=0 /></a>';
                $strcheck='<input type="checkbox" name="checkNews[]" id="checkNews" value="'.$cs_video->id[$i].'" />';
            }
            else
            {
                $strDelete = '';
                $strActive.='<a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&action=visible"><img src="images/admin/visible.gif" border=0 /></a>';
            }
             if($array[2]==0)
             {
                    $strDelete="";
                     $strcheck="";
                     
             }
             if($array[3]!=1)
             {
                 $strActive="";
             }
            if($cs_video->is_comment[$i]=="0")
            {
                $strIsComment='<a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&comment=active"><img src="images/admin/hidden.gif" border=0 /></a>';
            }
            else
            {
                $strIsComment='<a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&comment=visible"><img src="images/admin/visible.gif" border=0 /></a>';;
            }
            //user created
            $id_user_created ='';
            $id_user_created = $cs_video->id_user_created[$i];
            $username_created = '';
            $cs_account_created = new cs_account();
            $cs_account_created->select_user_by_id_user($id_user_created);
            if(count($cs_account_created->id)>0)
                $username_created = $cs_account_created->username[0];
            
            //user update
            $id_user_updated = '';
            $id_user_updated = $cs_video->id_user_updated[$i];
            $username_updated = '';
            $cs_account_updated = new cs_account();
            $cs_account_updated->select_user_by_id_user($id_user_updated);
            if(count($cs_account_updated->id)>0)
                $username_updated = $cs_account_updated->username[0];
                
            //date updated
            $date_updated = "";
            $date_updated =$cs_video->date_updated[$i];
           
           
            
            $str.='
                     <tr>
                        
                        <td>'.$cs_video->video[$i].'</td>
                        <td><img src="'.$folder_upload_media.$cs_video->small_image[$i].'" width=80 height=80 /></td>
                        <td align=center>'.$strActive.'</td>
                        <td align="center">'.$strIsComment.'</td>
                        <td>
                            <table cellpadding=5 cellpacing=5 border=0 class="tblFunction">
                                <td>';
                             if($array[3]==1)   
                            $str.='<a href="admin.php?3nss=media&cate='.$idcate.'&id='.$id.'&action=edit" title="S&#7917;a"><img src="images/admin/edit_action.gif" border=0 align="left"  />&nbsp;S&#7917;a</a>';
                             
                             $str.=   '</td>
                                '.$strDelete.'
                            </table>
                        </td>
                        <td>'.$strcheck.'</td>
                        
                     </tr>
                  ';
        }
    }
    
    
    $str.='<tr>
                <td colspan="6"  align="right">
                    <a href="#" class="linkCheckAll" onClick="checkAll(document.myform.checkNews)" >CheckAll</a> 
                    |
                    <a href="#" class="linkCheckAll" onClick="uncheckAll(document.myform.checkNews)">Uncheck</a> 
                    |
                    <input name="btnDelete" class="btDeleteAll"  value="Delete" type="submit" onclick="return check()"/> 
                </td>
            </tr>  
   
    </table>';

}
echo $str;
?>