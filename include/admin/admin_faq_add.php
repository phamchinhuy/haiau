<?php

//created by tran van tam 6-5-2010
include_once("include/class/cs_faq.php");
include_once("include/class/cs_date.php");

$cs_date=new cs_date();

if(isset($_POST["btAdd"])&&$_POST["btAdd"]!="")
{
    
        //get values from form
        $fullname               = isset($_POST['txtFullname'])?$_POST['txtFullname']:"";
        $address                = isset($_POST['txtAddress'])?$_POST['txtAddress']:"";
        $email                  = isset($_POST['txtEmail'])?$_POST['txtEmail']:"";
        $phone                  = isset($_POST['txtPhone'])?$_POST['txtPhone']:"";
        $question               = isset($_POST['txtQuestion'])?$_POST['txtQuestion']:"";
        $dateCreatedQuestion    = isset($_POST['txtDateCreatedQuestion'])?$_POST['txtDateCreatedQuestion']:"";
        $reply                  = isset($_POST['txtReply'])?$_POST['txtReply']:"";
        $status                 = isset($_POST['status'])?$_POST['status']:"";
        //check status variable
        if($status=="on")
        {
            $status=1;
        }
        else
        {
            $status=0;
        }
       
        $cs_date                        = new cs_date();    
        $cs_faq                         = new cs_faq();
        
        $cs_faq->fullname[0]            = $fullname;
        $cs_faq->address[0]             = $address;
        $cs_faq->email[0]               = $email;
        $cs_faq->phone[0]               = $phone;
        $cs_faq->question[0]            = $question;
        $cs_faq->dateCreatedQuestion[0] = $cs_date->formatYYYYMMDD($dateCreatedQuestion);
        $cs_faq->dateCreatedReply[0]    = date('Y-m-d');
        $cs_faq->status[0]              = $status;
        $cs_faq->reply[0]               = $reply;
        
        //Insert data from form into database
        $rs = false;
        $rs = $cs_faq->insertFAQ();
          
        if($rs==true)
        {
            
            echo navigateLink("admin.php?3nss=faq&action=list&complete=add");
        }
                
   
    
}

?>