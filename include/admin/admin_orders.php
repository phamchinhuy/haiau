<?php

require_once("include/class/cs_orders.php");
require_once("include/class/cs_order_detail.php");
require_once("include/class/cs_users.php");
require_once("include/class/cs_product.php");

$main_content_template = "admin/admin_orders.tpl";
$tblOrders = "admin/admin_orders_list.tpl";


if(isset($_POST["sbSearchCode"]))
{
    $txtCodeOrder = "";
    $txtCodeOrder = isset($_POST["txtOrderCode"])?$_POST["txtOrderCode"]:"";
    if($txtCodeOrder=="")
    {
        $link = "";
        $link = "admin.php?3nss=orders";
        echo getMessageJavascript("Vui lòng nhập mã đơn hàng",$link);
    }
    else
    {
        $link = "";
        $link = "admin.php?3nss=orders&id=".$txtCodeOrder;
        echo navigateLinkHeader($link);
    }
}

$arrayStatus = array(0,2,3,4);
$arrayStatusText = array("Ch&#432;a x&#7917; l&#253;"
                            , "&#272;ang x&#7917; l&#253;"
                            , "X&#7917; l&#253; ho&#224;n t&#7845;t"
                            , "&#272;&#417;n h&#224;ng tr&#7843; l&#7841;i");


$s = "";
$s = isset($_GET["s"])?$_GET["s"]:"";
$page->assign("s", $s);

$id = "";
$id = isset($_GET["id"])?$_GET["id"]:"";

if($s=="")
    $s = $arrayStatus[0];
$strDDLStatus = "";
for($i=0; $i<count($arrayStatus);$i++)
{
    if($s==$arrayStatus[$i])
        $strDDLStatus.=' <option value="admin.php?3nss=orders&s='.$arrayStatus[$i].'" selected="selected">'.$arrayStatusText[$i].'</option>';
    else
        $strDDLStatus.=' <option value="admin.php?3nss=orders&s='.$arrayStatus[$i].'">'.$arrayStatusText[$i].'</option>';
    
}
$page->assign("strDDLStatus", $strDDLStatus);


if($id!="")
{
    $tblOrders = "admin/admin_orders_edit.tpl";
    include("admin_orders_edit.php");
}

if($id=="")
{
    $tblOrders = "admin/admin_orders_list.tpl";

    $scriptOrders = "";
    $cs_orders = new cs_orders();
    $cs_orders->select_orders_by_status($s, "", "", "id", "desc");
    $sumNews = count($cs_orders->id);
    if($sumNews>0)
    {
        $countItemPage = 5;
        $countPage = 1;
        if($sumNews>$countItemPage)
            $countPage=$sumNews/$countItemPage;
        $modePage = $sumNews%$countItemPage;
        for($i=0;$i<$countPage;$i++)
        {
            $j = $i + 1;
            if($i==0)
            {
                $scriptOrders.="'include/admin/admin_orders_list.paging.php?page=".$j."&s=".$s."&count=".$countItemPage."'";
            }
                
            else
                $scriptOrders.=", 'include/admin/admin_orders_list.paging.php?page=".$j."&s=".$s."&count=".$countItemPage."'";
        }
    }
    $page->assign("scriptOrders",$scriptOrders);
}

$page->assign("tblOrders",$tblOrders);
$page->assign("id_order_detail",$id);
?>