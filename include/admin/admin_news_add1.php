<?php


include_once("include/class/cs_news.php");
include_once("include/class/cs_topic.php");
include_once("include/class/cs_date.php");


$folder_upload_news = "uploads/news/";
$size_file_image = "2148576"; 	
$id_user = "1";


if(isset($_POST["btAdd"])&&$_POST["btAdd"]!="")
{
    $p_url_cateid = "";
    if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
    
    if($p_url_cateid!="")
    {
        $id_Category = $p_url_cateid;
        
        $iscomment = "";
        $isComment =isset($_POST["isComment"])?$_POST["isComment"]:"0";
        if($isComment=="on")
        {
            $isComment=1;
        }
        $shortDescription=isset($_POST["txtShortDescription"])?$_POST["txtShortDescription"]:"";
        $longDescription=isset($_POST["description_news"])?$_POST["description_news"]:" ";
        //$small_image=$_FILES['fileSmall']['name'];
        //$large_image=$_FILES['fileLarge']['name'];
        $active=isset($_POST["active"])? $_POST["active"]:"2";
        $target=isset($_POST["target"])? $_POST["target"]:"0";
        $url = "";
        $url = isset($_POST["txtUrl"])?$_POST["txtUrl"]:"0";
        $urlVideo = isset($_POST["txtUrlVideo"])?$_POST["txtUrlVideo"]:"0";
        $topic=isset($_POST['topic'])?$_POST['topic']:"";
        
        //get date
        $begin_date         = isset($_POST['beginDate'])?$_POST['beginDate']:"";
        $begin_end          = isset($_POST['beginEnd'])?$_POST['beginEnd']:"";
        
        $date_created = date("Y-m-d");
        $id_user_created = $id_user;
        
        $date_updated = $date_created;
        $id_user_updated = $id_user;
        
        $date_deleted = "";
        $id_user_deleted = "";
        $page_title         = isset($_POST['txtTitleSEO'])?$_POST['txtTitleSEO']:"";
        $meta_keyword       = isset($_POST['txtDescriptionMeta'])?$_POST['txtDescriptionMeta']:"";
        $meta_description   = isset($_POST['txtKeywordMeta'])?$_POST['txtKeywordMeta']:"";
        
        $cs_date = new cs_date();
        $cs_news = new cs_news();
        
        $news = "";
        $news = isset($_POST["txtNews"])?$_POST["txtNews"]:"";
        
        $small_image = "";
        $large_image = "";
        
         //check type file
        $rsUpload = true;
        $strEXT1 = "";
        $strEXT2 = "";
        if($_FILES["fileSmall"]["tmp_name"]!="" && $_FILES["fileLarge"]["tmp_name"]!="")
        {
            
            $strEXT1 = getFileExtension($_FILES["fileSmall"]["name"]);
                
            $strEXT2 = getFileExtension($_FILES["fileLarge"]["name"]);
            
            $small_image="small_news_".uniqid("").$strEXT1;
            
            $large_image="large_news_".uniqid("").$strEXT2;
            
            
            $typeFileSmall=false;
            $typeFileSmall=$cs_news->upload_image("fileSmall",$size_file_image, $folder_upload_news, $small_image);
            
             //upload            
            $typeFileLarge=false;
            $typeFileLarge=$cs_news->upload_image("fileLarge",$size_file_image, $folder_upload_news, $large_image);
            
            if($typeFileLarge==true && $typeFileSmall==true)
                $rsUpload=true;
            else
                $rsUpload=false;
        }
        
        
        $cs_news->id_category[0]         = $p_url_cateid;
        $cs_news->news[0]                = $news;
        $cs_news->small_image[0]         = $small_image;
        $cs_news->large_image[0]         = $large_image;
        $cs_news->short_description[0]   = $shortDescription;
        $cs_news->long_description[0]    = $longDescription;
        $cs_news->status[0]              = $active;
        $cs_news->url[0]                 = $url;
        $cs_news->target[0]              = $target;
        $cs_news->date_created[0]        = $date_created;
        $cs_news->id_user_created[0]     = $id_user_created;
        $cs_news->date_updated[0]        = $date_updated;
        $cs_news->id_user_updated[0]     = $id_user_updated;
        $cs_news->date_deleted[0]        = $date_deleted;
        $cs_news->id_user_deleted[0]     = $id_user_deleted;
        $cs_news->file_extention[0]      =$strEXT1;  
        $cs_news->show_page[0]           = "";
        $cs_news->url_video[0]           = $urlVideo;
        $cs_news->id_topic[0]            = $topic;   
        
        $cs_news->keyword[0]             = "";
        $cs_news->page_title[0]          = $page_title;
        $cs_news->meta_description[0]    = $meta_description;
        $cs_news->meta_keyword[0]        = $meta_keyword;
        $cs_news->is_comment[0]          = $isComment;
        $cs_news->count_view[0]          = "";
        $cs_news->begin_date[0]          = $cs_date->formatYYYYMMDD($begin_date);
        $cs_news->begin_end[0]           = $cs_date->formatYYYYMMDD($begin_end);
        $cs_news->small_image[0]        = $small_image;
        $cs_news->large_image[0]        = $large_image;
        
        $bCheck = true;
        $strErrorPageTitleSEO = "";
        $cs_news_s = new cs_news();
        $cs_news_s->select_news_by_page_title($page_title);
        if(count($cs_news_s->id)>0)
        {
            $bCheck = false;
            $strErrorPageTitleSEO = "Ti&#234;u &#273;&#7873; cho SEO n&#224;y &#273;&#227; t&#7891;n t&#7841;i";
        }
        
        if($bCheck==false)
        {
            $page->assign("cs_news",$cs_news);
            $page->assign("strErrorPageTitleSEO", $strErrorPageTitleSEO);
        }
        else
        {
            if($rsUpload)
            {
                $rs = false;
                $rs = $cs_news->insert_news();
                if($rs==true)
                    echo navigateLink("admin.php?3nss=news&cate=".$id_Category."&action=list&complete=add");
            }
            else
            {
                
                echo  getMessageJavascript("Please check your information , fail action",""); 
                $rsUpload = false;
            }    
        }
      
     }
    
}

?>