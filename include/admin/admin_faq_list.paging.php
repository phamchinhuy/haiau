<?php
require_once '../../include/db.php';
require_once '../../include/lib.db.common.inc.php';
require_once '../../include/lib.util.common.inc.php';

include_once("../../include/class/cs_faq.php");
include_once("../../include/class/cs_account.php");
include_once("../../include/class/cs_date.php");
$cs_date=new cs_date();


$str = "";
$countItemPage = 5;
$_GET["page"]=isset($_GET["page"])?$_GET["page"]:"";
if ($_GET["page"]!="")
{
    $page=$_GET["page"]!=""?$_GET["page"]:"1";
    $page = $page-1;
    $start = $page*$countItemPage;
    $str.='
            <table cellpadding="0" cellspacing="0" width="100%" class="tbl">
                 <tr>
                    <td colspan="6" class="tdHEADER">DANH S&#193;CH S&#7842;N H&#7886;I &#272;&#193;P</td>
                </tr>
                
                <tr>
                    <th width="12%">Họ tên</th>
                    <th width="15%" align="center">&#272;&#7883;a ch&#7881;</th>
                    <th width="10%" align="center">Email</th>
                    <th width="8%" align="center">Ngày dăng ký</th>
                    <th width="5%" align="center">Tình trạng</th>
                    <th width="5%" align="center">Ch&#7913;c n&#259;ng</th>
                    
                </tr>
        ';
    
    $cs_faq = new cs_faq();
   
    $cs_faq->selectFAQLimit($start,$countItemPage);
    $count_faq = count($cs_faq->id);
    if($count_faq>0)
    {
        for($i=0;$i<$count_faq;$i++)
        {
            $id = "";
            $id = $cs_faq->id[$i];
            
            $strActive = "";
            $strDelete = '';
            if($cs_faq->status[$i]=="0")
            {
                
                $strActive.='<a href="admin.php?3nss=faq&id='.$id.'&action=active"><img src="images/admin/hidden.gif" border=0 /></a>';
            }
            else
            {
                
                $strActive.='<a href="admin.php?3nss=faq&id='.$id.'&action=visible"><img src="images/admin/visible.gif" border=0 /></a>';
            }
            
            
           
            
            $str.='
                     <tr>
                        <td>'.$cs_faq->fullname[$i].'</td>
                        <td align=center>'.$cs_faq->address[$i].'</td>
                        <td>'.$cs_faq->email[$i].'</td>
                        <td>'.date('d/m/Y',strtotime($cs_faq->dateCreatedQuestion[$i])).'</td>
                        <td>'.$strActive.'</td>
                        <td>
                            <table cellpadding=5 cellpacing=5 border=0 class="tblFunction">
                                <td>
                                    <a href="admin.php?3nss=faq&id='.$id.'&action=edit" title="S&#7917;a"><img src="images/admin/edit_action.gif" border=0 align="left"  />&nbsp;S&#7917;a</a>
                                </td>
                               
                            </table>
                        </td>
                     </tr>
                  ';
        }
    }
    
    
    $str.='  </table>';

}
echo $str;
?>