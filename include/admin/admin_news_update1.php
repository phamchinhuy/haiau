<?php


include_once("include/class/cs_news.php");
include_once("include/class/cs_date.php");
include_once("include/class/cs_date.php");

$folder_upload_news = "uploads/news/";
$size_file_image = "2148576"; 	
$id_user = "1";

$p_url_cateid = "";
if (isset($_GET["cate"]) || isset($_POST["cate"]))
        $p_url_cateid = isset($_GET["cate"]) ? $_GET["cate"] : $_POST["cate"];
        

$id = "";
if (isset($_GET["id"]) || isset($_POST["id"]))
    $id = isset($_GET["id"]) ? $_GET["id"] : $_POST["id"];


if(isset($_POST["btEdit"])&&$_POST["btEdit"]!="")
{
    if($p_url_cateid!="")
    {
        $id_Category = $p_url_cateid;
        
        $news = "";
        $news = isset($_POST["txtNews"])?$_POST["txtNews"]:"";
        
        $small_image = "";
        $large_image = "";
    
        $iscomment = "";
        $isComment =isset($_POST["isComment"])?$_POST["isComment"]:"0";
        if($isComment=="on")
        {
            $isComment=1;
        }
        $shortDescription=isset($_POST["txtShortDescription"])?$_POST["txtShortDescription"]:"";
        $longDescription=isset($_POST["description_news"])?$_POST["description_news"]:" ";
        $small_image=isset($_FILES['fileSmall']['name'])?$_FILES['fileSmall']['name']:"";
        $large_image=isset($_FILES['fileLarge']['name'])?$_FILES['fileLarge']['name']:"";
        $active=isset($_POST["active"])? $_POST["active"]:"2";
        $target=isset($_POST["target"])? $_POST["target"]:"0";
        $url = "";
        $url = isset($_POST["txtUrl"])?$_POST["txtUrl"]:"0";
        $urlVideo = isset($_POST["txtUrlVideo"])?$_POST["txtUrlVideo"]:"";
        $topic=isset($_POST['topic'])?$_POST['topic']:"";
        $page_title         = isset($_POST['txtTitleSEO'])?trim($_POST['txtTitleSEO']):"";
        $meta_keyword       = isset($_POST['txtDescriptionMeta'])?$_POST['txtDescriptionMeta']:"";
        $meta_description   = isset($_POST['txtKeywordMeta'])?$_POST['txtKeywordMeta']:"";
        //get date 
        $begin_date=isset($_POST['beginDate'])?$_POST['beginDate']:"";
        $begin_end=isset($_POST['beginEnd'])?$_POST['beginEnd']:"";
    
        
        
        $date_created = date("Y-m-d");
        $id_user_created = $id_user;
        
        $date_updated = $date_created;
        $id_user_updated = $id_user;
        
        $date_deleted = "";
        $id_user_deleted = "";
        
        $cs_news_image = new cs_news();
        $cs_news_image->select_news_by_id($p_url_id);
        $status= $cs_news_image->status[0];
        
        //UPLOAD IMAGE
        //check type file before update
        
        
        //END UPLOAD IMAGE
        if($status=="on")
            $status = "1";
        else
            $status="0";
            
        if($iscomment=="on")
            $iscomment="1";
        else
            $iscomment = "0";
       //upload image
        $typeFileSmall=false;
         //upload   
        $rsUpload = true;
        $cs_news = new cs_news();
        
        if($_FILES["fileSmall"]["tmp_name"]!="" && isset($_FILES["fileLarge"]["tmp_name"]))
        {
            if(isset($_FILES["fileSmall"]["tmp_name"])&&$_FILES["fileSmall"]["tmp_name"]!="")
            {        
               
                $strEXT1 = getFileExtension($_FILES["fileSmall"]["name"]);
     
                $small_image="small_news_".uniqid("").$strEXT1;
                
                $typeFileSmall=$cs_news->upload_image("fileSmall",$size_file_image,$folder_upload_news, $small_image);
                         
            }
            else
            {
                $typeFileSmall=true;
                
            }
            
            //upload large image
            $typeFileLarge=false;
            if(isset($_FILES["fileLarge"]["tmp_name"])&&$_FILES["fileLarge"]["tmp_name"]!="")
            {
            
                $strEXT2 = getFileExtension($_FILES["fileLarge"]["name"]);
                $large_image="large_news_".uniqid("").$strEXT2;
                $typeFileLarge=$cs_news->update_Imges("fileLarge", $size_file_image, $folder_upload_news, $large_image);           
            }
            else
            {
                  $typeFileLarge=true;   
            }
            
            if($typeFileLarge==true && $typeFileSmall==true)
                $rsUpload=true;
        }
            
        //END UPLOAD IMAGE                
        //get information from form into cs_news class   
        $cs_date = new cs_date();
        $cs_news->id[0]                  = $id;
        $cs_news->id_category[0]         = $id_Category;
        $cs_news->news[0]                = $news;
       
        $cs_news->short_description[0]   = $shortDescription;
        $cs_news->long_description[0]    = $longDescription;
        $cs_news->status[0]              = $active;
        $cs_news->url[0]                 = $url;
        $cs_news->target[0]              = $target;
        $cs_news->date_created[0]        = $date_created;
        $cs_news->id_user_created[0]     = $id_user_created;
        $cs_news->date_updated[0]        = $date_updated;
        $cs_news->id_user_updated[0]     = $id_user_updated;
        $cs_news->date_deleted[0]        = $date_deleted;
        $cs_news->id_user_deleted[0]     = $id_user_deleted;
        $cs_news->id_topic[0]            = $topic;
        $cs_news->show_page[0]           = "0";
        $cs_news->url_video[0]           = $urlVideo;
        $cs_news->keyword[0]             = "";
        $cs_news->page_title[0]          = $page_title;
        $cs_news->meta_description[0]    = $meta_description;
        $cs_news->meta_keyword[0]        = $meta_keyword;
        $cs_news->is_comment[0]          = $isComment;
        $cs_news->count_view[0]          = "";
        $cs_news->begin_date[0]          = $cs_date->formatYYYYMMDD($begin_date);
        $cs_news->begin_end[0]           = $cs_date->formatYYYYMMDD($begin_end);
        $cs_news->small_image[0]        = $small_image;
        $cs_news->large_image[0]        = $large_image;
      
       //check page_title
       
     
      if($rsUpload)
       {
            $rs = false;
            $rs = $cs_news->update_news();
            if($rs==true)
                echo navigateLink("admin.php?3nss=news&cate=".$id_Category."&action=list&complete=edit");
         
       }
            
       
       else 
       {    
              
              echo  getMessageJavascript("Please check your information , fail action","");  
              $rsUpload = false;
       }
        
        
    }
}


if($p_url_id!="")
{
    
    $cs_date = new cs_date();
    $cs_news = new cs_news();
    $cs_news->select_news_by_id($p_url_id);
    $cs_news->begin_date[0]=date('d/m/Y',strtotime($cs_news->begin_date[0]));
    $cs_news->begin_end[0] =date('d/m/Y',strtotime($cs_news->begin_end[0]));
    $page->assign("cs_news",$cs_news);
    $page->assign("folder_upload_news",$folder_upload_news);
}


?>